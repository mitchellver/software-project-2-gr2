package com.softwave.create.ui;

import java.awt.Dimension;

import com.softwave.core.ui.LoginPanel;
import com.softwave.core.ui.Window;
import com.softwave.core.ui.WindowManager;

/**
 * This frame class inherits from Window and is used to display the specific
 * start frame at the beginning of starting the program in creation mode.
 * 
 * @author nicolas
 * @version 1.0
 */
public class CreateFrame extends Window {
	/**
	 * The window manager that will register and change the view to the specfied
	 * panel.
	 */
	private WindowManager manager;

	/**
	 * Main constructor for the Frame.
	 * 
	 * @param title
	 *            the title that will be display in the titlebar of the window
	 * @param dim
	 *            the width and height of the frame
	 */
	public CreateFrame(String title, Dimension dim) {
		super(title, dim);
		init();
	}

	/**
	 * This method will initialize the Frame and populate it.
	 */
	@Override
	protected void init() {
		this.setMinimumSize(new Dimension(750, 500));

		manager = WindowManager.getInstance();
		manager.attachWindow(this);

		LoginPanel login = new LoginPanel(getMinimumSize());

		if (!manager.registerView(login)) {
			System.out.println("panel is null!");
		}

		this.setVisible(true);
	}

	/**
	 * This is the main method of the create part of the program, this is the
	 * beginning of the package that needs to be ran if the user wants to create
	 * a quiz.
	 * 
	 * @param args
	 *            the cli arguments when starting the program
	 */
	public static void main(String[] args) {
		CreateFrame frame = new CreateFrame("Creating", new Dimension(750, 500));
	}

	
}