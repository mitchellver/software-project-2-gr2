package com.softwave.quiz;

import java.awt.Dimension;
import java.awt.GridLayout;

import javax.swing.JFrame;

import com.softwave.core.container.GlobalState;
import com.softwave.core.container.quiz.Quiz;
import com.softwave.core.logic.QuizManagementHandler;
import com.softwave.core.ui.IntroPanel;
import com.softwave.core.ui.LoginPanel;
import com.softwave.core.ui.Window;
import com.softwave.core.ui.WindowManager;
import com.softwave.core.util.Language;
import com.softwave.core.util.Translator;

public class MainFrame extends Window {
	private WindowManager manager;
	private LoginPanel loginPanel;

	/**
	 * this is the original constructor nicolas wrote
	 * 
	 * @param title
	 *            the title of the frame
	 * @param dim
	 */
	public MainFrame(String title, Dimension dim) {
		super(title, dim);
		init();
	}

	@Override
	protected void init() {

		Translator.setLanguage(Language.ENGLISH);
		manager = WindowManager.getInstance();
		manager.attachWindow(this);

		IntroPanel intropanel = new IntroPanel();

		this.setMinimumSize(new Dimension(800, 600));
		this.setMaximumSize(new Dimension(800, 600));
		this.setResizable(false);
		this.setBounds(100, 100, 450, 300);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.getContentPane().setLayout(new GridLayout(0, 1, 0, 0));

		if (!manager.registerView(intropanel)) {
			System.out.println("panel is null!");
		}

		this.setVisible(true);
	}

	public static void main(String[] args) {
		// this would come out of designConf object
		Dimension dim = new Dimension(800, 500);

		MainFrame login = new MainFrame("Main", dim);

		login.setVisible(true);
	}

}
