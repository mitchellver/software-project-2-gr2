package com.softwave.core.container.user;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

public class PhonenumberTest {
	PhonenumberValidator phonenumber;
	
	@Before
	public void init(){
		phonenumber= new PhonenumberValidator();
	}
	@Test
	public void testTooManyPhonenumbers(){
	       assertFalse(phonenumber.validatePhonenumber("1231231231231231234"));
	}
	@Test
	public void testPhonenumberHasBraces(){
		assertTrue(phonenumber.validatePhonenumber("(004)-456-7880"));
	}
	@Test
	public void testPhonenumberHasHyphen(){
		assertTrue(phonenumber.validatePhonenumber("012-785-1202"));
	}
	@Test
	public void testPhonenumberHasDot()
	{
		assertTrue(phonenumber.validatePhonenumber("003.403.4545"));
	}
	@Test
	public void testPhonenumberHasSpace()
	{
		assertTrue(phonenumber.validatePhonenumber("070 909 7891"));
	}
	@Test
	public void testPhonenumberExtensionLength()
	{
		assertFalse(phonenumber.validatePhonenumber("001-778-5324 ext1288"));
	}
}