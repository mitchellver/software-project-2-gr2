package com.softwave.core.container.user;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

public class TelephonenumberTest {
	TelephonenumberValidator telephonenumber;
	
	@Before
	public void init()
	{
		telephonenumber = new TelephonenumberValidator();
	}
	@Test
	public void testTooManyTelephonenumbers()
	{
		assertFalse(telephonenumber.validateTelephonenumber("001456789876543217"));	}
	@Test
	public void testTelephonenumberHasBraces()
	{
		assertTrue(telephonenumber.validateTelephonenumber("(001)-654-7990"));
	}
	@Test
	public void testTelephonenumberHasHypen()
	{
		assertTrue(telephonenumber.validateTelephonenumber("002-773-4040"));
	}
	@Test
	public void testTelephonenumberHasDot()
	{
		assertTrue(telephonenumber.validateTelephonenumber("011.450.8201"));
	}
	@Test
	public void testTelephonenumberHasSpace()
	{
		assertTrue(telephonenumber.validateTelephonenumber("112 202 1478"));
	}
	@Test
	public void testTelephonenumberExtensionLength()
	{
		assertFalse(telephonenumber.validateTelephonenumber("011-454-5324 ext1288"));
	}
}