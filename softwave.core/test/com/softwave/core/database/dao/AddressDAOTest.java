package com.softwave.core.database.dao;

import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.softwave.core.container.user.Address;

public class AddressDAOTest {
	private static Address a;

	@BeforeClass
	public static void init(){
		a = new Address("mainStreet", "3", 1000, "Brussels", "Belgium");
	}
	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}
	
	@AfterClass
	public static void cleanup() {
		
	}

	@Test
	public void testsequence () {
		testPush();
		testUpdate();
		testPull();
		testDelete();
		// do this test as last, then we know we can already delete and push
		testCount();
	}
	
	public static void testPush() {
		long originalAddressID = a.getAddressID();
		try{
		AddressDAO.push(a);
		System.out.println("*** address pushed, new id: " + a.getAddressID());
		assertTrue("the addressID was not updated", originalAddressID != a.getAddressID());
		assertTrue("the addressID was not found", AddressDAO.existsByID(a.getAddressID()) == true);
		
		} catch (Exception e){
			e.printStackTrace();
		}
	
	}
	
	public static void testUpdate(){
		a.setStreet("newStreet");
		try{
			assertTrue("the address was not updated, update returned false", AddressDAO.update(a) == true);
			System.out.println("*** address updated, addressID: " + a.getAddressID());
		} catch (Exception e){	
			e.printStackTrace();
		}
	}

	public static void testPull() {
		Address a2;
		try{
			a2 = AddressDAO.pullByID(a.getAddressID());
			System.out.println("*** address pulled, pulled id: " + a2.getAddressID());
			
			assertTrue("addresses not the same", a.equals(a2));
		} catch (Exception e){
			e.printStackTrace();
		}
	}
	
	public static void testDelete(){
		long originalID = a.getAddressID();
		try{
			AddressDAO.deleteByObject(a);
			System.out.println("*** address deleted");
			assertTrue("the addresses is still found in database", AddressDAO.existsByID(originalID) == false);
		} catch (Exception e){
			e.printStackTrace();
		}
	}
	
	public static void testCount(){
		int countBefore = AddressDAO.countOf();
		Address b = new Address("mainStreet", "3", 1000, "Brussels", "Belgium");
		assertTrue("the address was not saved", AddressDAO.push(b));
		assertTrue("the count did not go up after creation of new row", countBefore+1 == AddressDAO.countOf());
		assertTrue("the address was not deleted", AddressDAO.deleteByObject(b));
		
		
	}
}
