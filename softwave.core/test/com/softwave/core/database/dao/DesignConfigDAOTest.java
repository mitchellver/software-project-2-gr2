package com.softwave.core.database.dao;

import static org.junit.Assert.assertTrue;

import java.awt.Color;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.softwave.core.container.design.DesignConfig;

public class DesignConfigDAOTest {
	private static DesignConfig dc;

	@BeforeClass
	public static void init() {
		dc = new DesignConfig();
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@AfterClass
	public static void cleanup() {

	}

	@Test
	public void testsequence() {
		testPush();
		testUpdate();
		testPull();
		testDelete();
		// do this test as last, then we know we can already delete and push
		testCount();
	}

	public static void testPush() {
		long originalDesignConfigID = dc.getDesignConfigID();
		try {
			DesignConfigDAO.push(dc);
			System.out.println("*** object pushed, new id: " + dc.getDesignConfigID());
			assertTrue("the designConfigID was not updated", originalDesignConfigID != dc.getDesignConfigID());
			assertTrue("the DesignConfigID was not found", DesignConfigDAO.existsByID(dc.getDesignConfigID()) == true);

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public static void testUpdate() {
		dc.setBackgroundColor(Color.BLACK);
		try {
			assertTrue("the dc was not updated, update returned false", DesignConfigDAO.update(dc) == true);
			System.out.println("*** dc updated, DesignConfigID: " + dc.getDesignConfigID());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void testPull() {
		DesignConfig dc2;
		try {
			dc2 = DesignConfigDAO.pullByID(dc.getDesignConfigID());
			System.out.println("*** designconfig pulled, pulled id: " + dc2.getDesignConfigID());

			assertTrue("designconfigs not the same", dc.equals(dc2));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void testDelete() {
		long originalID = dc.getDesignConfigID();
		try {
			DesignConfigDAO.deleteByObject(dc);
			System.out.println("*** designconfig deleted");
			assertTrue("the designconfigs is still found in database", DesignConfigDAO.existsByID(originalID) == false);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void testCount() {
		int countBefore = DesignConfigDAO.countOf();
		DesignConfig b = new DesignConfig();
		assertTrue("the designconfig was not saved", DesignConfigDAO.push(b));
		assertTrue("the count did not go up after creation of new row", countBefore + 1 == DesignConfigDAO.countOf());
		assertTrue("the designconfig was not deleted", DesignConfigDAO.deleteByObject(b));
	}
}
