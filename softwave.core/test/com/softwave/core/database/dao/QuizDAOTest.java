package com.softwave.core.database.dao;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.awt.Color;
import java.io.File;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.softwave.core.container.design.DesignConfig;
import com.softwave.core.container.quiz.QType;
import com.softwave.core.container.quiz.Question;
import com.softwave.core.container.quiz.Quiz;
import com.softwave.core.container.user.Address;
import com.softwave.core.container.user.Mail;
import com.softwave.core.container.user.Number;
import com.softwave.core.container.user.account.Client;
import com.softwave.core.container.user.account.UserInfo;
import com.softwave.core.security.HashString;
import com.softwave.core.security.Hashing;
import com.softwave.core.util.Language;

public class QuizDAOTest {
	private static Client client;
	private static Quiz quiz;

	@BeforeClass
	public static void init() {
		initClient();
		initQuiz();

	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@AfterClass
	public static void cleanup() {
		UserDAO.deleteByID(client.getUserID());
	}

	public static void initClient() {
		UserInfo userInfo = new UserInfo("eddi", "deLoste");
		userInfo.setAddress(new Address("mainStreet", "3", 1000, "Brussels", "Belgium"));
		userInfo.setEmail(new Mail("eddi@ehb.be"));
		String phone = "1234567890";
		userInfo.setPhonenumber(new Number(phone));
		client = new Client("eddi", new HashString(Hashing.hashPassword("password")));
		client.setUserInfo(userInfo);
		client.setDesignConfig(new DesignConfig());

		// save the client to the database, this sets the userID
		UserDAO.push(client);
	}

	public static void initQuiz() {
		quiz = new Quiz("quiz Test");
		quiz.setUserID(client.getUserID());
		quiz.setDescription("this is a quiz to test");
		quiz.setDesignConfig(new DesignConfig());

		// create 3 test questions
		for (int i = 1; i < 4; i++) {
			System.out.println("*** create question " + i);
			Question question = new Question();
			question.setName("question" + i);
			question.setDesignConfig(new DesignConfig());
			// question.setQuestionNumber(i);
			question.setTickAnswers(i);
			question.setCorrectAnswer("antwoord " + i);
			if (i == 1) {
				question.setNeedsAnswer(true);
				question.setType(QType.YES_NO);
				question.setQuestionLang(Language.createFromString("en_INT"));
			}
			if (i == 2) {
				question.setType(QType.OPEN);
				question.setNeedsAnswer(true);
				question.setQuestionLang(Language.createFromString("fr_BE"));
			}
			if (i == 3) {
				question.setType(QType.MULTIPLE_CHOICE);
				question.setNeedsAnswer(false);
				question.setQuestionLang(Language.createFromString("nl_BE"));
			}
			System.out.println("*** adding question " + question.getQuestionNumber());
			quiz.addQuestion(question);
		}

	}

	@Test
	public void testsequence() {
		testPush();
		testUpdate();
		testPull();
		testDelete();
		// do this test as last, then we know we can already delete and push
		testCount();
	}

	public static void testPush() {
		long originalQuizID = quiz.getQuizID();
		try {
			QuizDAO.push(quiz);
			System.out.println("*** object pushed, new id: " + quiz.getQuizID());
			assertTrue("the quizID was not updated", originalQuizID != quiz.getQuizID());
			assertTrue("the QuizID was not found", QuizDAO.existsByID(quiz.getQuizID()) == true);

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public static void testUpdate() {
		// save the original values
		String originalQuizName = quiz.getName();
		DesignConfig originalDc = quiz.getDesignConfig();
	
		// change the values
		quiz.setName("updatedQuiz");
		DesignConfig dc = new DesignConfig();
		dc.setBackgroundColor(Color.YELLOW);
		dc.setLogoFile("pic1.png");
		quiz.setDesignConfig(dc);
		
		quiz.getQuestions().stream().forEach(e -> {
			e.setName("quesUpdateQuiz");
		});

		// update and test the values
		try {
			assertTrue("the quiz was not updated, update returned false", QuizDAO.update(quiz) == true);
			System.out.println("*** quiz updated, QuizID: " + quiz.getQuizID());
			assertTrue("the quiz name not updated", quiz.getName() != originalQuizName);
			assertFalse("the quiz designconfig not updated", quiz.getDesignConfig().getLogoFile().equals(originalDc.getLogoFile()));
			quiz.getQuestions().stream().forEach(e -> {
				assertTrue("the question is not updated, questionID: " + e.getQuestionID(), e.getName() == "quesUpdateQuiz");
			});
			} catch (Exception e) {
			e.printStackTrace();
		}
		DesignConfigDAO.deleteByID(originalDc.getDesignConfigID());
	}

	public static void testPull() {
		Quiz quiz2;
		try {
			quiz2 = QuizDAO.pullByID(quiz.getQuizID());
			System.out.println("*** quiz pulled, pulled id: " + quiz2.getQuizID());

			assertTrue("the quiz name not the same", quiz.getName().equals(quiz2.getName()));
			// assertFalse("the quiz designconfig not the same",
			//		 quiz.getDesignConfig().getDesignConfigID() == quiz2.getDesignConfig().getDesignConfigID());
			// assertTrue("quizes not the same", quiz.equals(quiz2));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void testDelete() {
		long originalID = quiz.getQuizID();
		try {
			QuizDAO.deleteByID(quiz.getQuizID());
			System.out.println("*** quiz deleted");
			assertTrue("the quizs is still found in database", QuizDAO.existsByID(originalID) == false);
			// TODO check each question, and the designconfig, they should not exist anymore
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void testCount() {
		int countBefore = QuizDAO.countOf();
		Quiz b = new Quiz();
		b.setName("sdf");
		b.setDescription("sdsdf");
		b.setUserID(client.getUserID());
		
		
		assertTrue("the quiz was not saved", QuizDAO.push(b));
		assertTrue("the count did not go up after creation of new row", countBefore + 1 == QuizDAO.countOf());
		assertTrue("the quiz was not deleted", QuizDAO.deleteByObject(b));
	}
}
