package com.softwave.core.database.dao;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.softwave.core.container.design.DesignConfig;
import com.softwave.core.container.user.Address;
import com.softwave.core.container.user.Mail;
import com.softwave.core.container.user.Number;
import com.softwave.core.container.user.account.AnonUser;
import com.softwave.core.container.user.account.Client;
import com.softwave.core.container.user.account.User;
import com.softwave.core.container.user.account.UserInfo;
import com.softwave.core.security.HashString;
import com.softwave.core.security.Hashing;
import com.softwave.core.util.Date;

/**
 * This is a test for the UserDAO.
 * 
 * @TODO test the pullall, pullByFirstname, pullByLastname ...
 * @author eddi
 *
 */
public class UserDAOTest {
	private static Client client;
	private static AnonUser anonUser;

	@BeforeClass
	public static void init() {
		UserInfo userInfo = new UserInfo("eddi", "deLoste");
		userInfo.setAddress(new Address("mainStreet", "3", 1000, "Brussels", "Belgium"));
		userInfo.setEmail(new Mail("eddi@ehb.be"));
		String phone = "1234567890";
		userInfo.setPhonenumber(new Number(phone));
		client = new Client("eddi", new HashString(Hashing.hashPassword("password")));
		client.setDesignConfig(new DesignConfig());
		client.setUserInfo(userInfo);
		anonUser = new AnonUser();
	}


	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@AfterClass
	public static void cleanup() {

	}

	@Test
	public void testsequence() {
		testPushClient();
		testPushAnon();
		testUpdateClient();
		testPullClient();
		testPullAnon();
		testCanLogin();
		testDeleteClient();
		testDeleteAnon();
		// do this test as last, then we know we can already delete and push
		testCount();
	}
	
	public static void testPushClient() {
		long originalUserID = client.getUserID();
		Date originalDate = client.getCreateTime();
		try {
			UserDAO.push(client);
			System.out.println("*** object pushed, new id: " + client.getUserID());
			assertTrue("the ID was not updated", originalUserID != client.getUserID());
			assertTrue("the ID was not found", UserDAO.existsByID(client.getUserID()) == true);
			assertTrue("the create_time was not updated", originalDate != client.getCreateTime());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void testPushAnon() {
		long originalUserID = anonUser.getUserID();
		Date originalDate = client.getCreateTime();
		try {
			UserDAO.push(anonUser);
			System.out.println("*** object pushed, new id: " + anonUser.getUserID());
			assertTrue("the ID was not updated", originalUserID != anonUser.getUserID());
			assertTrue("the ID was not found", UserDAO.existsByID(anonUser.getUserID()) == true);
			assertTrue("the creat_time was not updated", originalDate != anonUser.getCreateTime());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void testUpdateClient() {
		client.getUserInfo().setFirstName("Frit");
		client.getUserInfo().setLastName("delanghe");

		try {
			assertTrue("the User was not updated, update returned false", UserDAO.update(client) == true);
			System.out.println("*** User updated, UserID: " + client.getUserID());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void testPullClient() {
		User user2;
		try {
			user2 = UserDAO.pullByID(client.getUserID());
			System.out.println("*** User pulled, pulled id: " + user2.getUserID());

			assertTrue("the pulled user is not of type Client", user2 instanceof Client);
			// assertTrue("Users not the same", ((Client) user2).equals(client));
			assertTrue("Firstnames are not the same",
					((Client) user2).getUserInfo().getFirstName().equals(client.getUserInfo().getFirstName()));
			assertTrue("Lastnames are not the same",
					((Client) user2).getUserInfo().getLastName().equals(client.getUserInfo().getLastName()));
			assertTrue("The DesignConfigs are not the same",
					((Client) user2).getDesignConfig().equals(client.getDesignConfig()));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void testPullAnon() {
		User user2;
		try {
			user2 = UserDAO.pullByID(anonUser.getUserID());
			System.out.println("*** User pulled, pulled id: " + user2.getUserID());

			assertTrue("the pulled user is not of type AnonUser", user2 instanceof AnonUser);
			assertTrue("Users not the same", ((AnonUser) user2).equals(anonUser));
			assertTrue("The DesignConfigs are not the same",
					((AnonUser) user2).getCreateTime().equals(anonUser.getCreateTime()));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void testCanLogin(){
		assertTrue("The Client should be able to login", UserDAO.canLogin("eddi", Hashing.hashPassword("password")));
		assertFalse("This user should NOT be able to login", UserDAO.canLogin("", Hashing.hashPassword("password")));
		System.out.println("*** The User logins are tested");

	}
	
	public static void testDeleteClient() {
		long originalID = client.getUserID();
		long originalAID = client.getUserInfo().getAddress().getAddressID();
		long originalDcID = client.getDesignConfig().getDesignConfigID();
		try {
			UserDAO.deleteByObject(client);
			System.out.println("*** User deleted");
			// TODO the On DELETE/UPDATE CASCADE in mysql is not working perfectly, ask mitchell
			//assertTrue("the designconfig is still found in database",
			//		DesignConfigDAO.existsByID(originalDcID) == false);
			// assertTrue("the address is still found in database", AddressDAO.existsByID(originalAID) == false);
			assertTrue("the User is still found in database", UserDAO.existsByID(originalID) == false);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void testDeleteAnon() {
		long originalID = anonUser.getUserID();
		try {
			UserDAO.deleteByObject(anonUser);
			System.out.println("*** User deleted");
			assertTrue("the User is still found in database", UserDAO.existsByID(originalID) == false);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void testCount() {
		int countBefore = UserDAO.countOf();
		User b = new AnonUser();
		UserDAO.pushOrUpdate(b);
		assertTrue("the count did not go up after creation of new row", countBefore + 1 == UserDAO.countOf());
		assertTrue("the user was not deleted", UserDAO.deleteByObject(b));
	}
}
