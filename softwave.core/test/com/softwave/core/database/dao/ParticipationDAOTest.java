package com.softwave.core.database.dao;

import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.softwave.core.container.design.DesignConfig;
import com.softwave.core.container.quiz.Answer;
import com.softwave.core.container.quiz.Participation;
import com.softwave.core.container.quiz.QType;
import com.softwave.core.container.quiz.Question;
import com.softwave.core.container.quiz.Quiz;
import com.softwave.core.container.user.Address;
import com.softwave.core.container.user.Mail;
import com.softwave.core.container.user.Number;
import com.softwave.core.container.user.account.Client;
import com.softwave.core.container.user.account.UserInfo;
import com.softwave.core.security.HashString;
import com.softwave.core.security.Hashing;
import com.softwave.core.util.Date;
import com.softwave.core.util.Language;

public class ParticipationDAOTest {
	private static Client client;
	private static Quiz quiz;
	private static Participation participation;
	
	@BeforeClass
	public static void init() {
		initClient();
		initQuiz();
		initParticipation();
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@AfterClass
	public static void cleanup() {
		QuizDAO.deleteByObject(quiz);
		UserDAO.deleteByID(client.getUserID());
	}

	public static void initParticipation(){
		participation = new Participation();
		participation.setQuiz(quiz);
		participation.setQuizzer(client);
		participation.setScore(3.5);
		participation.setStartTime(new Date());
		participation.setTimeElapsed(3600);
		
		quiz.getQuestions().stream().forEach(e -> {
			Answer answer = new Answer();
			answer.setQuestionID(e.getQuestionID());
			answer.setAnswer("my answer: " + e.getCorrectAnswer());
			answer.setStartTime(new Date());
			// make a different start and end date
			try {
				Thread.sleep(2000);
			} catch (Exception e1) {
				e1.printStackTrace();
			}
			answer.setEndTime(new Date());
			answer.setCorrectAnswer(true);
			participation.addAnswer(answer);
		});
		
	}
	
	public static void initClient() {
		UserInfo userInfo = new UserInfo("eddi", "deLoste");
		userInfo.setAddress(new Address("mainStreet", "3", 1000, "Brussels", "Belgium"));
		userInfo.setEmail(new Mail("eddi@ehb.be"));
		String phone = "1234567890";
		userInfo.setPhonenumber(new Number(phone));
		client = new Client("eddi", new HashString(Hashing.hashPassword("password")));
		client.setUserInfo(userInfo);
		client.setDesignConfig(new DesignConfig());

		// save the client to the database, this sets the userID
		UserDAO.push(client);
	}

	public static void initQuiz() {
		quiz = new Quiz("quiz Test");
		quiz.setUserID(client.getUserID());
		quiz.setDescription("this is a quiz to test");
		quiz.setDesignConfig(new DesignConfig());

		// create 3 test questions
		for (int i = 1; i < 4; i++) {
			System.out.println("*** create question " + i);
			Question question = new Question();
			question.setName("question" + i);
			question.setDesignConfig(new DesignConfig());
			// question.setQuestionNumber(i);
			question.setTickAnswers(i);
			question.setCorrectAnswer("antwoord " + i);
			if (i == 1) {
				question.setNeedsAnswer(true);
				question.setType(QType.YES_NO);
				question.setQuestionLang(Language.createFromString("en_INT"));
			}
			if (i == 2) {
				question.setType(QType.OPEN);
				question.setNeedsAnswer(true);
				question.setQuestionLang(Language.createFromString("fr_BE"));
			}
			if (i == 3) {
				question.setType(QType.MULTIPLE_CHOICE);
				question.setNeedsAnswer(false);
				question.setQuestionLang(Language.createFromString("nl_BE"));
			}
			System.out.println("*** adding question " + question.getQuestionNumber());
			quiz.addQuestion(question);
		}
		QuizDAO.push(quiz);

	}

	@Test
	public void testsequence() {
		testPush();
		testUpdate();
		testPull();
		testDelete();
		// do this test as last, then we know we can already delete and push
		testCount();
	}

	public static void testPush() {
		long originalParticipationID = participation.getParticipationID();
		try {
			ParticipationDAO.push(participation);
			System.out.println("*** object pushed, new id: " + participation.getParticipationID());
			assertTrue("the participationID was not updated", originalParticipationID != participation.getParticipationID());
			assertTrue("the ParticipationID was not found", ParticipationDAO.existsByID(participation.getParticipationID()) == true);

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public static void testUpdate() {
		// save the original values
		Date originalParticipationStart = participation.getStartTime();
		int originalTimeElapsed = participation.getTimeElapsed();
		String originalQuizName = participation.getQuiz().getName();
		
		// change the values
		participation.setTimeElapsed(14);
		participation.getQuiz().setName("testParticip");
		
		// update and test the values
		try {
			assertTrue("the participation was not updated, update returned false", ParticipationDAO.update(participation) == true);
			System.out.println("*** quiz updated, ParticipationID: " + participation.getParticipationID());
			assertTrue("the quiz name not updated", participation.getQuiz().getName() != originalQuizName);
			assertTrue("the startDate not updated", participation.getStartTime().equals(originalParticipationStart));

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void testPull() {
		Participation participation2;
		try {
			participation2 = ParticipationDAO.pullByID(participation.getParticipationID());
			System.out.println("*** participation pulled, pulled id: " + participation2.getParticipationID());

			assertTrue("TimeElapsed not the same", participation2.getTimeElapsed() == participation.getTimeElapsed());
			assertTrue("userID not the same", participation2.getQuizzer().getUserID() == participation.getQuizzer().getUserID());
			assertTrue("quizID not the same", participation2.getQuiz().getQuizID() == participation.getQuiz().getQuizID());
			
			} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void testDelete() {
		long originalID = participation.getParticipationID();
		try {
			ParticipationDAO.deleteByObject(participation);
			System.out.println("*** participation deleted");
			assertTrue("the participation is still found in database", ParticipationDAO.existsByID(originalID) == false);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void testCount() {
		int countBefore = ParticipationDAO.countOf();
		Participation b = new Participation();
		b.setTimeElapsed(300);
		b.setStartTime(new Date());
		b.setQuizzer(client);
		b.setQuiz(quiz);

		assertTrue("the participation was not saved", ParticipationDAO.push(b));
		assertTrue("the count did not go up after creation of new row", countBefore + 1 == ParticipationDAO.countOf());
		assertTrue("the participation was not deleted", ParticipationDAO.deleteByObject(b));
	}
}
