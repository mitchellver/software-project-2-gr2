package com.softwave.core.database.dao;

import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.softwave.core.container.quiz.QFile;

/**
 * This is a test for the QFileDAO. Important notice: this test depends on a
 * working QuestionDAO since it relies on Question to set the QuestionID in the
 * QFile object.
 * 
 * @author eddi
 *
 */
public class QFileDAOTest {
	private static QFile qf;

	/*
	 * please write a mock object to replace the database
	 * it would be a good software_testing exercise
	 */
	
	@BeforeClass
	public static void init() {
		qf = new QFile();
		qf.setPath("/first/path.png");
		qf.setQuestionID(1);
	}

	@Before
	public void setUpMock() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@AfterClass
	public static void cleanup() {

	}

	@Test
	public void testsequence() {
		testPush();
		testUpdate();
		testPull();
		testDelete();
		// do this test as last, then we know we can already delete and push
		testCount();
	}

	public static void testPush() {
		long originalQfileID = qf.getQfileID();
		try {
			QFileDAO.push(qf);
			System.out.println("*** object pushed, new id: " + qf.getQfileID());
			assertTrue("the fileID was not updated", originalQfileID != qf.getQfileID());
			assertTrue("the fileID was not found", QFileDAO.existsByID(qf.getQfileID()) == true);

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public static void testUpdate() {
		qf.setPath("een/ander/path");
		try {
			assertTrue("the QFILE was not updated, update returned false", QFileDAO.update(qf) == true);
			System.out.println("*** QFILE updated, QfileID: " + qf.getQfileID());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void testPull() {
		QFile qf2;
		try {
			qf2 = QFileDAO.pullByID(qf.getQfileID());
			System.out.println("*** QFile pulled, pulled id: " + qf2.getQfileID());

			assertTrue("QFiles not the same", qf.equals(qf2));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void testDelete() {
		long originalID = qf.getQfileID();
		try {
			QFileDAO.deleteByObject(qf);
			System.out.println("*** QFile deleted");
			assertTrue("the QFile is still found in database", QFileDAO.existsByID(originalID) == false);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void testCount() {
		int countBefore = QFileDAO.countOf();
		QFile b = new QFile();
		assertTrue("the designconfig was not saved", QFileDAO.push(b));
		assertTrue("the count did not go up after creation of new row", countBefore + 1 == QFileDAO.countOf());
		assertTrue("the designconfig was not deleted", QFileDAO.deleteByObject(b));
	}
}