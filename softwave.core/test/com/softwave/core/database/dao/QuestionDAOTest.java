package com.softwave.core.database.dao;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.softwave.core.container.design.DesignConfig;
import com.softwave.core.container.quiz.QFile;
import com.softwave.core.container.quiz.QType;
import com.softwave.core.container.quiz.Question;
import com.softwave.core.container.quiz.Quiz;
import com.softwave.core.util.Language;

/**
 * This is a test for the QuestionDAO. Important notice: this test depends on a
 * working QuestionDAO since it relies on Question to set the QuestionID in the
 * Question object.
 * 
 * @author eddi
 *
 */

public class QuestionDAOTest {
	private static Question question;
	private static Quiz quiz;
	@BeforeClass
	public static void init() {
		quiz = new Quiz("forQuestTest");
		
		question = new Question();
		
		question.setQuizID(quiz.getQuizID());
 		question.setName("QuizTest");
		question.setNeedsAnswer(true);
		question.addPicture(new QFile("pic1.png"));
		question.addPicture(new QFile("pic2.png"));
		question.setQuestionLang(Language.createFromString("nl_BE"));
		question.setDesignConfig(new DesignConfig());
		question.setCorrectAnswer("correct answser");
		question.setQuestionNumber(1);
		question.setTickAnswers(12);
		question.setTime(160);
		question.setType(QType.OPEN);
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@AfterClass
	public static void cleanup() {
		
	}

	@Test
	public void testsequence() {
		testPush();
		testUpdate();
		testPull();
		testDelete();
		// do this test as last, then we know we can already delete and push
		testCount();
	}

	public static void testPush() {
		long originalQuestionID = question.getQuestionID();
		try {
			assertTrue("question was not saved",QuestionDAO.push(question));
			System.out.println("*** object pushed, new id: " + question.getQuestionID());
			assertTrue("the fileID was not updated", originalQuestionID != question.getQuestionID());
			assertTrue("the fileID was not found", QuestionDAO.existsByID(question.getQuestionID()) == true);

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public static void testUpdate() {
		question.setName("a new name");
		question.addPicture(new QFile("pic3.png"));

		try {
			assertTrue("the Question was not updated, update returned false", QuestionDAO.update(question) == true);
			System.out.println("*** Question updated, QuestionID: " + question.getQuestionID());
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void testPull() {
		Question question2;
		try {
			question2 = QuestionDAO.pullByID(question.getQuestionID());
			System.out.println("*** Question pulled, pulled id: " + question2.getQuestionID());

			// assertTrue("question name not equal", question.getName() ==
			// question2.getName());
			// assertTrue("answer not equal", question.getCorrectAnswer() ==
			// question2.getCorrectAnswer());
			// assertTrue("lang not equal", question.getQuestionLang() ==
			// question2.getQuestionLang());
			// assertTrue("designconfig not equal",
			// question.getDesignConfig().equals(question2.getDesignConfig()));
			
			
			// this test fails because the logo path is not set since the file does not exist
			assertTrue("Questions not the same", question.equals(question2));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void testDelete() {
		long originalID = question.getQuestionID();
		ArrayList<Long> picIDs = new ArrayList<>();
		question.getPictures().stream()
			.forEach(e -> {
				picIDs.add(e.getQfileID());
			});
		
		try {
			if(QuestionDAO.deleteByObject(question) == false)
				throw new Exception("the question was not deleted");
			System.out.println("*** Question deleted");
			picIDs.stream()
				.forEach(ec -> {
					assertTrue("the picture is still found in database", QFileDAO.existsByID(ec) == false);
					});
			assertTrue("the Question is still found in database", QuestionDAO.existsByID(originalID) == false);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void testCount() {
		int countBefore = QuestionDAO.countOf();
		Question b = new Question("a last", QType.YES_NO, 1);
		
		try {
			assertTrue("the Question was not saved", QuestionDAO.push(b) == true);
		} catch (Exception e) {
			e.printStackTrace();
		}
		assertTrue("the count did not go up after creation of new row", countBefore + 1 == QuestionDAO.countOf());
		assertTrue("the designconfig was not deleted", QuestionDAO.deleteByObject(b));
		System.out.println("*** Question count was changed");
		QuestionDAO.deleteByObject(b);
	}
}