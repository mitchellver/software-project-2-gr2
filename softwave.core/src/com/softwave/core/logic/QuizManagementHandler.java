package com.softwave.core.logic;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

import javax.swing.JOptionPane;

import com.softwave.core.container.GlobalState;
import com.softwave.core.container.quiz.Quiz;
import com.softwave.core.database.dao.QuizDAO;
import com.softwave.core.ui.QuizManagementPanel;
import com.softwave.core.ui.WindowManager;
import com.softwave.core.ui.component.QuizPanel;
import com.softwave.core.util.FileChooser;

/**
 * This Class handles all the logic for the QuizManagement Panel. Like, loading
 * the panel with all the user's quizzes.
 * 
 * @author eddi
 *
 */
public class QuizManagementHandler {
	/**
	 * the variable to check if all dbOperations in a method where successful
	 */
	private static boolean dbOperationSuccess = true;
	/** the variable to count the numbers of dbOperation tries */
	private static int dbOperationTry = 0;
	/** the maximum number of times we may perform a dbOperation */
	private final int DB_MAX_TRIES = 3;

	/**
	 * A method to load the users Quizzes in the QuizManagementPanel. It creates
	 * an ArrayList of QuizPanels to pass to the QuizManagementPanel.
	 * 
	 * @return boolean true on succes, false on fail
	 */
	public static boolean loadQuizManagementPanel() {
		if (GlobalState.getClient() == null)
			return false;

		if (GlobalState.getCurrentQuiz() == null) {
			// get the quizzes linked to a user from the database
			GlobalState.setQuizzes(QuizDAO.pullByUserID(GlobalState.getClient().getUserID()));
			// Database.closeDatabase();
		}

		ArrayList<Quiz> quizzes = GlobalState.getQuizzes();
		if (quizzes == null)
			quizzes = new ArrayList<>(); // initialize the quizzes to an empty
											// list

		// create a list of QuizPanels to pass to the constructor of
		// QuizManagementPanel
		ArrayList<QuizPanel> userPanels = new ArrayList<>();
		quizzes.stream().forEach(e -> userPanels.add(new QuizPanel(e)));
		WindowManager.getInstance().registerView(new QuizManagementPanel(userPanels));
		return true;
	}

	/**
	 * Saves a specific quiz to the database, by calling the QuizDAO
	 * 
	 * @param quiz
	 *            the quiz we want to save to the database
	 * @return true on success false on fail
	 */
	public static boolean saveQuizToDatabase(Quiz quiz) {
		if (GlobalState.getClient() == null)
			return false;
		if (QuizDAO.pushOrUpdate(quiz)){
			return true;
		}
		return false;
	}

	/**
	 * Save the currently loaded quizzes to the database This calls the QuizDAO
	 * 
	 * @return
	 */
	public static boolean saveQuizesToDatabase() {
		if (GlobalState.getClient() == null)
			return false;

		GlobalState.getQuizzes().stream().forEach(e -> {
			try {
				if (QuizDAO.pushOrUpdate(e) == false)
					throw new Exception("The quiz was not saved, quizID: " + e.getQuizID());
			} catch (Exception ex) {
				dbOperationSuccess = false;
				System.out.println(ex.getMessage());
			}
		});

		// set the global state to 'updated' or 'nothing changed'
		stateChanged(false);
		// set the db try counter back to 0
		dbOperationTry = 0;

		if (dbOperationSuccess) {
			return true;
		}
		// TODO tell the user something went wrong
		return false;
	}

	/**
	 * Remove the quiz by calling the appropriate DAO and removing it from the
	 * GlobalState quizzes.
	 * 
	 * @param quiz
	 *            the quiz we want to remove
	 * @return true on success false on fail
	 */
	public static boolean removeQuizFromDatabase(Quiz quiz) {
		if (GlobalState.getQuizzes().contains(quiz))
			GlobalState.getQuizzes().remove(quiz);
		QuizManagementHandler.loadQuizManagementPanel();
		// TODO show message if deletion fails, and remove the quiz from the
		// list is the deletion was succesful.
		return QuizDAO.deleteByObject(quiz);
	}
	/**
	 * this method allows us to save a quiz offline as a serialized object
	 * 
	 * @param quiz
	 * @return true on success false on fail
	 */
	public static boolean saveQuizOffline(Quiz quiz) {
		ObjectOutputStream out = null;

		try {
			out = new ObjectOutputStream(new FileOutputStream(FileChooser.PATH_START + "quiz"));
			out.writeObject(quiz);
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				out.close();
			} catch (IOException e) {
				e.printStackTrace();

			}
			return false;
		}
	}

	/**
	 * this method loads a previously saved quiz, it will be mostly used by the quizzing application
	 * 
	 * @return
	 */
	public static boolean loadQuizOffline() {
		Quiz quiz = null;
		ObjectInputStream in = null;
		String quizPath = FileChooser.PATH_START + File.separator + "quiz";
		
		// if the quiz is not found let the user select it
		if(new File(quizPath).exists() == false){
			File otherQuiz = FileChooser.chooseFile();
			FileChooser.copyQuizToApplication(otherQuiz, FileChooser.PATH_START);
		}
			
		try {
			in = new ObjectInputStream(new FileInputStream(quizPath));
			quiz = (Quiz) in.readObject();
		} catch (IOException | ClassNotFoundException e) {
			JOptionPane.showMessageDialog(null, "The quiz could not be loaded");
			e.printStackTrace();
		} finally {
			try {
				in.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		if(quiz != null){
			GlobalState.setCurrentQuiz(quiz);
			return true;
		}
		return false;
	}
	
	
	/**
	 * a method to tell the global stat that something was changed on a quiz and
	 * we cannot quit the application before storing the changes, or they wil be
	 * lost
	 * 
	 * @param bool
	 *            the state of the quiz, changes is true, unchanges is false
	 */
	public static void stateChanged(boolean bool) {
		GlobalState.setStateChanged(bool);
	}
}
