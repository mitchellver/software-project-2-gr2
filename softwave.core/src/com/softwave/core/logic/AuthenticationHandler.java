package com.softwave.core.logic;

import javax.swing.JLabel;

import com.softwave.core.container.GlobalState;
import com.softwave.core.container.user.account.Client;
import com.softwave.core.database.dao.UserDAO;
import com.softwave.core.security.Hashing;
import com.softwave.core.ui.ControlDashboardPanel;
import com.softwave.core.ui.LoginPanel;
import com.softwave.core.ui.WindowManager;
import com.softwave.core.util.Translator;

/**
 * This Class handles the login, it is part of the logic layer that talks to the
 * gui and the DAO's.
 * 
 * @author eddi
 *
 */
public class AuthenticationHandler {

	public AuthenticationHandler() {
	}

	/**
	 * This is the main method to login a user. it receives a username and
	 * password, hashes the password and asks the UserDAO whether this user can
	 * login, meaning the username and password are correct.
	 * 
	 * @param username the users username, this is unique in the database
	 * @param password the users password, this is a hashed string
	 * @param lbError the JPanel where we can put an error message on fail
	 * @return true on success and false on fail 
	 */
	public static boolean login(String username, String password, JLabel lbError) {
		// check if the username and password match a user in the database
		// that may login
		if (UserDAO.canLogin(username, Hashing.hashPassword(password)) == true) {
			// log in, and show the controlDashBoardPanel
			GlobalState.setClient((Client) UserDAO.pullByUsername(username));
			if (GlobalState.getClient() == null) {
				lbError.setText(Translator.translate("label.login.pullUserFailed"));
				return false;
			} else {
				
				WindowManager.getInstance().registerView(new ControlDashboardPanel());
				return true;
			}
		} // show a red login failed message to the user on the loginpanel
		lbError.setText(Translator.translate("label.login.failed"));
		return false;
	}
	
	
	/**
	 * This is the method that logs a user out.
	 * @param user
	 */
	public static void logout(){
		// clear the cached info
		GlobalState.setQuizzer(null);
		GlobalState.setClient(null);
		GlobalState.setQuizzes(null);
		GlobalState.setCurrentQuiz(null);
		GlobalState.setCurrentQuestion(null);
				
		// load the loginpanel again, this also clears the history of panels / views
		WindowManager.getInstance().registerView(new LoginPanel());
	}
}