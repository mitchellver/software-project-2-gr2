package com.softwave.core.logic;

import java.util.ArrayList;

import com.softwave.core.container.GlobalState;
import com.softwave.core.container.quiz.Question;
import com.softwave.core.database.dao.QuestionDAO;
import com.softwave.core.ui.QuestionManagementPanel;
import com.softwave.core.ui.WindowManager;
import com.softwave.core.ui.component.QuestionPanel;

/**
 * This Class handles all the logic for the QuestionManagement Panel. Like, loading
 * the panel with all the user's questions.
 * 
 * @author eddi
 *
 */
public class QuestionManagementHandler {
	/**
	 * A method to load the users Quizzes in the QuizManagementPanel.
	 * It creates an ArrayList of QuizPanels to pass to the QuizManagementPanel.
	 * 
	 * @return boolean true on succes, false on fail
	 */
	public static boolean loadQuestionManagementPanel() {
		if (GlobalState.getClient() == null)
			return false;
		// TODO add JOptionPane to the method that called thus method.
		if(GlobalState.getCurrentQuiz() == null)
				return false;
		
		// loda the current questions
		ArrayList<Question> questions = GlobalState.getCurrentQuiz().getQuestions();
		if (questions == null)
			questions = new ArrayList<>();
		// create a list of QuizPanels to pass to the constructor of
		// QuizManagementPanel
		ArrayList<QuestionPanel> questionPanels = new ArrayList<>();
		questions.stream().forEach(
				e -> questionPanels.add(new QuestionPanel(e))
				);
		WindowManager.getInstance().registerView(new QuestionManagementPanel(questionPanels));
		return true;
	}
	
	public static boolean removeQuestion(Question question){
		if(question == null)
			return false;
		GlobalState.getQuizzes().stream()
							   .filter(e -> e.getQuizID() == question.getQuizID())
							   .forEach(e -> {
								   e.getQuestions().remove(question);
							   });
		QuestionManagementHandler.loadQuestionManagementPanel();
		return QuestionDAO.deleteByObject(question);
	}
}
