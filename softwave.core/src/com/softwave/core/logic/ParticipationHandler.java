package com.softwave.core.logic;

import java.util.ArrayList;

import com.softwave.core.container.GlobalState;
import com.softwave.core.container.quiz.Answer;
import com.softwave.core.container.quiz.Participation;
import com.softwave.core.container.quiz.Question;
import com.softwave.core.database.dao.ParticipationDAO;
import com.softwave.core.database.dao.UserDAO;
import com.softwave.core.ui.SingleQuestionPanel;
import com.softwave.core.ui.WindowManager;

/**
 * This handles all the logic for a participation in a quiz.
 * 
 * @author adeel en eddi
 *
 */
public class ParticipationHandler {
	/** defaul empty constructor */
	public ParticipationHandler() {
	}

	/**
	 * this method adds a new answer to a participation, it is triggered when a
	 * new question is ready to be answered (shown in the application)
	 * 
	 * @param participation
	 * @param question
	 */
	public static void addAnswer(Participation participation, Question question) {
		// first check if the answer already exists
		if (participation.getAnswers().size() >= question.getQuestionNumber()) {
			return;
		}
		// the answer does not exist, create it
		Answer answer = new Answer(question.getQuestionID());
		if (participation.getAnswers() == null)
			participation.setAnswers(new ArrayList<>());
		participation.getAnswers().add(answer);
	}

	/**
	 * this method tells the participation that we move to the next question, It
	 * will set the end Time of the previous answer and adds a new answer for
	 * the next question
	 * 
	 * @param participation
	 * @param questionNumber
	 */
	public static void gotoQuestion(int nextQuestionNumber) {
		// tell the windowmanager where we want to go
		WindowManager.getInstance().registerView(
				new SingleQuestionPanel(GlobalState.getCurrentQuiz(), GlobalState.getQuizzer(), nextQuestionNumber));

		// if we clicked on a previous question
		// if (nextQuestionNumber <
		// GlobalState.getCurrentParticipation().getAnswers().size()) {
		// return;
		// } else
		// addAnswer(participation,
		// participation.getQuiz().getQuestions().get(nextQuestionNumber));
	}

	/**
	 * A method to check if all the questions that require an answer are
	 * answered
	 * 
	 * @return true if the quizzer answered the required questions, false if te
	 *         quizzer did not.
	 */
	public static boolean allRequiredQuestionsAnswered() {
		Participation participation = GlobalState.getCurrentParticipation();
		// ArrayList<Answer> answers = participation.getAnswers();
		// ArrayList<Integer> requiredUncompleted = new ArrayList<Integer>();

		if (participation.getAnswers() == null
				|| participation.getQuiz().getQuestions().size() != participation.getAnswers().size())
			return false;
		return true;
	}

	public static boolean submitParticipation() {
		boolean submitBool = false;

		// if the quizzer did not answer all needed questions, return false
		if (!allRequiredQuestionsAnswered())
			return false;

		// save the participation
		if (UserDAO.pushOrUpdate(GlobalState.getCurrentParticipation().getQuizzer())){
			submitBool = ParticipationDAO.push(GlobalState.getCurrentParticipation());
		}
		else
			submitBool = false;
		return submitBool;
	}

}
