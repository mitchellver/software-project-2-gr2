//package com.softwave.core.xml;
//
//import org.w3c.dom.Document;
//
///**
// * This class is a pure override class that is used to build an xml document, it
// * needs to be used as an annonimous class in the xmlbuilder class
// * 
// * @author nicolas
// * @version 1.0
// */
//@FunctionalInterface
//public interface IXMLBuild {
////	/**
////	 * The template of the build method used as a lambda expression, in this
////	 * method will be specified how the structure of the xml file is build
////	 * <br />
////	 * for example, the body of this method inside a lambda could look like this:
////	 * <pre>
////	 * {@code
////	 * 	Document document = b.getDocumentBuilder().newDocument();
////	 * 
////	 * 	Element root = document.createElement("quiz");
////	 * 	Element basic = document.createElement("basic");
////	 * 	Text text = document.createTextNode("battle");
////	 * 
////	 * 	basic.appendChild(text);
////	 * 	root.appendChild(basic);
////	 * 	document.appendChild(root);
////	 * 
////	 * 	return document;
////	 * }
////	 * </pre>
////	 * 
////	 * @param b
////	 * @return
////	 */
////	Document build(XMLBuilder b);
//}
