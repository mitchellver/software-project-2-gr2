//package com.softwave.core.xml;
//
//import java.io.File;
//import java.io.FileNotFoundException;
//import java.io.FileOutputStream;
//import java.io.IOException;
//
//import javax.xml.parsers.DocumentBuilder;
//import javax.xml.parsers.DocumentBuilderFactory;
//import javax.xml.parsers.ParserConfigurationException;
//
//import org.w3c.dom.Document;
//
////import com.sun.org.apache.xml.internal.serialize.OutputFormat;
////import com.sun.org.apache.xml.internal.serialize.XMLSerializer;
//
///**
// * The XMLBuilder class is responsible for the export of quizzes to an structured
// * XML document
// * 
// * @author nicolas
// * @version 1.0
// */
//public class XMLBuilder {
////	/** Singleton class to make DocumentBuilder classes */
////	private DocumentBuilderFactory documentBuilderFactory;
////	
////	/** Allows the creation of structured documents */
////	private DocumentBuilder documentBuilder;
////	
////	/** The main xml document that will be created */
////	private Document xmlDocument;
////	
////	/** The path pointing to the to creating file */
////	private File path;
////	
////	/** The object that is needed to write the file on the filesystem */
////	private FileOutputStream outputStream;
////	
////	/** The formatting scheme used to build the xml file */
////	private OutputFormat outputFormat;
////	
////	/** Switch that describes if the file needs to be indented or not */
////	private boolean compressed;
////
////	/**
////	 * The constructor for the XMLBuilder class, this makes the basic xml document.
////	 * 
////	 * @throws ParserConfigurationException
////	 */
////	public XMLBuilder(File path) throws ParserConfigurationException, FileNotFoundException {
////		this(true, path);
////	}
////	
////	/**
////	 * The constructor for the XMLBuilder class, this makes the basic xml document.
////	 * 
////	 * @param compressed does the file needs to be indented for better readability
////	 * or not
////	 * @throws ParserConfigurationException
////	 */
////	public XMLBuilder(boolean compressed, File path) throws ParserConfigurationException, FileNotFoundException {
////		// if the file path is null or is a directory or if the file cannot be written
////		// stop the construction of the builder.
////		if (path == null) {
////			System.out.println("still things wrong with the file");
////			throw new FileNotFoundException();
////		}
////		
////		// allows the creation of document builders.
////		this.documentBuilderFactory = DocumentBuilderFactory.newInstance();
////		// allows the creation of structured xml documents.
////		this.documentBuilder = documentBuilderFactory.newDocumentBuilder();
////		// allows the creation of elements and other suff that you need inside the
////		// document.
////		this.xmlDocument = documentBuilder.newDocument();
////		
////		// path to store the xml file.
////		this.path = path;
////		this.outputStream = new FileOutputStream(path);
////		
////		// initialyze the fileformat.
////		this.outputFormat = new OutputFormat();
////		this.outputFormat.setIndenting(compressed);
////		this.outputFormat.setEncoding("utf-8");
////		
////		System.out.println("constructor xmlbuilder ran!");
////	}
////	
////	/**
////	 * This method will create a document object based on the instructions of the
////	 * xmlBuild object given as a parameter, the best way to approch this is to
////	 * make an annonimous inner class or use a lambda expression.
////	 * 
////	 * @param obj as a functional interface the object that holds the instruction for the structure of
////	 * the xml object.
////	 */
////	public void buildXML(XMLBuilder builder, IXMLBuild xmlBuild) throws NullPointerException{
////		if (builder == null) {
////			System.out.println("builder is null!");
////			throw new NullPointerException();
////		}
////		this.xmlDocument = xmlBuild.build(builder);
////		System.out.println("document has been gotten!");
////	}
////	
////	/**
////	 * This method will build the actual xml file on the file system, using the info
////	 * given to the class in the beginning and with the buildXML method.
////	 * 
////	 * @return the state of succes
////	 * @throws IOException
////	 */
////	public boolean buildFile() throws IOException, NullPointerException {
////		if (xmlDocument == null) throw new NullPointerException();
////		System.out.println("xmldoc is not null!");
////		if (outputFormat == null) throw new NullPointerException();
////		System.out.println("outputformat is not null!");
////		if (outputStream == null) throw new NullPointerException();
////		System.out.println("outputstream is not null!");
////		
////		XMLSerializer serializer = new XMLSerializer(outputStream, outputFormat);
////		serializer.serialize(xmlDocument);
////		System.out.println("the document has been made!");
////		return true;
////	}
////	
////	/**
////	 * A getter method for returning the Document object of the class.
////	 * 
////	 * @return the document object.
////	 */
////	public Document getDocument() {
////		return xmlDocument;
////	}
////
////	/**
////	 * A getter method for getting the documentBuilderFactory.
////	 * 
////	 * @return the documentBuilderFactory object.
////	 */
////	public DocumentBuilderFactory getDocumentBuilderFactory() {
////		return documentBuilderFactory;
////	}
////
////	/**
////	 * Get the documentBuilder from the class.
////	 * 
////	 * @return the documentBuilder object.
////	 */
////	public DocumentBuilder getDocumentBuilder() {
////		return documentBuilder;
////	}
////
////	/**
////	 * A getter for getting the document object
////	 * 
////	 * @return the xmldocument object.
////	 */
////	public Document getXmlDocument() {
////		return xmlDocument;
////	}
////
////	/**
////	 * A getter for getting the filepath for making the file.
////	 * 
////	 * @return the file object containing the filepath of the xml file.
////	 */
////	public File getPath() {
////		return path;
////	}
////	
////	/**
////	 * A setter method to set the path where the file is being stored, if the file
////	 * null nothing will happen.
////	 * 
////	 * @param file the file object with a path to the new location of the xml file.
////	 */
////	public void setFile(File file) {
////		if (file == null)
////			return;
////		
////		this.path = path;
////	}
////
////	/**
////	 * A getter method for getting the output format object.
////	 * 
////	 * @return the outputformat object.
////	 */
////	public OutputFormat getOutputFormat() {
////		return outputFormat;
////	}
////
////	/**
////	 * A getter for the compressed flag.
////	 * 
////	 * @return the compressed flag object as a boolean.
////	 */
////	public boolean isCompressed() {
////		return compressed;
////	}
//}
