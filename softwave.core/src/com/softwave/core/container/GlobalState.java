package com.softwave.core.container;

import java.util.ArrayList;

import com.softwave.core.container.quiz.Participation;
import com.softwave.core.container.quiz.Question;
import com.softwave.core.container.quiz.Quiz;
import com.softwave.core.container.user.account.Client;
import com.softwave.core.container.user.account.KnownUser;
import com.softwave.core.container.user.account.User;

/**
 * This is a class to hold the logged in user and the quizzes / participations linked to that user.
 * It creates a link between the DAO's and the UI.
 * @author eddi
 */

public class GlobalState {
	/** to tell the application if the quiz has been changed and not saved to db */
	private static boolean stateChanged;
	
	/** the logged in client */
	private static Client client;
	/** the anonymous user or quizer */
	private static User quizzer;
	
	/** the quiz we are working on */
	private static Quiz currentQuiz;
	/** the question we are working on */
	private static Question currentQuestion;
	/** The current participation of a user */
	private static Participation currentParticipation;
	
	/** a list of known users or useraccounts */
	private static ArrayList<KnownUser> knownUsers;
	/** the list of quizzes linked to a user */
	private static ArrayList<Quiz> quizzes;
	/** the list of participations for a given quiz */
	private static ArrayList<Participation> participations;
	
	public GlobalState(){}
	
	
	/**
	 * create a new quiz and add it to the quizlist,
	 * this changes the quizChangesstate to true. 
	 * @param quiz the quiz to be added
	 * @return true on success false on fail
	 */
	
	public static boolean addQuiz(Quiz quiz){
		if(quiz == null){
			return false;
		}
		// the arraylist with quizzes is empty, create a new one
		if(quizzes == null){
			quizzes = new ArrayList<>();
		}
		quizzes.add(quiz);
		return true;
	}
	
	public static Client getClient() {
		return client;
	}

	public static void setClient(Client client) {
		GlobalState.client = client;
	}

	public static ArrayList<Quiz> getQuizzes() {
		return quizzes;
	}

	public static void setQuizzes(ArrayList<Quiz> quizzes) {
		GlobalState.quizzes = quizzes;
	}

	public static ArrayList<Participation> getParticipations() {
		return participations;
	}

	public static void setParticipations(ArrayList<Participation> participations) {
		GlobalState.participations = participations;
	}

	public static Quiz getCurrentQuiz() {
		return currentQuiz;
	}

	public static void setCurrentQuiz(Quiz currentQuiz) {
		GlobalState.currentQuiz = currentQuiz;
	}

	public static Question getCurrentQuestion() {
		return currentQuestion;
	}

	public static void setCurrentQuestion(Question currentQuestion) {
		GlobalState.currentQuestion = currentQuestion;
	}

	public static ArrayList<KnownUser> getKnownUsers() {
		return knownUsers;
	}

	public static void setKnownUsers(ArrayList<KnownUser> knownUsers) {
		GlobalState.knownUsers = knownUsers;
	}

	public static boolean isStateChanged() {
		return stateChanged;
	}

	public static void setStateChanged(boolean stateChanged) {
		GlobalState.stateChanged = stateChanged;
	}

	public static Participation getCurrentParticipation() {
		return currentParticipation;
	}

	public static void setCurrentParticipation(Participation currentParticipation) {
		GlobalState.currentParticipation = currentParticipation;
	}

	public static User getQuizzer() {
		return quizzer;
	}

	public static void setQuizzer(User quizzer) {
		GlobalState.quizzer = quizzer;
	}
	
}