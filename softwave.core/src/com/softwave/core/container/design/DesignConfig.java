package com.softwave.core.container.design;

import java.awt.Color;
import java.awt.Font;
import java.io.File;
import java.io.Serializable;

import com.softwave.core.reference.DefaultDesign;

/**
 * an application with as purpose to configure the design of the main
 * application by changing the logo, the backgroundcolor and the font
 * 
 * @author sofian
 *
 */

public class DesignConfig implements Serializable {

	/**
	 * the id of the object which is also the id in the database, initialize on
	 * -1 to create a new entry in DB
	 */
	private long designConfigID = -1;

	/** the file to which the path of the logo refers */
	private String logoFile;

	/** the color of the background */
	private Color backgroundColor;

	/** the font of the text */
	private Font font;

	/**
	 * an empty constructor to use in the DAOs
	 */
	public DesignConfig() {
		this(DefaultDesign.LOGO_DEFAULT, DefaultDesign.PANEL_BACKGROUND_COLOR, DefaultDesign.FONT_MAIN, true);
	}

	/**
	 * The constructor of the DesignConfig class. logoPath will be converted to
	 * a File type that will be taken over by LogoFile.
	 * 
	 * @param logoPath
	 *            the path of the logo.
	 * @param backgroundColor
	 *            the color of the background.
	 * @param font
	 *            the font of the text.
	 */
	public DesignConfig(String logoFile, Color backgroundColor, Font font, boolean active) {
		super();
		this.logoFile = logoFile;
		// Converting the logo path to logo file
		this.logoFile = logoFile;
		this.backgroundColor = backgroundColor;
		this.font = font;
	}

	/**
	 * A getter method for returning the ID of the class.
	 * 
	 * @return the ID which is a long
	 */

	public long getDesignConfigID() {
		return designConfigID;
	}

	/**
	 * Sets the ID of the class, should be 0 on create, and the database sets
	 * this on push().
	 * 
	 * @param designConfigID
	 */
	public void setDesignConfigID(long designConfigID) {
		this.designConfigID = designConfigID;
	}

	/**
	 * A getter method for returning the logoFile object of the class.
	 * 
	 * @return the logoFile object
	 */
	public String getLogoFile() {
		return logoFile;
	}

	/**
	 * Sets the file of the logo by first checking if the path is correct.
	 * 
	 * @param logoFile
	 */
	public void setLogoFile(String logoFile) {
		if(logoFile == null){
			return;
		}
			this.logoFile = logoFile;
		} 

	/**
	 * A getter method for returning the background color of the class.
	 * 
	 * @return the backgroundColor object.
	 */
	public Color getBackgroundColor() {
		return backgroundColor;
	}

	/**
	 * Sets the background color of the design.
	 * 
	 * @param backgroundColor
	 *            the background color of the design.
	 */
	public void setBackgroundColor(Color backgroundColor) {
		if(backgroundColor == null)
			return;
		this.backgroundColor = backgroundColor;
	}

	/**
	 * A getter method for returning the font of the class.
	 * 
	 * @return the font object.
	 */
	public Font getFont() {
		return font;
	}

	/**
	 * Sets the font of the text.
	 * 
	 * @param font
	 *            the font of the text.
	 */
	public void setFont(Font font) {
		if(font == null)
			return;
		this.font = font;
	}

	/** The hashCode of the DesignConfig class. */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((backgroundColor == null) ? 0 : backgroundColor.hashCode());
		result = prime * result + ((font == null) ? 0 : font.hashCode());
		result = prime * result + ((logoFile == null) ? 0 : logoFile.hashCode());
		return result;
	}

	/** The equals method of the DesignConfig class. */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DesignConfig other = (DesignConfig) obj;
		if (backgroundColor == null) {
			if (other.getBackgroundColor() != null)
				return false;
		} else if (!backgroundColor.equals(other.getBackgroundColor()))
			return false;
		if (font == null) {
			if (other.getFont() != null)
				return false;
		} else if (!font.equals(other.getFont()))
			return false;
		if (logoFile == null) {
			if (other.getLogoFile() != null)
				return false;
		} else if (!logoFile.equals(other.getLogoFile()))
			return false;
		return true;
	}

	/** The toString method of the class DesignConfig. */
	@Override
	public String toString() {
		return "DesignConfig [logoPath=" + logoFile + "\n , backgroundColor=" + backgroundColor + "\n , font=" + font
				+ "]";
	}

}