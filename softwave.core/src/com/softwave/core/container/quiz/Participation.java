package com.softwave.core.container.quiz;

import java.util.ArrayList;

import com.softwave.core.container.user.account.User;
import com.softwave.core.util.Date;

/**
 * This class will hold the information of a quizzer by creating a participation
 * in the database and linking the necessary information to retrieve all
 * information about the quiz itself.
 *
 * @author sofian
 * 
 */
public class Participation {
	/** the id tha uniquely identifies the participation in the database */
	private long participationID = -1;
	
	/** the quizzer that participates to a quiz */
	private User quizzer;

	/** the quiz this participation belongs to */
	private Quiz quiz;

	/** the score that the quizzer gets by achieving the quiz */
	private double score;

	/** the time the quizzer started the quiz */
	private Date startTime;
	
	/** the time that the quizzer took to end the quiz */
	private int timeElapsed;

	/** all the answers of the quizzer */
	private ArrayList<Answer> answers;

	
	/** and empty constructor used by the DAO */
	public Participation(){}
	/**
	 * The main constructor of the Participation class.
	 * 
	 * @param quizzer
	 *            the user that fills the quiz in
	 * @param quiz
	 *            the name of the quiz to which has been participated
	 * @param score
	 *            the score of the quizzer
	 * @param timeElapsed
	 *            the amount of time that has been taken to en the quiz
	 * @param answers
	 *            the answers that the quizzer gave
	 */
	public Participation(User quizzer, Quiz quiz, double score, int timeElapsed, ArrayList<Answer> answers) {
		super();
		this.quizzer = quizzer;
		this.quiz = quiz;
		this.score = score;
		// set the startTime
		this.startTime = new Date();
		this.timeElapsed = timeElapsed;
		// ArrayList must be initialized.
		this.answers = new ArrayList<Answer>();
	}

	public boolean addAnswer(Answer answer){
		if(answer == null )
			return false;
		if(answers == null)
			answers = new ArrayList<>();
		answers.add(answer);
		return true;
	}
	/**
	 * This method will add an answer to the ArrayList answers.
	 * 
	 * @param answer
	 *            the answer to add
	 */
	public void addAnswers(Answer answer) {
		this.answers.add(answer);

	}

	/**
	 * A getter method for returning the user that participated to the quiz.
	 * 
	 * @return the quizzer object
	 */
	public User getQuizzer() {
		return quizzer;
	}

	/**
	 * Sets the quizzer that participated to the quiz.
	 * 
	 * @param quizzer
	 *            the user that participated to the quiz.
	 */
	public void setQuizzer(User quizzer) {
		this.quizzer = quizzer;
	}

	/**
	 * A getter method for returning the name of the quiz.
	 * 
	 * @return the quiz object
	 */
	public Quiz getQuiz() {
		return quiz;
	}

	/**
	 * Sets the name of the quiz.
	 * 
	 * @param quiz
	 *            the name of the quiz to which the quizzer participated.
	 */
	public void setQuiz(Quiz quiz) {
		this.quiz = quiz;
	}

	/**
	 * A getter method for returning the score of the quizzer.
	 * 
	 * @return the score object
	 */
	public double getScore() {
		return score;
	}

	/**
	 * Sets the score of the quizzer.
	 * 
	 * @param score
	 *            the score of the user that took part to the quiz.
	 */
	public void setScore(double score) {
		/*
		 * TODO a way to calculate the correct answers by comparing the answers
		 * that the quizzer gave with the correctAnswer of each question and
		 * without forgetting the fact that optional questions don't have a
		 * score.
		 */
		this.score = score;
	}

	/**
	 * A getter method for returning the time that elapsed to terminate the
	 * quiz.
	 * 
	 * @return the timeElapsed object
	 */
	public int getTimeElapsed() {
		return timeElapsed;
	}

	/**
	 * Sets the time that the quizzer took to end the quiz.
	 * 
	 * @param timeElapsed
	 *            the amount of time that the quizzer took to terminate the
	 *            quiz.
	 */
	public void setTimeElapsed(int timeElapsed) {
		this.timeElapsed = timeElapsed;
	}

	/**
	 * A getter method for returning the answers of the quiz.
	 * 
	 * @return answers all the answers of the quiz that the quizzer gave.
	 */
	public ArrayList<Answer> getAnswers() {
		return answers;
	}
	public long getParticipationID() {
		return participationID;
	}
	public void setParticipationID(long participationID) {
		this.participationID = participationID;
	}
	public Date getStartTime() {
		return startTime;
	}
	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}
	public void setAnswers(ArrayList<Answer> answers) {
		this.answers = answers;
	}

}