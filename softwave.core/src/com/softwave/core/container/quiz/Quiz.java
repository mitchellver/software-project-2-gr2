package com.softwave.core.container.quiz;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.softwave.core.container.design.DesignConfig;
import com.softwave.core.util.Translator;

/**
 * This class will hold the information of a quiz, the questions and the design
 * configuration.
 *
 * @author sofian
 * 
 */

public class Quiz implements Serializable{
	/** the quiz ID which uniquely identifies the quiz in the database */
	protected long quizID = -1;

	/** the id of the user that owns/created this quiz, */
	protected long userID;

	/** the name of the quiz. */
	protected String name;

	/** the description of the quiz. */
	protected String description;

	/** all the questions that belongs to the quiz. */
	protected ArrayList<Question> questions;

	/** the design configuration of the quiz. */
	protected DesignConfig designConfig;

	/**
	 * a non-argument constructor used by the DAO
	 */
	public Quiz() {
		this.questions = new ArrayList<Question>();
		this.designConfig = new DesignConfig();
		this.name = "untitled";
	}

	/**
	 * The constructor of the Quiz class. The minimum required to create a quiz
	 * is the quiz name.
	 * 
	 * @param name
	 *            the name of the quiz.
	 */
	public Quiz(String name) {
		super();
		this.name = name;
		this.designConfig = new DesignConfig();
		this.questions = new ArrayList<Question>();
	}

	/**
	 * The constructor of the Quiz class. The minimum required to create a quiz
	 * is the quiz name.
	 * 
	 * @param name
	 *            the name of the quiz.
	 * @Param userID the id of the user, making the quiz saveable to the
	 *        database
	 */
	public Quiz(long userID, String name) {
		super();
		this.userID = userID;
		this.name = name;
		this.designConfig = new DesignConfig();
		this.questions = new ArrayList<Question>();
	}
	
	/**
	 * This method will create a new quiz and fill in the default values.
	 * 
	 * @return the new empty quiz.
	 */
	public static Quiz newQuiz() {
		return new Quiz(Translator.translate("text.quiz.untitled"));
	}

	/**
	 * This method will add a new question to the ArrayList of questions if the
	 * minimum required information of the question is completed. When a
	 * question is created will the questionID go up with 1.
	 * 
	 * @param question
	 *            the new question to add.
	 * @return if the new question was added correctly.
	 */
	public boolean addQuestion(Question question) {
		// if null -> return false + syso
		if (question == null) {
			System.out.println("Please fill the question in.");
			return false;

		} else {
			this.questions.add(question);

			return true;
		}

	}

	/**
	 * This method will delete a specific question of the ArrayList of
	 * questions. As well as in the database.
	 * 
	 * @param questionID
	 *            the question to delete.
	 * @return if the question was correctly deleted.
	 */
	protected boolean removeQuestion(int questionID) {
		// TODO delete the question of database
		// TODO delete the question of ArrayList questions
		return false;
	}

	/**
	 * This method will purge a specific question of the ArrayList of questions.
	 * As well as in the database.
	 * 
	 * @param questionID
	 *            the question to purge.
	 * @return if the question was correctly purged.
	 */
	protected boolean purgeQuestion(int questionID) {
		// TODO delete the question of database
		// TODO delete the question of ArrayList questions
		return false;
	}

//	/**
//	 * This method will export an XML file by calling the doc function that will
//	 * add all the information to the XML document.
//	 * 
//	 * @return if it was correctly created.
//	 */
//	public boolean exportXML() {
//		try {
//			XMLBuilder builder = new XMLBuilder(new File("test.xml"));
//
//			builder.buildXML(builder, e -> doc(builder));
//			builder.buildFile();
//
//			return true;
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//		return false;
//	}

	// /**
	// * This method will fill the XML document in.
	// *
	// * @param builder
	// * responsible for the export to a structured XML document.
	// * @return the document after all information are in the XML document.
	// */
	// private Document doc(XMLBuilder builder) {
	// Document document = builder.getDocument();
	// Element quiz = document.createElement("Quiz");
	// Element quizName = document.createElement("QuizName");
	// Element description = document.createElement("Description");
	// Element questions = document.createElement("Questions");
	// Element question = document.createElement("Question");
	// Element questionName = document.createElement("QuestionName");
	// Element type = document.createElement("Type");
	// Element correctAnswer = document.createElement("CorrectAnswer");
	// Element time = document.createElement("Time");
	// Element pictures = document.createElement("Pictures");
	// Element picture = document.createElement("Picture");
	// Element needsAnswer = document.createElement("NeedsAnswer");
	// Element tickAnswer = document.createElement("TickAnswer");
	// Element questionLang = document.createElement("QuestionLang");
	//
	// quiz.appendChild(quizName);
	// quizName.appendChild(document.createTextNode(getName()));
	// quiz.appendChild(description);
	// description.appendChild(document.createTextNode(getDescription()));
	//
	// if (this.questions.size() > 0) {
	// quiz.appendChild(questions);
	//
	// for (Question quest : this.questions) {
	// questions.appendChild(question);
	// question.appendChild(document.createTextNode(String.valueOf(quest.getQuestionID())));
	// question.appendChild(questionName);
	// questionName.appendChild(document.createTextNode(quest.getName()));
	// question.appendChild(type);
	// type.appendChild(document.createTextNode(String.valueOf(quest.getType())));
	// question.appendChild(correctAnswer);
	// correctAnswer.appendChild(document.createTextNode(quest.getCorrectAnswer()));
	// question.appendChild(time);
	// time.appendChild(document.createTextNode(String.valueOf(quest.getTime())));
	// question.appendChild(pictures);
	// for (int j = 0; j < quest.getPictures().size(); j++) {
	// pictures.appendChild(picture);
	// picture.appendChild(document.createTextNode(String.valueOf(quest.getPictures())));
	// }
	// question.appendChild(needsAnswer);
	// needsAnswer.appendChild(document.createTextNode(String.valueOf(quest.isNeedsAnswer())));
	// question.appendChild(tickAnswer);
	// tickAnswer.appendChild(document.createTextNode(String.valueOf(quest.getTickAnswers())));
	// question.appendChild(questionLang);
	// questionLang.appendChild(document.createTextNode(quest.getQuestionLang().toCodeString()));
	//
	// }
	// }
	// document.appendChild(quiz);
	// return document;
	// }

	/**
	 * A getter method for returning the quizID which uniquely identifies the
	 * quiz in the database.
	 * 
	 * @return the quizID object.
	 */
	public long getQuizID() {
		return quizID;
	}

	/**
	 * Sets the id of the quiz.
	 * 
	 * @param quizID
	 *            the quiz ID which uniquely identifies the quiz in the
	 *            database.
	 */
	public void setQuizID(long quizID) {
		this.quizID = quizID;
	}

	/**
	 * A getter method for returning the userID of the user that created the
	 * quiz.
	 * 
	 * @return the userID object.
	 */
	public long getUserID() {
		return userID;
	}

	/**
	 * Sets the id of the user that created the quiz.
	 * 
	 * @param userID
	 *            the id of the user that created the quiz.
	 */
	public void setUserID(long userID) {
		this.userID = userID;
	}

	/**
	 * A getter method for returning the name of the quiz.
	 * 
	 * @return the name object.
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name of the quiz.
	 * 
	 * @param name
	 *            the name of the quiz.
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * A getter method for returning the description of the quiz.
	 * 
	 * @return the description object.
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Sets the description of the quiz.
	 * 
	 * @param description
	 *            the description of the quiz.
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * A getter method for returning the questions of the quiz
	 * 
	 * @return the questions ArrayList;
	 */
	public ArrayList<Question> getQuestions() {
		return questions;
	}

	/**
	 * Sets the questions of the quiz.
	 * 
	 * @param questions
	 *            the questions of the quiz.
	 */
	public void setQuestions(ArrayList<Question> questions) {
		this.questions = questions;
	}

	/**
	 * A getter method for returning the design of the quiz
	 * 
	 * @return the designConfig object.
	 */
	public DesignConfig getDesignConfig() {
		return designConfig;
	}

	/**
	 * Sets the design configuration of the quiz.
	 * 
	 * @param designConfig
	 *            the design configuration of the quiz.
	 */
	public void setDesignConfig(DesignConfig designConfig) {
		this.designConfig = designConfig;
	}

}
