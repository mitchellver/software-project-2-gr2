package com.softwave.core.container.quiz;

import com.softwave.core.util.Date;

/**
 * This class will hold the information for every answer that is linked to a
 * specific question
 *
 * @author sofian
 * 
 */

public class Answer {
	/** the id that uniquely defines the answer in the database */
	private long answerID = -1;
	/**
	 * the id that identifies the participation of the user connected to this
	 * answer
	 */
	private long participationID;
	/** the question id to know to which question the answer belongs */
	private long questionID;
	/** the answer string of a specific question */
	private String answer;
	/** was the answer correct ? optional */
	private boolean isCorrectAnswer;
	/** the moment the user loaded the question */
	private Date startTime;
	/** the moment the user submitted or left the questions */
	private Date endTime;

	/** an empty constructor to use in the DAO */
	public Answer() {
	}

	/**
	 * a constructor that takes in a questionID, used in the quizzing
	 * application.
	 * 
	 * @param questionID
	 *            the ID of the question this answer will be linked to.
	 */
	public Answer(long questionID) {
		this.questionID = questionID;
		this.startTime = new Date();
	}

	/**
	 * The main constructor of the Answer class.
	 * 
	 * @param questionID
	 *            the id of the question where the answer is put
	 * @param answerID
	 *            the id of the answer that has been filled in
	 * @param answer
	 *            the content of the answer itself
	 */
	public Answer(long questionID, long answerID, String answer) {
		super();
		this.questionID = questionID;
		this.answerID = answerID;
		this.answer = answer;
	}

	/**
	 * A getter method for returning the answerID object of the class.
	 * 
	 * @return the answerID object.
	 */
	public long getAnswerID() {
		return answerID;
	}

	/**
	 * Sets the id of the answer to which an answer was put.
	 * 
	 * @param answerID
	 *            the id of the answer.
	 */
	public void setAnswerID(long answerID) {
		this.answerID = answerID;
	}

	/**
	 * A getter method for returning the participationID object of the class.
	 * 
	 * @return the participationID object.
	 */
	public long getParticipationID() {
		return participationID;
	}

	/**
	 * Sets the id of a specific participation to a quiz
	 * 
	 * @param participationID
	 *            the id of a participation
	 */
	public void setParticipationID(long participationID) {
		this.participationID = participationID;
	}

	/**
	 * A getter method for returning the questionID object of the class.
	 * 
	 * @return the questionID object.
	 */
	public long getQuestionID() {
		return questionID;
	}

	/**
	 * Sets the question id of the question to which the answer belongs
	 * 
	 * @param questionID
	 *            the id of the question
	 */
	public void setQuestionID(long questionID) {
		this.questionID = questionID;
	}

	/**
	 * A getter method for returning the answer object of the class.
	 * 
	 * @return the answer object.
	 */
	public String getAnswer() {
		return answer;
	}

	/**
	 * Sets the answer
	 * 
	 * @param answer
	 *            the answer of a specific question
	 */
	public void setAnswer(String answer) {
		this.answer = answer;
	}

	/**
	 * A getter method for returning true or false if the answer put by the
	 * quizzer is correct or not.
	 * 
	 * @return true or false depending on the answer.
	 */
	public boolean isCorrectAnswer() {
		return isCorrectAnswer;
	}

	/**
	 * Sets the result of a specific answer to a specific question.
	 * 
	 * @param isCorrectAnswer
	 *            the result of the answer given by the quizzer.
	 */
	public void setCorrectAnswer(boolean isCorrectAnswer) {
		this.isCorrectAnswer = isCorrectAnswer;
	}

	/**
	 * A getter method for returning the startTime object of the class.
	 * 
	 * @return the startTime object.
	 */
	public Date getStartTime() {
		return startTime;
	}

	/**
	 * Sets the time of when the quiz started.
	 * 
	 * @param startTime
	 *            the starting time of the quiz.
	 */
	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	/**
	 * A getter method for returning the endTime object of the class.
	 * 
	 * @return the endTime object.
	 */
	public Date getEndTime() {
		return endTime;
	}

	/**
	 * Sets the time of when the quiz ended.
	 * 
	 * @param endTime
	 *            the ending time of the quiz.
	 */
	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	/** The hashCode of the Answer class. */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((answer == null) ? 0 : answer.hashCode());
		result = prime * result + (int) (answerID ^ (answerID >>> 32));
		result = prime * result + ((endTime == null) ? 0 : endTime.hashCode());
		result = prime * result + (isCorrectAnswer ? 1231 : 1237);
		result = prime * result + (int) (participationID ^ (participationID >>> 32));
		result = prime * result + (int) (questionID ^ (questionID >>> 32));
		result = prime * result + ((startTime == null) ? 0 : startTime.hashCode());
		return result;
	}

	/** The equals method of the Answer class. */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Answer other = (Answer) obj;
		if (answer == null) {
			if (other.answer != null)
				return false;
		} else if (!answer.equals(other.answer))
			return false;
		if (answerID != other.answerID)
			return false;
		if (endTime == null) {
			if (other.endTime != null)
				return false;
		} else if (!endTime.equals(other.endTime))
			return false;
		if (isCorrectAnswer != other.isCorrectAnswer)
			return false;
		if (participationID != other.participationID)
			return false;
		if (questionID != other.questionID)
			return false;
		if (startTime == null) {
			if (other.startTime != null)
				return false;
		} else if (!startTime.equals(other.startTime))
			return false;
		return true;
	}

	/** The toString method of the class Answer. */
	@Override
	public String toString() {
		return "Answer [answerID=" + answerID + ", participationID=" + participationID + ", questionID=" + questionID
				+ ", answer=" + answer + ", isCorrectAnswer=" + isCorrectAnswer + ", startTime=" + startTime
				+ ", endTime=" + endTime + "]";
	}

}
