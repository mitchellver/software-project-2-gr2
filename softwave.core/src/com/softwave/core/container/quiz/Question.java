package com.softwave.core.container.quiz;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import com.softwave.core.container.design.DesignConfig;
import com.softwave.core.util.Language;
import com.softwave.core.util.Translator;

/**
 * This class holds the information of every question
 *
 * @author sofian
 * 
 */
public class Question  implements Serializable{

	/** the id of the question */
	protected long questionID = -1;

	/** the id of the quiz this question belongs to */
	protected long quizID = -1;

	/**
	 * the number this question is in the quiz aka the position, like
	 * "the second question of this quiz"
	 */
	protected int questionNumber;

	/** the question itself */
	protected String name;

	/** the type of the question */
	protected QType type;
	
	/** The list of possible answers that can be applied to this question */
	protected List<String> possibleAnswers;

	/** the correct answer of the question */
	protected String correctAnswer;

	/** the limit of time to answer the question in seconds */
	protected int time;

	/** the pictures that has been used for the question */
	protected ArrayList<QFile> pictures;

	/** the design configuration for the question */
	protected DesignConfig designConfig;

	/** is the question optional or must it need an answer ? */
	protected boolean needsAnswer;

	/** the answer of a multiplechoice question */
	protected int tickAnswers;

	/** the language of the question */
	protected Language questionLang;

	/**
	 * an empty constructor for the DAO
	 * 
	 */
	public Question() {
		this.possibleAnswers = new ArrayList<>();
	}

	/**
	 * The constructor of the Question class. Only the name and type are
	 * minimally required to create a question.
	 * 
	 * @param name
	 *            the question itself
	 * @param type
	 *            the type of the question
	 */
	public Question(String name, QType type, long quizID) {
		super();
		this.name = name;
		this.type = type;
		this.quizID = quizID;
		this.needsAnswer = true;
		this.possibleAnswers = new ArrayList<>();
		this.questionLang = Language.ENGLISH;

	}
	
	public static Question newQuestion() {
		return new Question(Translator.translate("text.question.untitled"), QType.YES_NO, -1);
	}

	/**
	 * a simple method to add a picture
	 * 
	 * @param picture
	 *            a file of type QFile which contains the path
	 * @return true or false depending on the fulfillment.
	 */
	public boolean addPicture(QFile picture) {
		if (picture == null)
			return false;
		if (pictures == null)
			pictures = new ArrayList<QFile>();
		pictures.add(picture);
		return true;
	}
	
	/**
	 * a constructor that also takes in a question number
	 * 
	 * @param name
	 * @param type
	 * @param quizID
	 */
	public Question(String name, QType type, long quizID, int questionNumber) {
		super();
		if (name == null || name == "") {
			System.out.println("Question does not exist or is empty.");
			name = "Question does not exist or is empty";

		}
		this.name = name;
		this.type = type;
		this.quizID = quizID;
		this.needsAnswer = true;
		this.questionLang = Language.ENGLISH;
		this.questionNumber = questionNumber;
	}
	
	/**
	 * This method checks if the question needs an answer.
	 * 
	 * @return if the question needs an answer or not
	 */
	public boolean isNeedsAnswer() {
		return needsAnswer;
	}

	/**
	 * A getter method for returning the question id of the question.
	 * 
	 * @return the QuestionID object.
	 */
	public long getQuestionID() {
		return questionID;
	}

	/**
	 * Sets the question id of the question.
	 * 
	 * @param questionID
	 *            the id of the question.
	 */
	public void setQuestionID(long questionID) {
		this.questionID = questionID;
	}

	/**
	 * A getter method for returning the id of the quiz the question belongs to.
	 * 
	 * @return the quizID object.
	 */
	public long getQuizID() {
		return quizID;
	}

	/**
	 * Sets the id of the quiz the question belongs to.
	 * 
	 * @param quizID
	 *            the id of the quiz that belongs to the question.
	 */
	public void setQuizID(long quizID) {
		this.quizID = quizID;
	}

	/**
	 * a getter method to return the questionNumber
	 * 
	 * @return an int with the questionNumber
	 */
	public int getQuestionNumber() {
		return questionNumber;
	}

	/**
	 * Sets the questionNumber of a question.
	 * 
	 * @TODO checks if this position is still free, can put the question at the
	 *       end of the quiz by default
	 * @param questionNumber
	 */
	public void setQuestionNumber(int questionNumber) {
		// only positive numbers are allowed
		if (questionNumber < 0)
			return;
		// c
		this.questionNumber = questionNumber;
	}

	/**
	 * A getter method for returning the question itself.
	 * 
	 * @return the name object.
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the question itself.
	 * 
	 * @param name
	 *            the question itself.
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * A getter method for returning the type of the question.
	 * 
	 * @return the type object.
	 */
	public QType getType() {
		return type;
	}

	/**
	 * Sets the type of the question.
	 * 
	 * @param type
	 *            the type of the question.
	 */
	public void setType(QType type) {
		this.type = type;
	}

	/**
	 * A getter method for returning the correct answer of the question.
	 * 
	 * @return the correctAnswer object.
	 */
	public String getCorrectAnswer() {
		return correctAnswer;
	}

	/**
	 * Sets the correct answer of the question.
	 * 
	 * @param correctAnswer
	 *            the correct answer of the question.
	 */
	public void setCorrectAnswer(String correctAnswer) {
		this.correctAnswer = correctAnswer;
	}

	/**
	 * A getter method for returning the maximum amount of time to answer the
	 * question.
	 * 
	 * @return the time object.
	 */
	public int getTime() {
		return time;
	}

	/**
	 * Sets the maximum time of the question.
	 * 
	 * @param time
	 *            the maximum time of the question.
	 */
	public void setTime(int time) {
		this.time = time;
	}

	/**
	 * A getter method for returning the pictures of the question.
	 * 
	 * @return the pictures ArrayList.
	 */
	public ArrayList<QFile> getPictures() {
		return pictures;
	}

	/**
	 * Sets the pictures of the question.
	 * 
	 * @param pictures
	 *            the pictures of the question.
	 */
	public void setPictures(ArrayList<QFile> pictures) {
		this.pictures = pictures;
	}

	/**
	 * A getter method for returning the design configuration of the question.
	 * 
	 * @return the designConfig object.
	 */
	public DesignConfig getDesignConfig() {
		return designConfig;
	}

	/**
	 * Sets the design configuration of the question.
	 * 
	 * @param designConfig
	 *            the design configuration of the question.
	 */
	public void setDesignConfig(DesignConfig designConfig) {
		this.designConfig = designConfig;
	}

	/**
	 * Sets if the question needs an answer.
	 * 
	 * @param needsAnswer
	 *            does the question need an answer ?.
	 */
	public void setNeedsAnswer(boolean needsAnswer) {
		this.needsAnswer = needsAnswer;
	}

	/**
	 * A getter method for returning the correct answer of the multiple choice
	 * question.
	 * 
	 * @return the tickAnswers object.
	 */
	public int getTickAnswers() {
		return tickAnswers;
	}

	/**
	 * Sets the correct answer of a multiple choice question.
	 * 
	 * @param tickAnswer
	 *            the correct answer of a multiple choice question.
	 */
	public void setTickAnswers(int tickAnswers) {
		this.tickAnswers = tickAnswers;
	}

	/**
	 * A getter method for returning the language of the question.
	 * 
	 * @return the questionLang object.
	 */
	public Language getQuestionLang() {
		return questionLang;
	}

	/**
	 * Sets the language of the question.
	 * 
	 * @param questionLang
	 *            the language of the question.
	 */
	public void setQuestionLang(Language questionLang) {
		this.questionLang = questionLang;
	}
	
	/**
	 * Getter method for the list of possible answers for this question.
	 * 
	 * @return an HashSet with all the possible answers for the question.
	 */
	public List<String> getPossibleAnswers() {
		return this.possibleAnswers;
	}

	/**
	 * Add an non-existing item to the list of possible answers.
	 * 
	 * @param answer a unique answer to the list of possible answers.
	 */
	public void addPossibleAnswer(String answer) {
		if (possibleAnswers == null) {
			possibleAnswers = new ArrayList<>();
		}
		possibleAnswers.add(answer);
	}

	/**
	 * Add every answer from the collection to the list of possible answers.
	 * 
	 * @param answer a list of unique answers to the list of possible answers.
	 */
	public void addPossibleAnswer(ArrayList<String> answers) {
		if (possibleAnswers == null) {
			possibleAnswers = new ArrayList<>();
		}
		possibleAnswers.addAll(answers);
	}

	/**
	 * Add every answer from the collection to the list of possible answers.
	 * 
	 * @param answer a list of unique answers to the list of possible answers.
	 */
	public void addPossibleAnswer(List<String> answers) {
		if (possibleAnswers == null) {
			possibleAnswers = new ArrayList<>();
		}
		possibleAnswers.addAll(answers);
	}
	
	/**
	 * Remove an existing answer to the list of answers.
	 * 
	 * @param answer an answer that exists in the list.
	 */
	public void removePossibleAnswer(String answer) {
		if (possibleAnswers == null) {
			return;
		}
		possibleAnswers.remove(answer);
	}
	
	/**
	 * Remove every element in a given list from the list of possible answers.
	 * 
	 * @param answers a list of answers that need to be removed from the possible
	 * answers list.
	 */
	public void removePossibleAnswer(ArrayList<String> answers) {
		if (possibleAnswers == null) {
			return;
		}
		possibleAnswers.removeAll(answers);
	}
}
