package com.softwave.core.container.quiz;

import java.io.Serializable;

import com.softwave.core.util.Translator;

/**
 * QType specifies what type of question it is,
 * this class also support the toString method and will translate if needed.
 * 
 * @author nicolas
 */
public enum QType implements Serializable {
	YES_NO,
	MULTIPLE_CHOICE,
	OPEN,
	YES_NO_MEDIA,
	MULTIPLE_CHOICE_MEDIA,
	OPEN_MEDIA;
	
	/** The translation key for all the values in the enum */
	private final String TRANSLATE_ID = "qtype.name.";
	
	/**
	 * The toString method will return the name of the enum value and will translate
	 * that name if needed, based on the {@code TRANSLATE_ID}.
	 * 
	 * @return a translated string as the name of the enum value.
	 */
	@Override
	public String toString() {
		return Translator.translate(TRANSLATE_ID + this.name().toLowerCase());
	}
}