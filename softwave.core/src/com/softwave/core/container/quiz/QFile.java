package com.softwave.core.container.quiz;

import java.io.Serializable;

/**
 * This is a container class to store the path of a picture of 
 * @author eddi
 *
 */
public class QFile implements Serializable{

	protected long qfileID = -1;
	protected long questionID;
	protected String path;
	
	/**
	 * a non-argument constructor needed by the DAO
	 * 
	 */
	public QFile(){}
	/**
	 * a constructor that takes in a path
	 * @param path
	 */
	public QFile(String path){
		this.path = path;
	}
	
	public long getQuestionID() {
		return questionID;
	}
	public long getQfileID() {
		return qfileID;
	}
	public void setQfileID(long qfileID) {
		this.qfileID = qfileID;
	}
	public void setQuestionID(long questionID) {
		this.questionID = questionID;
	}
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((path == null) ? 0 : path.hashCode());
		result = prime * result + (int) (qfileID ^ (qfileID >>> 32));
		result = prime * result + (int) (questionID ^ (questionID >>> 32));
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		QFile other = (QFile) obj;
		if (path == null) {
			if (other.path != null)
				return false;
		} else if (!path.equals(other.path))
			return false;
		if (qfileID != other.qfileID)
			return false;
		if (questionID != other.questionID)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "QFile [qfileID=" + qfileID + ", questionID=" + questionID + ", path=" + path + "]";
	}

	
}