package com.softwave.core.container.user;

/**
 * An application created with as purpose to create or fill in a quiz. The class
 * Address is used to register and create an object Address for each user that
 * will be registered in the database.
 * 
 * @author adeel
 */

public class Address {

	/**
	 * The class address has a couple of private data members. 5 String's and 1
	 * Integer and 1 long that complete the class Address. The class Address
	 * contains Strings street, nr, poBox, city, country, long addressID and an
	 * Integer zipcode.
	 */

	// Data members
	private long addressID = -1;
	private String street;
	private String nr;
	private String poBox;
	private int zipcode;
	private String city;
	private String country;

	/**
	 * A non-argument constructor needed to work with DAO
	 */
	public Address() {
	}

	/**
	 * The getter getAddressID() is used to have access to the private data
	 * member AddressID of the class Address.
	 * 
	 * @return The getter returns the private data member AddressID.
	 */

	public long getAddressID() {
		return addressID;
	}

	/**
	 * The setter setAddressID() is used to set an AddressID for the user which
	 * is compulsory and needed in the database.
	 * 
	 * @param addressID
	 *            The setter sets the addressID for the object address.
	 */

	public void setAddressID(long addressID) {
		if (addressID == 0) {
			return;
		}

		this.addressID = addressID;
	}

	/**
	 * The getter getStreet() is used to have access to the private data member
	 * Street of the class Address.
	 * 
	 * @return The getter returns the private data member street.
	 */

	public String getStreet() {
		return street;
	}

	/**
	 * The setter setStreet() is used to set a street for the user.
	 * 
	 * @param street
	 *            The setter sets the street name for the object address.
	 */

	public void setStreet(String street) {
		if (street == null) {
			return;
		}
		this.street = street;
	}

	/**
	 * The getter getNr() is used to have access to the private data member nr
	 * of the class Address.
	 * 
	 * @return The getter returns the private data member nr.
	 */

	public String getNr() {
		return nr;
	}

	/**
	 * The setter setNr() is used to set a street nr for the user.
	 * 
	 * @param nr
	 *            The setter sets the street nr for the class/object address.
	 */

	public void setNr(String nr) {
		if (nr == null) {
			return;
		}
		this.nr = nr;
	}

	/**
	 * The getter getPoBox() is used to have access to the private data member
	 * PoBox of the class Address.
	 * 
	 * @return The getter returns the private data member poBox.
	 */

	public String getPoBox() {
		return poBox;
	}

	/**
	 * The setter setPoBox() is used to set a post box for the user.
	 * 
	 * @param poBox
	 *            The setter sets the post box for the class/object address.
	 */

	public void setPoBox(String poBox) {
		if (poBox == null) {
			return;
		}
		this.poBox = poBox;
	}

	/**
	 * The getter getZipcode() is used to have access to the private data member
	 * zipcode of the class Address.
	 * 
	 * @return The getter returns the private data member zipcode.
	 */

	public int getZipcode() {
		return zipcode;
	}

	/**
	 * The setter setZipcode() is used to set a zipcode for the user.
	 * 
	 * @param zipcode
	 *            The setter sets the zipcode code for the class/object address.
	 */

	public void setZipcode(int zipcode) {
		if (zipcode == 0) {
			return;
		}
		this.zipcode = zipcode;
	}

	/**
	 * The getter getCity() is used to have access to the private data member
	 * city of the class Address.
	 * 
	 * @return The getter returns the private data member city.
	 */

	public String getCity() {
		return city;
	}

	/**
	 * The setter setCity() is used to set a city for the user.
	 * 
	 * @param city
	 *            The setter sets the city for the class/object address.
	 */

	public void setCity(String city) {
		if (city == null) {
			return;
		}
		this.city = city;
	}

	/**
	 * The getter getCountry() is used to have access to the private data member
	 * country of the class Address.
	 * 
	 * @return The getter returns the private data member country.
	 */

	public String getCountry() {
		return country;
	}

	/**
	 * The setter setCountry() is used to set a country for the user.
	 * 
	 * @param country
	 *            The setter sets the country for the class/object address.
	 */

	public void setCountry(String country) {
		if (country == null) {
			return;
		}
		this.country = country;
	}
	


	/**
	 * This class has a total of two constructors. This is the main constructors
	 * which contains all of the private data members declared in this class as
	 * parameters of this constructor.
	 * 
	 * 
	 * 
	 * @param street
	 *            Sets the street for the user.
	 * @param nr
	 *            Sets the house number for the user.
	 * @param poBox
	 *            Sets the post box for the user house.
	 * @param postal
	 *            Sets the postal code for the user's address.
	 * @param city
	 *            Sets the city for the user's address.
	 * @param country
	 *            Sets the country name of where the user is living. 
	 * @param active 
	 *            Sets the active for the user
	 */

	public Address(String street, String nr, String poBox, int postal, String city, String country) {
		super();

		this.street = street;
		this.nr = nr;
		this.poBox = poBox;
		this.zipcode = zipcode;
		this.city = city;
		this.country = country;
	}

	/**
	 * The second constructor that can be found below does not contain the data
	 * member poBox in case there is no need for it. It sets all of the private
	 * data members except poBox.
	 * 
	 * @param street
	 *            Sets the street name for the user.
	 * @param nr
	 *            Sets the house number for the user.
	 * @param postal
	 *            Sets the postal code for the user.
	 * @param city
	 *            Sets the city for the user.
	 * @param country
	 *            Sets the country for the user.
	 * @param active
	 *            Sets the active for the user
	 */

	public Address(String street, String nr, int zipcode, String city, String country) {
		super();

		this.street = street;
		this.nr = nr;
		this.zipcode = zipcode;
		this.city = city;
		this.country = country;
	}

	/**
	 * The toString method is used to print out all of the information/data of
	 * the class Address.
	 * 
	 * @return prints all of the data members of this particular class.
	 */

	@Override
	public String toString() {
		return "Address: \n\n street= " + street + "\n nr= " + nr + "\n poBox= " + poBox + "\n zipcode= " + zipcode
				+ "\n city= " + city + "\n country= " + country;
	}

	/**
	 * The hashcode method allows algorithms and data structures to put objects
	 * into compartements. Objects that are equal must have the same hash code
	 * within a running process.
	 * 
	 * @return result The hashcode returns values that can be used to compare
	 *         objects.
	 */

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((nr == null) ? 0 : nr.hashCode());
		result = prime * result + ((street == null) ? 0 : street.hashCode());
		return result;
	}

	/**
	 * The equals method is used to make equal comparison between two objects.
	 * 
	 * @return boolean The function returns a true or false based on the result.
	 *         If there is a match of objects it will most likely return a true
	 *         where as if it is not a match a false will be returned.
	 */

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Address other = (Address) obj;
		if (nr == null) {
			if (other.getNr() != null)
				return false;
		} else if (!nr.equals(other.getNr()))
			return false;
		if (street == null) {
			if (other.street != null)
				return false;
		} else if (!street.equals(other.getStreet()))
			return false;
		return true;
	}

}
