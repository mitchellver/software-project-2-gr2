package com.softwave.core.container.user;

/**
 * an application with as purpose to create or fill in a quiz. The class Mail is
 * used to register a user's mail.
 * 
 * @author adeel
 *
 */

public class Mail {

	/**
	 * The data member(s) that will be used in this class: String email. The
	 * data member string email will save a user's email as an object Mail.
	 */

	private String email;

	/**
	 * The getter getEmail() will allow us to have access to the private data
	 * member String email. The method will return a String email which can be
	 * called by using the getEmail() method.
	 * 
	 * @return String email
	 */

	public String getEmail() {
		return email;
	}

	/**
	 * The setEmail() is mainly used to set an email address of the user. It
	 * contains a String email as parameter which is used to set the String
	 * address.
	 * 
	 * @param email
	 */

	public void setEmail(String email) {

		if (email == null) {
			return;
		}

		this.email = email;
	}

	/**
	 * The constructor of the class Mail is used to set a String email address
	 * and at the same time create an object Mail which can further be used in
	 * the code. As parameter the constructor takes in a String email.
	 * 
	 * @param email
	 */

	public Mail(String email) {
		super();

		if (email == null) {
			return;
		}
		this.email = email;
	}

	/**
	 * Method used to verify if a valid email has been entered. This method will
	 * check if all the necessary characters have been given in order to meet
	 * the correct email format.
	 * 
	 * @param email
	 *            The method takes in a String email.
	 * @return It returns a boolean which can be true in case the String email
	 *         format is correct and fails in case the email address has not
	 *         been entered correctly.
	 */

	public boolean verifyEmail(String email) {

		// Regular Expression (regex) to check email with all the different
		// characters.

		String email_check = "^[\\w-_\\.+]*[\\w-_\\.]\\@([\\w]+\\.)+[\\w]+[\\w]$";
		boolean b;

		if (b = email.matches(email_check)) {
			// If the email matches the "email_check" regex a true will be
			// returned.
			System.out.println("e-mail: " + email + " -> Valid = " + b);
		} else {
			// If the email does not match the "email_chech" regex a false will
			// be returned
			System.out.println("e-mail: " + email + " -> Not Valid = " + b);
		}
		return b;

	}

	/**
	 * The hashcode method allows algorithms and data structures to put objects
	 * into compartements. Objects that are equal must have the same hash code
	 * within a running process.
	 * 
	 * @return result The hashcode returns values that can be used to compare
	 *         objects.
	 */

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		return result;
	}

	/**
	 * The equals method is used to make equal comparison between two objects.
	 * 
	 * @return boolean The function returns a true or false based on the result.
	 *         If there is a match of objects it will most likely return a true
	 *         where as if it is not a match a false will be returned.
	 */

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Mail other = (Mail) obj;
		if (email == null) {
			if (other.getEmail() != null)
				return false;
		} else if (!email.equals(other.getEmail()))
			return false;
		return true;
	};
	
}

