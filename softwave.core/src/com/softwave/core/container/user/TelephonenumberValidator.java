package com.softwave.core.container.user;

/**
 * This class wil validate the telephone number of the user.
 * 
 * @author Turgay
 *
 */

public class TelephonenumberValidator extends Number {

	/** The default constructor of the TelephonenumberValidator class. */
	public TelephonenumberValidator() {
		super();
	}

	/**
	 * The second constructor of the TelephonenumberValidator class.
	 * 
	 * @param number
	 */
	public TelephonenumberValidator(String number) {
		super(number);
		if(number == null)
		{
			return;
		}
	}

	/**
	 * Start of the regular expressions to validate the number.
	 * 
	 * @param number
	 * @return the number object.
	 */
	public static boolean validateTelephonenumber(String number) {

		/*
		 * validate phone numbers of format "1234567890" or INT "0032123456789"
		 * max=15
		 */
		if (number.matches("\\d{15}"))
			return true;

		/*
		 * validating phone number where country code (including the dialing
		 * code e.g. 00 for Belgium) is in braces ()
		 */
		else if (number.matches("\\(\\d{3}\\)-\\d{3}-\\d{4}"))
			return true;

		/* validating phone number with -, . or spaces */
		else if (number.matches("\\d{3}[-\\.\\s]\\d{3}[-\\.\\s]\\d{4}"))
			return true;

		/* validating phone number with extension length from 3 to 5 */
		else if (number.matches("\\d{2}-\\d{2}-\\d{4}\\s(x|(ext))\\d{2,5}"))
			return true;

		/* if nothing matches then return false */
		else
			return false;
	}
}