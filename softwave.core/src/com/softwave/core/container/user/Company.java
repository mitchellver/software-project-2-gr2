package com.softwave.core.container.user;

/**
 * an application with as purpose to create or fill in a quiz. The class Company
 * is used to register a company.
 * 
 * @author adeel
 *
 */

public class Company {

	/**
	 * The class Company consists of a private String name and String
	 * accountSuffix. These two private data members will be needed in order to
	 * create an object Company.
	 */

	private long companyID = -1;
	private String name;
	private String accountSuffix;

	/**
	 * The getter getCompanyID() will allow us to have access to the private
	 * data member long CompanyID. The method will return a long CompanyID which
	 * can be called by using the getCompanyID() method.
	 * 
	 * @return String name
	 */

	public long getCompanyID() {
		return companyID;
	}

	/**
	 * The setCompanyID() is mainly used to set a companyID for the company. It
	 * contains a long companyID name as parameter which is used to set the long
	 * companyID.
	 * 
	 * @param companyID
	 */

	public void setCompanyID(long companyID) {
		if (companyID == 0) {
			return;
		}
		this.companyID = companyID;
	}

	/**
	 * The getter getName() will allow us to have access to the private data
	 * member String name. The method will return a String name which can be
	 * called by using the getName() method.
	 * 
	 * @return String name
	 */

	public String getName() {
		return name;
	}

	/**
	 * The setName() is mainly used to set a name for the company. It contains a
	 * String name as parameter which is used to set the String name.
	 * 
	 * @param name
	 */

	public void setName(String name) {
		if (name == null) {
			return;
		}
		this.name = name;
	}

	/**
	 * The getter getAccountSuffix() will allow us to have access to the private
	 * data member String accountSuffix. The method will return a String
	 * accountSuffix which can be called by using the getAccountSuffix() method.
	 * 
	 * @return String accountSuffix.
	 */

	public String getAccountSuffix() {
		return accountSuffix;
	}

	/**
	 * The setAccountSuffix() is mainly used to set an accountSuffix for the
	 * company. It contains a String accountSuffix as parameter which is used to
	 * set the String.
	 * 
	 * @param accountSuffix
	 */

	public void setAccountSuffix(String accountSuffix) {
		if (accountSuffix == null) {
			return;
		}
		this.accountSuffix = accountSuffix;
	}

	/**
	 * The constructor of the class Company is used to set a String name, String
	 * accountSuffix and at the same time create an object Company which can
	 * further be used in the code. As parameter the constructor takes in a
	 * String name and String accountSuffix.
	 * 
	 * @param name
	 * @param accountSuffix
	 */

	public Company(String name, String accountSuffix) {
		super();

		if (name == null || accountSuffix == null) {
			return;
		}
		
		this.name = name;
		this.accountSuffix = accountSuffix;
	}

	/**
	 * The toString method is simply used to print out all the information/data
	 * of a class.
	 * 
	 * @return String
	 */

	@Override
	public String toString() {
		return "Company: \n\n name= " + name + "\n accountSuffix= " + accountSuffix;
	}

	/**
	 * The hashcode method allows algorithms and data structures to put objects
	 * into compartements. Objects that are equal must have the same hash code
	 * within a running process.
	 * 
	 * @return result The hashcode returns values that can be used to compare
	 *         objects.
	 */

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (companyID ^ (companyID >>> 32));
		return result;
	}

	/**
	 * The equals method is used to make equal comparison between two objects.
	 * 
	 * @return boolean The function returns a true or false based on the result.
	 *         If there is a match of objects it will most likely return a true
	 *         where as if it is not a match a false will be returned.
	 */

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Company other = (Company) obj;
		if (companyID != other.getCompanyID())
			return false;
		return true;
	}

}
