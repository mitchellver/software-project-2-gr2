package com.softwave.core.container.user;

/**
 * This class will hold the phone number of a user.
 * 
 * @author Turgay
 *
 */

public class Number {
 
	/** the number of a user*/
	private String number;

	/** The default constructor of the Number class. */
	public Number() {
		super();
	}
	
	/**
	 * The second constructor of the Number class. The number 
	 * is required to know the user.
	 * 
	 * @param number
	 */
	public Number(String number) {
		super();
		if(number == null)
		{
			return;
		}
		this.number = number;
	}
	
	/**
	 * A getter method for returning the number of the user.
	 * 
	 * @return the number object.
	 */
	public String getNumber() {
		return number;
	}

	/**
	 * Sets the number of the user.
	 * 
	 * @param number
	 */
	public void setNumber(String number) {
		if(number == null)
		{
			return;
		}
		this.number = number;
	}
	
	/** The hashCode of the Number class. */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((number == null) ? 0 : number.hashCode());
		return result;
	}
	
	/** The equals method of the Number class. */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Number other = (Number) obj;
		if (number == null) {
			if (other.number != null)
				return false;
		} else if (!number.equals(other.number))
			return false;
		return true;
	}
	
	/** The toString method of the Number class. */
	@Override
	public String toString() {
	return "Number [number=" + number + "]";
	}
}