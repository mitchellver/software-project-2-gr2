package com.softwave.core.container.user.account.userexceptions;

/**
 * this exception can be thrown when the MapUserTableToUser method failed.
 * Or when it returns false
 * @author eddi
 *
 */
public class MapUserTableToUserFailedException extends Exception {
	public MapUserTableToUserFailedException(){
		super("The Mapping of the userTable to a user failed");
	}
	public MapUserTableToUserFailedException(String message){
		super(message);
	}
}