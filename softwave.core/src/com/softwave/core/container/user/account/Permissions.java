package com.softwave.core.container.user.account;

public enum Permissions {
	USER_CREATE,
	USER_REMOVE,
	USER_PURGE,
	QUIZ_CREATE,
	QUIZ_REMOVE,
	QUIZ_PURGE
}
