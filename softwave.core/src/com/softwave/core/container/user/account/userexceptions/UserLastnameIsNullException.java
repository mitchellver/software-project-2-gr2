package com.softwave.core.container.user.account.userexceptions;

/**
 * This exceptions is thrown when the user's lastname is null.
 * @author eddi
 *
 */
public class UserLastnameIsNullException extends Exception {
	public UserLastnameIsNullException(){
		super("The user's firstname is null");
	}
	public UserLastnameIsNullException(String message){
		super(message);
	}
	
}