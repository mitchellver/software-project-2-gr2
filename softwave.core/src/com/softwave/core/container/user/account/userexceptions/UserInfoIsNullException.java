package com.softwave.core.container.user.account.userexceptions;

/**
 * This exceptions is thrown when the userinfo is null.
 * @author eddi
 *
 */
public class UserInfoIsNullException extends Exception {
	public UserInfoIsNullException(){
		super("The userInfo is null");
	}
	public UserInfoIsNullException(String message){
		super(message);
	}
}