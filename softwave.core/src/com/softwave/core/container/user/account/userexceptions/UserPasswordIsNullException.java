package com.softwave.core.container.user.account.userexceptions;

/**
 * This exceptions is throw when a user's password is null.
 * 
 * @author Turgay
 *
 */
public class UserPasswordIsNullException extends Exception{
	public UserPasswordIsNullException(){
		super("The user's password is null");
	}
	public UserPasswordIsNullException(String message){
		super(message);
	}

}