package com.softwave.core.container.user.account;

/**
 * This is the Anonymous user, this user has no extra information
 * 
 * @author Turgay and Eddi
 *
 */

public class AnonUser extends User{
	//this user has no information, hence anonymous.
}
