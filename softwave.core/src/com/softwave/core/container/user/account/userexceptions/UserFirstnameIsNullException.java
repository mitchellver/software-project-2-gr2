package com.softwave.core.container.user.account.userexceptions;

/**
 * This exceptions is thrown when the user's firstname is null.
 * @author eddi
 *
 */
public class UserFirstnameIsNullException extends Exception {
	public UserFirstnameIsNullException(){
		super("The user's firstname is null");
	}
	public UserFirstnameIsNullException(String message){
		super(message);
	}
	
}