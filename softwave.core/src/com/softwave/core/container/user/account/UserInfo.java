package com.softwave.core.container.user.account;

import com.softwave.core.container.user.Address;
import com.softwave.core.container.user.Mail;
import com.softwave.core.container.user.Number;

/**
 * This is the class that keeps all extra userinfo, like name and address. Some
 * users don't have this object, like the anonymous quizzer
 * 
 * @author Turgay and Eddi
 * 
 */

public class UserInfo {
	
	/** the email of the user */
	private Mail email;
	
	/** the number of the user */
	private Number phonenumber;
	
	/** the address of the user */
	private Address address;
	
	/** the first name of the user */
	private String firstName;
	
	/** the last name of the user */
	private String lastName;
	
	/** the company of the user */
	//private Company company; 
	//Er moet nog een DAO geschreven worden hiervoor!
	
	/**
	 * a non-argument constructor used by the DAO
	 */
	public UserInfo(){}
	
	/**
	 * The second constructor of the UserInfo class. The first name and last name 
	 * are required to create a user.
	 * 
	 * @param firstName
	 * @param lastName
	 */
	public UserInfo(String firstName, String lastName) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
	}
	
	/**
	 * The third constructor of the UserInfo class. The phone number, email, address,
	 * first name and last name are required to create a user.
	 * 
	 * @param phonenumber
	 * @param email
	 * @param address
	 * @param firstName
	 * @param lastName
	 */
	public UserInfo(Mail email, Number phonenumber, Address address, String firstName, String lastName) {
		super();
		this.email = email;
		this.phonenumber = phonenumber;
		this.address = address;
		this.firstName = firstName;
		this.lastName = lastName;
	}

	/**
	 * A getter method for returning the phone number of the user.
	 * 
	 * @return the phonenumber object.
	 */
	public Number getPhonenumber() {
		return phonenumber;
	}
	
	/**
	 * Sets the phone number of the user.
	 * 
	 * @param phonenumber
	 */
	public void setPhonenumber(Number phonenumber) {
		this.phonenumber = phonenumber;
	}
	
	/**
	 * A getter method for returning the address of the user.
	 * 
	 * @return the address object.
	 */
	public Address getAddress() {
		return address;
	}
	
	/**
	 * Sets the address of the user.
	 * 
	 * @param address
	 */
	public void setAddress(Address address) {
		this.address = address;
	}
	
	/**
	 * A getter method for returning the first name of the user.
	 * 
	 * @return the firstName object.
	 */
	public String getFirstName() {
		return firstName;
	}
	
	/**
	 * Sets the first name of the user.
	 * 
	 * @param firstName
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	/**
	 * A getter method for returning the last name of the user.
	 * 
	 * @return the lastname object.
	 */
	public String getLastName() {
		return lastName;
	}
	
	/**
	 * Sets the last name of the user.
	 * 
	 * @param lastName
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	/**
	 * A getter method for returning the email of the user.
	 * 
	 * @return the email object.
	 */
	public Mail getEmail() {
		return email;
	}
	
	/**
	 * Sets the email of the user.
	 * 
	 * @param email
	 */
	public void setEmail(Mail email) {
		this.email = email;
	}
	
	/** The hashCode of the UserInfo class. */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((address == null) ? 0 : address.hashCode());
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result + ((firstName == null) ? 0 : firstName.hashCode());
		result = prime * result + ((lastName == null) ? 0 : lastName.hashCode());
		result = prime * result + ((phonenumber == null) ? 0 : phonenumber.hashCode());
		return result;
	}
	
	/** The equals method of the UserInfo class. */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UserInfo other = (UserInfo) obj;
		if (address == null) {
			if (other.address != null)
				return false;
		} else if (!address.equals(other.address))
			return false;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		if (firstName == null) {
			if (other.firstName != null)
				return false;
		} else if (!firstName.equals(other.firstName))
			return false;
		if (lastName == null) {
			if (other.lastName != null)
				return false;
		} else if (!lastName.equals(other.lastName))
			return false;
		if (phonenumber == null) {
			if (other.phonenumber != null)
				return false;
		} else if (!phonenumber.equals(other.phonenumber))
			return false;
		return true;
	}
	
	/** The toString method of the UserInfo class. */
	@Override
	public String toString() {
		return "UserInfo [email=" + email + ", phonenumber=" + phonenumber + ", address=" + address + ", firstName="
				+ firstName + ", lastName=" + lastName + "]";
	}
}