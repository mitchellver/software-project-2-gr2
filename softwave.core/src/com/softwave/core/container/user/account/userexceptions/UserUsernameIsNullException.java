package com.softwave.core.container.user.account.userexceptions;

/**
 * This exceptions is thrown when a user's username is null.
 * 
 * @author eddi
 *
 */
public class UserUsernameIsNullException extends Exception {
	public UserUsernameIsNullException(){
		super("The user's email is null");
	}
	public UserUsernameIsNullException(String message){
		super(message);
	}
}