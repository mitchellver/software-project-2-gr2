package com.softwave.core.container.user.account;

import com.softwave.core.util.Date;

/**
 * This class will hold the information of a user by creating a user in the
 * database and linking the necessary information to retrieve all information
 * about the user itself.
 * 
 * @author Turgay & mitchell
 * 
 */

public abstract class User {

	/** the id of the user */
	private long userID = -1;

	/** the info about the user */
	private UserInfo userInfo;

	/** the create time of an user */
	private Date createTime;

	/** the id of the user how created this user */
	private long createID = -1;

	/** The non-argument constructor of the User class to create an AnonUser. */
	public User() {
	}

	/**
	 * The constructor of the User class. that takes in UserInfo, user for a
	 * KnownUser.
	 * 
	 * @param userInfo
	 */
	public User(UserInfo userInfo) {
		super();
		if (userInfo == null) {
			return;
		}
		this.userInfo = userInfo;
	}

	/**
	 * A getter method for returning the user id of the user.
	 * 
	 * @return the userID object.
	 */
	public long getUserID() {
		return userID;
	}

	/**
	 * Sets the user id of the user.
	 * 
	 * @param userID
	 */
	public void setUserID(long userID) {
		this.userID = userID;
	}

	/**
	 * A getter method for returning the user info of the user.
	 * 
	 * @return the UserInfo object.
	 */
	public UserInfo getUserInfo() {
		return userInfo;
	}

	/**
	 * Sets the user info of the user with try catch exception for user info is
	 * null.
	 * 
	 * @param userInfo
	 */
	public void setUserInfo(UserInfo userInfo) {
		if (userInfo == null) {
			return;
		}
		this.userInfo = userInfo;
	}

	/**
	 * A getter method for returning the creation date of the user.
	 * 
	 * @return the CreateTime object.
	 */
	public Date getCreateTime() {
		return createTime;
	}

	/**
	 * Sets the creation time of the user
	 * 
	 * @param createTime
	 */
	public void setCreateTime(Date createTime) {
		if (createTime == null) {
			return;
		}
		this.createTime = createTime;
	}

	/**
	 * add the user info of the user
	 * 
	 * @param userInfo
	 */
	public void addInfo(UserInfo userInfo) {
		if (userInfo != null) {
			this.userInfo = userInfo;
		}
	}

	/**
	 * A getter method for returning the user id of how created this user
	 * 
	 * @return the createID object.
	 */
	public long getcreateID() {
		return createID;
	}

	/**
	 * Sets the createID of the user
	 * 
	 * @param userID
	 */
	public void setcreateID(long createID) {
		this.createID = createID;
	}

	/** The hashCode of the User class. */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (createID ^ (createID >>> 32));
		result = prime * result + ((createTime == null) ? 0 : createTime.hashCode());
		result = prime * result + (int) (userID ^ (userID >>> 32));
		result = prime * result + ((userInfo == null) ? 0 : userInfo.hashCode());
		return result;
	}

	/** The equals method of the User class. */

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		User other = (User) obj;
		if (createID != other.createID)
			return false;
		if (createTime == null) {
			if (other.createTime != null)
				return false;
		} else if (!createTime.equals(other.createTime))
			return false;
		if (userID != other.userID)
			return false;
		if (userInfo == null) {
			if (other.userInfo != null)
				return false;
		} else if (!userInfo.equals(other.userInfo))
			return false;
		return true;
	}

	/** The toString method of the User class. */

	@Override
	public String toString() {
		return "User [userID=" + userID + ", userInfo=" + userInfo + ", createTime=" + createTime + ", createID="
				+ createID + "]";
	}

}
