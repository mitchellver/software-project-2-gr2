package com.softwave.core.container.user.account;

import com.softwave.core.security.HashString;

/**
 * this is the client user, this type of user has a username to login 
 * to the application and perform certain functions.
 * 
 * @author Turgay and Eddi
 *
 */

public class Client extends KnownUser {

	/**
	 * The constructor of the client class is from the superclass.
	 * 
	 * @param username
	 * @param password
	 */
	public Client(String username, HashString password) {
		super(username, password);
		if(username == null || password == null)
		{
			return;
		}
		// TODO Auto-generated constructor stub
	}
}
