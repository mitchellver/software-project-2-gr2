package com.softwave.core.container.user.account;

import java.util.ArrayList;

import com.softwave.core.container.design.DesignConfig;
import com.softwave.core.security.HashString;

/**
 * this is the known user, this type of user has a password and username to
 * login to the application and perform certain functions
 * 
 * @author Turgay and Eddi
 * 
 */

public abstract class KnownUser extends User {
	
	/** the username of the user*/
	private String username;
	
	/** the hashed password of a user*/
	private HashString password;
	
	/** the design configuration for the question */
	private DesignConfig designConfig;
	
	/** the permissions of a user */
	private ArrayList<Byte> permissions;
	
	/**
	 * Default constructor of the KnwownUser class.
	 * 
	 * @param username
	 * @param password
	 */
	public KnownUser(String username, HashString password) {
		super();
		if(username == null || password == null)
		{
			return;
		}
		this.username = username;
		this.password = password;
	}
	
	/**
	 * The second constructor of the KnownUser class. The username, password, 
	 * userInfo and designConif are required to know the user.
	 * 
	 * @param username
	 * @param password
	 * @param designConfig
	 */
	public KnownUser(String username, HashString password, DesignConfig designConfig) {
		super();
		if(username == null || password == null || designConfig == null)
		{
			return;
		}
		this.username = username;
		this.password = password;
		this.designConfig = designConfig;
	}
	
	/**
	 * A getter method for returning the username of the user.
	 * 
	 * @return the username object.
	 */
	public String getUsername() {
		return username;
	}	
	
	/**
	 * Sets the username of the user.
	 * 
	 * @param username
	 */
	public void setUsername(String username) {
		if(username == null)
		{
			return;
		}
		this.username = username;
	}
	
	/**
	 * A getter method for returning the password of the user.
	 * 
	 * @return the password object.
	 */
	public HashString getPassword() {
		return password;
	}
	
	/**
	 * Sets the password of the user.
	 * 
	 * @param password
	 */
	public void setPassword(HashString password) {
		if(password == null)
		{
			return;
		}
		this.password = password;
	}
	
	/**
	 * A getter method for returning the designConfig of the user.
	 * 
	 * @return the designConfig object.
	 */
	public DesignConfig getDesignConfig() {
		return designConfig;
	}
	
	/**
	 * Sets the designCondig of the user.
	 * 
	 * @param designConfig
	 */
	public void setDesignConfig(DesignConfig designConfig) {
		if(designConfig == null)
		{
			return;
		}
		this.designConfig = designConfig;
	}

	public ArrayList<Byte> getPermissions() {
		return permissions;
	}

	public void setPermissions(ArrayList<Byte> permissions) {
		this.permissions = permissions;
	}
	
	
}
