package com.softwave.core.container.user.account.userexceptions;

/**
 * This exceptions is thrown when a user is null.
 * It can be used to validate a user before it is saved to the database,
 * @author eddi
 *
 */
public class UserIsNullException extends Exception {
	public UserIsNullException() {
		super("The user is null");
	}
	public UserIsNullException(String message){
		super(message);
	}
}