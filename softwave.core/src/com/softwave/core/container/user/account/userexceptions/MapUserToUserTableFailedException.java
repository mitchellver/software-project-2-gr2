package com.softwave.core.container.user.account.userexceptions;

/**
 * this esception can be thrown if the mapUserToUserTable method returns false,
 * hence it didn't map it correctly
 * @author eddi
 *
 */
public class MapUserToUserTableFailedException extends Exception {
	public MapUserToUserTableFailedException(){
		super("the mapping of a user to the usertable failed");
	}
	public MapUserToUserTableFailedException(String message){
		super(message);
	}
}