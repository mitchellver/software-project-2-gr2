package com.softwave.core.container.user.account.userexceptions;

/**
 * This exceptions is thrown when a user email is null.
 * 
 * @author eddi
 *
 */
public class UserEmailIsNullException extends Exception {
	public UserEmailIsNullException(){
		super("The user's email is null");
	}
	public UserEmailIsNullException(String message){
		super(message);
	}
}