package com.softwave.core.util;

import java.util.Locale;
import java.util.ResourceBundle;

/**
 * This static class will be used as a global config for the language in the
 * intire application, it will allow the setting of the language together with
 * marking where the user is located.
 * 
 * @author nicolas
 * @version 1.0
 */
public abstract class Translator {
	/** The object that holds the location data */
	private static Locale l = null;

	/**
	 * The resource bundle object will retrieve the data from the language file
	 */
	private static ResourceBundle lang = null;

	private static Language currentLanguage = Language.ENGLISH;

	/**
	 * This method will initialize the object's objects.
	 */
	public static void init() {
		String language = currentLanguage.toCodeString().split("_")[0];
		String country = currentLanguage.toCodeString().split("_")[1];
		l = new Locale(language, country);
		lang = ResourceBundle.getBundle("resources.languages.Bundle", l);
	}

	/**
	 * Give a String object that will be translated to the current language
	 * selected, there will be a lookup in the resource bundle to check the
	 * correct translation of the label text, if found the correct language will
	 * be used, if not the default english language will be used.
	 * 
	 * @param text
	 *            the text that needs to be translated.
	 * @return the translated text.
	 */
	public static String translate(String text) {
		if (lang == null) {
			init();
		}

		return lang.getString(text);
	}

	/**
	 * This method will build the language identification string based on the
	 * language code and the country code provided, as delimiter an underscore
	 * will be used.
	 * 
	 * @param lang
	 *            the code of the language.
	 * @param country
	 *            the code of the country for choosing the dialect of the
	 *            language
	 * @return a string representation of the language.
	 */
	public static String buildLanguage(LangCode lang, CountryCode country) {
		System.out.println(currentLanguage);
		return String.valueOf(lang).toLowerCase() + "_" + String.valueOf(country);
	}

	 public static void changeLanguage(Language language) {
	 // info on resourcebundles language switching
	 // https://dzone.com/articles/resource-bundle-tricks-and
	
	 setLanguage(language);
	
	 String lan = currentLanguage.toCodeString().split("_")[0];
	 String country = currentLanguage.toCodeString().split("_")[1];
	 Locale l1 = new Locale(lan, country);
	
	 ResourceBundle r = ResourceBundle.getBundle("resources.languages.Bundle",
	 l1);
	 // point the used resourcebundle to the new one.
	 lang = r;
	 }

	/**
	 * Setter for the language constructed out of the language and the country.
	 * 
	 * @param lang
	 *            the code of the language.
	 * @param country
	 *            the code of the country for choosing the dialect of the
	 *            language
	 * @return true if the operation succeed, false if not.
	 */
	public static boolean setLanguage(Language lang) {
		if (lang != null) {
			// System.out.println(String.valueOf(lang).toLowerCase());
			currentLanguage = lang;
			return true;
		}

		init();
		return false;
	}

	/**
	 * A getter for the current language as a string representation.
	 * 
	 * @return the current language as a string.
	 */
	public static Language getLanguage() {
		return currentLanguage;
	}

	public static Locale getL() {
		return l;
	}

	public static void setL(Locale l) {
		Translator.l = l;
	}

	public static ResourceBundle getLang() {
		return lang;
	}

	public static void setLang(ResourceBundle lang) {
		Translator.lang = lang;
	}

}
