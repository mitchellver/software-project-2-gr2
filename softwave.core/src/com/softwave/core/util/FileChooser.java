package com.softwave.core.util;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;

import javax.imageio.ImageIO;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;

/**
 * 
 * This is the method that takes care of files in the application, It can open a
 * popupwindow to let a user choose a logo e.g.
 * 
 * It also can save a file to the applications location for later use.
 * 
 * @author eddi
 *
 */
public class FileChooser {
	/** the start path of where the main folder is located */
	public static final String PATH_START = ".." + File.separator + "data" + File.separator;
	/** the path to the images folder */
	public static final String PATH_IMAGES = PATH_START + "media" + File.separator + "images" + File.separator;
	/** the path to the icons folder, located in the images folder */
	public static final String PATH_ICONS = PATH_IMAGES + "icons" + File.separator;
	/** the path to the logos folder, located in the images folder */
	public static final String PATH_LOGOS = PATH_IMAGES + "logos" + File.separator;
	/** the path to the videos folder */
	public static final String PATH_VIDEOS = "videos" + File.separator;

	/**
	 * This method opens a new panel where the user can select a file on his
	 * filesystem.
	 * 
	 * @return a File on success, null on fail
	 * @throws IOException
	 */
	public static File chooseFile() {
		JFileChooser fc = new JFileChooser();

		// open the jfilechooserwindow as a popup
		int returnVal = fc.showOpenDialog(null);
		if (returnVal == JFileChooser.CANCEL_OPTION || returnVal == JFileChooser.ERROR_OPTION)
			return null;
		return fc.getSelectedFile();
	}

	/**
	 * This is a method to choose a logo, a logo must be a jpg jpeg or png file
	 * 
	 * @return a File with the path set to the logo.
	 */
	public static File chooseLogo() {
		JFileChooser fc = new JFileChooser();

		// select the type of file that is chooseable
		// fc.addChoosableFileFilter(new FileNameExtensionFilter("JPG file",
		// "jpg"));
		fc.addChoosableFileFilter(new FileNameExtensionFilter("All images", ImageIO.getReaderFileSuffixes()));
		fc.setAcceptAllFileFilterUsed(false);

		// open the jfilechooserwindow as a popup
		int returnVal = fc.showOpenDialog(null);
		if (returnVal == JFileChooser.CANCEL_OPTION || returnVal == JFileChooser.ERROR_OPTION)
			return null;

		File logo = fc.getSelectedFile();

		return logo;
	}

	/**
	 * A method to copy a file to the application itself
	 * 
	 * @param file
	 *            the file to copy to the application
	 * @param path
	 */
	public static File copyToApplication(File file, String path) {
		String savePath = path + file.getName();
		File copyFile = null;
		// now that the file is selected, copy it to the applications folder
		try {
			if (file != null)
				// first create the directory if nescesarry
				new File(savePath).mkdirs();
			// then copy the file
			Files.copy(file.toPath(), (copyFile = new File(savePath)).toPath(), StandardCopyOption.REPLACE_EXISTING);
		} catch (IOException e1) {
			e1.printStackTrace();
		}

		return copyFile;
	}

	/**
	 * a method to copy a quiz to the application, it renames the file to "quiz",
	 * since there can only be 1 quiz each time.
	 * 
	 * @param file
	 *            the quiz we want to copy.
	 * @param path
	 *            the path we want to copy the quiz to.
	 * @return true on success, false on fail.
	 */
	public static File copyQuizToApplication(File file, String path) {
		String savePath = path + "quiz";
		File copyFile = null;
		// now that the file is selected, copy it to the applications folder
		try {
			if (file != null)
				// first create the directory if nescesarry
				new File(savePath).mkdirs();
			// then copy the file
			Files.copy(file.toPath(), (copyFile = new File(savePath)).toPath(), StandardCopyOption.REPLACE_EXISTING);
		} catch (IOException e1) {
			e1.printStackTrace();
		}

		return copyFile;
	}
}
