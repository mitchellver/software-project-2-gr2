package com.softwave.core.util;

import java.awt.Font;

public enum FontEnum {
	Monospaced(Font.MONOSPACED),
	SansSerif(Font.SANS_SERIF),
	Serif(Font.SERIF);
	
	private String fontName;
	
	private FontEnum(String fontName) {
		this.fontName = fontName;
	}

//	public Font generateFont(FontEnum f){
//		return new Font(f.name(), Font.PLAIN, 12);
//	}
}


