package com.softwave.core.util;

public enum Language {
	ENGLISH(LangCode.EN, CountryCode.INT),
	NEDERLANDS(LangCode.NL, CountryCode.BE),
	FRANCAIS(LangCode.FR, CountryCode.BE);
	
	private final String text;
	
	Language(LangCode lang, CountryCode countryCode) {
		text = Translator.buildLanguage(lang, countryCode);
	}
	
	public static Language createFromString(String langcode) {
		Language[] langs = Language.values();
		for (int i = 0; i < langs.length; i++) {
			if (langcode == langs[i].toCodeString()) {
				return langs[i];
			}
		}
		return ENGLISH;
	}
	
	public String toCodeString() {
		return text;
	}
	
	@Override
	public String toString() {
		return name().toLowerCase();
	}
}
