package com.softwave.core.util;

public class SafeCast {

	/**
	 * cast a string to an int, and check if it is betweeen a min and max value.
	 * 
	 * @param str
	 *            the string to cast to an integer
	 * @param min
	 *            the minimum value the resulting int must be above
	 * @param max
	 *            the maximum value the resulting int must be under
	 * @return the resulting int, or a NumberFormatException
	 */
	public static int stringToInt(String str, int min, int max) throws NumberFormatException{
		int resInt;
		if (str.isEmpty())
			throw new NumberFormatException("The string is empty");

		resInt = Integer.parseInt(str);
		if (resInt < min || resInt > max)
			throw new NumberFormatException("The min or max value was exceeded");
		return resInt;
	}
}
