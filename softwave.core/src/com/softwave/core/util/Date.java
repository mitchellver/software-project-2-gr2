package com.softwave.core.util;

import java.sql.Timestamp;
import java.time.LocalDateTime;

/*
 *  @author Mitchell Verbruggen
 *  @version 1.0
 */
public class Date {

	/**
	 * this is the main time member, all that changes starts or ends here.
	 * if we get the timestamp from the db we immediately update this localTime
	 */
	private LocalDateTime localTime;

	private String dateString;
	private String dateHuman;
	private long dateLong;
	private int year;
	private int month;
	private int day;
	private int hour;
	private int minutes;
	private int secondes;
	private char symbol = '/';
	private char symbolTime = ':';

	
	/** 
	 * a non-argument constructor used by the DAO
	 */
	public Date() {
		captureCurrentTime();
	}
	/**
	 * a constructor that gets an sql Timestamp and converts it to the localtime.
	 * To be used with the database
	 * @param timestamp, an sql Timestamp
	 */
	public Date(Timestamp t){
		convertTimestampToLocalTime(t);
	}

	public Date(LocalDateTime localTime){
		this.localTime = localTime;
		// set all the datamembers according to the localTime
		updateTimeMembers();
	}

	
	/** 
	 * this method converts an sql Timestamp to a localDateTimeObject,
	 * in fatd, it sets our localTime, and then calls the function to update all our other timeMembers
	 * @param t the Timestamp gotten from e.g. the database
	 */
	public void convertTimestampToLocalTime(Timestamp t) {
		localTime = t.toLocalDateTime();
		updateTimeMembers();
	}

	public Timestamp convertLocalToTimestamp() {
		return Timestamp.valueOf(localTime);
	}

	/*
	 * @return int of year
	 */
	public int getYear() {
		return year;
	}

	/*
	 * @param int year
	 * 
	 */
	public void setYear(int year) {
		this.year = year;
	}

	/*
	 * @return int of month
	 */
	public int getMonth() {
		return month;
	}

	/*
	 * @param int month
	 * 
	 */
	public void setMonth(int month) {
		this.month = month;
	}

	/*
	 * @return int of day
	 */
	public int getDay() {
		return day;
	}

	/*
	 * @param int day
	 * 
	 */
	public void setDay(int day) {
		this.day = day;
	}

	/*
	 * @return int of hour
	 */
	public int getHour() {
		return hour;
	}

	/*
	 * @param int hour
	 * 
	 */
	public void setHour(int hour) {
		this.hour = hour;
	}

	/*
	 * @return int of minutes
	 */
	public int getMinutes() {
		return minutes;
	}

	/*
	 * @param int minutes
	 * 
	 */
	public void setMinutes(int minutes) {
		this.minutes = minutes;
	}

	/*
	 * @return int of secondes
	 */
	public int getSecondes() {
		return secondes;
	}

	/*
	 * @param int secondes
	 * 
	 */
	public void setSecondes(int secondes) {
		this.secondes = secondes;
	}

	public long getDateLong() {
		return dateLong;
	}

	public void setDateLong(long dateLong) {
		this.dateLong = dateLong;
	}

	/*
	 * this method updates the localTime variable to the local time of the
	 * system.
	 * 
	 * @return returns a string (if time is true) dd/mm/yyyy/hh/MM/ss else
	 * dd/mm/yyyy
	 */
	public void captureCurrentTime() {
		localTime = LocalDateTime.now();
		updateTimeMembers();
	}
	
	public void updateTimeMembers(){
		hour = localTime.getHour();
		minutes = localTime.getMinute();
		secondes = localTime.getSecond();
		month = localTime.getMonthValue();
		day = localTime.getDayOfMonth();
		year = localTime.getYear();
		// update the other dates in this class
		Tohuman();
		parseTimeToString();
		parseTimeToLong();
	}

	/*
	 * @return return the current time in this format: yyyymmddhhmmss
	 */
	public void parseTimeToString() {

		StringBuilder sb = new StringBuilder();
		sb.append(year);
		if (month < 10) {
			sb.append(String.format("%02d", month));
		} else {
			sb.append(month);
		}
		if (day < 10) {

			sb.append(String.format("%02d", day));
		} else {
			sb.append(day);
		}

		if (hour < 10) {

			sb.append(String.format("%02d", hour));
		} else {
			sb.append(hour);
		}
		if (minutes < 10) {

			sb.append(String.format("%02d", minutes));
		} else {
			sb.append(minutes);
		}
		if (secondes < 10) {
			sb.append(String.format("%02d", secondes));
		} else {

			sb.append(secondes);
		}
		dateString = sb.toString();
	}

	public void parseTimeToLong() {
		setDateLong(Long.parseLong(dateString));
	}

	public String Tohuman() {
		StringBuilder sb = new StringBuilder();
		sb.append(day);
		sb.append(symbol);
		sb.append(month);
		sb.append(symbol);
		sb.append(year);
		sb.append(" ");
		sb.append(hour);
		sb.append(symbolTime);
		sb.append(minutes);
		sb.append(symbolTime);
		sb.append(secondes);
		dateHuman = sb.toString();
		return dateHuman;

	}

	
	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + day;
		result = prime * result + hour;
		result = prime * result + minutes;
		result = prime * result + month;
		result = prime * result + secondes;
		result = prime * result + year;
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Date other = (Date) obj;
		if (day != other.day)
			return false;
		if (hour != other.hour)
			return false;
		if (minutes != other.minutes)
			return false;
		if (month != other.month)
			return false;
		if (secondes != other.secondes)
			return false;
		if (year != other.year)
			return false;
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Date [year=" + year + ", month=" + month + ", day=" + day + ", hour=" + hour + ", minutes=" + minutes
				+ ", secondes=" + secondes + "]";
	}


}