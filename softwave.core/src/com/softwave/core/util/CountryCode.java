package com.softwave.core.util;

/**
 * A predefined list of all the country codes that the application knows to define
 * the correct language with the correct meanings.
 * 
 * @author nicolas
 */
public enum CountryCode {
	BE,
	FR,
	INT
}
