package com.softwave.core.util;

/**
 * The known Language codes that correspond to the names of the languages supported
 * by the application.
 * 
 * @author nicolas
 */
public enum LangCode {
	EN,
	NL,
	FR
}
