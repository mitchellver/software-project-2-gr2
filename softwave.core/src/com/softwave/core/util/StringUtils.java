package com.softwave.core.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

/**
 * This class holds some useful string manipulation code to help with the
 * way strings are used in the program.
 * 
 * @author nicolas
 */
public class StringUtils {
	/**
	 * This method will join a list of strings together with a specified delimiter.
	 * 
	 * @param values the list of values that need to be combined into one string.
	 * @return a single string build from the list of strings and the delimiter.
	 */
	public static String join(List<? extends String> values) {
		String joined = "";
		for (int i = 0; i < values.size(); i++) {
			joined += (i == 0 ? "" : ",") + values.get(i);
		}
		return joined;
	}
	
	/**
	 * This method will split the given string into seperate values and put them into
	 * a list of strings for future use.
	 * 
	 * @param value the to be split string.
	 * @return a list of seperate strings.
	 */
	public static List<String> split(String value) {
		if (value == null) {
			List<String> tmp = new ArrayList<>();
			tmp.add("");
			return tmp;
			
		}
		
		return Arrays.asList(value.split(","));
	}
}
