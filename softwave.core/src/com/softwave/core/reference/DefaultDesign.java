package com.softwave.core.reference;

import java.awt.Color;
import java.awt.Font;
import java.io.File;

public class DefaultDesign {
	public static final Color LIGHT_GREY = new Color(239,239,239);
	public static final Color LIGHT_BLUE = new Color(127,152,174);
	public static final Color MEDIUM_BLUE = new Color(65,101,133);
	public static final Color DARK_BLUE = new Color(37,68,98);
	public static final Color CYAN_LIGHT_BLUE = new Color(194,240,247);
	
	public static final Color PANEL_BACKGROUND_COLOR = LIGHT_BLUE;
	public static final Color PANEL_LEFT_BACKGROUND_COLOR = MEDIUM_BLUE;
	public static final Color PANEL_CONTEXT_BACKGROUND_COLOR = DARK_BLUE;
	public static final Color TXT_COLOR = LIGHT_GREY;

	/**
	 * these are the default Fonts used in a default DesignConfig
	 */
	public static final Font FONT_MAIN = new Font(Font.MONOSPACED, Font.BOLD, 12);
	
	/**
	 * this is the default logo.
	 * it's the logo used when no logo is set.
	 */
	public static final String LOGO_DEFAULT = "logo.png";
}
