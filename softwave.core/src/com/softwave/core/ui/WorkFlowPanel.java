package com.softwave.core.ui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.Image;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JPanel;

import com.softwave.core.reference.DefaultDesign;
import com.softwave.core.ui.component.LinkButton;
import com.softwave.core.ui.event.ChangeLanguageEvent;
import com.softwave.core.ui.event.GotoManageQuizEvent;
import com.softwave.core.ui.event.LogoutEvent;
import com.softwave.core.ui.event.ManipulateViewEvent;
import com.softwave.core.ui.event.QuitEvent;
import com.softwave.core.ui.event.SwitchViewEvent;
import com.softwave.core.util.Language;
import com.softwave.core.util.Translator;

/**
 * This class is a representation of all the panels that hold the default layout
 * for the workflow standaard of the application it describes what the general
 * layout of the panel is and all the child layouts it has to build the panel.
 * 
 * @author nicolas
 */
public abstract class WorkFlowPanel extends Panel implements IQuickAccessHolder {
	/** The layout of the panel */
	protected BorderLayout layout = null;

	/** The context panel at the top of the panel */
	protected JPanel contextPanel = null;

	/** The main content of the panel */
	protected JPanel contentPanel = null;

	/** The quick access panel on the left of the panel */
	protected JPanel quickAccessPanel = null;

	/**
	 * The default constructor of the panel.
	 */
	public WorkFlowPanel() {
	}

	/**
	 * The constructor of the object that sets the size of the panel using a
	 * dimension. if the panel is auto sized because of layouts like
	 * {@code BorderLayout.CENTER} giving the size to the constructor is
	 * useless.
	 * 
	 * @param dim
	 *            the size of the panel,
	 */
	public WorkFlowPanel(Dimension dim) {
		super(dim);
	}

	/**
	 * Initialize the context bar at the top
	 */
	protected void initContext() {
		int row = 1;
		int column = 2;

		GridLayout gridLayout = new GridLayout(row, column);

		contextPanel = new JPanel();

		contextPanel.setBackground(DefaultDesign.DARK_BLUE);
		// add(contextPanel);
		contextPanel.setLayout(gridLayout);

		//------------------------------------- HIDE & ComboBox(language) & BACK ---------------------------------------------

		JPanel leftButtons = new JPanel(new FlowLayout(FlowLayout.LEFT));
		JPanel rightButtons = new JPanel(new FlowLayout(FlowLayout.RIGHT));

		JButton btnQuickAccessControl = new JButton(Translator.translate("button.hide"));
		String path7 = "/mediaFiles/images/icons/quikAcces.png";
		ImageIcon icnQuickAcces = UISettings.makeImageIcon(path7, "QuickAcces button");
		
		// troubleshooting scale imageIcon --> stackoverflow code		
		Image image7 = icnQuickAcces.getImage(); // transform it 
		Image newimg7 = image7.getScaledInstance(35, 35,  java.awt.Image.SCALE_SMOOTH); // scale it the smooth way  
		icnQuickAcces = new ImageIcon(newimg7);  // transform it back
		
		btnQuickAccessControl.setIcon(icnQuickAcces);
		btnQuickAccessControl.addActionListener(new ManipulateViewEvent());
		
		// ComboBox(language)
		JComboBox<Language> cbLanguageSwitch = new JComboBox<>(Language.values());
		cbLanguageSwitch.setSelectedItem(Translator.getLanguage());
		cbLanguageSwitch.addActionListener(new ChangeLanguageEvent());
		
		// BACK
		JButton btnBackView = new JButton(Translator.translate("button.back"));
		String path8 = "/mediaFiles/images/icons/back.png";
		ImageIcon icnBack = UISettings.makeImageIcon(path8, "Back button");
		
		// troubleshooting scale imageIcon --> stackoverflow code		
		Image image8 = icnBack.getImage(); // transform it 
		Image newimg8 = image8.getScaledInstance(35, 35,  java.awt.Image.SCALE_SMOOTH); // scale it the smooth way  
		icnBack = new ImageIcon(newimg8);  // transform it back
		
		btnBackView.setIcon(icnBack);
		btnBackView.addActionListener(new SwitchViewEvent(WindowManager.PREVIOUS_VIEW));
		 btnQuickAccessControl.setForeground(DefaultDesign.LIGHT_GREY);
//		cbLanguageSwitch.setForeground(DefaultDesign.LIGHT_GREY);
		btnBackView.setForeground(DefaultDesign.LIGHT_GREY);

		// Makes btnQuickAccessControl and btnBackView transparent
		btnQuickAccessControl.setOpaque(false);
		btnQuickAccessControl.setContentAreaFilled(false);
		btnQuickAccessControl.setBorderPainted(false);
		btnBackView.setOpaque(false);
		btnBackView.setContentAreaFilled(false);
		btnBackView.setBorderPainted(false);

		leftButtons.add(btnQuickAccessControl);
		rightButtons.add(btnBackView);
		rightButtons.add(cbLanguageSwitch);
		contextPanel.add(leftButtons);
		contextPanel.add(rightButtons);

		leftButtons.setBackground(DefaultDesign.DARK_BLUE);
		rightButtons.setBackground(DefaultDesign.DARK_BLUE);
	}

	/**
	 * Initialize the quick access panel with buttons to functions the user will
	 * need at all times
	 */
	protected void initQuickAccess() {
		int row = 10;
		int column = 1;

		GridLayout gridLayout = new GridLayout(row, column);

		quickAccessPanel = new JPanel();
		// PANE_LEFT_BACKGROUND_COLOR = MEDIUM_BLUE;
		quickAccessPanel.setBackground(DefaultDesign.MEDIUM_BLUE);
		
		//------------------------------------- MANAGE QUIZZES ---------------------------------------------

		JButton btnManageQuizzes = new JButton(Translator.translate("button.managequiz.managequizzes"));

		String path9 = "/mediaFiles/images/icons/smallBtnManageQuizzes.png";
		ImageIcon icnManageQuizzes = UISettings.makeImageIcon(path9, "Quizzes button");
		
		// troubleshooting scale imageIcon --> stackoverflow code		
		Image image9 = icnManageQuizzes.getImage(); // transform it 
		Image newimg9 = image9.getScaledInstance(35, 35,  java.awt.Image.SCALE_SMOOTH); // scale it the smooth way  
		icnManageQuizzes = new ImageIcon(newimg9);  // transform it back
		
		btnManageQuizzes.setIcon(icnManageQuizzes);
		
		// Text color
		btnManageQuizzes.setForeground(DefaultDesign.LIGHT_GREY);
		
		// Makes btnSettings transparent
		btnManageQuizzes.setOpaque(false);
		btnManageQuizzes.setContentAreaFilled(false);
		btnManageQuizzes.setBorderPainted(false);

		//------------------------------------- MANAGE USERS ---------------------------------------------
		
		JButton btnManageUsers = new JButton(Translator.translate("button.managequiz.manageusers"));
		String path10 = "/mediaFiles/images/icons/smallBtnManageUsers.png";
		ImageIcon icnManageUsers = UISettings.makeImageIcon(path10, "Users button");
		
		// troubleshooting scale imageIcon --> stackoverflow code		
		Image image10 = icnManageUsers.getImage(); // transform it 
		Image newimg10 = image10.getScaledInstance(35, 35,  java.awt.Image.SCALE_SMOOTH); // scale it the smooth way  
		icnManageUsers = new ImageIcon(newimg10);  // transform it back
		
		btnManageUsers.setIcon(icnManageUsers);
		
		// Text color
		btnManageUsers.setForeground(DefaultDesign.LIGHT_GREY);
		
		// Makes btnSettings transparent
		btnManageUsers.setOpaque(false);
		btnManageUsers.setContentAreaFilled(false);
		btnManageUsers.setBorderPainted(false);

		//------------------------------------- STATISTICS ---------------------------------------------

		JButton btnStatistics = new JButton(Translator.translate("button.managequiz.statistics"));
		String path11 = "/mediaFiles/images/icons/smallBtnStatistics.png";
		ImageIcon icnStatistics = UISettings.makeImageIcon(path11, "Statistics button");
		
		// troubleshooting scale imageIcon --> stackoverflow code		
		Image image11 = icnStatistics.getImage(); // transform it 
		Image newimg11 = image11.getScaledInstance(35, 35,  java.awt.Image.SCALE_SMOOTH); // scale it the smooth way  
		icnStatistics = new ImageIcon(newimg11);  // transform it back
		
		btnStatistics.setIcon(icnStatistics);
		
		// Text color
		btnStatistics.setForeground(DefaultDesign.LIGHT_GREY);
		// Makes btnSettings transparent
		btnStatistics.setOpaque(false);
		btnStatistics.setContentAreaFilled(false);
		btnStatistics.setBorderPainted(false);

		//------------------------------------- LOGOUT ---------------------------------------------

		LinkButton btnLogout = new LinkButton(new LoginPanel(new Dimension(400, 300)),
		Translator.translate("button.logout"));
		String path4 = "/mediaFiles/images/icons/btnLogout.png";
		ImageIcon icnLogout = UISettings.makeImageIcon(path4, "Logout button");
		
		// troubleshooting scale imageIcon --> stackoverflow code		
		Image image12 = icnLogout.getImage(); // transform it 
		Image newimg12 = image12.getScaledInstance(35, 35,  java.awt.Image.SCALE_SMOOTH); // scale it the smooth way  
		icnLogout = new ImageIcon(newimg12);  // transform it back
		
		btnLogout.setIcon(icnLogout);
		btnLogout.addActionListener(new SwitchViewEvent());
		
		// Text Color
		btnLogout.setForeground(DefaultDesign.LIGHT_GREY);
		// Makes btnSettings transparent
		btnLogout.setOpaque(false);
		btnLogout.setContentAreaFilled(false);
		btnLogout.setBorderPainted(false);

		//------------------------------------- SETTINGS ---------------------------------------------

		JButton btnSettings = new JButton(Translator.translate("button.managequiz.settings"));
		String path5 = "/mediaFiles/images/icons/btnSettings.png";
		ImageIcon icnSettings = UISettings.makeImageIcon(path5, "Settings button");
		
		// troubleshooting scale imageIcon --> stackoverflow code		
		Image image13 = icnSettings.getImage(); // transform it 
		Image newimg13 = image13.getScaledInstance(35, 35,  java.awt.Image.SCALE_SMOOTH); // scale it the smooth way  
		icnSettings = new ImageIcon(newimg13);  // transform it back
		
		btnSettings.setIcon(icnSettings);
		btnSettings.addActionListener(new SwitchViewEvent());
		
		// Text color
		btnSettings.setForeground(DefaultDesign.LIGHT_GREY);
		// Makes btnSettings transparent
		btnSettings.setOpaque(false);
		btnSettings.setContentAreaFilled(false);
		btnSettings.setBorderPainted(false);

		
		//------------------------------------- QUIT ---------------------------------------------

		JButton btnQuit = new JButton("Quit");
		String path6 = "/mediaFiles/images/icons/btnQuit.png";
		ImageIcon icnQuit = UISettings.makeImageIcon(path6, "Quit button");
		
		// troubleshooting scale imageIcon --> stackoverflow code		
		Image image14 = icnQuit.getImage(); // transform it 
		Image newimg14 = image14.getScaledInstance(35, 35,  java.awt.Image.SCALE_SMOOTH); // scale it the smooth way  
		icnQuit = new ImageIcon(newimg14);  // transform it back
		
		btnQuit.setIcon(icnQuit);
		
		// Text color
		btnQuit.setForeground(DefaultDesign.LIGHT_GREY);
		// Makes btnSettings transparent
		btnQuit.setOpaque(false);
		btnQuit.setContentAreaFilled(false);
		btnQuit.setBorderPainted(false);

		btnManageQuizzes.addActionListener(new GotoManageQuizEvent());
		btnLogout.addActionListener(new LogoutEvent());
		btnQuit.addActionListener(new QuitEvent());

		quickAccessPanel.setLayout(gridLayout);
		quickAccessPanel.add(btnManageQuizzes);
		quickAccessPanel.add(btnManageUsers);
		quickAccessPanel.add(btnStatistics);
		quickAccessPanel.add(btnSettings);
		quickAccessPanel.add(btnLogout);
		quickAccessPanel.add(btnQuit);

	}

	/**
	 * Getter for the quickAccessPanel of the object.
	 * 
	 * @return the quickAccessPanel object.
	 */
	@Override
	public JPanel getQuickAccessPanel() {
		return quickAccessPanel;
	}

	/**
	 * Initializes the panel's member objects and constructs the panel.
	 */
	@Override
	protected abstract void init();

	/**
	 * Initializes the main content of the panel in the middle of the screen.
	 */
	protected abstract void initDashboard();
}