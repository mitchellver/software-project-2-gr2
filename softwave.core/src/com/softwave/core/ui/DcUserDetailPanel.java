package com.softwave.core.ui;

import javax.swing.JButton;

import com.softwave.core.container.design.DesignConfig;
import com.softwave.core.container.user.account.KnownUser;
import com.softwave.core.ui.event.ChooseUserLogoEvent;
import com.softwave.core.ui.event.UserDcAbsorptionEvent;

public class DcUserDetailPanel extends DesignConfigDetailPanel {
	public DcUserDetailPanel(DesignConfig dc, KnownUser kUser) {
		// the chooseUserLogoEvent checks the componentmap on null
		super(dc);
		((JButton) componentMap.get(BTN_LOGO)).addActionListener(new ChooseUserLogoEvent(componentMap, kUser));
		((JButton) componentMap.get(BTN_SAVE)).addActionListener(new UserDcAbsorptionEvent(componentMap, kUser));
	}

	
}
