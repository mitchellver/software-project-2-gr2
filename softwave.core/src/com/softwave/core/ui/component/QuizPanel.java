package com.softwave.core.ui.component;

import java.awt.FlowLayout;
import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import com.softwave.core.container.quiz.Quiz;
import com.softwave.core.ui.QuizDetailPanel;
import com.softwave.core.ui.QuizDetailPanel;
import com.softwave.core.ui.event.ExportQuizEvent;
import com.softwave.core.ui.event.RemoveQuizEvent;
import com.softwave.core.ui.event.SwitchViewEvent;
import com.softwave.core.util.Translator;

/**
 * This class represents a quiz that is displayed on the gui in the list view.
 * 
 * @author nicolas
 *
 */
public class QuizPanel extends JPanel {
	/** The quiz represented by the this panel */
	private Quiz quiz;

	/** name of the quiz put inside a panel */
	private JPanel namePanel;

	/** panel that holds the buttons to control the actions on the quiz */
	private JPanel buttonPanel;

	/** the name of the quiz */
	private JLabel quizName;

	/** the button to open the properties window for the quiz */
	private LinkButton btnOpen;

	/**
	 * the button to remove the current quiz from the list of quizzes assigned
	 * to the current logged in user
	 */
	private JButton btnRemove;

	/**
	 * the button to store a quiz on locally, or export it to use in the quizzing application
	 */
	private JButton btnExport;
	
	/**
	 * the constructor of the quizpanel that represents a quiz attached to the
	 * current logged in user.
	 * 
	 * @param quiz
	 *            the quiz displayed by this panel.
	 */
	public QuizPanel(Quiz quiz) {
		if (quiz != null) {
			quizName = new JLabel(quiz.getName());
			this.quiz = quiz;
		} else
			quizName = new JLabel("untitled");

		//this.setPreferredSize(new Dimension(100, 30));
		init();
	}

	/**
	 * Initializes the panel with the correct layouts and panels to represent
	 * the info from the quiz.
	 */
	protected void init() {
		this.setLayout(new GridLayout(1, 2));

		namePanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
		buttonPanel = new JPanel(new FlowLayout(FlowLayout.RIGHT));

		btnExport = new JButton(Translator.translate("button.managequiz.export"));
		btnOpen = new LinkButton(new QuizDetailPanel(quiz), Translator.translate("button.managequiz.open"));
		btnRemove = new JButton(Translator.translate("button.managequiz.remove"));

		btnOpen.addActionListener(new SwitchViewEvent(quiz));
		btnRemove.addActionListener(new RemoveQuizEvent(quiz));
		btnExport.addActionListener(new ExportQuizEvent(quiz));
		namePanel.add(quizName);
		buttonPanel.add(btnExport);
		buttonPanel.add(btnOpen);
		buttonPanel.add(btnRemove);
		
		this.add(namePanel);
		this.add(buttonPanel);
	}
}
