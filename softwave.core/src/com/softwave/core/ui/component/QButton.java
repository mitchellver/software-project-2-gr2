package com.softwave.core.ui.component;

import javax.swing.JButton;

public class QButton extends JButton {

	private int index;

	public QButton(int index) {
		super();
	}

	public int getIndex() {
		return index;
	}

	public void setIndex(int index) {
		this.index = index;
	}

}
