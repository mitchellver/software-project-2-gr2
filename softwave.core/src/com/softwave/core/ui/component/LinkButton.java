package com.softwave.core.ui.component;

import javax.swing.Action;
import javax.swing.Icon;

import com.softwave.core.ui.Panel;

/**
 * This class will represent a button that changes the view of the application when
 * clicked on, the button holds the link to the next view as a panel that is give
 * at construction.
 * 
 * @author nicolas
 */
public class LinkButton extends Button {
	/** The panel that will be displayed if the button is pressed. */
	private Panel link;

	/**
	 * Default constructor for the Button object.
	 * 
	 * @param panel the panel that will be displayed if the button is pressed.
	 */
	public LinkButton(Panel panel) {
		setLink(panel);
	}

	/**
	 * Constructor for a button object that will display an icon on the screen.
	 * 
	 * @param panel the panel that will be displayed if the button is pressed.
	 * @param icon the icon displayed inside the button.
	 */
	public LinkButton(Panel panel, Icon icon) {
		super(icon);
		setLink(panel);
	}

	/**
	 * constructor for a button object that will display text on the screen.
	 * 
	 * @param panel the panel that will be displayed if the button is pressed.
	 * @param text the text displayed inside the button.
	 */
	public LinkButton(Panel panel, String text) {
		super(text);
		setLink(panel);
	}

	/**
	 * Constructor for a button object where the properties of the button are taken
	 * from the action object.
	 * 
	 * @param panel the panel that will be displayed if the button is pressed.
	 * @param a the action object that gives info about the button.
	 */
	public LinkButton(Panel panel, Action a) {
		super(a);
		setLink(panel);
	}

	/**
	 * Constructor for a button that will display text and an icon on the screen.
	 * 
	 * @param panel the panel that will be displayed if the button is pressed.
	 * @param text the text displayed by the button.
	 * @param icon the icon displayed by the button.
	 */
	public LinkButton(Panel panel, String text, Icon icon) {
		super(text, icon);
		setLink(panel);
	}

	/**
	 * A private setter to set the panel where the button will link to if the button
	 * is pressed.
	 * 
	 * @param panel the panel where the buttn will link to.
	 */
	private void setLink(Panel panel) {
		if (panel == null) {
			return;
		} else {
			this.link = panel;
		}
	}
	
	/**
	 * Getter for the link of the button.
	 * 
	 * @return the panel where the button will link to.
	 */
	public Panel getLink() {
		return link;
	}
}
