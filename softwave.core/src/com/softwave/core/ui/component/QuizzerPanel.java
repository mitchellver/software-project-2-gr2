package com.softwave.core.ui.component;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import com.softwave.core.container.user.account.Client;

public class QuizzerPanel extends JPanel {

	private Client client;
	
	private JPanel namePanel;
	
	private JPanel buttonPanel;
	
	private JLabel quizzerName;
	
	private LinkButton btnOpen;
	
	private JButton btnRemore;
	
	public QuizzerPanel(Client client){
		if(client != null){
			quizzerName = new JLabel(client.getUsername());
		}
		else{
			quizzerName = new JLabel("untitled");
		}
		this.setPreferredSize(new Dimension(100, 30));
		init();
	}
	
	protected void init(){
		this.setLayout(new GridLayout(1, 2));
		
		namePanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
		buttonPanel = new JPanel(new FlowLayout(FlowLayout.RIGHT));
		// 
		// btnOpen = new LinkButton(new ControlDashboardPanel(), Translator.translate("button.managequizzer.open"));
		// btnRemore = new JButton(Translator.translate("button.managequizzer.remove"));
		
		// btnOpen.addActionListener(new SwitchViewEvent());
		
		namePanel.add(quizzerName);
		// buttonPanel.add(btnOpen);
		// buttonPanel.add(btnRemore);
		
		this.add(namePanel);
		this.add(buttonPanel);
	}
}