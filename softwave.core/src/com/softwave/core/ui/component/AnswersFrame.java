package com.softwave.core.ui.component;

import java.awt.Color;
import java.awt.Dimension;
import java.util.HashMap;
import java.util.Map;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.Spring;
import javax.swing.SpringLayout;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;

import com.mysql.jdbc.StatementInterceptor;
import com.softwave.core.container.quiz.QType;
import com.softwave.core.container.quiz.Question;
import com.softwave.core.ui.Window;
import com.softwave.core.ui.event.AnswersAbsorptionEvent;
import com.softwave.core.util.Translator;

/**
 * Answer frame will allow the user to manage the valid answers for a specific
 * question.
 * @author nicolas
 */
public class AnswersFrame extends Window {
	/** Store the type for which the */
	private Question question;
	
	/** The key of the button to add an answer to the list. */
	public static final String BTN_ADD = "add button";
	
	/** The key of the button to remove all selected answers from the list */
	public static final String BTN_REMOVE = "remove button";
	
	/** The key of the button to save the answers to the current question */
	public static final String BTN_SAVE = "save button";
	
	/** The key of the button to close the window after or before saving */
	public static final String BTN_CLOSE = "Close button";
	
	/** The key of the label of the answers list */
	public static final String LB_ANSWERS = "answers label";
	
	/** The key of the list of answers */
	public static final String ANSWERS_LIST = "answers list";
	
	/** The key of the input field where new answers can be stored */
	public static final String TXT_ADD = "answers input field";
	
	/** The key of the label of the correct answers field */
	public static final String LB_CORRECT_ANSWER = "correct answer label";
	
	/** The key of the selection box for the correct answer */
	public static final String CB_CORRECT_ANSWER = "correct answer";
	
	/**
	 * The map with all the components where the key is the name of the component
	 * as a string, the value is the component itself.
	 */
	private Map<String, JComponent> componentMap;
	
	/**
	 * This constructor will set all the defaults of the frame, including the title,
	 * width and height, together with the type of question for which the frame is made.
	 * 
	 * @param title the title of the window
	 * @param dim the width and height of the window
	 * @param type based on the type of question there will be a different filling
	 * for the window.
	 */
	public AnswersFrame(String title, Dimension dim, Question question) {
		super(title, dim);
		this.question = question;
		componentMap = new HashMap<String, JComponent>();
		init();
	}

	/**
	 * The init method will initialize the frame and all the components on it, it
	 * will set all the layouts, make the components and ready all the events.
	 */
	@Override
	protected void init() {
		if (question.getType() == QType.MULTIPLE_CHOICE) {
			this.getContentPane().add(makeMultipleChoisePanel());
		} else if (question.getType() == QType.YES_NO) {
			this.getContentPane().add(makeYesNoPanel());
		} else if (question.getType() == QType.OPEN) {
			this.getContentPane().add(makeOpenPanel());
		}
		
		this.setResizable(false);
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		this.setVisible(true);
	}
	
	private JPanel makeYesNoPanel() {
		JPanel panel = new JPanel();
		
		return panel;
	}
	
	private JPanel makeMultipleChoisePanel() {
		JPanel panel = new JPanel();
		
		SpringLayout spring = new SpringLayout();
		panel.setLayout(spring);
		
		componentMap.put(TXT_ADD, new JTextField());
		componentMap.put(ANSWERS_LIST, new JList<String>());
		componentMap.put(LB_ANSWERS, new JLabel(Translator.translate("label.answer.answer")));
		componentMap.put(BTN_ADD, new JButton(Translator.translate("button.answer.add")));
		componentMap.put(BTN_REMOVE, new JButton(Translator.translate("button.answer.remove")));
		componentMap.put(BTN_CLOSE, new JButton(Translator.translate("button.answer.close")));
		componentMap.put(BTN_SAVE, new JButton(Translator.translate("button.answer.save")));
		componentMap.put(CB_CORRECT_ANSWER, new JComboBox<>());
		componentMap.put(LB_CORRECT_ANSWER, new JLabel(Translator.translate("label.answer.correct")));

		DefaultListModel<String> model = null;
		if (question != null && question.getPossibleAnswers() != null) {
			model = new DefaultListModel<>();
			JList<String> list = (JList)componentMap.get(ANSWERS_LIST);
			JComboBox<String> cb = (JComboBox)componentMap.get(CB_CORRECT_ANSWER); 
			list.setModel(model);
			question.getPossibleAnswers().stream().forEach(model::addElement);
			question.getPossibleAnswers().stream().forEach(cb::addItem);
			cb.setSelectedItem(question.getCorrectAnswer());
		}

		AnswersAbsorptionEvent answersAbsorptionEvent = new AnswersAbsorptionEvent(componentMap, model);
		answersAbsorptionEvent.setQuestion(question);
		((JButton)componentMap.get(BTN_ADD)).addActionListener(answersAbsorptionEvent);
		((JButton)componentMap.get(BTN_REMOVE)).addActionListener(answersAbsorptionEvent);
		((JButton)componentMap.get(BTN_SAVE)).addActionListener(answersAbsorptionEvent);
		((JButton)componentMap.get(BTN_CLOSE)).addActionListener(e -> this.dispose());

		((JList)componentMap.get(ANSWERS_LIST)).setBorder(new CompoundBorder(new LineBorder(Color.GRAY), new EmptyBorder(1, 3, 1, 1)));

		componentMap.forEach(panel::add);
		

		spring.putConstraint(SpringLayout.WEST, componentMap.get(LB_ANSWERS), 10, SpringLayout.WEST, panel);
		spring.putConstraint(SpringLayout.WEST, componentMap.get(ANSWERS_LIST), 10, SpringLayout.WEST, panel);
		spring.putConstraint(SpringLayout.EAST, componentMap.get(ANSWERS_LIST), -10, SpringLayout.EAST, panel);
		spring.putConstraint(SpringLayout.WEST, componentMap.get(TXT_ADD), 10, SpringLayout.WEST, panel);
		spring.putConstraint(SpringLayout.EAST, componentMap.get(TXT_ADD), -10, SpringLayout.EAST, panel);
		spring.putConstraint(SpringLayout.WEST, componentMap.get(BTN_ADD), 0, SpringLayout.WEST, componentMap.get(LB_ANSWERS));
		spring.putConstraint(SpringLayout.WEST, componentMap.get(BTN_SAVE), 0, SpringLayout.WEST, componentMap.get(LB_ANSWERS));
		spring.putConstraint(SpringLayout.WEST, componentMap.get(LB_CORRECT_ANSWER), 0, SpringLayout.WEST, componentMap.get(LB_ANSWERS));
		spring.putConstraint(SpringLayout.WEST, componentMap.get(BTN_REMOVE), 10, SpringLayout.EAST, componentMap.get(BTN_ADD));
		spring.putConstraint(SpringLayout.WEST, componentMap.get(CB_CORRECT_ANSWER), 10, SpringLayout.WEST, panel);
		spring.putConstraint(SpringLayout.EAST, componentMap.get(CB_CORRECT_ANSWER), -10, SpringLayout.EAST, panel);
		spring.putConstraint(SpringLayout.WEST, componentMap.get(BTN_CLOSE), 10, SpringLayout.EAST, componentMap.get(BTN_SAVE));

		spring.putConstraint(SpringLayout.NORTH, componentMap.get(LB_ANSWERS), 10, SpringLayout.NORTH, panel);
		spring.putConstraint(SpringLayout.NORTH, componentMap.get(ANSWERS_LIST), 10, SpringLayout.SOUTH, componentMap.get(LB_ANSWERS));
		spring.putConstraint(SpringLayout.SOUTH, componentMap.get(TXT_ADD), -5, SpringLayout.NORTH, componentMap.get(BTN_ADD));
		spring.putConstraint(SpringLayout.SOUTH, componentMap.get(BTN_ADD), -10, SpringLayout.NORTH, componentMap.get(LB_CORRECT_ANSWER));
		spring.putConstraint(SpringLayout.SOUTH, componentMap.get(BTN_REMOVE), -10, SpringLayout.NORTH, componentMap.get(LB_CORRECT_ANSWER));
		spring.putConstraint(SpringLayout.SOUTH, componentMap.get(BTN_SAVE), -10, SpringLayout.SOUTH, panel);
		spring.putConstraint(SpringLayout.SOUTH, componentMap.get(BTN_CLOSE), -10, SpringLayout.SOUTH, panel);
		spring.putConstraint(SpringLayout.SOUTH, componentMap.get(CB_CORRECT_ANSWER), -10, SpringLayout.NORTH, componentMap.get(BTN_SAVE));
		spring.putConstraint(SpringLayout.SOUTH, componentMap.get(LB_CORRECT_ANSWER), -5, SpringLayout.NORTH, componentMap.get(CB_CORRECT_ANSWER));
		spring.putConstraint(SpringLayout.SOUTH, componentMap.get(ANSWERS_LIST), -10, SpringLayout.NORTH, componentMap.get(TXT_ADD));
		
		return panel;
	}
	
	private JPanel makeOpenPanel() {
		JPanel panel = new JPanel();
		
		return panel;
	}
}
