package com.softwave.core.ui.component;

import javax.swing.Action;
import javax.swing.Icon;
import javax.swing.JButton;

/**
 * Wrapper class for a JButton class that is provided by the jdk.
 * 
 * @author nicolas
 */
public class Button extends JButton {

	/**
	 * Default constructor for the Button object.
	 */
	public Button() {
	}

	/**
	 * Constructor for a button object that will display an icon on the screen.
	 * 
	 * @param icon the icon displayed inside the button.
	 */
	public Button(Icon icon) {
		super(icon);
	}

	/**
	 * constructor for a button object that will display text on the screen.
	 * 
	 * @param text the text displayed inside the button.
	 */
	public Button(String text) {
		super(text);
	}

	/**
	 * Constructor for a button object where the properties of the button are taken
	 * from the action object.
	 * 
	 * @param a the action object that gives info about the button.
	 */
	public Button(Action a) {
		super(a);
	}

	/**
	 * Constructor for a button that will display text and an icon on the screen.
	 * 
	 * @param text the text displayed by the button.
	 * @param icon the icon displayed by the button.
	 */
	public Button(String text, Icon icon) {
		super(text, icon);
	}

}
