package com.softwave.core.ui.component;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import com.softwave.core.container.quiz.Question;
import com.softwave.core.ui.QuestionDetailPanel;
import com.softwave.core.ui.event.RemoveQuestionEvent;
import com.softwave.core.ui.event.SwitchViewEvent;
import com.softwave.core.util.Translator;

/**
 * This panel gives a graphical representation of the questions in a quiz overview
 * in a gui, all the questions will be shows as a list item with there name display,
 * together with a button to open the details of that specific question and a remote
 * button to delete it from the list.
 * 
 * @author nicolas
 */
public class QuestionPanel extends JPanel {
	/** This question represented by the panel. */
	private Question question;
	
	/** name of the panel aka panel with the name of the question */
	private JPanel namePanel;
	
	/** panel with the control buttons for the question */ 
	private JPanel buttonPanel;

	/** the name of the question */
	private JLabel questionName;
	
	/** the button to open the properties view of the question */
	private JButton btnOpen;
	
	/** the button to remote the question from the current quiz */
	private JButton btnRemove;

	/**
	 * The constructor for the question panel to put inside the questions list view.
	 * 
	 * @param question the question holding the info to display in the list.
	 */
	public QuestionPanel(Question question) {
		if (question != null && !question.getName().isEmpty()) {
			this.question = question;
			questionName = new JLabel(question.getName());
		} else {
			questionName = new JLabel("untitled");
		}

		this.setPreferredSize(new Dimension(100, 30));
		init();
	}

	/**
	 * Initializes the panel with the correct layouts and panels to display to corrent
	 * info about the question.
	 */
	protected void init() {
		this.setLayout(new GridLayout(1, 2));

		// TODO make this come out of the db

		namePanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
		buttonPanel = new JPanel(new FlowLayout(FlowLayout.RIGHT));

		btnOpen = new LinkButton(new QuestionDetailPanel(this.question), Translator.translate("button.managequiz.open"));
		btnRemove = new JButton(Translator.translate("button.managequiz.remove"));

		btnOpen.addActionListener(new SwitchViewEvent(this.question));
		btnRemove.addActionListener(new RemoveQuestionEvent(question));
		
		namePanel.add(questionName);
		buttonPanel.add(btnOpen);
		buttonPanel.add(btnRemove);

		this.add(namePanel);
		this.add(buttonPanel);
	}
}