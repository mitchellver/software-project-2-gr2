package com.softwave.core.ui;

import java.awt.Dimension;

import javax.swing.BoxLayout;
import javax.swing.JFrame;

import com.softwave.core.container.GlobalState;
import com.softwave.core.util.Language;
import com.softwave.core.util.Translator;

public class QuizDisplay extends Window {

	private WindowManager manager;

	public QuizDisplay(String title, Dimension dim) {
		super(title, dim);
		init();
	}

	@Override
	protected void init() {

		Translator.setLanguage(Language.ENGLISH);
		manager = WindowManager.getInstance();
		// manager.attachWindow(this);

		SingleQuestionPanel questionpanel = new SingleQuestionPanel(GlobalState.getCurrentQuiz(),
				GlobalState.getClient());

		this.setMinimumSize(new Dimension(800, 600));
		// this.setMaximumSize(new Dimension(800, 600));
		// this.setResizable(false);
		// this.setBounds(100, 100, 450, 300);
		this.setLocationRelativeTo(null);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.getContentPane().setLayout(new BoxLayout(this.getContentPane(), BoxLayout.Y_AXIS));

		if (!manager.registerView(questionpanel)) {
			System.out.println("panel is null!");
		}
		// TODO this needs to be fixed, we shouldn't create a new Window ?
		// this.setVisible(true);
	}

	public static void main(String[] args) {
		// this would come out of designConf object
		Dimension dim = new Dimension(800, 600);
		QuizDisplay quiz = new QuizDisplay("Quiz", dim);
		// quiz.setVisible(true);

	}

}