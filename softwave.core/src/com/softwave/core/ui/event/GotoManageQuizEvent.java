package com.softwave.core.ui.event;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import com.softwave.core.logic.QuizManagementHandler;

public class GotoManageQuizEvent implements ActionListener {
	@Override
	public void actionPerformed(ActionEvent e) {
		QuizManagementHandler.loadQuizManagementPanel();
	}
}
