package com.softwave.core.ui.event;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Map;

import javax.swing.JButton;
import javax.swing.JComponent;

public abstract class DesignConfigAbsorptionEvent implements ActionListener {
	private Map<String, JComponent> componentMap;

	public DesignConfigAbsorptionEvent(Map<String, JComponent> componentMap) {
		setComponentMap(componentMap);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() instanceof JButton) {
			getData(componentMap);
		}
	}

	public abstract void getData(Map<String, JComponent> e);

	public Map<String, JComponent> getComponentMap() {
		return componentMap;
	}

	public void setComponentMap(Map<String, JComponent> componentMap) {
		if (componentMap == null) {
			return;
		}

		this.componentMap = componentMap;
	}

}
