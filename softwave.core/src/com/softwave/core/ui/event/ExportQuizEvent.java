package com.softwave.core.ui.event;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;

import com.softwave.core.container.quiz.Quiz;
import com.softwave.core.logic.QuizManagementHandler;
import com.softwave.core.util.Translator;

public class ExportQuizEvent implements ActionListener {
	private Quiz quiz;
	
	public ExportQuizEvent(Quiz quiz) {
		if(quiz == null)
			throw new NullPointerException("The quiz is null");
		this.quiz = quiz;
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		QuizManagementHandler.saveQuizOffline(quiz);
		JOptionPane.showMessageDialog(null, Translator.translate("message.quiz.export.success"));
	}

}
