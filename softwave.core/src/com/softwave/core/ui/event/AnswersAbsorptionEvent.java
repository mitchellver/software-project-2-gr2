package com.softwave.core.ui.event;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import javax.swing.DefaultListModel;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JList;
import javax.swing.JTextField;

import com.softwave.core.container.GlobalState;
import com.softwave.core.container.quiz.Answer;
import com.softwave.core.container.quiz.Question;
import com.softwave.core.ui.component.AnswersFrame;

/**
 * This class will handle all the button events of the Answersframe class.
 * 
 * @author nicolas
 */
public class AnswersAbsorptionEvent implements ActionListener {
	/**
	 * The map with components that are placed on the frame and hold the info,
	 * the key to identify then
	 */
	private Map<String, JComponent> componentMap;
	
	/** The elements and other info needed by the list to displqy its elements */
	private DefaultListModel model;
	
	/** The question to put the values in to */
	private Question question;
	
	/**
	 * Constructor to create the event object responsible for extracting info from
	 * the panel/frame.
	 * 
	 * @param componentMap the map of components that hold the data needed to extract,
	 * @param model the model with the data that will be dislayed by the list.
	 * @throws a nullpointerexception if the map of components is null.
	 */
	public AnswersAbsorptionEvent(Map<String, JComponent> componentMap, DefaultListModel model) throws NullPointerException {
		if (componentMap != null) {
			this.componentMap = componentMap;
		} else {
			System.out.println("[anwers] null of componentMap");
			throw new NullPointerException();
		}
		
		if (model != null) {
			this.model = model;
		} else {
			this.model = new DefaultListModel();
			((JList)this.componentMap.get(AnswersFrame.ANSWERS_LIST)).setModel(this.model);
		}
	}

	/**
	 * This method will handle the things that need to happen if the event is fired,
	 * it will extract all the data out of the frame/panel.
	 * 
	 * @param e the actionevent that is the original creation of the event.
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
//		System.out.println("[anwers] action registerd");
		if (e.getSource().equals(componentMap.get(AnswersFrame.BTN_ADD))) {
			addItemToModel();
		} else if (e.getSource().equals(componentMap.get(AnswersFrame.BTN_REMOVE))) {
			removeItemFromModel();
		} else if (e.getSource().equals(componentMap.get(AnswersFrame.BTN_SAVE))) {
			saveAnswers();
//			System.out.println("[anwers] button save pressed!");
		}
	}

	/**
	 * This method will handle the extraction of the info when an item is added
	 * to the list of currect answers.
	 */
	private void addItemToModel() {
		JTextField txt = (JTextField)componentMap.get(AnswersFrame.TXT_ADD);
//		System.out.println("[anwers] button add pressed!");
		if (!txt.getText().trim().isEmpty()) {
//			System.out.println("[anwers] reading and processing value of textbox!");
			this.model.addElement(txt.getText().trim());
			((JComboBox)componentMap.get(AnswersFrame.CB_CORRECT_ANSWER)).addItem(txt.getText().trim());
		}
	}
	
	/**
	 * This method will handle the extractor of the info when one or more answers
	 * are removed from the list, all the selected items will be removed from the
	 * list model.
	 */
	private void removeItemFromModel() {
//		System.out.println("[anwers] button add pressed!");
		JList list = (JList)componentMap.get(AnswersFrame.ANSWERS_LIST);
		if (!list.isSelectionEmpty()) {
			List<Object> values = Arrays.asList(((JList)componentMap.get(AnswersFrame.ANSWERS_LIST)).getSelectedValues());
			JComboBox box = (JComboBox)componentMap.get(AnswersFrame.CB_CORRECT_ANSWER);
//			System.out.println("[anwers] reading and processing value of textbox!");
			values.stream().forEach(this.model::removeElement);
			values.stream().forEach(box::removeItem);
		}
	}
	
	/**
	 * This method will handle the event if the save button is pressed, it will
	 * extract the data out of all the components of the panel/frame and give them
	 * back to the current question being modified.
	 */
	private void saveAnswers() {
		if (componentMap != null) {
			ArrayList<String> answers = new ArrayList<>();
			String correctAnswer = null;
			if (componentMap.get(AnswersFrame.ANSWERS_LIST) instanceof JList) {
				JList list = (JList)componentMap.get(AnswersFrame.ANSWERS_LIST);
				for (int i = 0; i < list.getModel().getSize(); i++) {
					answers.add((String)list.getModel().getElementAt(i));
				}
			}
			
			if (componentMap.get(AnswersFrame.CB_CORRECT_ANSWER) instanceof JComboBox) {
				JComboBox cb = (JComboBox)componentMap.get(AnswersFrame.CB_CORRECT_ANSWER);
				correctAnswer = (String)cb.getSelectedItem();
			}
			question.setCorrectAnswer(correctAnswer);
			question.addPossibleAnswer(answers);
			System.out.println("question saved with: " + question.getCorrectAnswer());
			System.out.println(GlobalState.getCurrentQuestion().getCorrectAnswer());
		}
	}
	
	/**
	 * Getter method for the question used by the answers event.
	 * 
	 * @return the question used by the event.
	 */
	public Question getQuestion() {
		return question;
	}
	
	/**
	 * Setter that sets the question where the event 
	 */
	public void setQuestion() {
		if (GlobalState.getCurrentQuestion() != null) {
			this.question = GlobalState.getCurrentQuestion();
		}
	}
	
	public void setQuestion(Question question) {
		if (question != null) {
			this.question = question;
		}
	}
}
