package com.softwave.core.ui.event;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;

import com.softwave.core.container.GlobalState;
import com.softwave.core.logic.AuthenticationHandler;
import com.softwave.core.logic.QuizManagementHandler;
import com.softwave.core.util.Translator;

/**
 * This is the logout event, it is triggered when a user clicks on e.g. the
 * logout button. it checks if things need to be stored before we can logout and
 * clear the cached data.
 * 
 * @author eddi
 *
 */
public class LogoutEvent implements ActionListener {

	/**
	 * this is the action performed whe the user wants to logout it checks if
	 * things need to be stored and calls the appropriate methods
	 */
	@Override
	public void actionPerformed(ActionEvent e) {

		// check if we first need to store things that where changed
		if (GlobalState.isStateChanged()) {
			// show a dialog to ask the user what todo
			int choice = JOptionPane.showConfirmDialog(null, Translator.translate("confirm.state.save.changes"),
					Translator.translate("confirm.state.logout"), JOptionPane.YES_NO_CANCEL_OPTION);
		
			if (choice == JOptionPane.YES_OPTION) {
				QuizManagementHandler.saveQuizesToDatabase();
			} else if (choice == JOptionPane.CANCEL_OPTION) {
				return;
			}
		}
		// TODO check if a participation has changed and needs to be stored

		// if all is done, logout
		AuthenticationHandler.logout();
	}

}
