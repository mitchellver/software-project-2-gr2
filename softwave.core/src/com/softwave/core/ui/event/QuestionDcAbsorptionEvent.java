package com.softwave.core.ui.event;

import java.util.ArrayList;
import java.util.Map;

import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import com.softwave.core.container.design.DesignConfig;
import com.softwave.core.container.quiz.Question;
import com.softwave.core.ui.DesignConfigDetailPanel;
import com.softwave.core.ui.DesignConfigPanelValidator;
import com.softwave.core.ui.QuestionDetailPanel;
import com.softwave.core.ui.WindowManager;
import com.softwave.core.util.FontEnum;
import com.softwave.core.util.FontStyleEnum;

/**
 * This Event is fired when a user clicks on the save button in the designconfig panel.
 * It get's the relevant fields out of the panel and stores it inside a DesignConfig, updating the question's DesignConfig.
 * 
 * @author eddi
 *
 */
public class QuestionDcAbsorptionEvent extends DesignConfigAbsorptionEvent {
	private Question question;	
	public QuestionDcAbsorptionEvent(Map<String, JComponent> componentMap, Question question) {
		super(componentMap);
		if(question == null)
			throw new NullPointerException("The question is null");
		this.question = question;
		if(question.getDesignConfig() == null)
			question.setDesignConfig(new DesignConfig());
	}

	@Override
	public void getData(Map<String, JComponent> e) {
		DesignConfig dc = question.getDesignConfig();
		ArrayList<String> errors = new ArrayList<>();

		String fontStr = "";
		String fontStyle = "";
		int fontSize = 12;
		String red = "";
		String green = "";
		String blue = "";
		String path = "";

		// get the logo info
		// TODO maybe only implement a file selector
		if (e.get(DesignConfigDetailPanel.TXT_LOGO) != null
				&& e.get(DesignConfigDetailPanel.TXT_LOGO) instanceof JTextField) {
			path = ((JTextField) e.get(DesignConfigDetailPanel.TXT_LOGO)).getText().trim();
			if (path.isEmpty())
				errors.add(DesignConfigDetailPanel.TXT_LOGO + ": has no text");
		}

		// get the font info
		if (e.get(DesignConfigDetailPanel.CB_FONT) != null
				&& e.get(DesignConfigDetailPanel.CB_FONT) instanceof JComboBox<?>) {
			fontStr = (String) ((FontEnum) ((JComboBox<FontEnum>) e.get(DesignConfigDetailPanel.CB_FONT))
					.getSelectedItem()).name();
			if (fontStr.isEmpty())
				errors.add(DesignConfigDetailPanel.CB_FONT + ": has no text");
		}

		if (e.get(DesignConfigDetailPanel.CB_FONT_STYLE) != null
				&& e.get(DesignConfigDetailPanel.CB_FONT_STYLE) instanceof JComboBox<?>) {
			fontStyle = (String) ((FontStyleEnum) ((JComboBox<FontStyleEnum>) e
					.get(DesignConfigDetailPanel.CB_FONT_STYLE)).getSelectedItem()).name();
			if (fontStyle.isEmpty())
				errors.add(DesignConfigDetailPanel.CB_FONT_STYLE + ": has no text");
		}
		if (e.get(DesignConfigDetailPanel.CB_FONT_SIZE) != null
				&& e.get(DesignConfigDetailPanel.CB_FONT_SIZE) instanceof JComboBox<?>) {
			fontSize = (int) ((JComboBox<Integer>) e.get(DesignConfigDetailPanel.CB_FONT_SIZE)).getSelectedItem();
			if (fontStyle.isEmpty())
				errors.add(DesignConfigDetailPanel.CB_FONT_SIZE + ": has no text");
		}

		// get the color info
		if (e.get(DesignConfigDetailPanel.TXT_RED) != null
				&& e.get(DesignConfigDetailPanel.TXT_RED) instanceof JTextField) {
			red = ((JTextField) e.get(DesignConfigDetailPanel.TXT_RED)).getText().trim();
			if (red.isEmpty())
				errors.add(DesignConfigDetailPanel.TXT_RED + ": has no text");
		}
		if (e.get(DesignConfigDetailPanel.TXT_GREEN) != null
				&& e.get(DesignConfigDetailPanel.TXT_GREEN) instanceof JTextField) {
			green = ((JTextField) e.get(DesignConfigDetailPanel.TXT_GREEN)).getText().trim();
			if (green.isEmpty())
				errors.add(DesignConfigDetailPanel.TXT_GREEN + ": has no text");
		}
		if (e.get(DesignConfigDetailPanel.TXT_BLUE) != null
				&& e.get(DesignConfigDetailPanel.TXT_BLUE) instanceof JTextField) {
			blue = ((JTextField) e.get(DesignConfigDetailPanel.TXT_RED)).getText().trim();
			if (blue.isEmpty())
				errors.add(DesignConfigDetailPanel.TXT_BLUE + ": has no text");
		}

		String msg = "Some fields have invalid value, please check the following fields\n";

		if (errors.isEmpty()) {

			try {
				dc.setFont(DesignConfigPanelValidator.validateFont(fontStr, fontStyle, fontSize));
				dc.setBackgroundColor(DesignConfigPanelValidator.validateColor(red, green, blue));
				// the logo is set by the chooseFileEvent

			} catch (Exception e1) {
				e1.printStackTrace();
				JOptionPane.showMessageDialog(null, "Try again: " + e1.getMessage());
			}

		} else {
			for (String error : errors) {
				msg += " - " + error + "\n";
			}
			JOptionPane.showMessageDialog(WindowManager.getInstance().getWindow(), msg, "Invalid fields",
					JOptionPane.OK_OPTION);
		}

		// now save the designconfig in the question
		question.setDesignConfig(dc);	
		WindowManager.getInstance().requestPreviousView();
		return;
	}

}
