package com.softwave.core.ui.event;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;

import com.softwave.core.container.GlobalState;
import com.softwave.core.logic.QuizManagementHandler;
import com.softwave.core.util.Translator;

public class QuitEvent implements ActionListener {

	@Override
	public void actionPerformed(ActionEvent e) {

		// check if we first need to store things that where changed
		if (GlobalState.isStateChanged()) {
			// show a dialog to ask the user what todo
			int choice = JOptionPane.showConfirmDialog(null, Translator.translate("confirm.state.save.changes"),
					"confirm.state.logout", JOptionPane.YES_NO_CANCEL_OPTION);
		
			if (choice == JOptionPane.YES_OPTION) {
				QuizManagementHandler.saveQuizesToDatabase();
			} else if (choice == JOptionPane.CANCEL_OPTION) {
				return;
			}
		}
		// TODO check if a participation has changed and needs to be stored
		
		// TODO check if we need to store something offline
		
		// quit the application
		System.exit(0);
	}

}
