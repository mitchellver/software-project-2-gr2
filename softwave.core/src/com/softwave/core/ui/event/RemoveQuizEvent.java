package com.softwave.core.ui.event;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;

import com.softwave.core.container.quiz.Quiz;
import com.softwave.core.logic.QuizManagementHandler;
import com.softwave.core.util.Translator;

public class RemoveQuizEvent implements ActionListener {
	private Quiz quiz = null;

	// public RemoveQuizEvent(){}
	// this way we are sure the right quiz will be deleted
	public RemoveQuizEvent(Quiz quiz) {
		// GlobalState.setCurrentQuiz(quiz);
		this.quiz = quiz;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (JOptionPane.OK_OPTION == JOptionPane.showConfirmDialog(null, Translator.translate("confirm.quiz.remove.message"), Translator.translate("confirm.quiz.remove.title"),
				JOptionPane.OK_CANCEL_OPTION)) {
			QuizManagementHandler.removeQuizFromDatabase(quiz);
			QuizManagementHandler.loadQuizManagementPanel();
		}
	}
}
