package com.softwave.core.ui.event;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import com.softwave.core.container.GlobalState;

public class StateChangedEvent implements ActionListener{

	@Override
	public void actionPerformed(ActionEvent e) {
		GlobalState.setStateChanged(true);
	}
	

}
