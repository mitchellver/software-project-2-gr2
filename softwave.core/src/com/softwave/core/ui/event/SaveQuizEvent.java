package com.softwave.core.ui.event;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import com.softwave.core.container.GlobalState;
import com.softwave.core.container.quiz.Quiz;
import com.softwave.core.logic.QuizManagementHandler;

public class SaveQuizEvent implements ActionListener{
	Quiz quiz = null;
	
	public SaveQuizEvent(){}
	public SaveQuizEvent(Quiz quiz) {
		this.quiz = quiz;
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		// check if we have an unadded quiz in the currentQuiz
		if (!GlobalState.getQuizzes().contains(GlobalState.getCurrentQuiz()))
			GlobalState.getQuizzes().add(GlobalState.getCurrentQuiz());
		if(quiz == null)
			QuizManagementHandler.saveQuizesToDatabase();
		else
			QuizManagementHandler.saveQuizToDatabase(quiz);
		QuizManagementHandler.loadQuizManagementPanel();
	}
}
