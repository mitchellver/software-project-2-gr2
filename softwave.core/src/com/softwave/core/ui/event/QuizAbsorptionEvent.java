package com.softwave.core.ui.event;

import java.awt.event.ActionEvent;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.ArrayList;
import java.util.Map;
import java.util.Map;

import javax.swing.JButton;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.JOptionPane;
import javax.swing.JTextArea;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import com.softwave.core.container.GlobalState;
import com.softwave.core.container.quiz.Quiz;
import com.softwave.core.container.quiz.Quiz;
import com.softwave.core.logic.QuizManagementHandler;
import com.softwave.core.ui.QuizDetailPanel;
import com.softwave.core.ui.QuizDetailPanel;
import com.softwave.core.ui.WindowManager;
import com.softwave.core.ui.WindowManager;

public class QuizAbsorptionEvent implements ActionListener {
	private Map<String, JComponent> componentMap;

	public QuizAbsorptionEvent(Map<String, JComponent> componentMap) {
		setComponentMap(componentMap);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() instanceof JButton) {
			getData(componentMap);
		}
	}

	private void getData(Map<String, JComponent> e) {
		Quiz quiz = GlobalState.getCurrentQuiz();

		ArrayList<String> errors = new ArrayList<>();

		String name = "";
		String description = "";

		if (e.get(QuizDetailPanel.TXT_QUIZ_NAME) != null
				&& e.get(QuizDetailPanel.TXT_QUIZ_NAME) instanceof JTextField) {
			name = ((JTextField) e.get(QuizDetailPanel.TXT_QUIZ_NAME)).getText().trim();
			if (name.isEmpty())
				errors.add(QuizDetailPanel.TXT_QUIZ_NAME + ": has no text");
		}

		if (e.get(QuizDetailPanel.TXT_DESCRIPTION) != null
				&& e.get(QuizDetailPanel.TXT_DESCRIPTION) instanceof JTextArea) {
			description = ((JTextArea) e.get(QuizDetailPanel.TXT_DESCRIPTION)).getText().trim();
		}

		String msg = "Some fields have invalid value, please check the following fields\n";

		if (errors.isEmpty()) {
			quiz.setName(name);
			quiz.setDescription(description);
		} else {
			for (String error : errors) {
				msg += " - " + error + "\n";
			}
			JOptionPane.showMessageDialog(WindowManager.getInstance().getWindow(), msg, "Invalid fields",
					JOptionPane.OK_OPTION);
		}
		// check if it is a new quiz and save the quiz to the cache
		// if (!GlobalState.getQuizzes().contains(quiz))
		// GlobalState.getQuizzes().add(quiz);
		GlobalState.setCurrentQuiz(quiz);

		return;
	}

	public Map<String, JComponent> getComponentMap() {
		return componentMap;
	}

	public void setComponentMap(Map<String, JComponent> componentMap) {
		if (componentMap == null) {
			return;
		}

		this.componentMap = componentMap;
	}
}
