package com.softwave.core.ui.event;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JOptionPane;

import com.softwave.core.container.GlobalState;
import com.softwave.core.container.quiz.Participation;
import com.softwave.core.logic.ParticipationHandler;
import com.softwave.core.logic.QuizManagementHandler;
import com.softwave.core.ui.IntroPanel;
import com.softwave.core.ui.Window;
import com.softwave.core.ui.WindowManager;
import com.softwave.core.util.Translator;

public class SubmitParticipationEvent implements ActionListener {
	Participation participation;

	ArrayList<Integer> requiredUncompleted = new ArrayList<Integer>();

	public SubmitParticipationEvent(Participation participation) {
		super();
		this.participation = participation;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		int choice = JOptionPane.showConfirmDialog(null, Translator.translate("message.participation.submit.sure"),
				"Submit", JOptionPane.YES_NO_OPTION);
		if (choice == JOptionPane.YES_OPTION) {

			if (ParticipationHandler.submitParticipation()) {
				JOptionPane.showMessageDialog(null, Translator.translate("message.participation.submit.success"));
				WindowManager.getInstance().registerView(new IntroPanel());
			} else {
				JOptionPane.showMessageDialog(null, Translator.translate("message.participation.submit.unanswered"));
			}
		}
	}

}
