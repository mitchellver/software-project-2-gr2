package com.softwave.core.ui.event;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JComboBox;

import com.softwave.core.util.Language;
import com.softwave.core.util.Translator;

public class ChangeLanguageEvent implements ActionListener {

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() instanceof JComboBox<?>) {
			JComboBox<Language> combo = (JComboBox<Language>) e.getSource();
			Translator.changeLanguage(Language.values()[combo.getSelectedIndex()]);
		}
	}

}
