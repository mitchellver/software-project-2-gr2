package com.softwave.core.ui.event;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;

import com.softwave.core.ui.WindowManager;

/**
 * This class will handle all the events that transform the panel viewed by the
 * user
 * 
 * @author nicolas
 * @version 1.0
 */
public class ManipulateViewEvent implements ActionListener {

	/**
	 * Handles the event of a button that is clicked to change the view content.
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() instanceof JButton) {
			WindowManager.getInstance().manipulate(WindowManager.HIDE_SIDEBAR);
		}
	}

}
