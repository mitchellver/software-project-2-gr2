package com.softwave.core.ui.event;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.Map;

import javax.swing.JComponent;
import javax.swing.JTextField;

import com.softwave.core.ui.DesignConfigDetailPanel;
import com.softwave.core.util.FileChooser;

public class ChooseLogoEvent implements ActionListener {
	private Map<String, JComponent> componentMap;
	
	public ChooseLogoEvent(Map<String, JComponent> componentMap) {
		this.componentMap = componentMap;
	}
	@Override
	public void actionPerformed(ActionEvent e) {
		// call the method that opens the fileChooser popup
		File file = FileChooser.chooseLogo();

		// set the logo in the textField
		if (componentMap != null)
			((JTextField) componentMap.get(DesignConfigDetailPanel.TXT_LOGO)).setText(file.getPath());
	}
}
