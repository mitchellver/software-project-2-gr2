package com.softwave.core.ui.event;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.Map;

import javax.swing.JComponent;
import javax.swing.JTextField;

import com.softwave.core.container.quiz.Quiz;
import com.softwave.core.ui.DcQuizDetailPanel;
import com.softwave.core.util.FileChooser;

public class ChooseQuizLogoEvent implements ActionListener {
	private Map<String, JComponent> componentMap;
	private Quiz quiz;
	
	public ChooseQuizLogoEvent(Map<String, JComponent> componentMap, Quiz quiz) {
		if (componentMap == null)
			throw new NullPointerException("The componentmap is null");
		if (quiz == null)
			throw new NullPointerException("The quiz is null");
		this.componentMap = componentMap;
		this.quiz = quiz;
	}


	@Override
	public void actionPerformed(ActionEvent e) {
		// call the method that opens the fileChooser popup
		File file = FileChooser.chooseLogo();
		if (file != null) {
			// TODO check if we have a correct extention, and other sanity
			// checks
			// TODO maybe put the sanity checks in another class (FileChooser)
			
			// create a location to save the file to and the filename
			String saveLocation = FileChooser.PATH_START + "quiz_" + quiz.getQuizID() + File.separator;

			// update logopath txt in panel
				((JTextField) componentMap.get(DcQuizDetailPanel.TXT_LOGO)).setText(saveLocation);

			// copy the file to the application
			FileChooser.copyToApplication(file, saveLocation);
			
			// add the new logo to the quiz
			quiz.getDesignConfig().setLogoFile(saveLocation + file.getName());
		}
	}
}
