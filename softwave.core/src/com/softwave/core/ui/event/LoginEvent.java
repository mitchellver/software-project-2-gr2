package com.softwave.core.ui.event;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JLabel;
import javax.swing.JTextField;

import com.softwave.core.logic.AuthenticationHandler;
import com.softwave.core.util.Translator;

/**
 * This is the Actionlistener for the login button on the loginpanel. It calls
 * the UserDAO to check if a given username and password are correct.
 * 
 * @author eddi
 *
 */
public class LoginEvent implements ActionListener, KeyListener {
	/** The given username */
	JTextField username;
	/** The given password */
	JTextField password;
	/**
	 * a label right unde the textfields, to tell the user that the login failed
	 */
	JLabel failedMessage;

	/**
	 * This is the constructor, it receives the parameters from the loginpanel
	 * 
	 * @param username
	 *            the username given by the user
	 * @param password
	 *            the password (unhashed) by the user
	 * @param failedMessage
	 *            the label where we can tell the user the login failed
	 */
	public LoginEvent(JTextField username, JTextField password, JLabel failedMessage) {
		// TODO check on null values
		// throw nullpointerexception
		if (username == null || password == null || failedMessage == null)
			throw new NullPointerException();
		this.username = username;
		this.password = password;
		this.failedMessage = failedMessage;
	}

	/**
	 * this is the actual code that gets called when the user clicks on the
	 * login-button. it calls the UserDAO and checks if the given username and
	 * password match a user in the database. If so, the ControlDashboardPanel
	 * is shown. If the user is not found in the database / username-password is
	 * not correct the label is filled in showing the user that the login
	 * failed.
	 * 
	 * @Param e captures the event and contains info about the event
	 * 
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		// check if the username and password are filled in
		if (!username.getText().trim().isEmpty() && !password.getText().trim().isEmpty()) {
			AuthenticationHandler.login(username.getText().trim(), password.getText().trim(), failedMessage);

		} else
			// show a red login failed message to the user on the loginpanel
			failedMessage.setText(Translator.translate("label.login.failed"));
	}

	/**
	 * this methode checks if the user press enter in the password field that
	 * the authenticationhandler runs (does the same as the methode
	 * actionPerformed)
	 * 
	 * @param e captures the event and contains info about the event
	 * 
	 */
	@Override
	public void keyPressed(KeyEvent e) {
		if (e.getKeyCode() == KeyEvent.VK_ENTER) {
			if (!username.getText().trim().isEmpty() && !password.getText().trim().isEmpty()) {
				AuthenticationHandler.login(username.getText().trim(), password.getText().trim(), failedMessage);
			} else {
				// show a red login failed message to the user on the loginpanel
				failedMessage.setText(Translator.translate("label.login.failed"));
			}
		}
	}

	/**
	 * this is a methode we don't use
	 * 
	 * @param e captures the event and contains info about the event
	 * 
	 */
	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub

	}

	/**
	 * this is a methode we don't use
	 * 
	 * @param e captures the event and contains info about the event
	 * 
	 */
	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub

	}
}
