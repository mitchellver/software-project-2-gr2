package com.softwave.core.ui.event;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.Map;

import javax.swing.JComponent;
import javax.swing.JTextField;

import com.softwave.core.container.GlobalState;
import com.softwave.core.container.user.account.KnownUser;
import com.softwave.core.ui.DesignConfigDetailPanel;
import com.softwave.core.util.FileChooser;

public class ChooseUserLogoEvent implements ActionListener {
	private Map<String, JComponent> componentMap;
	private KnownUser kUser;
	
	public ChooseUserLogoEvent(Map<String, JComponent> componentMap, KnownUser kUser) {
		if(componentMap == null)
			throw new NullPointerException("The componentmap is null");
		this.componentMap = componentMap;
		
		if(kUser == null)
			throw new NullPointerException("The user is null");
		this.kUser = kUser;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// call the method that opens the fileChooser popup
		File file = FileChooser.chooseLogo();
		if (file != null) {
			// TODO check if we have a correct extention, and other sanity
			// checks
			// TODO maybe put the sanity checks in another class (FileChooser)

			// create a location to save the file to and the filename
			String saveLocation = FileChooser.PATH_START + "user_" + GlobalState.getClient().getUserID() + File.separator;

			// update logopath txt in panel
			((JTextField) componentMap.get(DesignConfigDetailPanel.TXT_LOGO)).setText(saveLocation);

			// copy the file to the application
			FileChooser.copyToApplication(file, saveLocation);
			
			// add the new logo to the user
			kUser.getDesignConfig().setLogoFile(saveLocation + file.getName());
		}
	}
}
