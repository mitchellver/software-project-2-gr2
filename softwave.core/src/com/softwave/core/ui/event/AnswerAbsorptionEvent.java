package com.softwave.core.ui.event;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Enumeration;

import javax.swing.AbstractButton;
import javax.swing.ButtonGroup;
import javax.swing.JOptionPane;

import com.softwave.core.container.GlobalState;
import com.softwave.core.container.quiz.Answer;
import com.softwave.core.container.quiz.Participation;
import com.softwave.core.container.quiz.Question;
import com.softwave.core.logic.ParticipationHandler;
import com.softwave.core.ui.SingleQuestionPanel;
import com.softwave.core.ui.component.QButton;
import com.softwave.core.util.Date;

public class AnswerAbsorptionEvent implements ActionListener {
	SingleQuestionPanel singlePanel;

	private Participation participation;
	ArrayList<QButton> buttons;
	private int nextQuestionNumber;
	private int currentQuestionNumber;

	public AnswerAbsorptionEvent(SingleQuestionPanel singlePanel, Participation participation,
			ArrayList<QButton> buttons, int nextQuestionNumber, int currentQuestionNumber) {
		if (participation == null)
			throw new NullPointerException("The participation is null");
		this.participation = participation;
		this.singlePanel = singlePanel;
		this.nextQuestionNumber = nextQuestionNumber;
		this.currentQuestionNumber = currentQuestionNumber;
		this.buttons = buttons;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// check if the answer exists
		if (nextQuestionNumber - 1 > participation.getAnswers().size()) {
			return;
		}
		Question currentQuestion = participation.getQuiz().getQuestions().get(currentQuestionNumber);
		Answer currentAnswer = GlobalState.getCurrentParticipation().getAnswers().get(currentQuestionNumber);

		// we need to do different tasks depending on the question type
		switch (currentQuestion.getType()) {
		case OPEN:
			String textareaAnswer = singlePanel.getTextAnswer().getText().trim();
			// if om te checken als antwoord gewijzigd moet worden of niet
			if (currentAnswer.getAnswer() == null || !currentAnswer.getAnswer().equals(textareaAnswer)) {
				currentAnswer.setAnswer(textareaAnswer);
				currentAnswer.setEndTime(new Date());
			}

			System.out.println(participation.getAnswers());

			//

			break;
		case YES_NO:
			// get the selected button
			String radioAnswer = "";
			ButtonGroup btnGroup = singlePanel.getRadioBtnAnswers();

			for (Enumeration<AbstractButton> buttons = btnGroup.getElements(); buttons.hasMoreElements();) {
				AbstractButton button = buttons.nextElement();

				if (button.isSelected()) {
					radioAnswer = button.getText();
				}
			}

			// if om te checken als antwoord gewijzigd moet worden of niet
			if (currentAnswer.getAnswer() == null || !currentAnswer.getAnswer().equals(radioAnswer)) {
				currentAnswer.setAnswer(radioAnswer);
				currentAnswer.setEndTime(new Date());
			}

			break;
		case MULTIPLE_CHOICE:
			break;
		default:
			System.out.println("This is an unknonw QType:" + currentQuestion.getType());
		}
		// go to the next question
		ParticipationHandler.gotoQuestion(nextQuestionNumber);
	}

}
