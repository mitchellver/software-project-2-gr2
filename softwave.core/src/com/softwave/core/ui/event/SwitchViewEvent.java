package com.softwave.core.ui.event;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;

import com.softwave.core.container.GlobalState;
import com.softwave.core.container.quiz.Question;
import com.softwave.core.container.quiz.Quiz;
import com.softwave.core.logic.QuestionManagementHandler;
import com.softwave.core.logic.QuizManagementHandler;
import com.softwave.core.ui.DesignConfigDetailPanel;
import com.softwave.core.ui.Panel;
import com.softwave.core.ui.QuestionDetailPanel;
import com.softwave.core.ui.QuestionManagementPanel;
import com.softwave.core.ui.QuizDetailPanel;
import com.softwave.core.ui.QuizManagementPanel;
import com.softwave.core.ui.WindowManager;
import com.softwave.core.ui.component.LinkButton;

/**
 * This class will handle all the button events that change the view of the
 * window using the window manager
 * 
 * @author nicolas
 * @version 1.0
 */
public class SwitchViewEvent implements ActionListener {
	private Quiz quiz;
	private Question question;

	/** Set the type of SwitchViewEvent */
	private final short todo;

	/**
	 * The default constructor of the object.
	 */
	public SwitchViewEvent() {
		todo = WindowManager.NOTHING;
	}

	/**
	 * The constructor that passes the currentQuestion.
	 */
	public SwitchViewEvent(Question question) {
		if (question == null)
			throw new NullPointerException("The question is null");
		this.question = question;
		GlobalState.setCurrentQuestion(question);

		todo = WindowManager.NOTHING;
	}

	/**
	 * The constructor that passes the currentQuiz.
	 */
	public SwitchViewEvent(Quiz quiz) {
		if (quiz == null)
			throw new NullPointerException("The quiz is null");
		this.quiz = quiz;
		GlobalState.setCurrentQuiz(quiz);

		todo = WindowManager.NOTHING;
	}

	/**
	 * The constructor of the object.
	 * 
	 * @param todo
	 *            tell what to do when the switch happens
	 */
	public SwitchViewEvent(short todo) {
		this.todo = todo;
	}

	/**
	 * Will run when the button is pressed and the event is fired.
	 * 
	 * @param e
	 *            the action event that is fired by the pressing of the button.
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		Component button = (Component) e.getSource();
		if (e.getSource() instanceof LinkButton) {
			LinkButton linkButton = (LinkButton) button;

			if (todo == WindowManager.NOTHING) {
				Panel p = linkButton.getLink();
				if (p == null) {
					return;
				} // a listener to go to the quizManagementPanel by eddi
				else if (p instanceof QuizManagementPanel) {
					GlobalState.setCurrentQuiz(quiz);
					QuizManagementHandler.loadQuizManagementPanel();
				} else if (p instanceof QuizDetailPanel) {
					GlobalState.setCurrentQuiz(quiz);
					WindowManager.getInstance().registerView(p);
				} else if (p instanceof QuestionManagementPanel) {
					GlobalState.setCurrentQuestion(question);
					QuestionManagementHandler.loadQuestionManagementPanel();
				} else if (p instanceof QuestionDetailPanel) {
					GlobalState.setCurrentQuestion(question);
					WindowManager.getInstance().registerView(p);
				}else if (p instanceof DesignConfigDetailPanel) {
					// GlobalState.setCurrentQuiz(quiz);
					WindowManager.getInstance().registerView(p);
				} else {
					WindowManager.getInstance().registerView(p);
				}
			}
		} else if (button instanceof JButton) {
			if (todo == WindowManager.PREVIOUS_VIEW) {
				WindowManager.getInstance().requestPreviousView();
			}
		}
	}
}
