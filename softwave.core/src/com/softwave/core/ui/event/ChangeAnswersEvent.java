package com.softwave.core.ui.event;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JComboBox;
import javax.swing.JComponent;

import com.softwave.core.container.GlobalState;
import com.softwave.core.container.quiz.QType;
import com.softwave.core.container.quiz.Question;
import com.softwave.core.ui.component.AnswersFrame;

public class ChangeAnswersEvent implements ActionListener {
	private final JComponent info;
	
	public ChangeAnswersEvent(JComponent info) {
		this.info = info;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (info instanceof JComboBox) {
			JComboBox control = (JComboBox)info;
			Question question = GlobalState.getCurrentQuestion();
			question.setType(QType.values()[control.getSelectedIndex()]);
			AnswersFrame answersFrame = new AnswersFrame("Answers", new Dimension(400, 300), question);
		}
	}
}
