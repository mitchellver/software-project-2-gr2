package com.softwave.core.ui.event;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import com.softwave.core.container.quiz.Question;
import com.softwave.core.logic.QuestionManagementHandler;

public class RemoveQuestionEvent implements ActionListener {
	
	private Question question;
	
	public RemoveQuestionEvent(Question question){
		this.question  = question;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		QuestionManagementHandler.removeQuestion(question);
		
	}
	
}
