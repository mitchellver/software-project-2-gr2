package com.softwave.core.ui.event;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Map;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import com.softwave.core.container.GlobalState;
import com.softwave.core.container.quiz.QType;
import com.softwave.core.container.quiz.Question;
import com.softwave.core.logic.QuestionManagementHandler;
import com.softwave.core.ui.QuestionDetailPanel;
import com.softwave.core.ui.WindowManager;
import com.softwave.core.util.Language;

/**
 * This class will handle the extraction of the data out of the fields of the question
 * details panel, here all the data sush as name, type, ... will be retrieved and
 * put into a question object to be pushed to the database.
 * 
 * @author nicolas
 */
public class QuestionAbsorptionEvent implements ActionListener {
	/**
	 * The map of components where the key is the name of the component and the value
	 * is a reference to the object of the component.
	 */
	private Map<String, JComponent> componentMap;
	
	/**
	 * This constructor will create the event with the correct map of components,
	 * for data retrieval.
	 * 
	 * @param componentMap the map with the components and their keys.
	 */
	public QuestionAbsorptionEvent(Map<String, JComponent> componentMap) {
		setComponentMap(componentMap);
	}

	/**
	 * When a subscribed button is pressed and the event is fired, this method will
	 * be ran, it will check for a valid subscriptor and retrieve the data from
	 * the gui if valid.
	 * 
	 * @param e the event fired by the system.
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() instanceof JButton) {
			getData(componentMap);
		}
	}

	/**
	 * This method will handle the extraction of data out of the elements of the 
	 * component map
	 * 
	 * @param e the map of cothumponents with their keys
	 */
	private void getData(Map<String, JComponent> e) {
		// TODO this is overwritten by the wrong question, allthough the
		// setCurrentQuestion() should be called upon "open" in the
		// QuestionManagementPanel
		Question question = GlobalState.getCurrentQuestion();

		ArrayList<String> errors = new ArrayList<>();

		String name = "";
		QType type = QType.YES_NO;
		boolean isRequired = false;
		Language lang = Language.ENGLISH;
		int time = 0;

		if (e.get(QuestionDetailPanel.TXT_QUESTION) != null
				&& e.get(QuestionDetailPanel.TXT_QUESTION) instanceof JTextArea) {
			name = ((JTextArea) e.get(QuestionDetailPanel.TXT_QUESTION)).getText().trim();
			if (name.isEmpty())
				errors.add(QuestionDetailPanel.TXT_QUESTION + ": has no text");
		}

		if (e.get(QuestionDetailPanel.CB_TYPE) != null && e.get(QuestionDetailPanel.CB_TYPE) instanceof JComboBox) {
			int index = ((JComboBox) e.get(QuestionDetailPanel.CB_TYPE)).getSelectedIndex();
			if (index > -1)
				type = QType.values()[index];
			else
				type = QType.YES_NO;
		}

		if (e.get(QuestionDetailPanel.CHB_REQUIRED) != null
				&& e.get(QuestionDetailPanel.CHB_REQUIRED) instanceof JCheckBox) {
			isRequired = ((JCheckBox) e.get(QuestionDetailPanel.CHB_REQUIRED)).isSelected();
		}

		if (e.get(QuestionDetailPanel.CB_QUESTIONS_LANG) != null
				&& e.get(QuestionDetailPanel.CB_QUESTIONS_LANG) instanceof JComboBox) {
			lang = (Language) ((JComboBox) e.get(QuestionDetailPanel.CB_QUESTIONS_LANG)).getSelectedItem();
		}

		if (e.get(QuestionDetailPanel.TXT_TIME) != null && e.get(QuestionDetailPanel.TXT_TIME) instanceof JTextField) {
			String stime = ((JTextField) e.get(QuestionDetailPanel.TXT_TIME)).getText().trim();
			if (stime.isEmpty())
				stime = "0";
			try {
				time = Integer.parseInt(stime);
			} catch (NumberFormatException ex) {
				errors.add(QuestionDetailPanel.TXT_TIME);
				time = 0;
			}
			if (time != 0 && time < 5)
				errors.add(QuestionDetailPanel.TXT_TIME + ": has an invalid time period");
		}

		String msg = "Some fields have invalid value, please check the following fields\n";

		if (errors.isEmpty()) {
			question.setName(name);
			question.setType(type);
			question.setNeedsAnswer(isRequired);
			question.setQuestionLang(lang);
			question.setTime(time);

			// check if the question is already added to a quiz, if not, set
			// it's questionnumber and quizID
			if (question.getQuestionNumber() == 0) {
				// set the questionNumber
				question.setQuestionNumber(GlobalState.getCurrentQuiz().getQuestions().size() + 1);
				// set the questions quizID
				question.setQuizID(GlobalState.getCurrentQuiz().getQuizID());
				GlobalState.getCurrentQuiz().addQuestion(question);
				GlobalState.setCurrentQuestion(null);
			}
			QuestionManagementHandler.loadQuestionManagementPanel();
		} else {
			for (String error : errors) {
				msg += " - " + error + "\n";
			}
			JOptionPane.showMessageDialog(WindowManager.getInstance().getWindow(), msg, "Invalid fields",
					JOptionPane.OK_OPTION);
		}
//		QuestionManagementHandler.loadQuestionManagementPanel();
	}
	
	/**
	 * Getter for the map of components with the keys and their values.
	 * 
	 * @return the map of components and their values.
	 */
	public Map<String, JComponent> getComponentMap() {
		return componentMap;
	}

	/**
	 * Setter for the map of components and their values.
	 * 
	 * @param componentMap the map of components and their values.
	 */
	public void setComponentMap(Map<String, JComponent> componentMap) {
		if (componentMap == null) {
			return;
		}

		this.componentMap = componentMap;
	}
}
