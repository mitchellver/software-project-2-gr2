package com.softwave.core.ui.event;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.Map;

import javax.swing.JComponent;
import javax.swing.JTextField;

import com.softwave.core.container.quiz.Question;
import com.softwave.core.ui.DesignConfigDetailPanel;
import com.softwave.core.util.FileChooser;

public class ChooseQuestionLogoEvent implements ActionListener {
	private Map<String, JComponent> componentMap;
	private Question question;

	public ChooseQuestionLogoEvent(Map<String, JComponent> componentMap, Question question) {
		if (componentMap == null)
			throw new NullPointerException("The componentmap is null");
		if (question == null)
			throw new NullPointerException("The question is null");
		this.componentMap = componentMap;
		this.question = question;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// call the method that opens the fileChooser popup
		File file = FileChooser.chooseLogo();
		if (file != null) {
			// TODO check if we have a correct extention, and other sanity
			// checks
			// TODO maybe put the sanity checks in another class (FileChooser)

			// create a location to save the file to and the filename
			String saveLocation = FileChooser.PATH_START + "quiz_" + question.getQuizID() + File.separator;

			// update logopath txt in panel
			JTextField logoText = ((JTextField) componentMap.get(DesignConfigDetailPanel.TXT_LOGO));
			
			((JTextField) componentMap.get(DesignConfigDetailPanel.TXT_LOGO)).setText(saveLocation);

			// copy the file to the application
			FileChooser.copyToApplication(file, saveLocation);
			
			// add the new logo to the question
			question.getDesignConfig().setLogoFile(saveLocation + file.getName());
		}
	}
}
