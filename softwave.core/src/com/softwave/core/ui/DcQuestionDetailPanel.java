package com.softwave.core.ui;

import javax.swing.JButton;

import com.softwave.core.container.design.DesignConfig;
import com.softwave.core.container.quiz.Question;
import com.softwave.core.ui.event.ChooseQuestionLogoEvent;
import com.softwave.core.ui.event.QuestionDcAbsorptionEvent;

public class DcQuestionDetailPanel extends DesignConfigDetailPanel {

	public DcQuestionDetailPanel(DesignConfig dc, Question question) {
		super(dc);
		if (question == null)
			throw new NullPointerException("The question is null");
		((JButton) componentMap.get(BTN_LOGO)).addActionListener(new ChooseQuestionLogoEvent(componentMap, question));
		((JButton) componentMap.get(BTN_SAVE)).addActionListener(new QuestionDcAbsorptionEvent(componentMap, question));
	}

}
