package com.softwave.core.ui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;
import javax.swing.SpringLayout;

import com.softwave.core.container.design.DesignConfig;
import com.softwave.core.ui.event.SwitchViewEvent;
import com.softwave.core.util.FontEnum;
import com.softwave.core.util.FontStyleEnum;
import com.softwave.core.util.Translator;

public class DesignConfigDetailPanel extends WorkFlowPanel {
	/** designconfig with the info displayed on the panel */
	private DesignConfig dc;
	/** The key of the label of the logo path */
	public static final String LB_LOGO = "Logo label";
	/** The key of the path of the logo */
	public static final String TXT_LOGO = "Logo txt";
	/** The key of the button to select the logo */
	public static final String BTN_LOGO = "Logo button";

	/** The key of the label of the font */
	public static final String LB_FONT = "Font label";
	/** The key of the dropdown menu for choosing a font */
	public static final String CB_FONT = "Font selection";
	/** the key of the font style */
	public static final String CB_FONT_STYLE = "Font style";
	/** the key of the font size */
	public static final String CB_FONT_SIZE = "Font size";

	/** The key of the label to of the backgroundColor */
	public static final String LB_BACKGROUND = "Background label";

	/** the key of the label for the red of the colour */
	public static final String LB_RED = "Red label";
	/** the key of the textfield the red of the colour */
	public static final String TXT_RED = "Red text";

	/** the key of the label for the green of the colour */
	public static final String LB_GREEN = "Green label";
	/** the key of the textfield for the green of the colour */
	public static final String TXT_GREEN = "Green text";

	/** the key of the label for the blue of the colour */
	public static final String LB_BLUE = "Blue label";
	/** the key of the textfield for the blue of the colour */
	public static final String TXT_BLUE = "Blue text";

	/** The key for the save button */
	public static final String BTN_SAVE = "Save button";
	/** The key for the cancel button */
	public static final String BTN_CANCEL = "Cancel button";

	/** the componentmap with all the panels components */
	protected Map<String, JComponent> componentMap;

	// /** the ChooseFileEvent to call */
	// private ActionListener chooseFileEvent;

	/**
	 * the constructor taking in parameters.
	 * 
	 * @param dc
	 *            the designconfig the user will change
	 * @param chooseFileEvent
	 *            an event to choose a file linked to the logo button
	 */
	public DesignConfigDetailPanel(DesignConfig dc) {
		if (dc == null)
			throw new NullPointerException("The DesignConfig is null");
		this.dc = dc;

		// if (chooseFileEvent == null)
		// throw new NullPointerException("The chooseFileEvent is null");
		// this.chooseFileEvent = chooseFileEvent;
		init();
	}

	public DesignConfigDetailPanel(Dimension dim) {
		super(dim);
		init();
	}

	@Override
	protected void init() {
		// prepare top panel.
		initContext();
		// prepare left panel.
		initQuickAccess();
		// prepare middle panel.
		initDashboard();

		layout = new BorderLayout();

		this.setLayout(layout);

		this.add(contentPanel, BorderLayout.CENTER);
		this.add(contextPanel, BorderLayout.NORTH);
		this.add(quickAccessPanel, BorderLayout.WEST);
	}

	/**
	 * Initialize the dashboard of the panel (this represents the main
	 * functionality that the user will need to operate the application)
	 */
	@Override
	protected void initDashboard() {
		SpringLayout spring = new SpringLayout();
		JPanel panel = new JPanel(spring);

		componentMap = new HashMap<String, JComponent>();

		componentMap.put(LB_LOGO, new JLabel("Logo"));
		componentMap.put(TXT_LOGO, new JTextField(this.dc.getLogoFile()));
		componentMap.put(BTN_LOGO, new JButton("Logo"));

		componentMap.put(LB_FONT, new JLabel("Font"));
		componentMap.put(CB_FONT, new JComboBox<FontEnum>(FontEnum.values()));
		componentMap.put(CB_FONT_STYLE, new JComboBox<FontStyleEnum>(FontStyleEnum.values()));
		ArrayList<Integer> fontSizes = new ArrayList<>();
		for (int i = 6; i <= 22; i++) {
			fontSizes.add(i);
		}
		JComboBox<Integer> cbFontSize = new JComboBox<Integer>();
		fontSizes.forEach(e -> cbFontSize.addItem(e));
		cbFontSize.setSelectedItem(dc.getFont().getSize());
		componentMap.put(CB_FONT_SIZE, cbFontSize);

		componentMap.put(LB_BACKGROUND, new JLabel(Translator.translate("label.designconfig.background")));
		componentMap.put(LB_RED, new JLabel(Translator.translate("label.designconfig.background.red")));
		componentMap.put(TXT_RED, new JTextField());
		componentMap.put(LB_GREEN, new JLabel(Translator.translate("label.designconfig.background.green")));
		componentMap.put(TXT_GREEN, new JTextField());
		componentMap.put(LB_BLUE, new JLabel(Translator.translate("label.designconfig.background.blue")));
		componentMap.put(TXT_BLUE, new JTextField());

		componentMap.put(BTN_SAVE, new JButton(Translator.translate("button.designconfig.save")));
		componentMap.put(BTN_CANCEL, new JButton(Translator.translate("button.designconfig.cancel")));

		((JTextField) componentMap.get(TXT_LOGO)).setEditable(false);
		// The childs of this abstract class add the ActionLusteners for these buttons
		// ((JButton) componentMap.get(BTN_LOGO)).addActionListener(new
		// ChooseLogoEvent(componentMap));
		// ((JButton) componentMap.get(BTN_SAVE)).addActionListener(new
		// DesignConfigAbsorptionEvent(componentMap));
		((JButton) componentMap.get(BTN_CANCEL)).addActionListener(new SwitchViewEvent(WindowManager.PREVIOUS_VIEW));
		
		// TODO this is not setting the correct values in the combobox
		((JComboBox<FontEnum>) componentMap.get(CB_FONT)).setSelectedItem(FontEnum.valueOf(this.dc.getFont().getName()));
		((JComboBox<FontStyleEnum>) componentMap.get(CB_FONT_STYLE)).setSelectedItem(this.dc.getFont().getStyle());

		((JTextField) componentMap.get(TXT_RED)).setText(String.valueOf(this.dc.getBackgroundColor().getRed()));
		((JTextField) componentMap.get(TXT_GREEN)).setText(String.valueOf(this.dc.getBackgroundColor().getGreen()));
		((JTextField) componentMap.get(TXT_BLUE)).setText(String.valueOf(this.dc.getBackgroundColor().getBlue()));

		componentMap.forEach(panel::add);

		JScrollPane dcView = new JScrollPane(panel, ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS,
				ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);

		// horizontal
		spring.putConstraint(SpringLayout.WEST, componentMap.get(LB_LOGO), 10, SpringLayout.WEST, panel);
		spring.putConstraint(SpringLayout.WEST, componentMap.get(TXT_LOGO), 0, SpringLayout.WEST,
				componentMap.get(LB_LOGO));
		spring.putConstraint(SpringLayout.EAST, componentMap.get(TXT_LOGO), -10, SpringLayout.EAST, panel);
		spring.putConstraint(SpringLayout.WEST, componentMap.get(BTN_LOGO), 0, SpringLayout.WEST,
				componentMap.get(LB_LOGO));

		spring.putConstraint(SpringLayout.WEST, componentMap.get(LB_FONT), 0, SpringLayout.WEST,
				componentMap.get(BTN_LOGO));
		spring.putConstraint(SpringLayout.WEST, componentMap.get(CB_FONT), 0, SpringLayout.WEST,
				componentMap.get(LB_LOGO));
		spring.putConstraint(SpringLayout.WEST, componentMap.get(CB_FONT_STYLE), 10, SpringLayout.EAST,
				componentMap.get(CB_FONT));
		spring.putConstraint(SpringLayout.WEST, componentMap.get(CB_FONT_SIZE), 10, SpringLayout.EAST,
				componentMap.get(CB_FONT_STYLE));

		spring.putConstraint(SpringLayout.WEST, componentMap.get(LB_BACKGROUND), 0, SpringLayout.WEST,
				componentMap.get(LB_LOGO));
		spring.putConstraint(SpringLayout.WEST, componentMap.get(LB_RED), 0, SpringLayout.WEST,
				componentMap.get(LB_LOGO));
		spring.putConstraint(SpringLayout.WEST, componentMap.get(TXT_RED), 0, SpringLayout.WEST,
				componentMap.get(LB_LOGO));
		spring.putConstraint(SpringLayout.WEST, componentMap.get(LB_GREEN), 20, SpringLayout.EAST,
				componentMap.get(LB_RED));
		spring.putConstraint(SpringLayout.WEST, componentMap.get(TXT_GREEN), 0, SpringLayout.WEST,
				componentMap.get(LB_GREEN));
		spring.putConstraint(SpringLayout.WEST, componentMap.get(LB_BLUE), 20, SpringLayout.EAST,
				componentMap.get(LB_GREEN));
		spring.putConstraint(SpringLayout.WEST, componentMap.get(TXT_BLUE), 0, SpringLayout.WEST,
				componentMap.get(LB_BLUE));

		spring.putConstraint(SpringLayout.WEST, componentMap.get(BTN_SAVE), 0, SpringLayout.WEST,
				componentMap.get(LB_LOGO));
		spring.putConstraint(SpringLayout.WEST, componentMap.get(BTN_CANCEL), 10, SpringLayout.EAST,
				componentMap.get(BTN_SAVE));

		// vertical
		spring.putConstraint(SpringLayout.NORTH, componentMap.get(LB_LOGO), 10, SpringLayout.NORTH, panel);
		spring.putConstraint(SpringLayout.NORTH, componentMap.get(TXT_LOGO), 5, SpringLayout.SOUTH,
				componentMap.get(LB_LOGO));
		spring.putConstraint(SpringLayout.NORTH, componentMap.get(BTN_LOGO), 5, SpringLayout.SOUTH,
				componentMap.get(TXT_LOGO));
		spring.putConstraint(SpringLayout.NORTH, componentMap.get(LB_FONT), 10, SpringLayout.SOUTH,
				componentMap.get(BTN_LOGO));
		spring.putConstraint(SpringLayout.NORTH, componentMap.get(CB_FONT), 5, SpringLayout.SOUTH,
				componentMap.get(LB_FONT));
		spring.putConstraint(SpringLayout.NORTH, componentMap.get(CB_FONT_STYLE), 5, SpringLayout.SOUTH,
				componentMap.get(LB_FONT));
		spring.putConstraint(SpringLayout.NORTH, componentMap.get(CB_FONT_SIZE), 5, SpringLayout.SOUTH,
				componentMap.get(LB_FONT));

		spring.putConstraint(SpringLayout.NORTH, componentMap.get(LB_BACKGROUND), 10, SpringLayout.SOUTH,
				componentMap.get(CB_FONT));
		spring.putConstraint(SpringLayout.NORTH, componentMap.get(LB_RED), 5, SpringLayout.SOUTH,
				componentMap.get(LB_BACKGROUND));
		spring.putConstraint(SpringLayout.NORTH, componentMap.get(TXT_RED), 5, SpringLayout.SOUTH,
				componentMap.get(LB_RED));
		spring.putConstraint(SpringLayout.NORTH, componentMap.get(LB_GREEN), 5, SpringLayout.SOUTH,
				componentMap.get(LB_BACKGROUND));
		spring.putConstraint(SpringLayout.NORTH, componentMap.get(TXT_GREEN), 5, SpringLayout.SOUTH,
				componentMap.get(LB_RED));
		spring.putConstraint(SpringLayout.NORTH, componentMap.get(LB_BLUE), 5, SpringLayout.SOUTH,
				componentMap.get(LB_BACKGROUND));
		spring.putConstraint(SpringLayout.NORTH, componentMap.get(TXT_BLUE), 5, SpringLayout.SOUTH,
				componentMap.get(LB_RED));

		spring.putConstraint(SpringLayout.NORTH, componentMap.get(BTN_SAVE), 10, SpringLayout.SOUTH,
				componentMap.get(TXT_RED));
		spring.putConstraint(SpringLayout.NORTH, componentMap.get(BTN_CANCEL), 10, SpringLayout.SOUTH,
				componentMap.get(TXT_RED));

		contentPanel = new JPanel();
		contentPanel.setLayout(new BorderLayout());
		contentPanel.add(dcView, BorderLayout.CENTER);
	}
}
