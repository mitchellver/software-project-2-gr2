package com.softwave.core.ui;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.util.ArrayList;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;

import com.softwave.core.container.GlobalState;
import com.softwave.core.container.quiz.Quiz;
import com.softwave.core.reference.DefaultDesign;
import com.softwave.core.ui.component.LinkButton;
import com.softwave.core.ui.component.QuizPanel;
import com.softwave.core.ui.event.SwitchViewEvent;
import com.softwave.core.util.Translator;

/**
 * This panel will allow the user to manage a list of all the the quizzes
 * assigned to the currently logged in user.
 * 
 * @author nicolas
 */
public class QuizManagementPanel extends WorkFlowPanel {
	ArrayList<QuizPanel> quizzes;

	/**
	 * The main constructor of the quizmanagement panel.
	 */
	public QuizManagementPanel() {
		super();
		// initialize the quizzes to an empty list
		quizzes = new ArrayList<>();
		init();
	}

	/**
	 * a constructor that takes is a list of QuizPanels
	 * 
	 * @param quizzes
	 */
	public QuizManagementPanel(ArrayList<QuizPanel> quizzes) {
		super();
		// set the quizzes
		setQuizzes(quizzes);
		init();
	}

	/**
	 * Initialize the panels that make up this panel.
	 */
	@Override
	protected void init() {
		// prepare top panel.
		initContext();
		// prepare left panel.
		initQuickAccess();
		// prepare middle panel.
		initDashboard();

		layout = new BorderLayout();

		this.setLayout(layout);

		this.add(contentPanel, BorderLayout.CENTER);
		this.add(contextPanel, BorderLayout.NORTH);
		this.add(quickAccessPanel, BorderLayout.WEST);
	}

	/**
	 * Initialize the dashboard of the panel (this represents the main
	 * functionality that the user will need to operate the application)
	 */
	@Override
	protected void initDashboard() {
		contentPanel = new JPanel();
		contentPanel.setBackground(DefaultDesign.LIGHT_BLUE);

		JPanel panel = new JPanel();
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));

		// the add button
		 JPanel addPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
		 Quiz quiz = new Quiz(GlobalState.getClient().getUserID() , "untitled");
		 LinkButton btnAdd = new LinkButton(new QuizDetailPanel(quiz),
		 Translator.translate("button.question.add"));
		 btnAdd.addActionListener(new SwitchViewEvent(quiz));
		 addPanel.add(btnAdd);

		Box box = Box.createVerticalBox();

		 box.add(add(addPanel));
		quizzes.stream().forEach(box::add);
		panel.add(box);
		JScrollPane quizView = new JScrollPane(panel, ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS,
				ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);

		BorderLayout borderLayout = new BorderLayout();
		contentPanel.setLayout(borderLayout);
		contentPanel.add(quizView, BorderLayout.CENTER);
	}

	public ArrayList<QuizPanel> getQuizzes() {
		return quizzes;
	}

	public void setQuizzes(ArrayList<QuizPanel> quizzes) {
		if (quizzes == null)
			this.quizzes = new ArrayList<>();
		else
			this.quizzes = quizzes;
	}

}