package com.softwave.core.ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.util.HashMap;
import java.util.Map;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;
import javax.swing.SpringLayout;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import com.softwave.core.container.GlobalState;
import com.softwave.core.container.GlobalState;
import com.softwave.core.container.design.DesignConfig;
import com.softwave.core.container.quiz.QType;
import com.softwave.core.container.quiz.Question;
import com.softwave.core.ui.component.LinkButton;
import com.softwave.core.ui.event.ChangeAnswersEvent;
import com.softwave.core.ui.event.QuestionAbsorptionEvent;
import com.softwave.core.ui.event.SwitchViewEvent;
import com.softwave.core.util.Language;
import com.softwave.core.util.Translator;

/**
 * This panel will allow the user to manage the specific properties about a
 * question assigned to the currently quiz that the user selected.
 *
 * @author nicolas and eddi
 */
public class QuestionDetailPanel extends WorkFlowPanel {
	/** The current question that is being manipulated */
	private Question question;

	/** The key of the label of the question name */
	public static final String LB_QUESTION = "Question label";

	/** The key of the textfield of the name of the question */
	public static final String TXT_QUESTION = "Question name";

	/** The key of the label of the question type */
	public static final String LB_TYPE = "Type label";

	/** The key of the dropdown menu for choosing a question type */
	public static final String CB_TYPE = "Type selection";

	/** The key of the checkbox to specify if a question is required */
	public static final String CHB_REQUIRED = "Required checkbox";

	/** The key of the button to specify the answers of the question */
	public static final String BTN_ANSWERS = "Answers";

	/** The key of the button to link to the question management panel */
	public static final String BTN_DESIGN = "DesignConfig";

	/** The key of the label of the question language */
	public static final String LB_LANG = "Lang label";

	/** The key of the combo box to specify the question language */
	public static final String CB_QUESTIONS_LANG = "Question language selection";

	/** The key of the label of the time field */
	public static final String LB_TIME = "Time label";

	/**
	 * The key of the textfield to specify how much time a quizzer has for the
	 * question
	 */
	public static final String TXT_TIME = "Time";

	/** The key of the save button */
	public static final String BTN_SAVE = "Save";

	/** The key of the quit button */
	public static final String BTN_QUIT = "Quit";
	
	/** the componentmap containing all the panels components */
	protected Map<String, JComponent> componentMap;

	/**
	 * The main constructor of the quizmanagement panel.
	 */
	public QuestionDetailPanel(Question question) {
		super();
		if (question == null) {
			throw new NullPointerException();
		}
		this.question = question;
		GlobalState.setCurrentQuestion(question);
		init();
	}

	/**
	 * Initialize the panels that make up this panel.
	 */
	@Override
	protected void init() {
		// prepare top panel.
		initContext();
		// prepare left panel.
		initQuickAccess();
		// prepare middle panel.
		initDashboard();

		layout = new BorderLayout();

		this.setLayout(layout);

		this.add(contentPanel, BorderLayout.CENTER);
		this.add(contextPanel, BorderLayout.NORTH);
		this.add(quickAccessPanel, BorderLayout.WEST);
	}

	/**
	 * Initialize the dashboard of the panel (this represents the main
	 * functionality that the user will need to operate the application)
	 */
	@Override
	protected void initDashboard() {
		SpringLayout spring = new SpringLayout();
		JPanel panel = new JPanel(spring);

		componentMap = new HashMap<String, JComponent>();

		componentMap.put(LB_QUESTION, new JLabel(Translator.translate("label.question.question")));
		componentMap.put(TXT_QUESTION, new JTextArea(this.question.getName()));
		componentMap.put(LB_TYPE, new JLabel(Translator.translate("label.question.type")));
		componentMap.put(CB_TYPE, new JComboBox<>(QType.values()));
		componentMap.put(CHB_REQUIRED, new JCheckBox(Translator.translate("checkbox.question.required")));
		componentMap.put(LB_LANG, new JLabel(Translator.translate("label.question.language")));
		componentMap.put(CB_QUESTIONS_LANG, new JComboBox<Language>(Language.values()));
		componentMap.put(LB_TIME, new JLabel(Translator.translate("label.question.time")));
		componentMap.put(TXT_TIME, new JTextField(8));
		componentMap.put(BTN_ANSWERS, new JButton(Translator.translate("button.question.answers")));
		LinkButton btnDesign = new LinkButton(
				new DcQuestionDetailPanel(new DesignConfig(), question), "Design");
		componentMap.put(BTN_DESIGN, btnDesign);
		
		componentMap.put(BTN_SAVE, new JButton(Translator.translate("button.question.save")));
		componentMap.put(BTN_QUIT, new JButton(Translator.translate("button.question.quit")));
		QuestionAbsorptionEvent aqe = new QuestionAbsorptionEvent(componentMap);
		((JButton) componentMap.get(BTN_SAVE)).addActionListener(aqe);
		((JButton) componentMap.get(BTN_QUIT)).addActionListener(new SwitchViewEvent(WindowManager.PREVIOUS_VIEW));
		((JButton) componentMap.get(BTN_ANSWERS)).addActionListener(new ChangeAnswersEvent(componentMap.get(CB_TYPE)));
		((JButton) componentMap.get(BTN_DESIGN)).addActionListener(new SwitchViewEvent(this.question));

		((JTextArea) componentMap.get(TXT_QUESTION)).setRows(3);
		((JTextArea) componentMap.get(TXT_QUESTION))
				.setBorder(new CompoundBorder(new LineBorder(Color.GRAY), new EmptyBorder(1, 3, 1, 1)));

		((JTextField) componentMap.get(TXT_TIME)).setText(String.valueOf(this.question.getTime()));
		// ((JTextField) componentMap.get(TXT_TIME)).setBorder(new
		// CompoundBorder(new LineBorder(Color.GRAY), new EmptyBorder(1, 3, 1,
		// 1)));

		((JComboBox) componentMap.get(CB_TYPE)).setSelectedItem(this.question.getType());
		((JCheckBox) componentMap.get(CHB_REQUIRED)).setSelected(this.question.isNeedsAnswer());

		componentMap.forEach(panel::add);

		JScrollPane questionView = new JScrollPane(panel, ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS,
				ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);

		// horizontal
		spring.putConstraint(SpringLayout.WEST, componentMap.get(LB_QUESTION), 10, SpringLayout.WEST, panel);
		spring.putConstraint(SpringLayout.WEST, componentMap.get(LB_TYPE), 0, SpringLayout.WEST,
				componentMap.get(LB_QUESTION));
		spring.putConstraint(SpringLayout.WEST, componentMap.get(TXT_QUESTION), 0, SpringLayout.WEST,
				componentMap.get(LB_QUESTION));
		spring.putConstraint(SpringLayout.EAST, componentMap.get(TXT_QUESTION), -10, SpringLayout.EAST, panel);
		spring.putConstraint(SpringLayout.WEST, componentMap.get(CB_TYPE), 0, SpringLayout.WEST,
				componentMap.get(LB_QUESTION));
		spring.putConstraint(SpringLayout.WEST, componentMap.get(CHB_REQUIRED), 0, SpringLayout.WEST,
				componentMap.get(LB_QUESTION));
		spring.putConstraint(SpringLayout.WEST, componentMap.get(CB_QUESTIONS_LANG), 0, SpringLayout.WEST,
				componentMap.get(LB_QUESTION));
		spring.putConstraint(SpringLayout.WEST, componentMap.get(BTN_ANSWERS), 0, SpringLayout.WEST,
				componentMap.get(LB_QUESTION));
		spring.putConstraint(SpringLayout.WEST, componentMap.get(BTN_DESIGN), 10, SpringLayout.EAST,
				componentMap.get(BTN_ANSWERS));
		spring.putConstraint(SpringLayout.WEST, componentMap.get(LB_LANG), 0, SpringLayout.WEST,
				componentMap.get(LB_QUESTION));
		spring.putConstraint(SpringLayout.WEST, componentMap.get(TXT_TIME), 0, SpringLayout.WEST,
				componentMap.get(LB_QUESTION));
		spring.putConstraint(SpringLayout.WEST, componentMap.get(LB_TIME), 0, SpringLayout.WEST,
				componentMap.get(LB_QUESTION));
		spring.putConstraint(SpringLayout.WEST, componentMap.get(BTN_SAVE), 0, SpringLayout.WEST,
				componentMap.get(LB_QUESTION));

		spring.putConstraint(SpringLayout.WEST, componentMap.get(BTN_QUIT), 5, SpringLayout.EAST,
				componentMap.get(BTN_SAVE));

		// vertical
		spring.putConstraint(SpringLayout.NORTH, componentMap.get(LB_QUESTION), 10, SpringLayout.NORTH, panel);
		spring.putConstraint(SpringLayout.NORTH, componentMap.get(TXT_QUESTION), 5, SpringLayout.SOUTH,
				componentMap.get(LB_QUESTION));
		spring.putConstraint(SpringLayout.NORTH, componentMap.get(LB_TYPE), 10, SpringLayout.SOUTH,
				componentMap.get(TXT_QUESTION));
		spring.putConstraint(SpringLayout.NORTH, componentMap.get(CB_TYPE), 5, SpringLayout.SOUTH,
				componentMap.get(LB_TYPE));
		spring.putConstraint(SpringLayout.NORTH, componentMap.get(CHB_REQUIRED), 10, SpringLayout.SOUTH,
				componentMap.get(CB_TYPE));
		spring.putConstraint(SpringLayout.NORTH, componentMap.get(BTN_ANSWERS), 10, SpringLayout.SOUTH,
				componentMap.get(CHB_REQUIRED));
		spring.putConstraint(SpringLayout.NORTH, componentMap.get(BTN_DESIGN), 10, SpringLayout.SOUTH,
				componentMap.get(CHB_REQUIRED));
		spring.putConstraint(SpringLayout.NORTH, componentMap.get(LB_LANG), 10, SpringLayout.SOUTH,
				componentMap.get(BTN_ANSWERS));
		spring.putConstraint(SpringLayout.NORTH, componentMap.get(CB_QUESTIONS_LANG), 5, SpringLayout.SOUTH,
				componentMap.get(LB_LANG));
		spring.putConstraint(SpringLayout.NORTH, componentMap.get(LB_TIME), 10, SpringLayout.SOUTH,
				componentMap.get(CB_QUESTIONS_LANG));
		spring.putConstraint(SpringLayout.NORTH, componentMap.get(TXT_TIME), 5, SpringLayout.SOUTH,
				componentMap.get(LB_TIME));
		spring.putConstraint(SpringLayout.NORTH, componentMap.get(BTN_SAVE), 10, SpringLayout.SOUTH,
				componentMap.get(TXT_TIME));
		spring.putConstraint(SpringLayout.NORTH, componentMap.get(BTN_QUIT), 10, SpringLayout.SOUTH,
				componentMap.get(TXT_TIME));
		
		contentPanel = new JPanel();
		contentPanel.setLayout(new BorderLayout());
		contentPanel.add(questionView, BorderLayout.CENTER);
	}
}
