package com.softwave.core.ui;

import java.awt.BorderLayout;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Dimension;
import java.util.HashMap;
import java.util.HashMap;
import java.util.Map;
import java.util.Map;

import javax.swing.JButton;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;
import javax.swing.ScrollPaneConstants;
import javax.swing.SpringLayout;
import javax.swing.SpringLayout;
import javax.swing.border.CompoundBorder;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.border.LineBorder;

import com.softwave.core.container.quiz.Quiz;
import com.softwave.core.container.quiz.Quiz;
import com.softwave.core.ui.component.LinkButton;
import com.softwave.core.ui.component.LinkButton;
import com.softwave.core.ui.event.ChooseQuizLogoEvent;
import com.softwave.core.ui.event.QuizAbsorptionEvent;
import com.softwave.core.ui.event.QuizAbsorptionEvent;
import com.softwave.core.ui.event.SaveQuizEvent;
import com.softwave.core.ui.event.SwitchViewEvent;
import com.softwave.core.ui.event.SwitchViewEvent;
import com.softwave.core.util.Translator;
import com.softwave.core.util.Translator;

public class QuizDetailPanel extends WorkFlowPanel {
	/** Quiz with the info displayed on the panel */
	private Quiz quiz;

	/** The key of the label of the question name */
	public static final String LB_QUIZ_NAME = "Quiz label";

	/** The key of the textfield of the name of the quiz */
	public static final String TXT_QUIZ_NAME = "Quiz name";

	/** The key of the label of the quiz description */
	public static final String LB_DESCRIPTION = "Description label";

	/** The key of the textarea for the description of the quiz */
	public static final String TXT_DESCRIPTION = "Description";

	/** The key of the button to link to the question management panel */
	public static final String BTN_QUESTIONS = "Questions";
	
	/** The key of the button to link to the question management panel */
	public static final String BTN_DESIGN = "DesignConfig";

	/** The key of the save button */
	public static final String BTN_SAVE = "Save";

	/** The key of the quit button */
	public static final String BTN_QUIT = "Quit";
	
	/** the component map for all panel components */
	protected Map<String, JComponent> componentMap;

	public QuizDetailPanel(Quiz quiz) {
		if (quiz == null) {
			throw new NullPointerException("The Quiz is null");
		}
		this.quiz = quiz;
		init();
	}

	public QuizDetailPanel(Dimension dim) {
		super(dim);
		init();
	}

	@Override
	protected void init() {
		// prepare top panel.
		initContext();
		// prepare left panel.
		initQuickAccess();
		// prepare middle panel.
		initDashboard();

		layout = new BorderLayout();

		this.setLayout(layout);

		this.add(contentPanel, BorderLayout.CENTER);
		this.add(contextPanel, BorderLayout.NORTH);
		this.add(quickAccessPanel, BorderLayout.WEST);
	}

	/**
	 * Initialize the dashboard of the panel (this represents the main
	 * functionality that the user will need to operate the application)
	 */
	@Override
	protected void initDashboard() {
		SpringLayout spring = new SpringLayout();
		JPanel panel = new JPanel(spring);

		componentMap = new HashMap<String, JComponent>();

		componentMap.put(LB_QUIZ_NAME, new JLabel(Translator.translate("label.quiz.quiz")));
		// max 45 chars
		componentMap.put(TXT_QUIZ_NAME, new JTextField(this.quiz.getName()));
		// max 150 chars
		componentMap.put(LB_DESCRIPTION, new JLabel(Translator.translate("label.quiz.description")));
		componentMap.put(TXT_DESCRIPTION, new JTextArea(this.quiz.getDescription()));

		LinkButton btnQuestions = new LinkButton(new QuestionManagementPanel(),
				Translator.translate("button.quiz.questions"));
		componentMap.put(BTN_QUESTIONS, btnQuestions);

		LinkButton btnDesign = new LinkButton(
				new DcQuizDetailPanel(quiz.getDesignConfig(), quiz), "Design");
		componentMap.put(BTN_DESIGN, btnDesign);

		JButton btnSave = new JButton(Translator.translate("button.quiz.save"));
		componentMap.put(BTN_SAVE, btnSave);
		componentMap.put(BTN_QUIT, new JButton(Translator.translate("button.quiz.quit")));

		// TODO we shouldn add 2 actionlisteners to the same button
		btnQuestions.addActionListener(new QuizAbsorptionEvent(componentMap));
		btnSave.addActionListener(new SaveQuizEvent(quiz));

		QuizAbsorptionEvent aqe = new QuizAbsorptionEvent(componentMap);
		((JButton) componentMap.get(BTN_SAVE)).addActionListener(aqe);
		((JButton) componentMap.get(BTN_QUIT)).addActionListener(new SwitchViewEvent(WindowManager.PREVIOUS_VIEW));
		((JButton) componentMap.get(BTN_QUESTIONS)).addActionListener(new SwitchViewEvent(this.quiz));
		((JButton) componentMap.get(BTN_DESIGN)).addActionListener(new SwitchViewEvent(this.quiz));

		((JTextArea) componentMap.get(TXT_DESCRIPTION)).setRows(3);
		((JTextArea) componentMap.get(TXT_DESCRIPTION))
				.setBorder(new CompoundBorder(new LineBorder(Color.GRAY), new EmptyBorder(1, 3, 1, 1)));

		componentMap.forEach(panel::add);

		JScrollPane quizView = new JScrollPane(panel, ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS,
				ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);

		// horizontal
		spring.putConstraint(SpringLayout.WEST, componentMap.get(LB_QUIZ_NAME), 10, SpringLayout.WEST, panel);
		spring.putConstraint(SpringLayout.WEST, componentMap.get(TXT_QUIZ_NAME), 0, SpringLayout.WEST,
				componentMap.get(LB_QUIZ_NAME));
		spring.putConstraint(SpringLayout.EAST, componentMap.get(TXT_QUIZ_NAME), -10, SpringLayout.EAST, panel);
		spring.putConstraint(SpringLayout.WEST, componentMap.get(LB_DESCRIPTION), 0, SpringLayout.WEST,
				componentMap.get(LB_QUIZ_NAME));
		spring.putConstraint(SpringLayout.WEST, componentMap.get(TXT_DESCRIPTION), 0, SpringLayout.WEST,
				componentMap.get(LB_QUIZ_NAME));
		spring.putConstraint(SpringLayout.EAST, componentMap.get(TXT_DESCRIPTION), -10, SpringLayout.EAST, panel);

		spring.putConstraint(SpringLayout.WEST, componentMap.get(BTN_QUESTIONS), 0, SpringLayout.WEST,
				componentMap.get(LB_QUIZ_NAME));
		spring.putConstraint(SpringLayout.WEST, componentMap.get(BTN_DESIGN), 10, SpringLayout.EAST,
				componentMap.get(BTN_QUESTIONS));
		spring.putConstraint(SpringLayout.WEST, componentMap.get(BTN_SAVE), 0, SpringLayout.WEST,
				componentMap.get(LB_QUIZ_NAME));
		spring.putConstraint(SpringLayout.WEST, componentMap.get(BTN_QUIT), 5, SpringLayout.EAST,
				componentMap.get(BTN_SAVE));

		// vertical
		spring.putConstraint(SpringLayout.NORTH, componentMap.get(LB_QUIZ_NAME), 10, SpringLayout.NORTH, panel);
		spring.putConstraint(SpringLayout.NORTH, componentMap.get(TXT_QUIZ_NAME), 5, SpringLayout.SOUTH,
				componentMap.get(LB_QUIZ_NAME));
		spring.putConstraint(SpringLayout.NORTH, componentMap.get(LB_DESCRIPTION), 10, SpringLayout.SOUTH,
				componentMap.get(TXT_QUIZ_NAME));
		spring.putConstraint(SpringLayout.NORTH, componentMap.get(TXT_DESCRIPTION), 5, SpringLayout.SOUTH,
				componentMap.get(LB_DESCRIPTION));

		spring.putConstraint(SpringLayout.NORTH, componentMap.get(BTN_QUESTIONS), 10, SpringLayout.SOUTH,
				componentMap.get(TXT_DESCRIPTION));
		spring.putConstraint(SpringLayout.NORTH, componentMap.get(BTN_DESIGN), 10, SpringLayout.SOUTH,
				componentMap.get(TXT_DESCRIPTION));
		spring.putConstraint(SpringLayout.NORTH, componentMap.get(BTN_SAVE), 10, SpringLayout.SOUTH,
				componentMap.get(BTN_QUESTIONS));
		spring.putConstraint(SpringLayout.NORTH, componentMap.get(BTN_QUIT), 10, SpringLayout.SOUTH,
				componentMap.get(BTN_QUESTIONS));

		contentPanel = new JPanel();
		contentPanel.setLayout(new BorderLayout());
		contentPanel.add(quizView, BorderLayout.CENTER);
	}
}