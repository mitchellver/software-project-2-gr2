package com.softwave.core.ui;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.util.ArrayList;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;

import com.softwave.core.container.quiz.Question;
import com.softwave.core.reference.DefaultDesign;
import com.softwave.core.ui.component.LinkButton;
import com.softwave.core.ui.component.QuestionPanel;
import com.softwave.core.ui.event.SaveQuizEvent;
import com.softwave.core.ui.event.SwitchViewEvent;
import com.softwave.core.util.Translator;

/**
 * This panel will allow the user to manage a list of all the the quizzes
 * assigned to the currently logged in user.
 * 
 * @author nicolas
 */
public class QuestionManagementPanel extends WorkFlowPanel {
	ArrayList<QuestionPanel> questions;

	/**
	 * The main constructor of the quizmanagement panel.
	 */
	public QuestionManagementPanel() {
		super();
		setQuestions(null);
		init();
	}

	/**
	 * a constructor that takes is a list of QuizPanels
	 * 
	 * @param quizzes
	 */
	public QuestionManagementPanel(ArrayList<QuestionPanel> questions) {
		super();
		// set the questions
		setQuestions(questions);
		init();
	}

	/**
	 * Initialize the panels that make up this panel.
	 */
	@Override
	protected void init() {
		// prepare top panel.
		initContext();
		// prepare left panel.
		initQuickAccess();
		// prepare middle panel.
		initDashboard();

		layout = new BorderLayout();

		this.setLayout(layout);

		this.add(contentPanel, BorderLayout.CENTER);
		this.add(contextPanel, BorderLayout.NORTH);
		this.add(quickAccessPanel, BorderLayout.WEST);
	}

	/**
	 * Initialize the dashboard of the panel (this represents the main
	 * functionality that the user will need to operate the application)
	 */
	@Override
	protected void initDashboard() {
		contentPanel = new JPanel();
		contentPanel.setBackground(DefaultDesign.LIGHT_BLUE);

		JPanel panel = new JPanel();
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));

		JPanel addPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
		Question question = Question.newQuestion();
		LinkButton btnAdd = new LinkButton(new QuestionDetailPanel(question),
				Translator.translate("button.question.add"));
		btnAdd.addActionListener(new SwitchViewEvent(question));
		JButton btnSaveQuiz = new JButton("Save Quiz");
		btnSaveQuiz.addActionListener(new SaveQuizEvent());
		
		Box box = Box.createVerticalBox();

		// TODO this button needs to come first in the list please, then the
		// questions
		addPanel.add(btnAdd);
		addPanel.add(btnSaveQuiz);
		
		box.add(addPanel);
		questions.stream().forEach(box::add);
		panel.add(box);


		JScrollPane questionView = new JScrollPane(panel, ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS,
				ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);

		BorderLayout borderLayout = new BorderLayout();
		contentPanel.setLayout(borderLayout);
		contentPanel.add(questionView, BorderLayout.CENTER);
	}

	public ArrayList<QuestionPanel> getQuestions() {
		return questions;
	}

	public void setQuestions(ArrayList<QuestionPanel> questions) {
		if (questions == null)
			this.questions = new ArrayList<>();
		else
			this.questions = questions;
	}
}