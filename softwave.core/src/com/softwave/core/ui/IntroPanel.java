package com.softwave.core.ui;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.SpringLayout;
import javax.swing.SwingConstants;

import com.softwave.core.container.GlobalState;
import com.softwave.core.container.quiz.Answer;
import com.softwave.core.container.quiz.Participation;
import com.softwave.core.container.quiz.Quiz;
import com.softwave.core.container.user.account.AnonUser;
import com.softwave.core.logic.QuizManagementHandler;
import com.softwave.core.reference.DefaultDesign;
import com.softwave.core.ui.component.LinkButton;
import com.softwave.core.ui.event.SwitchViewEvent;
import com.softwave.core.util.Date;
import com.softwave.core.util.Translator;

@SuppressWarnings("serial")
public class IntroPanel extends Panel {
	Quiz quiz;

	public IntroPanel() {
		init();
	}

	protected void init() {

		// Load the quiz into our globalstate
		if (QuizManagementHandler.loadQuizOffline() == false){
				System.exit(1);
		}

		SpringLayout sl_panel = new SpringLayout();
		this.setLayout(sl_panel);
		this.setBackground(DefaultDesign.DARK_BLUE);

		JButton btnStartQuiz = new JButton("Start Quiz");
		sl_panel.putConstraint(SpringLayout.WEST, btnStartQuiz, 251, SpringLayout.WEST, this);
		sl_panel.putConstraint(SpringLayout.SOUTH, btnStartQuiz, -238, SpringLayout.SOUTH, this);
		sl_panel.putConstraint(SpringLayout.EAST, btnStartQuiz, -221, SpringLayout.EAST, this);
		btnStartQuiz.setFont(new Font("Tahoma", Font.PLAIN, 24));

		this.add(btnStartQuiz);

		JLabel lblQuizTitle = new JLabel(GlobalState.getCurrentQuiz().getName());
		lblQuizTitle.setForeground(Color.WHITE);
		sl_panel.putConstraint(SpringLayout.NORTH, btnStartQuiz, 86, SpringLayout.SOUTH, lblQuizTitle);
		sl_panel.putConstraint(SpringLayout.SOUTH, lblQuizTitle, -478, SpringLayout.SOUTH, this);
		sl_panel.putConstraint(SpringLayout.WEST, lblQuizTitle, 200, SpringLayout.WEST, this);
		sl_panel.putConstraint(SpringLayout.EAST, lblQuizTitle, -177, SpringLayout.EAST, this);
		lblQuizTitle.setFont(new Font("Tahoma", Font.PLAIN, 16));
		this.add(lblQuizTitle);
		lblQuizTitle.setHorizontalAlignment(SwingConstants.CENTER);

		LinkButton btnLogin = new LinkButton(new LoginPanel(), Translator.translate("button.login"));
		btnLogin.addActionListener(new SwitchViewEvent());

		sl_panel.putConstraint(SpringLayout.SOUTH, btnLogin, -10, SpringLayout.SOUTH, this);
		sl_panel.putConstraint(SpringLayout.EAST, btnLogin, -10, SpringLayout.EAST, this);
		this.add(btnLogin);
		btnLogin.setHorizontalAlignment(SwingConstants.RIGHT);

		btnStartQuiz.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent arg0) {
				// set the user
				// check if a quizzer logged in (this will be set in the Client
				// of global state)
				if (GlobalState.getClient() == null)
					GlobalState.setQuizzer(new AnonUser());
				else
					GlobalState.setQuizzer(GlobalState.getClient());

				// set the participation, if the participation does not exist,
				// create it
				if (GlobalState.getCurrentParticipation() == null) {
					GlobalState.setCurrentParticipation(new Participation());
					GlobalState.getCurrentParticipation().setAnswers(new ArrayList<>());

					// set the needed values of the participation
					GlobalState.getCurrentParticipation().setStartTime(new Date());
					GlobalState.getCurrentParticipation().setAnswers(new ArrayList<Answer>());
					GlobalState.getCurrentParticipation().setQuiz(quiz);
					GlobalState.getCurrentParticipation().setQuizzer(GlobalState.getQuizzer());
				}

				WindowManager.getInstance()
						.registerView(new SingleQuestionPanel(GlobalState.getCurrentQuiz(), GlobalState.getQuizzer()));
			}
		});

	}
}
