package com.softwave.core.ui;

import java.awt.Dimension;
import java.awt.Image;
import java.awt.Toolkit;

import javax.swing.ImageIcon;
import javax.swing.JFrame;

/**
 * This class is an abstraction of the JFrame class, it forces all its childs
 * to implement a initialization method for automating the setting up of the
 * window.
 * 
 * @author nicolas
 */
public abstract class Window extends JFrame {
	/** Caputres the dimesions of the screen and holds them */
	public static final Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
	
	/**
	 * main constructor of the window class, this method will auto set the close
	 * operation of the frame and let te frame appear in the middle of the screen.
	 * 
	 * @param title the title of the window displayed in the frame header
	 * @param dim the initial size of the frame
	 */
	public Window(String title, Dimension dim) {
		super(title);
		if (dim == null) {
			this.setSize(screenSize);
		} else {
			this.setSize(dim);
		}
		this.setLocationRelativeTo(null);
//		this.pack();
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		//System.out.println("window springlayout is " + )
		String path = "/mediaFiles/images/icons/logo_softwave.png";
		ImageIcon favicon = UISettings.makeImageIcon(path, "favicon");
		Image image = favicon.getImage(); // transform it
		favicon = new ImageIcon(image);
		ImageIcon img = new ImageIcon(path,"favicon");
		this.setIconImage(favicon.getImage());
	}
	
	/**
	 * A basic initialize method that need to be implemented by all the childs.
	 */
	protected abstract void init();
}