package com.softwave.core.ui;

import javax.swing.JPanel;

/**
 * This interface describes a panel that holds a quickAccessPanel object and forces
 * these to have a getter for this member.
 * 
 * @author nicolas
 */
public interface IQuickAccessHolder {
	/**
	 * A getter for the quickAccessPanel object the object has.
	 * 
	 * @return the quickAccessPanel object of the implementing class.
	 */
	public JPanel getQuickAccessPanel();
}