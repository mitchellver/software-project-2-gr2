package com.softwave.core.ui;

import java.awt.Component;
import java.util.LinkedList;

import javax.swing.JPanel;

/**
 * This class is responsible for managing the history and displayment of the
 * panels inside the main window, if a new panel needs to be displayed, a
 * windowManager object will be notified
 * 
 * @author nicolas
 */
public class WindowManager {
	/** The only instance allowed of this class */
	private static WindowManager instance;

	private static boolean quickAccessState;

	/** The maximum view history supported by the application */
	public static final byte MAX_VIEWS = 10;

	/** The state of the quickaccess panel when it is minimized (not visible) */
	public static final boolean QA_MINIMIZED = false;

	/** The state of the quickaccess panel when it is not minimized (visible) */
	public static final boolean QA_MAXIMIZED = true;

	/** Instrution for the windowmanager to do nothing */
	public static final short NOTHING = 0x00;

	/** Instruction for the 'manipulateView' method to hide the sidebar */
	public static final short HIDE_SIDEBAR = 0x01;

	/** Instruction for the 'manipulateView' method to close the window */
	public static final short CLOSE_WINDOW = 0x02;

	/**
	 * Instrution for the 'change view' to change the view to a view registerd
	 * in the history.
	 */
	public static final short PREVIOUS_VIEW = 0x03;

	/** The history of views stored as a linkedList */
	private LinkedList<Panel> views;

	/** The index of the current view. */
	private byte currentView = 0;

	/** The window on which the panels need to be managed */
	private Window window = null;

	/**
	 * The private constructor of this class that will instantiate the list of
	 * history.
	 */
	private WindowManager() {
		views = new LinkedList<>();
		quickAccessState = QA_MAXIMIZED;
	}

	/**
	 * Gives the instance of this class, if the instance was never used before
	 * it will create one, else it will make one and return that one.
	 * 
	 * @return the instance of this class.
	 */
	public static synchronized WindowManager getInstance() {
		if (instance == null) {
			instance = new WindowManager();
		}
		return instance;
	}

	/**
	 * Will set the view of the current window to the new panel, the method will
	 * keep a maximum number of panels in the history to prevent memory issues
	 * if the history is to long, the oldest panel in the history will be pushed
	 * out of the list.
	 * 
	 * @param panel
	 *            the new panel to display
	 * @return if the new view was added and registerd correctly.
	 */
	public synchronized boolean registerView(Panel panel) {
		return registerView(panel, false);
	}

	/**
	 * Will set the view of the current window to the new panel, the method will
	 * keep a maximum number of panels in the history to prevent memory issues
	 * if the history is to long, the oldest panel in the history will be pushed
	 * out of the list.
	 * 
	 * @param panel
	 *            the new panel to display
	 * @param allowBack allow the user to go back when the login button is pressed.
	 * @return if the new view was added and registerd correctly.
	 */
	public synchronized boolean registerView(Panel panel, boolean allowBack) {
		if (panel != null) {
			if (isBacklogFull()) {
				// remote the **first** element.
				views.pollFirst();
				currentView--;
			}
			if (allowBack == false && panel instanceof LoginPanel) {
				currentView = 0;
				views.clear();
			}
			// add the panel at the **end**.
			views.add(panel);
			currentView = (byte) views.size();
			// System.out.println("[registerview] the current view index is " +
			// currentView);
			// System.out.println("[registerview] the current view size is " +
			// views.size());
			nextView();
			return true;
		}

		return false;
	}

	/**
	 * Method that will manipulate the current view based on the instruction
	 * given in the argument.
	 * 
	 * @param todo
	 *            the instruction that tells what to do.
	 */
	public void manipulate(short todo) {
		switch (todo) {
		case HIDE_SIDEBAR:
			Component sideBar = null;
			if (window.getContentPane().getComponent(0) instanceof IQuickAccessHolder) {
				sideBar = ((IQuickAccessHolder) window.getContentPane().getComponent(0)).getQuickAccessPanel();
			} else {
				throw new IllegalArgumentException();
			}

			if (sideBar.isVisible()) {
				sideBar.setVisible(false);
			} else {
				sideBar.setVisible(true);
			}
			toggleQuickAccessState();
			break;
		case CLOSE_WINDOW:
			window.dispose();
		default:
			break;
		}
	}

	/**
	 * Will add the newly registered view to the frame while removing the older
	 * view, this view is still stored in the history.
	 */
	private synchronized void nextView() {
		if (views.size() < 1)
			return;
		window.getContentPane().removeAll();
		setQuickAccessState();

		window.getContentPane().add(views.getLast());
		window.paintAll(window.getGraphics());
	}

	/**
	 * Will reset the current view to the previous view in the history.
	 */
	private synchronized void previousView() {
		// TODO: need furter testing to see if it does work.
		if (views.size() > 1) {
			window.getContentPane().removeAll();
			views.pollLast();
		} else {
			// System.out.println("[previous] return statement hit!");
			return;
		}
		setQuickAccessState();
		window.getContentPane().add(views.get(--currentView - 1));
		// System.out.println("[previous] the current view index is " +
		// currentView);
		// System.out.println("[previous] the current view size is " +
		// views.size());
		window.paintAll(window.getGraphics());
	}

	/**
	 * Check if the history list has reached it's maximum.
	 * 
	 * @return true if the list has reached it's maximum, false if not
	 */
	private boolean isBacklogFull() {
		if (views.size() >= MAX_VIEWS) {
			return true;
		}
		return false;
	}

	/**
	 * Attach a window object to the window manager as the window that needs to
	 * be managed.
	 * 
	 * @param window
	 *            the window that needs to be managed.
	 * @return false if the window is already assigned, true if the window was
	 *         not yet assigned.
	 */
	public boolean attachWindow(Window window) {
		if (this.window == null) {
			this.window = window;
			return true;
		}
		return false;
	}

	/**
	 * Request the previous view registered in the history.
	 * 
	 * @return true if the history is not empty and the view is rolled back,
	 *         false if there is no view to go back to.
	 */
	public boolean requestPreviousView() {
		if (views.size() > 1) {
			previousView();
			return true;
		}
		return false;
	}

	/**
	 * Update the whole view: let all the components inside the window repaint
	 * themself.
	 */
	public void updateView() {
		window.getContentPane().removeAll();
		views.add(views.getLast());
		
		// views.pollLast();
		setQuickAccessState();
		window.getContentPane().add(views.get(currentView - 1));

		window.paintAll(window.getGraphics());

	}

	/**
	 * Return the current view of the Window.
	 * 
	 * @return a panel object that is being displayed on the window.
	 */
	public Panel getCurrentView() {
		return views.getLast();
	}

	/**
	 * This method will track the state of the quickaccess panel and check if it
	 * is visible or not, this is used to set the correct state for the next
	 * view.
	 */
	public void toggleQuickAccessState() {
		if (quickAccessState == QA_MAXIMIZED) {
			quickAccessState = QA_MINIMIZED;
		} else {
			quickAccessState = QA_MAXIMIZED;
		}
	}

	/**
	 * This method will set the visibility state of the quick access panel to
	 * the state of the quickaccessstate holding variable this will only apply
	 * if the currently displayed panel is a quickaccess panel holder.
	 */
	private void setQuickAccessState() {
		if (views.getLast() instanceof IQuickAccessHolder) {
			((IQuickAccessHolder) views.getLast()).getQuickAccessPanel().setVisible(quickAccessState);
		}
	}

	/**
	 * Getter for the attached window for this singleton class.
	 * 
	 * @return the current window on which the window manager will work.
	 */
	public Window getWindow() {
		return window;
	}

}
