package com.softwave.core.ui;

import java.awt.Dimension;

import javax.swing.JPanel;

/**
 * This class abstracts the panel to a dimensional layer: this class lets his childs
 * to implement and setup an initialization section where setup code can be run.
 * 
 * @author nicolas
 */
public abstract class Panel extends JPanel {
	/**
	 * The prefered size of the panel, when the panel is auto sized using a special
	 * layout like 'borderlayout' this will be useless.
	 */
	protected Dimension dim;
	
	/**
	 * This constructor constructs a panel that has not specified a specific size
	 * for the panel, this is not needed when using a {@code BorderLayout.CENTER}
	 */
	public Panel() {
		this.dim = null;
	}
	
	/**
	 * This constructor constructs a panel that is set to a specific size, if the
	 * size is null the size is not set.
	 * 
	 * @param dim the prefered size of the panel.
	 */
	public Panel(Dimension dim) {
		if (dim == null) {
			this.dim = new Dimension(600, 400);
		} else {
			this.dim = dim;	
		}
		this.setPreferredSize(this.dim);
	}
	
	/**
	 * run a standaard initialization method for all the panels but is specific to
	 * the panel.
	 */
	protected abstract void init();
}