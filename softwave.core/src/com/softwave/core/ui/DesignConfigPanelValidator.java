package com.softwave.core.ui;

import java.awt.Color;
import java.awt.Font;

import com.softwave.core.reference.DefaultDesign;
import com.softwave.core.util.SafeCast;

/**
 * A class with static methods to do validation for a DesignConfig. It receives
 * input from e.g. a DesignConfigDetailPanel and validates it.
 * 
 * @author eddi
 *
 */
public class DesignConfigPanelValidator {

	/**
	 * Create a Font from input received from the user.
	 * 
	 * @param fontType
	 *            the String that comes out of the FontEnum
	 * @param fontStyle
	 *            a String that comes from the FontStyleEnum
	 * @param fontSize
	 *            a String that should be cast to an int for the fontSize
	 * @return on successs the chosen font, on fail the default font of the
	 *         application
	 */
	public static Font validateFont(String fontType, String fontStyle, int fontSize) {
		Font f = DefaultDesign.FONT_MAIN;

		f = Font.decode(fontType.trim() + "-" + fontStyle.trim() + "-" + String.valueOf(fontSize));

		return f;
	}

	/**
	 * A method to validate input form e.g. a Panel with JTextfields. It
	 * validates the paramterers of a Color object
	 * 
	 * @param red
	 *            the red spectrum of the Color object
	 * @param green
	 *            the green spectrum of the Color object
	 * @param blue
	 *            the blue spectrum of the Color object
	 * @return a Color object on success and null on fail
	 */
	public static Color validateColor(String redStr, String greenStr, String blueStr) throws Exception {
		// initialize a default color from the applications design
		Color color = DefaultDesign.MEDIUM_BLUE;
		int red, green, blue;

		red = SafeCast.stringToInt(redStr, 0, 255);
		green = SafeCast.stringToInt(greenStr, 0, 255);
		blue = SafeCast.stringToInt(blueStr, 0, 255);
		color = new Color(red, green, blue);
		
		return color;
	}
}
