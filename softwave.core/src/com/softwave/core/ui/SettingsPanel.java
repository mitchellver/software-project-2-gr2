package com.softwave.core.ui;


import java.awt.BorderLayout;
import java.util.ArrayList;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SpringLayout;

import com.softwave.core.container.user.account.Client;
import com.softwave.core.ui.component.QuizPanel;

/**
 * This panel will allow the user to manage a list of all the users assigned
 * to the currently logged in user.
 * 
 * @author Turgay
 */
public class SettingsPanel extends WorkFlowPanel {

	/**
	 * The main constructor of the UserManagementPanel panel.
	 */
	public SettingsPanel(){
		super();
		init();
	}

	/**
	 * Initialize the panels that make up this panel.
	 */
	@Override
	protected void init() {
		// prepare top panel.
		initContext();
		// prepare left panel.
		initQuickAccess();
		// prepare middle panel.
		initDashboard();
		
		layout = new BorderLayout();
		
		this.setLayout(layout);
		
		this.add(contentPanel, BorderLayout.CENTER);
		this.add(contextPanel, BorderLayout.NORTH);
		this.add(quickAccessPanel, BorderLayout.WEST);
	}

	@Override
	protected void initDashboard() {
	Client client = new Client("test username", null);
	contentPanel = new JPanel();
	ArrayList<QuizPanel> users = new ArrayList<>();
//	users.add(new UserPanel(client));
//	users.add(new UserPanel(client));
//	users.add(new UserPanel(client));
//	users.add(new UserPanel(client));
	
	JPanel panel = new JPanel();
	SpringLayout spring = new SpringLayout();
	panel.setLayout(spring);
		
	users.stream().forEach(panel::add);
	JScrollPane userView = new JScrollPane(panel, ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS, ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
	userView.getViewport().setSize(contentPanel.getSize());
		
		
	// TODO: fix scroll bug 	
		
	//quizView.getViewport().setLayout(spring);
	//quizView.setViewportView(panel);
	for (int i = 1; i < users.size(); i++) {
		spring.putConstraint(SpringLayout.NORTH, users.get(i), 2, SpringLayout.SOUTH, users.get(i - 1));
		spring.putConstraint(SpringLayout.EAST, users.get(i - 1), 0, SpringLayout.EAST, panel);
		spring.putConstraint(SpringLayout.WEST, users.get(i - 1), 0, SpringLayout.WEST, panel);
		if (i == users.size() - 1) {
			spring.putConstraint(SpringLayout.EAST, users.get(i), 0, SpringLayout.EAST, panel);
			spring.putConstraint(SpringLayout.WEST, users.get(i), 0, SpringLayout.WEST, panel);
		}
	}	
		
	BorderLayout borderLayout = new BorderLayout();
	contentPanel.setLayout(borderLayout);
	contentPanel.add(userView, BorderLayout.CENTER);	
	}
}
