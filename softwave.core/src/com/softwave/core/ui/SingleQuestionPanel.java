package com.softwave.core.ui;

import java.awt.BorderLayout;
import java.awt.Canvas;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.util.ArrayList;

import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextArea;
import javax.swing.SwingConstants;

import com.softwave.core.container.GlobalState;
import com.softwave.core.container.quiz.Answer;
import com.softwave.core.container.quiz.Participation;
import com.softwave.core.container.quiz.QType;
import com.softwave.core.container.quiz.Question;
import com.softwave.core.container.quiz.Quiz;
import com.softwave.core.container.user.account.User;
import com.softwave.core.logic.ParticipationHandler;
import com.softwave.core.logic.QuizManagementHandler;
import com.softwave.core.reference.DefaultDesign;
import com.softwave.core.ui.component.QButton;
import com.softwave.core.ui.event.AnswerAbsorptionEvent;
import com.softwave.core.ui.event.SubmitParticipationEvent;
import com.softwave.core.util.Date;
//import com.softwave.core.util.VideoPlayer;

@SuppressWarnings("serial")
public class SingleQuestionPanel extends Panel {

	private JPanel headerPanel;
	private JLabel lblQuizTitle;
	private JLabel lblTime;
	private JLabel lblAnonymous;
	private JPanel questionPanel;
	private JLabel lblQuestion;
	private JPanel answerPanel;
	private JPanel navigationPanel;
	// the panels containing little squares per question
	// TODO paging if we have a quiz with more then 10 question
	private JPanel buttonsPanel = new JPanel();
	// Collection van JButtons
	private ArrayList<QButton> buttons = new ArrayList<>();
	int currentQuestionNumber;
	private Participation participation;
	private Quiz quiz;
	private User quizzer;
	// for open questions
	private JTextArea textAnswer = new JTextArea(19, 50);
	// the radiobuttons for a YES_NO or MULTIPLE_CHOISE question
	private ButtonGroup radioBtnAnswers;

	/**
	 * Create the application.
	 */
	public SingleQuestionPanel(Quiz quiz, User quizzer) {
		super();

		if (quiz == null)
			throw new NullPointerException("The quiz is null");
		this.quiz = quiz;
		if (quizzer == null)
			throw new NullPointerException("The quizzer is null");
		this.quizzer = quizzer;

		this.currentQuestionNumber = 0;

		// set the participation, if the participation does not exist, create it
		if (GlobalState.getCurrentParticipation() == null) {
			GlobalState.setCurrentParticipation(new Participation());
			this.participation = GlobalState.getCurrentParticipation();
			participation.setAnswers(new ArrayList<>());

			// set the needed values of the participation
			participation.setStartTime(new Date());
			participation.setAnswers(new ArrayList<Answer>());
			participation.setQuiz(quiz);
			participation.setQuizzer(quizzer);
		} else {
			participation = GlobalState.getCurrentParticipation();
			participation.setQuiz(quiz);
		}

		init();
	}

	public SingleQuestionPanel(Quiz quiz, User quizzer, int nextQuestionNumber) {
		super();

		if (quiz == null)
			throw new NullPointerException("The quiz is null");
		this.quiz = quiz;
		if (quizzer == null)
			throw new NullPointerException("The quizzer is null");
		this.quizzer = quizzer;

		this.currentQuestionNumber = nextQuestionNumber - 1;

		// set the participation, if the participation does not exist, create it
		if (GlobalState.getCurrentParticipation() == null) {
			GlobalState.setCurrentParticipation(new Participation());
			this.participation = GlobalState.getCurrentParticipation();
			participation.setAnswers(new ArrayList<>());

			// set the needed values of the participation
			participation.setStartTime(new Date());
			participation.setAnswers(new ArrayList<Answer>());
			participation.setQuiz(quiz);
			participation.setQuizzer(quizzer);
		} else {
			participation = GlobalState.getCurrentParticipation();
		}

		init();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	@Override
	protected void init() {

		this.setLayout(new BorderLayout());
		headerPanel = new JPanel();
		headerPanel.setPreferredSize(new Dimension(10, 30));
		headerPanel.setLayout(new GridLayout(1, 3));
		headerPanel.setBackground(DefaultDesign.DARK_BLUE);
		this.add(headerPanel, BorderLayout.NORTH);

		lblAnonymous = new JLabel("Anonymous");
		lblAnonymous.setForeground(Color.WHITE);
		lblAnonymous.setHorizontalAlignment(SwingConstants.LEFT);
		// lblAnonymous.setBounds(10, 5, 69, 14);
		headerPanel.add(lblAnonymous);

		lblQuizTitle = new JLabel(quiz.getName());
		lblQuizTitle.setForeground(Color.WHITE);
		lblQuizTitle.setHorizontalAlignment(SwingConstants.CENTER);
		// lblQuizTitle.setBounds(294, 8, 195, 14);
		headerPanel.add(lblQuizTitle);

		lblTime = new JLabel("Time");
		lblTime.setForeground(Color.WHITE);
		lblTime.setHorizontalAlignment(SwingConstants.RIGHT);
		// lblTime.setBounds(728, 8, 46, 14);
		headerPanel.add(lblTime);

		questionPanel = new JPanel();
		questionPanel.setPreferredSize(new Dimension(10, 80));
		// this.add(questionPanel);
		questionPanel.setLayout(new BorderLayout());

		lblQuestion = new JLabel("Question");
		lblQuestion.setForeground(Color.WHITE);
		lblQuestion.setHorizontalAlignment(SwingConstants.CENTER);
		questionPanel.add(lblQuestion, BorderLayout.CENTER);
		questionPanel.setBackground(DefaultDesign.MEDIUM_BLUE);

		JPanel centerPanel = new JPanel(new BorderLayout());
		centerPanel.add(questionPanel, BorderLayout.NORTH);
		this.add(centerPanel, BorderLayout.CENTER);
		answerPanel = new JPanel();
		answerPanel.setBackground(DefaultDesign.LIGHT_BLUE);

		// answerPanel.setPreferredSize(new Dimension(10, 400));
		// frame.getContentPane().add(answerPanel);

		centerPanel.add(answerPanel, BorderLayout.CENTER);

		navigationPanel = new JPanel();
		// navigationPanel.setPreferredSize(new Dimension(10, 60));
		// frame.getContentPane().add(navigationPanel);
		this.add(navigationPanel, BorderLayout.SOUTH);
		navigationPanel.setLayout(new GridLayout(0, 1));

		// currentQuestion[0] = 0;
		// show this question
		addCurrentQuestion(quiz.getQuestions().get(currentQuestionNumber));
		// create a new answer for this question if needed
		ParticipationHandler.addAnswer(participation, quiz.getQuestions().get(currentQuestionNumber));

		navigationPanel.add(buttonsPanel);
		buttonsPanel.setLayout(new GridLayout(1, quiz.getQuestions().size(), 0, 0));
		buttonsPanel.setBackground((DefaultDesign.LIGHT_BLUE));
		
	

		

		// add bottom question buttons
		for (int i = 0; i < quiz.getQuestions().size(); i++) {
			buttons.add(new QButton(i));
			buttons.get(i).setIndex(i);
			buttons.get(i).setText(Integer.toString(i + 1));
			buttons.get(i).setBackground(DefaultDesign.LIGHT_GREY);
			// set the color for the question buttons that have been answered
			if (participation.getAnswers().size() - 1 >= i && participation.getAnswers().get(i) != null
					&& participation.getAnswers().get(i).getAnswer() != null) {

				//RGB-Color code for light green.
				buttons.get(i).setBackground(new Color(102,255,102));
			}

			// set the color of the current question button
			if (i == currentQuestionNumber) {
				buttons.get(currentQuestionNumber).setBackground(DefaultDesign.CYAN_LIGHT_BLUE);
			}
		}
		// add the buttons to the panel
		buttons.stream().forEach(buttonsPanel::add);

		// load the answerPanel
		for (int i = 0; i < buttons.size(); i++) {
			int nextQuestion = buttons.get(i).getIndex() + 1;
			System.out.println(buttons.get(i).getIndex());
			buttons.get(i).addActionListener(
					new AnswerAbsorptionEvent(this, participation, buttons, nextQuestion, currentQuestionNumber));
		}

		// JPanel panel_1 = new JPanel();
		// navigationPanel.add(panel_1);

		// SUBMIT BUTTON
		JPanel submitPanel = new JPanel();
		// submitPanel.setPreferredSize(new Dimension(1, 1));
		JButton submitButton = new JButton("Submit");
		// add the submit actionlistener
		submitButton.addActionListener(new SubmitParticipationEvent(participation));

		// only show the submit button when all questions that need an answer
		// are filled in
		// if(ParticipationHandler.allRequiredQuestionsAnswered())
		// submitPanel.add(submitButton);
		submitPanel.add(submitButton);

		submitButton.setHorizontalAlignment(SwingConstants.LEFT);
		submitButton.setBackground(DefaultDesign.LIGHT_BLUE);
		submitButton.setForeground(Color.WHITE);
		navigationPanel.add(submitPanel);

		submitPanel.setBackground(DefaultDesign.DARK_BLUE);
	}

	// JTextArea textarea1 = new JTextArea(19, 50);
	public void addCurrentQuestion(Question question) {
		answerPanel.removeAll();
		answerPanel.revalidate();
		answerPanel.repaint();
		switch (question.getType()) {
		case OPEN:
			lblQuestion.setText(question.getName());
			// checks if the answer already exists and if they are different
			// from each other
			// if(participation.getAnswers().size() >=
			// question.getQuestionNumber()){
			// if
			// (participation.getAnswers().get(question.getQuestionNumber()).toString()
			// != null) {
			// if (textarea1.getText().trim() !=
			// participation.getAnswers().get(question.getQuestionNumber())
			// .toString().trim()) {
			// textarea1.setText(participation.getAnswers().get(question.getQuestionNumber()).getAnswer());
			// }
			// }
			// }

			// check if the answer exists, and use the answer to fill in the
			// textfield
			if (participation.getAnswers().size() - 1 >= currentQuestionNumber) {
				if (participation.getAnswers().get(currentQuestionNumber) != null) {
					textAnswer.setText(participation.getAnswers().get(currentQuestionNumber).getAnswer());
				}
			}
			answerPanel.add(textAnswer);

			break;
		case YES_NO:
			JPanel yesnoPanel = new JPanel();
			yesnoPanel.setLayout(new BoxLayout(yesnoPanel, BoxLayout.Y_AXIS));
			lblQuestion.setText(question.getName());
			radioBtnAnswers = new ButtonGroup();
			JRadioButton rbtnYes = new JRadioButton();
			rbtnYes.setBackground((DefaultDesign.LIGHT_BLUE));
			// TODO user the translator
			rbtnYes.setText("Yes");
			JRadioButton rbtnNo = new JRadioButton();
			rbtnNo.setBackground((DefaultDesign.LIGHT_BLUE));
			// TODO user the translator
			rbtnNo.setText("No");

			radioBtnAnswers.add(rbtnYes);
			radioBtnAnswers.add(rbtnNo);

			yesnoPanel.add(rbtnYes);
			yesnoPanel.add(rbtnNo);
			yesnoPanel.setBackground((DefaultDesign.LIGHT_BLUE));

			answerPanel.add(yesnoPanel);

			// check if the answer exists, and use the answer to fill in the
			// textfield
			if (participation.getAnswers().size() - 1 >= currentQuestionNumber) {
				if (participation.getAnswers().get(currentQuestionNumber) != null) {
					if (participation.getAnswers().get(currentQuestionNumber).getAnswer() == "Yes")
						rbtnYes.setSelected(true);
					else if (participation.getAnswers().get(currentQuestionNumber).getAnswer() == "No")
						rbtnNo.setSelected(true);
				}
			}

			break;
		case MULTIPLE_CHOICE:
			lblQuestion.setText(question.getName());
			break;
		case OPEN_MEDIA:
			//answerPanel.add(new VideoPlayer(quiz, currentQuestionNumber));
			break;
		case YES_NO_MEDIA:

			break;
		case MULTIPLE_CHOICE_MEDIA:

			break;

		}
		buttons.stream().forEach(buttonsPanel::add);

	}

	public JTextArea getTextAnswer() {
		return textAnswer;
	}

	public void setTextAnswer(JTextArea textAnswer) {
		this.textAnswer = textAnswer;
	}

	public ButtonGroup getRadioBtnAnswers() {
		return radioBtnAnswers;
	}

	public void setRadioBtnAnswers(ButtonGroup radioBtnAnswers) {
		this.radioBtnAnswers = radioBtnAnswers;
	}

	public static void main(String[] args) {

		Quiz quiz = new Quiz("Honden Quiz");
		quiz.addQuestion(new Question("wat is de naam van uw hond", QType.OPEN, quiz.getQuizID(), 1));
		quiz.addQuestion(new Question("Hoeveel porten heeft een hond", QType.OPEN, quiz.getQuizID(), 2));
		quiz.addQuestion(new Question("Heeft een hond een neus", QType.YES_NO, quiz.getQuizID(), 3));
		quiz.addQuestion(new Question("Vond u het een leuke quiz", QType.YES_NO, quiz.getQuizID(), 4));
	
//		quest.setVideoURL("promo video.mp4");
		quiz.setUserID(4);
		quiz.setDescription("Deze quiz is een quiz die vragen stelt over honden");
		QuizManagementHandler.saveQuizOffline(quiz);
	}
}
