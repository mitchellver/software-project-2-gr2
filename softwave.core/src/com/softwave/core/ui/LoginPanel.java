package com.softwave.core.ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.SpringLayout;
import javax.swing.border.Border;

import com.softwave.core.reference.DefaultDesign;
import com.softwave.core.ui.event.LoginEvent;
import com.softwave.core.util.Translator;

/**
 * Login panel is a panel responsible for displaying the login screen.
 * 
 * @author nicolas
 */
public class LoginPanel extends Panel {
	/**
	 * The panel which is placed in the middle of the screen, with the items needed
	 * to log in.
	 */
	private JPanel groupPanel;
	
	/** The login button, activates the login sequence. */
	private JButton btnLogin;
	
	/** The label that shows which field to put the the username in */
	private JLabel lbUsername;
	
	/** The label that shows which field to put the password in */
	private JLabel lbPassword;
	
	/** The textfield where the user writes his username */
	private JTextField txtUsername;
	
	/** The textfield where the user writes his password */
	private JPasswordField txtPassword;
	
	/** The textfield where we tell the user that the login failed */
	private JLabel lbLoginFailed;

	/**
	 * Constructor that initializes the components on the panel.
	 */
	public LoginPanel() {
		super();
		init();
	}
	
	/**
	 * Constructor that initializes the components on the panel and sets it dimension.
	 * 
	 * @param dim the prefered size of the panel.
	 */
	public LoginPanel(Dimension dim) {
		super(dim);
		init();
	}

	/**
	 * Initialize the components on the panel and lays them out correctly.
	 */
	@Override
	protected void init() {
		// TODO: fix the layout of the login panel.
//		SpringLayout masterSpring = new SpringLayout();
//		this.setLayout(new BorderLayout());
		this.setLayout(new BorderLayout());
		
//		JPanel masterPanel = new JPanel(masterSpring);
		
		SpringLayout spring = new SpringLayout();
		
		groupPanel = new JPanel();
		groupPanel.setLayout(spring);
		groupPanel.setBackground(DefaultDesign.LIGHT_BLUE);

		
		lbUsername = new JLabel(Translator.translate("label.login.username"));
		lbPassword = new JLabel(Translator.translate("label.login.password"));
		lbLoginFailed = new JLabel("");
		lbLoginFailed.setForeground(Color.RED);
		
		txtUsername = new JTextField(20);
		txtPassword = new JPasswordField(20);
		btnLogin = new JButton(Translator.translate("button.login"));
		btnLogin.addActionListener(new LoginEvent(txtUsername, txtPassword, lbLoginFailed));
		txtPassword.addKeyListener(new LoginEvent(txtUsername, txtPassword, lbLoginFailed));
		groupPanel.add(lbUsername);
		groupPanel.add(txtUsername);
		groupPanel.add(lbPassword);
		groupPanel.add(txtPassword);
		groupPanel.add(lbLoginFailed);
		groupPanel.add(btnLogin);

		// x-axis / horizonal placement
		spring.putConstraint(SpringLayout.WEST, lbUsername, 280, SpringLayout.WEST, groupPanel);
		spring.putConstraint(SpringLayout.WEST, txtUsername, 0, SpringLayout.WEST, lbUsername);
		spring.putConstraint(SpringLayout.WEST, lbPassword, 0, SpringLayout.WEST, lbUsername);
		spring.putConstraint(SpringLayout.WEST, txtPassword, 0, SpringLayout.WEST, lbUsername);
		spring.putConstraint(SpringLayout.WEST, lbLoginFailed, 0, SpringLayout.WEST, lbUsername);
		spring.putConstraint(SpringLayout.EAST, btnLogin, 0, SpringLayout.EAST, txtPassword);

		
		// y-axis / vertical placement
		spring.putConstraint(SpringLayout.NORTH, lbUsername, 160, SpringLayout.NORTH, groupPanel);
		
		spring.putConstraint(SpringLayout.NORTH, txtUsername, 5, SpringLayout.SOUTH, lbUsername);
		spring.putConstraint(SpringLayout.NORTH, lbPassword, 20, SpringLayout.SOUTH, txtUsername);
		spring.putConstraint(SpringLayout.NORTH, txtPassword, 5, SpringLayout.SOUTH, lbPassword);
		spring.putConstraint(SpringLayout.NORTH, btnLogin, 20, SpringLayout.SOUTH, txtPassword);
		spring.putConstraint(SpringLayout.NORTH, lbLoginFailed, 20, SpringLayout.SOUTH, btnLogin);

		this.add(groupPanel, BorderLayout.CENTER);
//		masterPanel.add(groupPanel);
//		this.add(masterPanel, BorderLayout.CENTER);
//		
//		masterSpring.putConstraint(SpringLayout.HORIZONTAL_CENTER, groupPanel, 0, SpringLayout.HORIZONTAL_CENTER, masterPanel);
//		masterSpring.putConstraint(SpringLayout.VERTICAL_CENTER, groupPanel, 0, SpringLayout.VERTICAL_CENTER, masterPanel);
//		
//		this.setMinimumSize(new Dimension(400, 300));
	}
}