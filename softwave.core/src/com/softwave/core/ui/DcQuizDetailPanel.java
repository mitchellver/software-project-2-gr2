package com.softwave.core.ui;

import javax.swing.JButton;

import com.softwave.core.container.design.DesignConfig;
import com.softwave.core.container.quiz.Quiz;
import com.softwave.core.ui.event.ChooseQuizLogoEvent;
import com.softwave.core.ui.event.QuizDcAbsorptionEvent;

public class DcQuizDetailPanel extends DesignConfigDetailPanel {
	public DcQuizDetailPanel(DesignConfig dc, Quiz quiz) {
		super(dc);
		if(quiz == null)
			throw new NullPointerException("The quiz is null");
		((JButton) componentMap.get(BTN_LOGO)).addActionListener(new ChooseQuizLogoEvent(componentMap, quiz));
		((JButton) componentMap.get(BTN_SAVE)).addActionListener(new QuizDcAbsorptionEvent(componentMap, quiz));
	}
	

}
