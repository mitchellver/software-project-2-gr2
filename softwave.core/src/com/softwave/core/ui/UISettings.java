package com.softwave.core.ui;

import java.awt.Color;
import java.awt.Font;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.UIManager;

public class UISettings {

	public static void scaleImageIcon(ImageIcon imageIcon, JButton btn) {
		
//		// now scale the icon
//		Image img = imageIcon.getImage();
//		BufferedImage bi = new BufferedImage(img.getWidth(null), img.getHeight(null), BufferedImage.TYPE_INT_RGB);
//		Graphics graphic = btn.getGraphics();
//		graphic.drawImage(img, 0, 0, btn.getWidth(), btn.getHeight(), null);
//		
//		// IconImage newIcon = new IconImage(bi);

	}

	/**
	 * This is a method found in the javadoc:
	 * 
	 * @see https://docs.oracle.com/javase/tutorial/uiswing/components/icon.html
	 *      It returns an ImageIcon, or null if the path was invalid.
	 * 
	 * @param path
	 *            the path where the file / logo can be found
	 * @param description
	 *            the description of the file/logo
	 * @return an ImageIcon on success null on false.
	 */
	public static ImageIcon makeImageIcon(String path, String description) {

		// TODO since this is a static method we need an object to call the
		// .getClass() method on,
		// create a nicer cleaner way todo this

		java.net.URL imgURL = String.class.getResource(path);
		if (imgURL != null) {
			return new ImageIcon(imgURL, description);
		} else {
			System.err.println("Couldn't find file: " + path);

			return null;
		}
	}

	/**
	 * a method to change the backgroundcolor TODO under construction
	 * 
	 * @param color
	 */
	public static void changeOverallBackgroundColor(Color color) {
		UIManager.put("Panel.backgroundcolor", color);
	}

	/**
	 * // * This is a method found on
	 * http://www.jguru.com/faq/view.jsp?EID=340519. It sets the font on most of
	 * the java swing components.
	 * 
	 * @param f
	 *            the font to use.
	 */
	public static void changeOverallFont(Font f) {
		UIManager.put("Button.font", f);
		UIManager.put("ToggleButton.font", f);
		UIManager.put("RadioButton.font", f);
		UIManager.put("CheckBox.font", f);
		UIManager.put("ColorChooser.font", f);
		UIManager.put("ComboBox.font", f);
		UIManager.put("Label.font", f);
		UIManager.put("List.font", f);
		UIManager.put("MenuBar.font", f);
		UIManager.put("MenuItem.font", f);
		UIManager.put("RadioButtonMenuItem.font", f);
		UIManager.put("CheckBoxMenuItem.font", f);
		UIManager.put("Menu.font", f);
		UIManager.put("PopupMenu.font", f);
		UIManager.put("OptionPane.font", f);
		UIManager.put("Panel.font", f);
		UIManager.put("ProgressBar.font", f);
		UIManager.put("ScrollPane.font", f);
		UIManager.put("Viewport.font", f);
		UIManager.put("TabbedPane.font", f);
		UIManager.put("Table.font", f);
		UIManager.put("TableHeader.font", f);
		UIManager.put("TextField.font", f);
		UIManager.put("PasswordField.font", f);
		UIManager.put("TextArea.font", f);
		UIManager.put("TextPane.font", f);
		UIManager.put("EditorPane.font", f);
		UIManager.put("TitledBorder.font", f);
		UIManager.put("ToolBar.font", f);
		UIManager.put("ToolTip.font", f);
		UIManager.put("Tree.font", f);
	}
}
