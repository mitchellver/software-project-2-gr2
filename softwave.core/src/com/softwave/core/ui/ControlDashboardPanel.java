package com.softwave.core.ui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.Image;
import java.io.File;

import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JPanel;

import com.softwave.core.reference.DefaultDesign;
import com.softwave.core.ui.component.LinkButton;
import com.softwave.core.ui.event.LogoutEvent;
import com.softwave.core.ui.event.QuitEvent;
import com.softwave.core.ui.event.SwitchViewEvent;
import com.softwave.core.util.FileChooser;
import com.softwave.core.util.Translator;

/**
 * ControlDashboard panel represents the home screen of the quiz creator.
 * 
 * @author nicolas
 */
public class ControlDashboardPanel extends WorkFlowPanel {

	/**
	 * Constructor for making a strandaard ControlDashboardPanel object.
	 */
	public ControlDashboardPanel() {
		super();
		init();
	}

	/**
	 * Constructor for making a strandaard ControlDashboardPanel object with a
	 * given size.
	 * 
	 * @param dim
	 *            the size of the panel.
	 */
	public ControlDashboardPanel(Dimension dim) {
		super(dim);
		init();
	}

	/**
	 * Initialize the panels that make up this panel.
	 */
	@Override
	protected void init() {
		// prepare top panel.
		initContext();
		// prepare middle panel.
		initDashboard();
		// prepare left panel.
		initQuickAccess();

		layout = new BorderLayout();

		this.setLayout(layout);

		this.add(contentPanel, BorderLayout.CENTER);
		this.add(contextPanel, BorderLayout.NORTH);
		this.add(quickAccessPanel, BorderLayout.WEST);

	}

	/**
	 * Initialize the dashboard of the panel (this represents the main
	 * functionality that the user will need to operate the application)
	 */
	@Override
	protected void initDashboard() {
		int row = 2;
		int column = 2;

		GridLayout gridLayout = new GridLayout(row, column);
		contentPanel = new JPanel();
		contentPanel.setBackground(DefaultDesign.LIGHT_BLUE);
		// add(contentPanel);
		contentPanel.setLayout(gridLayout);

		contentPanel.setBackground(DefaultDesign.LIGHT_BLUE);

		// ------------------------------------- MANAGE QUIZZES
		// ---------------------------------------------

		LinkButton btnManageQuizzes = new LinkButton(new QuizManagementPanel(),
				Translator.translate("button.managequiz.managequizzes"));

		String path = "/mediaFiles/images/icons/btnManageQuizzes.png";
		ImageIcon icnManageQuizzes = UISettings.makeImageIcon(path, "Quizzes button");

		// troubleshooting scale imageIcon --> stackoverflow code
		Image image = icnManageQuizzes.getImage(); // transform it
		Image newimg = image.getScaledInstance(180, 180, java.awt.Image.SCALE_SMOOTH); // scale
																						// it
																						// the
																						// smooth
																						// way
		icnManageQuizzes = new ImageIcon(newimg); // transform it back

		btnManageQuizzes.setIcon(icnManageQuizzes);
		btnManageQuizzes.addActionListener(new SwitchViewEvent());
		btnManageQuizzes.setBounds(200, 500, 450, 100); // *******************************************
		// Text color
		btnManageQuizzes.setForeground(DefaultDesign.LIGHT_GREY);

		// Makes btnManageQuizzes transparent
		btnManageQuizzes.setOpaque(false);
		btnManageQuizzes.setContentAreaFilled(false);
		btnManageQuizzes.setBorderPainted(false);

		// btnManageQuizzes.setLocation(500, 700);
		// btnManageQuizzes.setSize(500, 115);
		// btnManageQuizzes.setIcon(icnManageQuizzes);

		// ------------------------------------- MANAGE USERS
		// ---------------------------------------------

		LinkButton btnManageUsers = new LinkButton(new UserManagementPanel(),
				Translator.translate("button.managequiz.manageusers"));

		String path2 = "/mediaFiles/images/icons/btnManageUsers.png";
		ImageIcon icnManageUsers = UISettings.makeImageIcon(path2, "Users button");

		// troubleshooting scale imageIcon --> stackoverflow code
		Image image2 = icnManageUsers.getImage(); // transform it
		Image newimg2 = image2.getScaledInstance(180, 180, java.awt.Image.SCALE_SMOOTH); // scale
																							// it
																							// the
																							// smooth
																							// way
		icnManageUsers = new ImageIcon(newimg2); // transform it back

		btnManageUsers.setIcon(icnManageUsers);
		btnManageUsers.addActionListener(new SwitchViewEvent());

		// Text color
		btnManageUsers.setForeground(DefaultDesign.LIGHT_GREY);

		// Makes btnManageQuizzers transparent
		btnManageUsers.setOpaque(false);
		btnManageUsers.setContentAreaFilled(false);
		btnManageUsers.setBorderPainted(false);

		// ------------------------------------- STATISTICS
		// ---------------------------------------------

		LinkButton btnStatistics = new LinkButton(new StatisticsPanel(),
				Translator.translate("button.managequiz.statistics"));

		String path3 = "/mediaFiles/images/icons/btnStatistics.png";
		ImageIcon icnStatistics = UISettings.makeImageIcon(path3, "Statistics button");

		// troubleshooting scale imageIcon --> stackoverflow code
		Image image3 = icnStatistics.getImage(); // transform it
		Image newimg3 = image3.getScaledInstance(180, 180, java.awt.Image.SCALE_SMOOTH); // scale
																							// it
																							// the
																							// smooth
																							// way
		icnStatistics = new ImageIcon(newimg3); // transform it back

		btnStatistics.setIcon(icnStatistics);
		btnStatistics.addActionListener(new SwitchViewEvent());

		// JButton btnStatistics = new
		// JButton(Translator.translate("button.managequiz.statistics"));

		// Text color
		btnStatistics.setForeground(DefaultDesign.LIGHT_GREY);

		// Makes btnStatistics transparent
		btnStatistics.setOpaque(false);
		btnStatistics.setContentAreaFilled(false);
		btnStatistics.setBorderPainted(false);

		// Add all buttons of the dashboard
		contentPanel.add(btnManageQuizzes);
		contentPanel.add(btnManageUsers);
		contentPanel.add(btnStatistics);
	}

	/**
	 * Initialize the quick access panel with buttons to functions the user will
	 * need at all times
	 */
	protected void initQuickAccess() {
		int row = 10;
		int column = 1;

		GridLayout gridLayout = new GridLayout(row, column);

		quickAccessPanel = new JPanel();
		// PANE_LEFT_BACKGROUND_COLOR = MEDIUM_BLUE;
		quickAccessPanel.setBackground(DefaultDesign.MEDIUM_BLUE);
		add(quickAccessPanel);

		// ------------------------------------- LOGOUT
		// ---------------------------------------------

		// JPanel leftButtons = new JPanel(new BoxLayout(quickAccessPanel,
		// BoxLayout.Y_AXIS));

		LinkButton btnLogout = new LinkButton(new LoginPanel(), Translator.translate("button.logout"));

		String path4 = "/mediaFiles/images/icons/btnLogout.png";
		ImageIcon icnLogout = UISettings.makeImageIcon(path4, "Logout button");

		// troubleshooting scale imageIcon --> stackoverflow code
		Image image4 = icnLogout.getImage(); // transform it
		Image newimg4 = image4.getScaledInstance(35, 35, java.awt.Image.SCALE_SMOOTH); // scale
																						// it
																						// the
																						// smooth
																						// way
		icnLogout = new ImageIcon(newimg4); // transform it back

		btnLogout.setIcon(icnLogout);
		btnLogout.addActionListener(new SwitchViewEvent());
		// btnLogout.setBounds(200, 500, 450, 100);
		// //*******************************************

		// LinkButton btnLogout = new LinkButton(new LoginPanel(),
		// Translator.translate("button.logout"));
		// btnLogout.addActionListener(new LogoutEvent());

		// Text color
		btnLogout.setForeground(DefaultDesign.LIGHT_GREY);

		// Makes btnLogout transparent
		btnLogout.setOpaque(false);
		btnLogout.setContentAreaFilled(false);
		btnLogout.setBorderPainted(false);

		// ------------------------------------- SETTINGS
		// ---------------------------------------------
		LinkButton btnSettings = new LinkButton(new SettingsPanel(),
				Translator.translate("button.managequiz.settings"));

		String path5 = "/mediaFiles/images/icons/btnSettings.png";
		ImageIcon icnSettings = UISettings.makeImageIcon(path5, "Settings button");

		// troubleshooting scale imageIcon --> stackoverflow code
		Image image5 = icnSettings.getImage(); // transform it
		Image newimg5 = image5.getScaledInstance(35, 35, java.awt.Image.SCALE_SMOOTH); // scale
																						// it
																						// the
																						// smooth
																						// way
		icnSettings = new ImageIcon(newimg5); // transform it back

		btnSettings.setIcon(icnSettings);
		btnSettings.addActionListener(new SwitchViewEvent());

		// JButton btnSettings = new
		// JButton(Translator.translate("button.managequiz.settings"));
		// // btnSettings.addActionListener(new SwitchViewEvent());

		// Text color
		btnSettings.setForeground(DefaultDesign.LIGHT_GREY);

		// Makes btnSettings transparent
		btnSettings.setOpaque(false);
		btnSettings.setContentAreaFilled(false);
		btnSettings.setBorderPainted(false);

		// ------------------------------------- QUIT
		// ---------------------------------------------
		// LinkButton btnQuit = new LinkButton(new SettingsPanel(),
		// Translator.translate("button.quit"));

		JButton btnQuit = new JButton("Quit");
		String path6 = "/mediaFiles/images/icons/btnQuit.png";
		ImageIcon icnQuit = UISettings.makeImageIcon(path6, "Quit button");

		// troubleshooting scale imageIcon --> stackoverflow code
		Image image6 = icnQuit.getImage(); // transform it
		Image newimg6 = image6.getScaledInstance(35, 35, java.awt.Image.SCALE_SMOOTH); // scale
																						// it
																						// the
																						// smooth
																						// way
		icnQuit = new ImageIcon(newimg6); // transform it back

		btnQuit.setIcon(icnQuit);

		// Text color
		btnQuit.setForeground(DefaultDesign.LIGHT_GREY);

		// Makes btnSettings transparent
		btnQuit.setOpaque(false);
		btnQuit.setContentAreaFilled(false);
		btnQuit.setBorderPainted(false);
		btnQuit.addActionListener(new QuitEvent());

		quickAccessPanel.setLayout(gridLayout);
		quickAccessPanel.add(btnSettings);
		quickAccessPanel.add(btnLogout);
		quickAccessPanel.add(btnQuit);

		// leftButtons.add(btnSettings);
		// leftButtons.add(btnLogout);
		// leftButtons.add(btnQuit);
		// quickAccessPanel.add(leftButtons);

	}
}