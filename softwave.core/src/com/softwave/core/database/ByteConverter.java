package com.softwave.core.database;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/**
 * This class is written to save small objects as a Binary
 * 
 * @author eddi
 *
 */
public class ByteConverter {
	/**
	 * this converts the object to a BinaryStream useable by jdbc
	 * 
	 * @param o
	 *            the object to convert
	 * @return the ByteArrayInputStream to use with e.g. setBinaryStream in jdbc
	 * @throws IOException
	 */
	public static ByteArrayInputStream getBinaryInputStream(Object o) throws IOException {
		return new ByteArrayInputStream(convertToByteArray(o));
	}

	// // this method converts an inputstream from e.g. jdbc to a java object.
	//
	// public static Object convertStreamToObject(ByteArrayInputStream b){
	// return
	// }

	/**
	 * convert an object to a ByteArray. To use with jdbc, to store small
	 * objects like e.g. Color and Font
	 * 
	 * @param o
	 *            the object to convert
	 * @return a byteArray of the object
	 * @throws IOException
	 */
	// convert the object to a byteArray
	public static byte[] convertToByteArray(Object o) throws IOException {
		byte[] bytes = null;
		ByteArrayOutputStream byteOutStream = null;
		ObjectOutputStream objectOutStream = null;
		try {
			byteOutStream = new ByteArrayOutputStream();
			objectOutStream = new ObjectOutputStream(byteOutStream);
			objectOutStream.writeObject(o);
			objectOutStream.flush();
			bytes = byteOutStream.toByteArray();
		} finally {
			if (objectOutStream != null) {
				objectOutStream.close();
			}
			if (byteOutStream != null) {
				byteOutStream.close();
			}
		}
		return bytes;
	}

	/**
	 * convert the byteArray to an object,
	 * 
	 * @param i
	 *            the Bytearray the comes from e.g. jdbc result.getBinaryStream
	 * 
	 * @return returns the original object, make sure you cast it to what Class
	 *         you are expecting, and add your checks. returns null on fail.
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	public static Object convertToObject(InputStream i) throws IOException, ClassNotFoundException {
		Object o = null;
		ByteArrayInputStream byteInStream = (ByteArrayInputStream) i;
		ObjectInputStream objectInStream = null;
		try {
			objectInStream = new ObjectInputStream(byteInStream);
			o = objectInStream.readObject();
		} finally {
			if (byteInStream != null) {
				byteInStream.close();
			}
			if (objectInStream != null) {
				objectInStream.close();
			}
		}
		return o;
	}
}
