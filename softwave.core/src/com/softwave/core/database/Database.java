package com.softwave.core.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * The database class is used by the whole project, it is responible for the
 * only database that can be connected to by the application.
 *
 * It Connects to a mysql database.
 * 
 * @author Nicolas and eddi
 * 
 */

public class Database {
	/** connection object used to communicate with the database. */
	private static Connection c = null;

	/** The class from the mysql library to connect to the dbfile. */
	public static final String DB_PACKAGE = "com.mysql.jdbc.Driver";
	/**
	 * The path via the connection driver to the file: after '://' comes the
	 * path, url or IP address.
	 */
	public static final String DB_DRIVER = "jdbc:mysql://";

	/**
	 * These next variables are the database to user, the user and it's
	 * password.
	 */
	public static final String DB_URL_PROD = "10.3.50.31:3306/";
	private static final String DB_PROD = "PRready";
	private static final String USER_PROD = "userpr";
	private static final String PASS_PROD = "sjUqY83ehVwG3Eqt";

	/**
	 * This function will open a connection to the database by first loading the
	 * mysql database driver, if the database file does not exist it will create
	 * one.
	 *
	 */
	public static void openDatabase() {
		try {
			Class.forName(DB_PACKAGE);
			if (c == null) {
				c = DriverManager.getConnection(DB_DRIVER + DB_URL_PROD + DB_PROD, USER_PROD, PASS_PROD);
			}
		} catch (Exception e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(1);
		}
	}

	/**
	 * This method will close the connection to the database.
	 */
	public static void closeDatabase() {
		if (c != null) {
			try {
				c.close();
				c = null;
			} catch (SQLException e) {
				System.err.println(e.getClass().getName() + ": " + e.getMessage());
				System.exit(1);
			}
		}
	}

	/**
	 * Get the connection object of this class.
	 *
	 * @return the connection object with the data to the database file.
	 */
	public static Connection getConnection() {
		return c;
	}

}
