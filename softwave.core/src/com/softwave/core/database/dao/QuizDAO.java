package com.softwave.core.database.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import com.softwave.core.container.quiz.Quiz;
import com.softwave.core.database.Database;

/**
 * QuizDAO class is used to access the individual tables in the database, every
 * method in this class is static will do a specific thing: adding an item to
 * the quiz table, removing, updating and reading data from the database.
 *
 * @author eddi
 */
public class QuizDAO {

	public static boolean pushOrUpdate(Quiz quiz) {
		if (quiz == null)
			return false;

		// check if the address is not number 0, does not exist allready
		if (quiz.getQuizID() == -1 || existsByID(quiz.getQuizID()) == false)
			return push(quiz); // create a new address
		// update the address
		return update(quiz);
	}

	/**
	 * This method will add an quiz to the quiz table.
	 *
	 * @param addres
	 *            the quiz to be stored
	 * @return the newly created ID, or -1 on fail
	 */
	public static boolean push(Quiz quiz) {
		// check if the quiz has a valid ID
		if (quiz.getQuizID() != -1)
			return false;
		if (UserDAO.existsByID(quiz.getUserID()) == false)
			return false;
		// check if the quiz does not exist allready
		if (existsByID(quiz.getQuizID()) == true)
			return false;

		try {
			Connection c = Database.getConnection();
			if (c == null) {
				Database.openDatabase();
				c = Database.getConnection();
			}

			// save the designconfig first
			DesignConfigDAO.pushOrUpdate(quiz.getDesignConfig());

			// create a prepared statement and a statement
			PreparedStatement preparedPush = null;
			String pushStatement = "INSERT INTO quiz (userID, name, description, designconfigID) VALUES (?,?,?,?);";

			// don't autocommit the statement
			c.setAutoCommit(false);
			// set the prepeared statement
			preparedPush = c.prepareStatement(pushStatement, PreparedStatement.RETURN_GENERATED_KEYS);

			// set the parameters of the prepared statement
			preparedPush.setLong(1, quiz.getUserID());
			preparedPush.setString(2, quiz.getName());
			preparedPush.setString(3, quiz.getDescription());
			if (quiz.getDesignConfig() != null)
				preparedPush.setLong(4, quiz.getDesignConfig().getDesignConfigID());
			else
				preparedPush.setNull(4, java.sql.Types.INTEGER);

			// execute the query, this returns a resultset
			int rowsUpdated = preparedPush.executeUpdate();

			// update the Quiz ID
			try (ResultSet insertID = preparedPush.getGeneratedKeys()) {
				if (insertID.next()) {
					quiz.setQuizID(insertID.getLong(1));
					// set the questions quizID
					if (quiz.getQuestions() != null) {
						quiz.getQuestions().stream().forEach(e -> {
							e.setQuizID(quiz.getQuizID());
							if (QuestionDAO.pushOrUpdate(e) == false)
								System.out.println("quizdao: question not pushed");
						});
					}
					return true;
				} else {
					throw new SQLException("Creating user failed, no ID obtained.");
				}
			} finally

			{
				preparedPush.close();
				c.commit();
				// Database.closeDatabase();
				// if the push did not work and more or less then 1 row changed
				if (rowsUpdated != 1) {
					return false;
				}
			}

		} catch (

		SQLException e)

		{
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(1);
		}

		// something else went wrong
		return false;

	}

	/**
	 * this is a method to update an existing row in the database. It checks if
	 * the row exists, and then updates it
	 * 
	 * @param a
	 *            the quiz to update, should have a valid id
	 * @return true on success false on fail
	 * @TODO ADD PREPARED STATEMENT !
	 */
	public static boolean update(Quiz quiz) {
		if (quiz == null)
			return false;
		// check if the row exists in the database
		if (existsByID(quiz.getQuizID()) == false)
			return false;

		// TODO update the designconfig first
		if (DesignConfigDAO.pushOrUpdate(quiz.getDesignConfig()) == false)
			return false;

		try {
			Connection c = Database.getConnection();
			if (c == null) {
				Database.openDatabase();
				c = Database.getConnection();
			}

			// create a prepared statement and a statement
			PreparedStatement preparedUpdate = null;
			String pushStatement = "UPDATE quiz SET userID=?, name=?, description=?, designconfigID=? WHERE quizID=?;";

			// don't autocommit the statement
			c.setAutoCommit(false);
			// set the prepeared statement
			preparedUpdate = c.prepareStatement(pushStatement);
			// set the parameters of the prepared statement
			// set the parameters of the prepared statement
			preparedUpdate.setLong(1, quiz.getUserID());
			preparedUpdate.setString(2, quiz.getName());
			preparedUpdate.setString(3, quiz.getDescription());
			if (quiz.getDesignConfig() != null)
				preparedUpdate.setLong(4, quiz.getDesignConfig().getDesignConfigID());
			else
				preparedUpdate.setNull(4, java.sql.Types.INTEGER);
			preparedUpdate.setLong(5, quiz.getQuizID());

			// execute the query, this returns a resultset
			int rowsUpdated = preparedUpdate.executeUpdate();

			// TODO save the questions, now that we have a valid quizID
			// TODO if we close the connection in a statement after this
			// method gets
			// called, then we can put this code IN the try catch where the
			// saving
			// is done. it would also mean that we work with only 1
			// connection for
			// all pushes
			if (rowsUpdated > 0) {
				quiz.getQuestions().stream().forEach(e -> {
					if (QuestionDAO.pushOrUpdate(e) == false)
						System.out.println("quizdao: question not pushed");
				});
			}
			preparedUpdate.close();
			c.commit();
			// Database.closeDatabase();

			// check if the row was updated
			if (rowsUpdated == 1)
				return true;
			return false;

		} catch (SQLException e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(1);
		}

		// TODO update the questions
		quiz.getQuestions().stream().forEach(e -> {
			if (QuestionDAO.pushOrUpdate(e) == false)
				System.out.println("quizdao: question not updated");
		});

		return false;
	}

	/**
	 * This method will look up every quiz in the database that has the same
	 * city as the given one. It will then return an Arraylist of those quizes
	 * 
	 * @param String
	 *            this is the city you want to look for
	 * @return null if no quiz was found, Quiz if the quiz was found an pulled.
	 */
	public static Quiz pullByID(long quizID) {
		if (quizID < 0)
			return null;
		if (existsByID(quizID) == false)
			return null;

		Quiz quiz = null;

		try {
			Connection c = Database.getConnection();
			if (c == null) {
				Database.openDatabase();
				c = Database.getConnection();
			}

			// create a prepared statement and a statement
			PreparedStatement preparedPull = null;
			String pullStatement = "SELECT * FROM quiz WHERE quizID = ?;";

			// don't autocommit the statement
			c.setAutoCommit(false);
			// set the prepeared statement
			preparedPull = c.prepareStatement(pullStatement);
			// set the parameters of the prepared statement
			preparedPull.setLong(1, quizID);

			// execute the query, this returns a resultset
			ResultSet rs = preparedPull.executeQuery();

			if (rs.next()) {
				quiz = new Quiz();
				quiz.setQuizID(quizID);
				quiz.setName(rs.getString("name"));
				quiz.setDescription(rs.getString("description"));
				quiz.setUserID(rs.getLong("userID"));
				// set the designconfig
				quiz.setDesignConfig(DesignConfigDAO.pullByID(rs.getLong("designconfigID")));
				// set the questions
				quiz.setQuestions(QuestionDAO.pullByQuizID(quiz.getQuizID()));

			}

			// close statement and connection
			preparedPull.close();
			// Database.closeDatabase();

		} catch (SQLException e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(1);
		}

		return quiz;
	}

	/**
	 * This method will read all the quizes in the quiz table and return every
	 * single record back as an item object array.
	 *
	 * @return an item object array constructed from all the quizes in the
	 *         database.
	 */
	public static ArrayList<Quiz> pullAll() {
		ArrayList<Quiz> quizes = new ArrayList<>();

		Quiz quiz = null;

		try {
			Connection c = Database.getConnection();
			if (c == null) {
				Database.openDatabase();
				c = Database.getConnection();
			}

			Statement s = null;

			s = c.createStatement();

			ResultSet rs = s.executeQuery("SELECT * FROM quiz;");

			while (rs.next()) {
				quiz = new Quiz();
				quiz.setQuizID(rs.getLong("quizID"));
				quiz.setName(rs.getString("name"));
				quiz.setDescription(rs.getString("description"));
				quiz.setUserID(rs.getLong("userID"));

				// set the designconfig
				quiz.setDesignConfig(DesignConfigDAO.pullByID(rs.getLong("designconfigID")));
				// set the questions
				quiz.setQuestions(QuestionDAO.pullByQuizID(quiz.getQuizID()));

				quizes.add(quiz);
			}

			// close the connection
			s.close();
			// Database.closeDatabase();
		} catch (SQLException e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(1);
		}

		return quizes;
	}

	/**
	 * This method will read all the quizes in the quiz table and return every
	 * single record back as an item object array.
	 *
	 * @return an item object array constructed from all the quizes in the
	 *         database.
	 */
	public static ArrayList<Quiz> pullByUserID(long userID) {
		if (userID < 0)
			return null;
		// check if the user exists
		if (UserDAO.existsByID(userID) == false)
			return null;

		ArrayList<Quiz> quizes = new ArrayList<>();

		Quiz quiz = null;

		try {
			Connection c = Database.getConnection();
			if (c == null) {
				Database.openDatabase();
				c = Database.getConnection();
			}

			// create a prepared statement and a statement
			PreparedStatement preparedPull = null;
			String pullStatement = "SELECT * FROM quiz WHERE userID=?;";

			// don't autocommit the statement
			c.setAutoCommit(false);
			// set the prepeared statement
			preparedPull = c.prepareStatement(pullStatement);
			// set the parameters of the prepared statement
			preparedPull.setLong(1, userID);

			// execute the query, this returns a resultset
			ResultSet rs = preparedPull.executeQuery();

			while (rs.next()) {
				quiz = new Quiz();
				quiz.setQuizID(rs.getLong("quizID"));
				quiz.setName(rs.getString("name"));
				quiz.setDescription(rs.getString("description"));
				quiz.setUserID(rs.getLong("userID"));

				// set the designconfig
				quiz.setDesignConfig(DesignConfigDAO.pullByID(rs.getLong("designconfigID")));
				// set the questions
				quiz.setQuestions(QuestionDAO.pullByQuizID(quiz.getQuizID()));

				quizes.add(quiz);
			}

			// close the connection
			preparedPull.close();
			c.commit();
			// Database.closeDatabase();
		} catch (SQLException e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(1);
		}

		return quizes;
	}

	/**
	 * This method will delete the quiz row from the tabel
	 * 
	 * @param Quiz
	 *            the quiz to delete
	 * @return true on success, false on fail
	 */
	public static boolean deleteByID(long quizID) {
		if (quizID < 0)
			return false;
		if (existsByID(quizID) == false)
			return false;
		try {
			Connection c = Database.getConnection();
			if (c == null) {
				Database.openDatabase();
				c = Database.getConnection();
			}

			// delete the questions and get the designconfigID
			QuestionDAO.deleteByQuizID(quizID);
			long dcID = getDesignConfigIDByQuizID(quizID);

			// create a prepared statement and a statement
			PreparedStatement deleteQuiz = null;
			String deleteStatement = "DELETE FROM quiz WHERE quizID = ?;";
			// don't autocommit the statement
			c.setAutoCommit(false);
			// set the prepeared statement
			deleteQuiz = c.prepareStatement(deleteStatement);
			// set the parameters of the prepared statement
			deleteQuiz.setLong(1, quizID);
			// execute the statement
			deleteQuiz.executeUpdate();

			c.commit();
			// Database.closeDatabase();

			// delete teh remaining designconfig
			DesignConfigDAO.deleteByID(dcID);

			return true;

		} catch (SQLException e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(1);
		}
		return false;
	}

	/**
	 * This method will delete the quiz row from the tabel
	 * 
	 * @param Quiz
	 *            the quiz to delete
	 * @return true on success, false on fail
	 */
	public static boolean deleteByObject(Quiz a) {
		if (a == null)
			return false;
		if (existsByID(a.getQuizID()) == false)
			return false;
		try {
			Connection c = Database.getConnection();
			if (c == null) {
				Database.openDatabase();
				c = Database.getConnection();
			}

			// get the designconfigID out of the quiz, so we can delete it after
			// we delete the quiz(foreign key restrictions)
			long dcID;
			if (a.getDesignConfig() == null)
				dcID = -1;
			else
				dcID = a.getDesignConfig().getDesignConfigID();

			// delete the questions
			QuestionDAO.deleteByQuizID(a.getQuizID());

			c.setAutoCommit(false);
			Statement s = null;

			s = c.createStatement();
			String sql = "DELETE FROM quiz WHERE quizID = " + a.getQuizID() + ";";
			s.executeUpdate(sql);

			s.close();
			c.commit();
			// Database.closeDatabase();

			// delete the designconfig
			DesignConfigDAO.deleteByID(dcID);

			return true;
		} catch (SQLException e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(1);
		}
		return false;
	}

	/**
	 * check if an quiz with a certain ID exists in the database
	 * 
	 * @param long
	 *            the id of the quiz to look for
	 * @return false if the quiz does not exists, true if the quiz exists
	 * @TODO refactor this, not actually pulling the whole quiz, just returning
	 *       the ID oof the quiz and compare to be sure it's the same.
	 */
	public static boolean existsByID(long quizID) {
		if (quizID < 0)
			return false;

		try {
			Connection c = Database.getConnection();
			if (c == null) {
				Database.openDatabase();
				c = Database.getConnection();
			}

			// create a prepared statement and a statement
			PreparedStatement preparedPull = null;
			String pullStatement = "SELECT quizID FROM quiz WHERE quizID = ?;";

			// don't autocommit the statement
			c.setAutoCommit(false);
			// set the prepeared statement
			preparedPull = c.prepareStatement(pullStatement);
			// set the parameters of the prepared statement
			preparedPull.setLong(1, quizID);

			// execute the query, this returns a resultset
			ResultSet rs = preparedPull.executeQuery();

			long pulledID = -1;
			if (rs.next()) {
				pulledID = rs.getLong(1);
			}
			// close statement and connection
			preparedPull.close();
			// Database.closeDatabase();

			if (pulledID != quizID)
				return false;
			return true;

		} catch (SQLException e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(1);
		}

		return false;
	}

	/**
	 * This method retrieves the count of rows in this table
	 * 
	 * @return an int, the number or rows in this table
	 */
	public static int countOf() {
		try {
			Connection c = Database.getConnection();
			if (c == null) {
				Database.openDatabase();
				c = Database.getConnection();
			}

			int count = 0;
			Statement s = null;

			s = c.createStatement();

			ResultSet rs = s.executeQuery("SELECT count(*) FROM quiz;");

			if (rs.next()) {
				count = rs.getInt(1);
			}

			// close the connection
			s.close();
			// Database.closeDatabase();

			return count;
		} catch (SQLException e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(1);
		}

		return 0;
	}

	/**
	 * this is a method that finds a designconfigID for a given quizID
	 * 
	 * @param questionID
	 *            the id for which we want to find a designconfigID in the table
	 * @return -1 on fail, or a long with the designconfigID
	 */
	public static long getDesignConfigIDByQuizID(long quizID) {
		if (quizID < 0)
			return -1;

		try {
			Connection c = Database.getConnection();
			if (c == null) {
				Database.openDatabase();
				c = Database.getConnection();
			}

			// create a prepared statement and a statement
			PreparedStatement preparedPull = null;
			String pullStatement = "SELECT designconfigID FROM quiz WHERE quizID = ?;";

			// don't autocommit the statement
			c.setAutoCommit(false);
			// set the prepeared statement
			preparedPull = c.prepareStatement(pullStatement);
			// set the parameters of the prepared statement
			preparedPull.setLong(1, quizID);

			// execute the query, this returns a resultset
			ResultSet rs = preparedPull.executeQuery();

			long pulledID = -1;
			if (rs.next()) {
				pulledID = rs.getLong(1);
			}
			// close statement and connection
			preparedPull.close();
			// Database.closeDatabase();

			return pulledID;

		} catch (SQLException e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(1);
		}

		return -1;
	}

}