package com.softwave.core.database.dao;

import java.awt.Color;
import java.awt.Font;
import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import com.softwave.core.container.design.DesignConfig;
import com.softwave.core.database.ByteConverter;
import com.softwave.core.database.Database;

/**
 * DesignCofigDAO class is used to access the individual tables in the database,
 * every method in this class is static will do a specific thing: adding an item
 * to the designconfig table, removing, updating and reading data from the
 * database.
 *
 * @author eddi
 */
public class DesignConfigDAO {

	public static boolean pushOrUpdate(DesignConfig designConfig) {
		if (designConfig == null)
			return false;

		// check if the address is not number 0, does not exist allready
		if (designConfig.getDesignConfigID() == -1 || existsByID(designConfig.getDesignConfigID()) == false)
			return push(designConfig); // create a new address
		// update the address
		return update(designConfig);
	}

	/**
	 * This method will add an designConfig to the designConfig table.
	 *
	 * @param addres
	 *            the designConfig to be stored
	 * @return the newly created ID, or -1 on fail
	 */
	public static boolean push(DesignConfig designConfig) {
		// check if the designConfig has a valid ID
		if (designConfig.getDesignConfigID() != -1)
			return false;
		// check if the designConfig does not exist allready
		if (existsByID(designConfig.getDesignConfigID()) == true)
			return false;

		try {
			Connection c = Database.getConnection();
			if (c == null) {
				Database.openDatabase();
				c = Database.getConnection();
			}

			// create a prepared statement and a statement
			PreparedStatement preparedPush = null;
			String pushStatement = "INSERT INTO designconfig (logo, backgroundcolor, font, active) VALUES (?,?,?,?);";

			// don't autocommit the statement
			c.setAutoCommit(false);
			// set the prepeared statement
			preparedPush = c.prepareStatement(pushStatement, PreparedStatement.RETURN_GENERATED_KEYS);

			// set the parameters of the prepared statement
			preparedPush.setString(1, designConfig.getLogoFile());
			preparedPush.setBinaryStream(2, ByteConverter.getBinaryInputStream(designConfig.getBackgroundColor()));
			preparedPush.setBinaryStream(3, ByteConverter.getBinaryInputStream(designConfig.getFont()));
			preparedPush.setBoolean(4, true);

			// execute the query, this returns a resultset
			int rowsUpdated = preparedPush.executeUpdate();

			// update the DesignConfig ID
			try (ResultSet insertID = preparedPush.getGeneratedKeys()) {
				if (insertID.next()) {
					designConfig.setDesignConfigID(insertID.getLong(1));
					return true;
				} else {
					throw new SQLException("Creating user failed, no ID obtained.");
				}
			} finally {
				preparedPush.close();
				c.commit();
				// Database.closeDatabase();
				// if the push did not work and more or less then 1 row changed
				if (rowsUpdated != 1) {
					return false;
				}
			}

		} catch (SQLException | IOException e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(1);
		}
		// something else went wrong
		return false;
	}

	/**
	 * this is a method to update an existing row in the database. It checks if
	 * the row exists, and then updates it
	 * 
	 * @param a
	 *            the designConfig to update, should have a valid id
	 * @return true on success false on fail
	 * @TODO ADD PREPARED STATEMENT !
	 */
	public static boolean update(DesignConfig designConfig) {
		// check if the row exists in the database
		if (existsByID(designConfig.getDesignConfigID()) == false)
			return false;

		try {
			Connection c = Database.getConnection();
			if (c == null) {
				Database.openDatabase();
				c = Database.getConnection();
			}

			// create a prepared statement and a statement
			PreparedStatement preparedUpdate = null;
			String pushStatement = "UPDATE designconfig SET logo=?, backgroundcolor=?, font=?, active =? WHERE designconfigID=?;";

			// don't autocommit the statement
			c.setAutoCommit(false);
			// set the prepeared statement
			preparedUpdate = c.prepareStatement(pushStatement);
			// set the parameters of the prepared statement
			preparedUpdate.setString(1, designConfig.getLogoFile());
			preparedUpdate.setBinaryStream(2, ByteConverter.getBinaryInputStream(designConfig.getBackgroundColor()));
			preparedUpdate.setBinaryStream(3, ByteConverter.getBinaryInputStream(designConfig.getFont()));
			preparedUpdate.setBoolean(4, true);
			preparedUpdate.setLong(5, designConfig.getDesignConfigID());

			// execute the query, this returns a resultset
			int rowsUpdated = preparedUpdate.executeUpdate();

			preparedUpdate.close();
			c.commit();
			// Database.closeDatabase();

			// check if the row was updated
			if (rowsUpdated == 1)
				return true;
			return false;

		} catch (SQLException | IOException e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(1);
		}
		return false;
	}

	/**
	 * This method will look up every designConfig in the database that has the
	 * same city as the given one. It will then return an Arraylist of those
	 * designConfiges
	 * 
	 * @param String
	 *            this is the city you want to look for
	 * @return null if no designConfig was found, DesignConfig if the
	 *         designConfig was found an pulled.
	 */
	public static DesignConfig pullByID(long designConfigID) {
		if (designConfigID < 0)
			return null;
		if (existsByID(designConfigID) == false)
			return null;

		DesignConfig designConfig = null;
		
		try {
			Connection c = Database.getConnection();
			if (c == null) {
				Database.openDatabase();
				c = Database.getConnection();
			}

			// create a prepared statement and a statement
			PreparedStatement preparedPull = null;
			String pullStatement = "SELECT * FROM designconfig WHERE designconfigID = ?;";

			// don't autocommit the statement
			c.setAutoCommit(false);
			// set the prepeared statement
			preparedPull = c.prepareStatement(pullStatement);
			// set the parameters of the prepared statement
			preparedPull.setLong(1, designConfigID);

			// execute the query, this returns a resultset
			ResultSet rs = preparedPull.executeQuery();

			
			if (rs.next()) {
				designConfig = new DesignConfig();
				designConfig.setDesignConfigID(rs.getLong("designconfigID"));
				designConfig.setLogoFile(rs.getString("logo"));
				designConfig.setBackgroundColor(
						(Color) ByteConverter.convertToObject(rs.getBinaryStream("backgroundcolor")));
				designConfig.setFont((Font) ByteConverter.convertToObject(rs.getBinaryStream("font")));
			}

			// close statement and connection
			preparedPull.close();
			// Database.closeDatabase();

		} catch (SQLException | ClassNotFoundException | IOException e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(1);
		}

		return designConfig;
	}

	/**
	 * This method will read all the designConfiges in the designConfig table
	 * and return every single record back as an item object array.
	 *
	 * @return an item object array constructed from all the designConfiges in
	 *         the database.
	 */
	public static ArrayList<DesignConfig> pullAll() {
		ArrayList<DesignConfig> designConfigs = new ArrayList<>();
		DesignConfig designConfig = null;
		try {
			Connection c = Database.getConnection();
			if (c == null) {
				Database.openDatabase();
				c = Database.getConnection();
			}

			Statement s = null;

			s = c.createStatement();

			ResultSet rs = s.executeQuery("SELECT * FROM designconfig;");

			while (rs.next()) {
				designConfig = new DesignConfig();
				designConfig.setDesignConfigID(rs.getLong("designconfigID"));
				designConfig.setLogoFile(rs.getString("logo"));
				designConfig.setBackgroundColor((Color)ByteConverter.convertToObject(rs.getBinaryStream("backgroundcolor")));
				designConfig.setFont((Font)ByteConverter.convertToObject(rs.getBinaryStream("font")));
				designConfigs.add(designConfig);
			}

			// close the connection
			s.close();
			// Database.closeDatabase();
		} catch (SQLException | ClassNotFoundException | IOException e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(1);
		}

		return designConfigs;
	}

	/**
	 * This method will delete the designConfig row from the tabel
	 * 
	 * @param DesignConfig
	 *            the designConfig to delete
	 * @return true on success, false on fail
	 */
	public static boolean deleteByID(long designConfigID) {
		if (designConfigID < 0)
			return false;
		if (existsByID(designConfigID) == false)
			return false;
		try {
			Connection c = Database.getConnection();
			if (c == null) {
				Database.openDatabase();
				c = Database.getConnection();
			}

			// create a prepared statement and a statement
			PreparedStatement deleteDesignConfig = null;
			String deleteStatement = "DELETE FROM designconfig WHERE designconfigID = ?;";
			// don't autocommit the statement
			c.setAutoCommit(false);
			// set the prepeared statement
			deleteDesignConfig = c.prepareStatement(deleteStatement);
			// set the parameters of the prepared statement
			deleteDesignConfig.setLong(1, designConfigID);
			// execute the statement
			deleteDesignConfig.executeUpdate();

			c.commit();
			// Database.closeDatabase();
			return true;

		} catch (SQLException e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(1);
		}
		return false;
	}

	/**
	 * This method will delete the designConfig row from the tabel
	 * 
	 * @param DesignConfig
	 *            the designConfig to delete
	 * @return true on success, false on fail
	 */
	public static boolean deleteByObject(DesignConfig a) {
		if (a == null)
			return false;
		if (existsByID(a.getDesignConfigID()) == false)
			return false;
		try {
			Connection c = Database.getConnection();
			if (c == null) {
				Database.openDatabase();
				c = Database.getConnection();
			}

			c.setAutoCommit(false);
			Statement s = null;

			s = c.createStatement();
			String sql = "DELETE FROM designconfig WHERE designconfigID = " + a.getDesignConfigID() + ";";
			s.executeUpdate(sql);

			s.close();
			c.commit();
			// Database.closeDatabase();
			return true;
		} catch (SQLException e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(1);
		}
		return false;
	}

	/**
	 * check if an designConfig with a certain ID exists in the database
	 * 
	 * @param long
	 *            the id of the designConfig to look for
	 * @return false if the designConfig does not exists, true if the
	 *         designConfig exists
	 * @TODO refactor this, not actually pulling the whole designConfig, just
	 *       returning the ID oof the designConfig and compare to be sure it's
	 *       the same.
	 */
	public static boolean existsByID(long designConfigID) {
		if (designConfigID < 0)
			return false;

		try {
			Connection c = Database.getConnection();
			if (c == null) {
				Database.openDatabase();
				c = Database.getConnection();
			}

			// create a prepared statement and a statement
			PreparedStatement preparedPull = null;
			String pullStatement = "SELECT designconfigID FROM designconfig WHERE designconfigID = ?;";

			// don't autocommit the statement
			c.setAutoCommit(false);
			// set the prepeared statement
			preparedPull = c.prepareStatement(pullStatement);
			// set the parameters of the prepared statement
			preparedPull.setLong(1, designConfigID);

			// execute the query, this returns a resultset
			ResultSet rs = preparedPull.executeQuery();

			long pulledID = -1;
			if (rs.next()) {
				pulledID = rs.getLong(1);
			}
			// close statement and connection
			preparedPull.close();
			// Database.closeDatabase();

			if (pulledID != designConfigID)
				return false;
			return true;

		} catch (SQLException e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(1);
		}

		return false;
	}

	/**
	 * This method retrieves the count of rows in this table
	 * 
	 * @return an int, the number or rows in this table
	 */
	public static int countOf() {
		try {
			Connection c = Database.getConnection();
			if (c == null) {
				Database.openDatabase();
				c = Database.getConnection();
			}

			int count = 0;
			Statement s = null;

			s = c.createStatement();

			ResultSet rs = s.executeQuery("SELECT count(*) FROM designconfig;");

			if (rs.next()) {
				count = rs.getInt(1);
			}

			// close the connection
			s.close();
			// Database.closeDatabase();

			return count;
		} catch (SQLException e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(1);
		}

		return 0;
	}
	/**
	 * This method will update the row active to 1 from the designconfig
	 * 
	 * @param DesignConfigID
	 *            the designConfigID to update
	 * @return true on success, false on fail
	 */
	public static boolean setinactive(long designConfigID) {
		if (designConfigID < 0)
			return false;
		if (existsByID(designConfigID) == false)
			return false;
		try {
			Connection c = Database.getConnection();
			if (c == null) {
				Database.openDatabase();
				c = Database.getConnection();
			}

			// create a prepared statement and a statement
			PreparedStatement inactiveDesignConfig = null;
			String deleteStatement = "UPDATE designconfig set active = 1 WHERE designconfigID = ?;";
			// don't autocommit the statement
			c.setAutoCommit(false);
			// set the prepeared statement
			inactiveDesignConfig = c.prepareStatement(deleteStatement);
			// set the parameters of the prepared statement
			inactiveDesignConfig.setLong(1, designConfigID);
			// execute the statement
			inactiveDesignConfig.executeUpdate();

			c.commit();
			// Database.closeDatabase();
			return true;

		} catch (SQLException e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(1);
		}
		return false;
	}
	/**
	 * This method will update the row active from designconfig
	 * 
	 * @param DesignConfig
	 *            the designConfig to update
	 * @return true on success, false on fail
	 */
	public static boolean setinactive(DesignConfig a) {
		if (a == null)
			return false;
		if (existsByID(a.getDesignConfigID()) == false)
			return false;
		try {
			Connection c = Database.getConnection();
			if (c == null) {
				Database.openDatabase();
				c = Database.getConnection();
			}

			c.setAutoCommit(false);
			Statement s = null;

			s = c.createStatement();
			String sql = "update designconfig set active = 1 WHERE designconfigID = " + a.getDesignConfigID() + ";";
			s.executeUpdate(sql);

			s.close();
			c.commit();
			// Database.closeDatabase();
			return true;
		} catch (SQLException e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(1);
		}
		return false;
	}
	/**
	 * This method will update the row active to 0 from the designconfig
	 * 
	 * @param DesignConfigID
	 *            the designConfigID to update
	 * @return true on success, false on fail
	 */
	public static boolean setactive(long designConfigID) {
		if (designConfigID < 0)
			return false;
		if (existsByID(designConfigID) == false)
			return false;
		try {
			Connection c = Database.getConnection();
			if (c == null) {
				Database.openDatabase();
				c = Database.getConnection();
			}

			// create a prepared statement and a statement
			PreparedStatement activeDesignConfig = null;
			String deleteStatement = "UPDATE designconfig set active = 0 WHERE designconfigID = ?;";
			// don't autocommit the statement
			c.setAutoCommit(false);
			// set the prepeared statement
			activeDesignConfig = c.prepareStatement(deleteStatement);
			// set the parameters of the prepared statement
			activeDesignConfig.setLong(1, designConfigID);
			// execute the statement
			activeDesignConfig.executeUpdate();

			c.commit();
			// Database.closeDatabase();
			return true;

		} catch (SQLException e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(1);
		}
		return false;
	}
	/**
	 * This method will update the row active from designconfig
	 * 
	 * @param DesignConfig
	 *            the designConfig to update
	 * @return true on success, false on fail
	 */
	public static boolean setactive(DesignConfig a) {
		if (a == null)
			return false;
		if (existsByID(a.getDesignConfigID()) == false)
			return false;
		try {
			Connection c = Database.getConnection();
			if (c == null) {
				Database.openDatabase();
				c = Database.getConnection();
			}

			c.setAutoCommit(false);
			Statement s = null;

			s = c.createStatement();
			String sql = "update designconfig set active = 0 WHERE designconfigID = " + a.getDesignConfigID() + ";";
			s.executeUpdate(sql);

			s.close();
			c.commit();
			// Database.closeDatabase();
			return true;
		} catch (SQLException e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(1);
		}
		return false;
	}
}
