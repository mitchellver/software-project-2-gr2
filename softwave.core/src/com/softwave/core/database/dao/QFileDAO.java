package com.softwave.core.database.dao;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import com.softwave.core.container.quiz.QFile;
import com.softwave.core.database.Database;

/**
 * FileDAO class is used to access the individual tables in the database,
 * every method in this class is static will do a specific thing: adding an item
 * to the file table, removing, updating and reading data from the
 * database.
 *
 * @author eddi
 */
public class QFileDAO {
		public static boolean pushOrUpdate(QFile file) {
			if (file == null)
				return false;

			// check if the file is not number 0, does not exist allready
			if (file.getQfileID() == -1 || existsByID(file.getQfileID()) == false)
				return push(file); // create a new address
			// update the address
			return update(file);
		}

		/**
		 * This method will add an file to the file table.
		 *
		 * @param addres
		 *            the file to be stored
		 * @return the newly created ID, or -1 on fail
		 */
		public static boolean push(QFile file) {
			// check if the file has a valid ID
			if (file.getQfileID() != -1)
				return false;
			// check if the file does not exist allready
			if (existsByID(file.getQfileID()) == true)
				return false;

			try {
				Connection c = Database.getConnection();
				if (c == null) {
					Database.openDatabase();
					c = Database.getConnection();
				}

				// create a prepared statement and a statement
				PreparedStatement preparedPush = null;
				String pushStatement = "INSERT INTO file (questionID, path) VALUES (?,?);";

				// don't autocommit the statement
				c.setAutoCommit(false);
				// set the prepeared statement
				preparedPush = c.prepareStatement(pushStatement, PreparedStatement.RETURN_GENERATED_KEYS);

				// set the parameters of the prepared statement
				preparedPush.setLong(1, file.getQuestionID());
				preparedPush.setString(2, file.getPath());
				
				// execute the query, this returns a resultset
				int rowsUpdated = preparedPush.executeUpdate();

				// update the QFile ID
				try (ResultSet insertID = preparedPush.getGeneratedKeys()) {
					if (insertID.next()) {
						file.setQfileID(insertID.getLong(1));
					} else {
						throw new SQLException("Creating user failed, no ID obtained.");
					}
				} finally {
					preparedPush.close();
					c.commit();
					// Database.closeDatabase();
				}
				// if the push did not work and more or less then 1 row changed
				if (rowsUpdated == 1) {
					return true;
				}
				return false;

			} catch (SQLException e) {
				System.err.println(e.getClass().getName() + ": " + e.getMessage());
				System.exit(1);
			}
			// something else went wrong
			return false;
		}

		/**
		 * this is a method to update an existing row in the database. It checks if
		 * the row exists, and then updates it
		 * 
		 * @param a
		 *            the file to update, should have a valid id
		 * @return true on success false on fail
		 * @TODO ADD PREPARED STATEMENT !
		 */
		public static boolean update(QFile file) {
			// check if the row exists in the database
			if (existsByID(file.getQfileID()) == false)
				return false;

			try {
				Connection c = Database.getConnection();
				if (c == null) {
					Database.openDatabase();
					c = Database.getConnection();
				}

				// create a prepared statement and a statement
				PreparedStatement preparedUpdate = null;
				String pushStatement = "UPDATE file SET questionID=?, path=? WHERE fileID=?;";

				// don't autocommit the statement
				c.setAutoCommit(false);
				// set the prepeared statement
				preparedUpdate = c.prepareStatement(pushStatement);
				
				// set the parameters of the prepared statement
				preparedUpdate.setLong(1, file.getQuestionID());
				preparedUpdate.setString(2, file.getPath());
				preparedUpdate.setLong(3, file.getQfileID());
				
				// execute the query, this returns a resultset
				int rowsUpdated = preparedUpdate.executeUpdate();

				preparedUpdate.close();
				c.commit();
				// Database.closeDatabase();

				// check if the row was updated
				if (rowsUpdated == 1)
					return true;
				return false;

			} catch (SQLException e) {
				System.err.println(e.getClass().getName() + ": " + e.getMessage());
				System.exit(1);
			}
			return false;
		}

		/**
		 * This method will look up every file in the database that has the
		 * same city as the given one. It will then return an Arraylist of those
		 * filees
		 * 
		 * @param QFile
		 *            this is the city you want to look for
		 * @return null if no file was found, QFile if the
		 *         file was found an pulled.
		 */
		public static QFile pullByID(long qfileID) {
			if (qfileID < 0)
				return null;
			if (existsByID(qfileID) == false)
				return null;

			QFile file = null;
			
			try {
				Connection c = Database.getConnection();
				if (c == null) {
					Database.openDatabase();
					c = Database.getConnection();
				}

				// create a prepared statement and a statement
				PreparedStatement preparedPull = null;
				String pullStatement = "SELECT * FROM file WHERE fileID = ?;";

				// don't autocommit the statement
				c.setAutoCommit(false);
				// set the prepeared statement
				preparedPull = c.prepareStatement(pullStatement);
				// set the parameters of the prepared statement
				preparedPull.setLong(1, qfileID);

				// execute the query, this returns a resultset
				ResultSet rs = preparedPull.executeQuery();

				
				if (rs.next()) {
					file = new QFile();
					file.setQfileID(rs.getLong("fileID"));
					file.setQuestionID(rs.getLong("questionID"));
					file.setPath(rs.getString("path"));
				}

				// close statement and connection
				preparedPull.close();
				// Database.closeDatabase();

			} catch (SQLException e) {
				System.err.println(e.getClass().getName() + ": " + e.getMessage());
				System.exit(1);
			}

			return file;
		}

		/**
		 * This method will read all the filees in the file table
		 * and return every single record back as an item object array.
		 *
		 * @return an item object array constructed from all the filees in
		 *         the database.
		 */
		public static ArrayList<QFile> pullAll() {
			ArrayList<QFile> files = new ArrayList<>();
			QFile file = null;
			try {
				Connection c = Database.getConnection();
				if (c == null) {
					Database.openDatabase();
					c = Database.getConnection();
				}

				Statement s = null;

				s = c.createStatement();

				ResultSet rs = s.executeQuery("SELECT * FROM file;");

				while (rs.next()) {
					file = new QFile();
					file.setQfileID(rs.getLong("fileID"));
					file.setQuestionID(rs.getLong("questionID"));
					file.setPath(rs.getString("path"));
					// add the file to the ArrayList
					files.add(file);
				}

				// close the connection
				s.close();
				// Database.closeDatabase();
			} catch (SQLException e) {
				System.err.println(e.getClass().getName() + ": " + e.getMessage());
				System.exit(1);
			}

			return files;
		}

		/**
		 * This method will read all the files in the file table
		 * and returns every single file that belongs to a ginven questionIDback as an item object array.
		 *
		 * @param questionID the id for which we want to get all the files
		 * @return an item object array constructed from all the filees in
		 *         the database.
		 */
		public static ArrayList<QFile> pullByQuestionID(long questionID) {
			if (questionID < 0)
				return null;
			if (existsByID(questionID) == false)
				return null;

			QFile file = null;
			ArrayList<QFile> files = new ArrayList<>();

			try {
				Connection c = Database.getConnection();
				if (c == null) {
					Database.openDatabase();
					c = Database.getConnection();
				}
				
				
				// create a prepared statement and a statement
				PreparedStatement preparedPull = null;
				String pullStatement = "SELECT * FROM file WHERE questionID = ?;";

				// don't autocommit the statement
				c.setAutoCommit(false);
				// set the prepeared statement
				preparedPull = c.prepareStatement(pullStatement);
				// set the parameters of the prepared statement
				preparedPull.setLong(1, questionID);

				// execute the query, this returns a resultset
				ResultSet rs = preparedPull.executeQuery();

				while (rs.next()) {
					file = new QFile();
					file.setQfileID(rs.getLong("fileID"));
					file.setQuestionID(rs.getLong("questionID"));
					file.setPath(rs.getString("path"));
					// add the file to the ArrayList
					files.add(file);
				}

				// close the connection
				preparedPull.close();
				// Database.closeDatabase();
			} catch (SQLException e) {
				System.err.println(e.getClass().getName() + ": " + e.getMessage());
				System.exit(1);
			}

			return files;
		}
		
		/**
		 * This method will delete the file row from the tabel
		 * 
		 * @param QFile
		 *            the file to delete
		 * @return true on success, false on fail
		 */
		public static boolean deleteByID(long qfileID) {
			if (qfileID < 0)
				return false;
			if (existsByID(qfileID) == false)
				return false;
			try {
				Connection c = Database.getConnection();
				if (c == null) {
					Database.openDatabase();
					c = Database.getConnection();
				}

				// create a prepared statement and a statement
				PreparedStatement deleteString = null;
				String deleteStatement = "DELETE FROM file WHERE fileID = ?" + ";";
				// don't autocommit the statement
				c.setAutoCommit(false);
				// set the prepeared statement
				deleteString = c.prepareStatement(deleteStatement);
				// set the parameters of the prepared statement
				deleteString.setLong(1, qfileID);
				// execute the statement
				deleteString.executeUpdate();

				c.commit();
				// Database.closeDatabase();
				return true;

			} catch (SQLException e) {
				System.err.println(e.getClass().getName() + ": " + e.getMessage());
				System.exit(1);
			}
			return false;
		}
		
		/**
		 * This method will delete the file row from the tabel that belongs to a given questionID?
		 * 
		 * @param long the questionID for which we want to delete hthe files
		 * @return true on success, false on fail
		 */
		public static boolean deleteByQuestionID(long questionID) {
			if (questionID < 0)
				return false;
			if (existsByID(questionID) == false)
				return false;
			try {
				Connection c = Database.getConnection();
				if (c == null) {
					Database.openDatabase();
					c = Database.getConnection();
				}

				// create a prepared statement and a statement
				PreparedStatement deleteString = null;
				String deleteStatement = "DELETE file WHERE questionID = ?" + ";";
				// don't autocommit the statement
				c.setAutoCommit(false);
				// set the prepeared statement
				deleteString = c.prepareStatement(deleteStatement);
				// set the parameters of the prepared statement
				deleteString.setLong(1, questionID);
				// execute the statement
				deleteString.executeUpdate();

				c.commit();
				// Database.closeDatabase();
				return true;

			} catch (SQLException e) {
				System.err.println(e.getClass().getName() + ": " + e.getMessage());
				System.exit(1);
			}
			return false;
		}
		
		/**
		 * This method will delete the file row from the tabel
		 * 
		 * @param QFile
		 *            the file to delete
		 * @return true on success, false on fail
		 */
		public static boolean deleteByObject(QFile a) {
			if (a == null)
				return false;
			if (existsByID(a.getQfileID()) == false)
				return false;
			try {
				Connection c = Database.getConnection();
				if (c == null) {
					Database.openDatabase();
					c = Database.getConnection();
				}

				c.setAutoCommit(false);
				Statement s = null;

				s = c.createStatement();
				String sql = "DELETE FROM file WHERE fileID = " + a.getQfileID() + ";";
				s.executeUpdate(sql);

				s.close();
				c.commit();
				// Database.closeDatabase();
				return true;
			} catch (SQLException e) {
				System.err.println(e.getClass().getName() + ": " + e.getMessage());
				System.exit(1);
			}
			return false;
		}

		/**
		 * check if an file with a certain ID exists in the database
		 * 
		 * @param long
		 *            the id of the file to look for
		 * @return false if the file does not exists, true if the
		 *         file exists
		 * @TODO refactor this, not actually pulling the whole file, just
		 *       returning the ID oof the file and compare to be sure it's
		 *       the same.
		 */
		public static boolean existsByID(long qfileID) {
			if (qfileID < 0)
				return false;

			try {
				Connection c = Database.getConnection();
				if (c == null) {
					Database.openDatabase();
					c = Database.getConnection();
				}

				// create a prepared statement and a statement
				PreparedStatement preparedPull = null;
				String pullStatement = "SELECT fileID FROM file WHERE fileID = ?;";

				// don't autocommit the statement
				c.setAutoCommit(false);
				// set the prepeared statement
				preparedPull = c.prepareStatement(pullStatement);
				// set the parameters of the prepared statement
				preparedPull.setLong(1, qfileID);

				// execute the query, this returns a resultset
				ResultSet rs = preparedPull.executeQuery();

				long pulledID = -1;
				if (rs.next()) {
					pulledID = rs.getLong(1);
				}
				// close statement and connection
				preparedPull.close();
				// Database.closeDatabase();

				if (pulledID != qfileID)
					return false;
				return true;

			} catch (SQLException e) {
				System.err.println(e.getClass().getName() + ": " + e.getMessage());
				System.exit(1);
			}

			return false;
		}

		/**
		 * This method retrieves the count of rows in this table
		 * 
		 * @return an int, the number or rows in this table
		 */
		public static int countOf() {
			try {
				Connection c = Database.getConnection();
				if (c == null) {
					Database.openDatabase();
					c = Database.getConnection();
				}

				int count = 0;
				Statement s = null;

				s = c.createStatement();

				ResultSet rs = s.executeQuery("SELECT count(*) FROM file;");

				if (rs.next()) {
					count = rs.getInt(1);
				}

				// close the connection
				s.close();
				// Database.closeDatabase();

				return count;
			} catch (SQLException e) {
				System.err.println(e.getClass().getName() + ": " + e.getMessage());
				System.exit(1);
			}

			return 0;
		}
}
