package com.softwave.core.database.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import com.softwave.core.container.user.Address;
import com.softwave.core.database.Database;

/**
 * AddressDAO class is used to access the individual tables in the database,
 * every method in this class is static will do a specific thing: adding an item
 * to the address table, removing, updating and reading data from the database.
 *
 * @author Nicolas en eddi en Mitchell
 */
public abstract class AddressDAO {

	public static boolean pushOrUpdate(Address address) {
		if (address == null)
			return false;
		// check if the address is not number 0, does not exist allready
		if (address.getAddressID() == -1 || existsByID(address.getAddressID()) == false)
			return push(address); // create a new address
		// update the address
		return update(address);
	}

	/**
	 * This method will add an address to the address table.
	 *
	 * @param addres
	 *            the address to be stored
	 * @return the newly created ID, or -1 on fail
	 */
	public static boolean push(Address address) {
		// check if the address has a valid ID
		if (address.getAddressID() != -1)
			return false;
		// check if the address does not exist allready
		if (existsByID(address.getAddressID()) == true)
			return false;

		try {
			Connection c = Database.getConnection();
			if (c == null) {
				Database.openDatabase();
				c = Database.getConnection();
			}

			// create a prepared statement and a statement
			PreparedStatement preparedPush = null;
			String pushStatement = "INSERT INTO address (street, nr, poBox, zipcode, city, country, active) VALUES (?,?,?,?,?,?,?);";

			// don't autocommit the statement
			c.setAutoCommit(false);
			// set the prepeared statement
			preparedPush = c.prepareStatement(pushStatement, PreparedStatement.RETURN_GENERATED_KEYS);
			// set the parameters of the prepared statement
			preparedPush.setString(1, address.getStreet());
			preparedPush.setString(2, address.getNr());
			preparedPush.setString(3, address.getPoBox());
			preparedPush.setInt(4, address.getZipcode());
			preparedPush.setString(5, address.getCity());
			preparedPush.setString(6, address.getCountry());
			preparedPush.setBoolean(7, true);
			// execute the query, this returns a resultset
			int rowsUpdated = preparedPush.executeUpdate();

			// update the Address ID
			try (ResultSet insertID = preparedPush.getGeneratedKeys()) {
				if (insertID.next()) {
					address.setAddressID(insertID.getLong(1));
					return true;
				} else {
					throw new SQLException("Creating user failed, no ID obtained.");
				}
			} finally {
				preparedPush.close();
				c.commit();
				// Database.closeDatabase();
				// if the push did not work and more or less then 1 row changed
				if (rowsUpdated != 1) {
					return false;
				}
			}

		} catch (SQLException e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(1);
		}
		// something else went wrong
		return false;
	}

	/**
	 * this is a method to update an existing row in the database. It checks if
	 * the row exists, and then updates it
	 * 
	 * @param a
	 *            the address to update, should have a valid id
	 * @return true on success false on fail
	 * @TODO ADD PREPARED STATEMENT !
	 */
	public static boolean update(Address address) {
		// check if the row exists in the database
		if (existsByID(address.getAddressID()) == false)
			return false;

		try {
			Connection c = Database.getConnection();
			if (c == null) {
				Database.openDatabase();
				c = Database.getConnection();
			}

			// create a prepared statement and a statement
			PreparedStatement preparedUpdate = null;
			String pushStatement = "UPDATE address SET street=?, nr=?, poBox=?, zipcode=?, city=?, country=?,active=? WHERE addressID=?;";

			// don't autocommit the statement
			c.setAutoCommit(false);
			// set the prepeared statement
			preparedUpdate = c.prepareStatement(pushStatement);
			// set the parameters of the prepared statement
			preparedUpdate.setString(1, address.getStreet());
			preparedUpdate.setString(2, address.getNr());
			preparedUpdate.setString(3, address.getPoBox());
			preparedUpdate.setInt(4, address.getZipcode());
			preparedUpdate.setString(5, address.getCity());
			preparedUpdate.setString(6, address.getCountry());
			preparedUpdate.setBoolean(7, true);
			preparedUpdate.setLong(8, address.getAddressID());

			// execute the query, this returns a resultset
			int rowsUpdated = preparedUpdate.executeUpdate();

			preparedUpdate.close();
			c.commit();
			// Database.closeDatabase();

			// check if the row was updated
			if (rowsUpdated == 1)
				return true;
			return false;

		} catch (SQLException e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(1);
		}
		return false;
	}

	/**
	 * This method will look up every address in the database that has the same
	 * city as the given one. It will then return an Arraylist of those
	 * addresses
	 * 
	 * @param String
	 *            this is the city you want to look for
	 * @return null if no address was found, Address if the address was found an
	 *         pulled.
	 */
	public static Address pullByID(long addressID) {
		if (addressID < 0)
			return null;
		if (existsByID(addressID) == false)
			return null;

		Address address = null;

		try {
			Connection c = Database.getConnection();
			if (c == null) {
				Database.openDatabase();
				c = Database.getConnection();
			}

			// create a prepared statement and a statement
			PreparedStatement preparedPull = null;
			String pullStatement = "SELECT * FROM address WHERE addressID = ?;";

			// don't autocommit the statement
			c.setAutoCommit(false);
			// set the prepeared statement
			preparedPull = c.prepareStatement(pullStatement);
			// set the parameters of the prepared statement
			preparedPull.setLong(1, addressID);

			// execute the query, this returns a resultset
			ResultSet rs = preparedPull.executeQuery();

			if (rs.next()) {
				address = new Address();
				address.setAddressID(rs.getLong("addressID"));
				address.setStreet(rs.getString("street"));
				address.setNr(rs.getString("nr"));
				address.setPoBox(rs.getString("poBox"));
				address.setZipcode(rs.getInt("zipcode"));
				address.setCity(rs.getString("city"));
				address.setCountry(rs.getString("country"));
			}

			// close statement and connection
			preparedPull.close();
			// Database.closeDatabase();

		} catch (SQLException e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(1);
		}

		return address;
	}

	/**
	 * This method will look up every address in the database that has the same
	 * city as the given one. It will then return an Arraylist of those
	 * addresses
	 * 
	 * @param String
	 *            this is the city you want to look for
	 * @return an arraylist with all addresses that have the specified city
	 */
	public static ArrayList<Address> pullByCity(String city) {
		ArrayList<Address> addresses = new ArrayList<>();

		try {
			Connection c = Database.getConnection();
			if (c == null) {
				Database.openDatabase();
				c = Database.getConnection();
			}

			// create a prepared statement and a statement
			PreparedStatement preparedPull = null;
			String pullStatement = "SELECT * FROM address WHERE lower(city) LIKE lower('%?%');";

			// don't autocommit the statement
			c.setAutoCommit(false);
			// set the prepeared statement
			preparedPull = c.prepareStatement(pullStatement);
			// set the parameters of the prepared statement
			preparedPull.setString(1, city);

			// execute the query, this returns a resultset
			ResultSet rs = preparedPull.executeQuery();

			while (rs.next()) {
				Address address = new Address();
				address.setAddressID(rs.getLong("addressID"));
				address.setStreet(rs.getString("street"));
				address.setNr(rs.getString("nr"));
				address.setPoBox(rs.getString("poBox"));
				address.setZipcode(rs.getInt("zipcode"));
				address.setCity(rs.getString("city"));
				address.setCountry(rs.getString("country"));
				addresses.add(address);
			}

			// close the statement and connection
			preparedPull.close();
			// Database.closeDatabase();

		} catch (SQLException e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(1);
		}

		return addresses;
	}

	/**
	 * This method will read all the addresses in the address table and return
	 * every single record back as an item object array.
	 *
	 * @return an item object array constructed from all the addresses in the
	 *         database.
	 */
	public static ArrayList<Address> pullAll() {
		ArrayList<Address> addresses = new ArrayList<>();

		try {
			Connection c = Database.getConnection();
			if (c == null) {
				Database.openDatabase();
				c = Database.getConnection();
			}

			Statement s = null;

			s = c.createStatement();

			ResultSet rs = s.executeQuery("SELECT * FROM address;");

			while (rs.next()) {
				Address address = new Address();
				address.setAddressID(rs.getLong("addressID"));
				address.setStreet(rs.getString("street"));
				address.setNr(rs.getString("nr"));
				address.setPoBox(rs.getString("poBox"));
				address.setZipcode(rs.getInt("zipcode"));
				address.setCity(rs.getString("city"));
				address.setCountry(rs.getString("country"));

				addresses.add(address);
			}

			// close the connection
			s.close();
			// Database.closeDatabase();
		} catch (SQLException e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(1);
		}

		return addresses;
	}

	/**
	 * This method will delete the address row from the tabel
	 * 
	 * @param Address
	 *            the address to delete
	 * @return true on success, false on fail
	 */
	public static boolean deleteByID(long addressID) {
		if (addressID < 0)
			return false;
		if (existsByID(addressID) == false)
			return false;
		try {
			Connection c = Database.getConnection();
			if (c == null) {
				Database.openDatabase();
				c = Database.getConnection();
			}

			// create a prepared statement and a statement
			PreparedStatement deleteAddress = null;
			String deleteStatement = "DELETE FROM address WHERE addressID =?;";
			// don't autocommit the statement
			c.setAutoCommit(false);
			// set the prepeared statement
			deleteAddress = c.prepareStatement(deleteStatement);
			// set the parameters of the prepared statement
			deleteAddress.setLong(1, addressID);
			// execute the statement
			deleteAddress.executeUpdate();

			c.commit();
			// Database.closeDatabase();
			return true;

		} catch (SQLException e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(1);
		}
		return false;
	}

	/**
	 * This method will delete the address row from the tabel
	 * 
	 * @param Address
	 *            the address to delete
	 * @return true on success, false on fail
	 */
	public static boolean deleteByObject(Address a) {
		if (a == null)
			return false;
		if (existsByID(a.getAddressID()) == false)
			return false;
		try {
			Connection c = Database.getConnection();
			if (c == null) {
				Database.openDatabase();
				c = Database.getConnection();
			}

			c.setAutoCommit(false);
			Statement s = null;

			s = c.createStatement();
			String sql = "DELETE FROM address WHERE addressID = " + a.getAddressID() + ";";
			s.executeUpdate(sql);

			s.close();
			c.commit();
			// Database.closeDatabase();
			return true;
		} catch (SQLException e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(1);
		}
		return false;
	}

	/**
	 * check if an address with a certain ID exists in the database
	 * 
	 * @param long
	 *            the id of the address to look for
	 * @return false if the address does not exists, true if the address exists
	 * @TODO refactor this, not actually pulling the whole address, just
	 *       returning the ID oof the address and compare to be sure it's the
	 *       same.
	 */
	public static boolean existsByID(long addressID) {
		if (addressID < 0)
			return false;

		try {
			Connection c = Database.getConnection();
			if (c == null) {
				Database.openDatabase();
				c = Database.getConnection();
			}

			// create a prepared statement and a statement
			PreparedStatement preparedPull = null;
			String pullStatement = "SELECT addressID FROM address WHERE addressID = ?;";

			// don't autocommit the statement
			c.setAutoCommit(false);
			// set the prepeared statement
			preparedPull = c.prepareStatement(pullStatement);
			// set the parameters of the prepared statement
			preparedPull.setLong(1, addressID);

			// execute the query, this returns a resultset
			ResultSet rs = preparedPull.executeQuery();

			long pulledID = -1;
			if (rs.next()) {
				pulledID = rs.getLong(1);
			}
			// close statement and connection
			preparedPull.close();
			// Database.closeDatabase();

			if (pulledID != addressID)
				return false;
			return true;

		} catch (SQLException e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(1);
		}

		return false;
	}

	/**
	 * This method retrieves the count of rows in this table
	 * 
	 * @return an int, the number or rows in this table
	 */
	public static int countOf() {
		try {
			Connection c = Database.getConnection();
			if (c == null) {
				Database.openDatabase();
				c = Database.getConnection();
			}

			int count = 0;
			Statement s = null;

			s = c.createStatement();

			ResultSet rs = s.executeQuery("SELECT count(*) FROM address;");

			if (rs.next()) {
				count = rs.getInt(1);
			}

			// close the connection
			s.close();
			// Database.closeDatabase();

			return count;
		} catch (SQLException e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(1);
		}

		return 0;
	}

	/**
	 * This method will update the active row from the address
	 * 
	 * @param addressID
	 *            the ID to update the address
	 * @return true on success, false on fail
	 */
	public boolean setinactive(long addressID) {
		if (addressID < 0)
			return false;
		if (existsByID(addressID) == false)
			return false;
		try {
			Connection c = Database.getConnection();
			if (c == null) {
				Database.openDatabase();
				c = Database.getConnection();
			}

			// create a prepared statement and a statement
			PreparedStatement setinactiveAddress = null;
			String deleteStatement = "UPDATE address set active=1 WHERE addressID =?;";
			// don't autocommit the statement
			c.setAutoCommit(false);
			// set the prepeared statement
			setinactiveAddress = c.prepareStatement(deleteStatement);
			// set the parameters of the prepared statement
			setinactiveAddress.setLong(1, addressID);
			// execute the statement
			setinactiveAddress.executeUpdate();

			c.commit();
			// Database.closeDatabase();
			return true;

		} catch (SQLException e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(1);
		}
		return false;
	}

	/**
	 * This method will update the address row from the tabel
	 * 
	 * @param Address
	 *            the address to update
	 * @return true on success, false on fail
	 */
	public boolean setinactive(Address a) {
		if (a == null)
			return false;
		if (existsByID(a.getAddressID()) == false)
			return false;
		try {
			Connection c = Database.getConnection();
			if (c == null) {
				Database.openDatabase();
				c = Database.getConnection();
			}

			c.setAutoCommit(false);
			Statement s = null;

			s = c.createStatement();
			String sql = "UPDATE address set active= 1 WHERE addressID = " + a.getAddressID() + ";";
			s.executeUpdate(sql);

			s.close();
			c.commit();
			// Database.closeDatabase();
			return true;
		} catch (SQLException e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(1);
		}
		return false;
	}

	/**
	 * this is a method to update an existing row of active in the database. It
	 * checks if the row exists, and then updates it
	 * 
	 * @param AddressID
	 *            the address to update, should have a valid id
	 * @return true on success false on fail
	 * @TODO ADD PREPARED STATEMENT !
	 */
	public boolean setactive(long addressID) {
		if (addressID < 0)
			return false;
		if (existsByID(addressID) == false)
			return false;
		try {
			Connection c = Database.getConnection();
			if (c == null) {
				Database.openDatabase();
				c = Database.getConnection();
			}

			// create a prepared statement and a statement
			PreparedStatement setinactiveAddress = null;
			String deleteStatement = "UPDATE address set active=0 WHERE addressID =?;";
			// don't autocommit the statement
			c.setAutoCommit(false);
			// set the prepeared statement
			setinactiveAddress = c.prepareStatement(deleteStatement);
			// set the parameters of the prepared statement
			setinactiveAddress.setLong(1, addressID);
			// execute the statement
			setinactiveAddress.executeUpdate();

			c.commit();
			// Database.closeDatabase();
			return true;

		} catch (SQLException e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(1);
		}
		return false;
	}

	/**
	 * this is a method to update an existing row active in the database. It
	 * checks if the row exists, and then updates it
	 * 
	 * @param a
	 *            the address to update, should have a valid id
	 * @return true on success false on fail
	 * @TODO ADD PREPARED STATEMENT !
	 */
	public boolean setactive(Address address) {
		if (address == null)
			return false;
		if (existsByID(address.getAddressID()) == false)
			return false;
		try {
			Connection c = Database.getConnection();
			if (c == null) {
				Database.openDatabase();
				c = Database.getConnection();
			}

			c.setAutoCommit(false);
			Statement s = null;

			s = c.createStatement();
			String sql = "UPDATE address set active= 0 WHERE addressID = " + address.getAddressID() + ";";
			s.executeUpdate(sql);

			s.close();
			c.commit();
			// Database.closeDatabase();
			return true;
		} catch (SQLException e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(1);
		}
		return false;
	}
}
