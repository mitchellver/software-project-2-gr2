package com.softwave.core.database.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import com.softwave.core.container.quiz.Answer;
import com.softwave.core.database.Database;
import com.softwave.core.util.Date;

/**
 * the AnswerDAO class is used to access the individual tables in the database,
 * every method in this class is static will do a specific thing: adding an item
 * to the answertable, removing, updating and reading data from the database.
 *
 * @author eddi
 */
public class AnswerDAO {

	public static boolean pushOrUpdate(Answer answer) {
		if (answer == null)
			return false;

		// check if the address is not number 0, does not exist allready
		if (answer.getAnswerID() == -1 || existsByID(answer.getAnswerID()) == false)
			return push(answer); // create a new address
		// update the address
		return update(answer);
	}

	/**
	 * This method will add an answer to the answer table.
	 *
	 * @param addres
	 *            the answer to be stored
	 * @return the newly created ID, or -1 on fail
	 */
	public static boolean push(Answer answer) {
		// check if the answer has a valid ID
		if (answer.getAnswerID() != -1)
			return false;
		// check if the answer does not exist allready
		if (existsByID(answer.getAnswerID()) == true)
			return false;

		try {
			Connection c = Database.getConnection();
			if (c == null) {
				Database.openDatabase();
				c = Database.getConnection();
			}

			// create a prepared statement and a statement
			PreparedStatement preparedPush = null;
			String pushStatement = "INSERT INTO answer (participationID, questionID, answerQuizer, answerDateStart, answerDateEnd, correctanswer) VALUES (?,?,?,?,?,?);";

			// don't autocommit the statement
			c.setAutoCommit(false);
			// set the prepeared statement
			preparedPush = c.prepareStatement(pushStatement, PreparedStatement.RETURN_GENERATED_KEYS);

			// set the parameters of the prepared statement
			preparedPush.setLong(1, answer.getParticipationID());
			preparedPush.setLong(2, answer.getQuestionID());
			preparedPush.setString(3, answer.getAnswer());
			if (answer.getStartTime() != null)
				preparedPush.setTimestamp(4, answer.getStartTime().convertLocalToTimestamp());
			else
				preparedPush.setNull(4, java.sql.Types.TIMESTAMP);
			if (answer.getEndTime() != null)
				preparedPush.setTimestamp(5, answer.getEndTime().convertLocalToTimestamp());
			else
				preparedPush.setNull(5, java.sql.Types.TIMESTAMP);
			preparedPush.setBoolean(6, answer.isCorrectAnswer());

			// execute the query, this returns a resultset
			int rowsUpdated = preparedPush.executeUpdate();

			// update the Answer ID
			try (ResultSet insertID = preparedPush.getGeneratedKeys()) {
				if (insertID.next()) {
					answer.setAnswerID(insertID.getLong(1));
					return true;
				} else {
					throw new SQLException("Creating answer failed, no ID obtained.");
				}
			} finally {
				preparedPush.close();
				c.commit();
				// Database.closeDatabase();
				// if the push did not work and more or less then 1 row changed
				if (rowsUpdated != 1) {
					return false;
				}
			}

		} catch (SQLException e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(1);
		}
		// something else went wrong
		return false;
	}

	/**
	 * this is a method to update an existing row in the database. It checks if
	 * the row exists, and then updates it
	 * 
	 * @param a
	 *            the answer to update, should have a valid id
	 * @return true on success false on fail
	 * @TODO ADD PREPARED STATEMENT !
	 */
	public static boolean update(Answer answer) {
		// check if the row exists in the database
		if (existsByID(answer.getAnswerID()) == false)
			return false;

		try {
			Connection c = Database.getConnection();
			if (c == null) {
				Database.openDatabase();
				c = Database.getConnection();
			}

			// create a prepared statement and a statement
			PreparedStatement preparedUpdate = null;
			String pushStatement = "UPDATE answer SET participationID=?, questionID=?, answerQuizer=?, answerDateStart=?, answerDateEnd=?, correctanswer=? WHERE answerID=?;";

			// don't autocommit the statement
			c.setAutoCommit(false);
			// set the prepeared statement
			preparedUpdate = c.prepareStatement(pushStatement);
			// set the parameters of the prepared statement
			preparedUpdate.setLong(1, answer.getParticipationID());
			preparedUpdate.setLong(2, answer.getQuestionID());
			preparedUpdate.setString(3, answer.getAnswer());
			if (answer.getStartTime() != null)
				preparedUpdate.setTimestamp(4, answer.getStartTime().convertLocalToTimestamp());
			else
				preparedUpdate.setNull(4, java.sql.Types.TIMESTAMP);
			if (answer.getEndTime() != null)
				preparedUpdate.setTimestamp(5, answer.getEndTime().convertLocalToTimestamp());
			else
				preparedUpdate.setNull(5, java.sql.Types.TIMESTAMP);
			preparedUpdate.setBoolean(6, answer.isCorrectAnswer());
			preparedUpdate.setLong(7, answer.getAnswerID());

			// execute the query, this returns a resultset
			int rowsUpdated = preparedUpdate.executeUpdate();

			preparedUpdate.close();
			c.commit();
			// Database.closeDatabase();

			// check if the row was updated
			if (rowsUpdated == 1)
				return true;
			return false;

		} catch (SQLException e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(1);
		}
		return false;
	}

	/**
	 * This method will look up every answer in the database that has the same
	 * city as the given one. It will then return an Arraylist of those answeres
	 * 
	 * @param String
	 *            this is the city you want to look for
	 * @return null if no answer was found, Answer if the answer was found an
	 *         pulled.
	 */
	public static Answer pullByID(long answerID) {
		if (answerID < 0)
			return null;
		if (existsByID(answerID) == false)
			return null;

		Answer answer = null;

		try {
			Connection c = Database.getConnection();
			if (c == null) {
				Database.openDatabase();
				c = Database.getConnection();
			}

			// create a prepared statement and a statement
			PreparedStatement preparedPull = null;
			String pullStatement = "SELECT * FROM answer WHERE answerID = ?;";

			// don't autocommit the statement
			c.setAutoCommit(false);
			// set the prepeared statement
			preparedPull = c.prepareStatement(pullStatement);
			// set the parameters of the prepared statement
			preparedPull.setLong(1, answerID);

			// execute the query, this returns a resultset
			ResultSet rs = preparedPull.executeQuery();

			if (rs.next()) {
				answer = new Answer();
				answer.setAnswerID(rs.getLong("answerID"));
				answer.setParticipationID(rs.getLong("participationID"));
				answer.setQuestionID(rs.getLong("questionID"));
				answer.setAnswer(rs.getString("answerQuizer"));
				answer.setCorrectAnswer(rs.getBoolean("correctanswer"));
				answer.setStartTime(new Date(rs.getTimestamp("answerDateStart")));
				answer.setEndTime(new Date(rs.getTimestamp("answerDateEnd")));
			}

			// close statement and connection
			preparedPull.close();
			// Database.closeDatabase();

		} catch (SQLException e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(1);
		}

		return answer;
	}

	/**
	 * This method will pull all the answers in the answer table that correspond
	 * to a give participationID as an ArrrayList.
	 *
	 * @return an ArrayList constructed from all the answers in the database
	 *         linked to a participationID.
	 */
	public static ArrayList<Answer> pullByParticipationID(long participationID) {
		if(participationID < 0)
			return null;
		if(ParticipationDAO.existsByID(participationID) == false)
			return null;
		
		ArrayList<Answer> answers = new ArrayList<>();
		Answer answer = null;
		try {
			Connection c = Database.getConnection();
			if (c == null) {
				Database.openDatabase();
				c = Database.getConnection();
			}

			// create a prepared statement and a statement
			PreparedStatement preparedPull = null;
			String pullStatement = "SELECT * FROM answer WHERE participationID = ?;";

			// don't autocommit the statement
			c.setAutoCommit(false);
			// set the prepeared statement
			preparedPull = c.prepareStatement(pullStatement);
			// set the parameters of the prepared statement
			preparedPull.setLong(1, participationID);

			// execute the query, this returns a resultset
			ResultSet rs = preparedPull.executeQuery();

			while (rs.next()) {
				answer = new Answer();
				answer.setAnswerID(rs.getLong("answerID"));
				answer.setParticipationID(rs.getLong("participationID"));
				answer.setQuestionID(rs.getLong("questionID"));
				answer.setAnswer(rs.getString("answerQuizer"));
				answer.setCorrectAnswer(rs.getBoolean("correctanswer"));
				answer.setStartTime(new Date(rs.getTimestamp("answerDateStart")));
				answer.setEndTime(new Date(rs.getTimestamp("answerDateEnd")));

				answers.add(answer);
			}

			// close the connection
			preparedPull.close();
			// Database.closeDatabase();
		} catch (SQLException e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(1);
		}

		return answers;
	}

	/**
	 * This method will pull all the answers in the answer table that correspond
	 * to a give participationID as an ArrrayList.
	 *
	 * @return an ArrayList constructed from all the answers in the database
	 *         linked to a participationID.
	 */
	public static ArrayList<Answer> pullByQuestionID(long questionID) {
		if (questionID < 0)
			return null;

		ArrayList<Answer> answers = new ArrayList<>();
		Answer answer = null;
		try {
			Connection c = Database.getConnection();
			if (c == null) {
				Database.openDatabase();
				c = Database.getConnection();
			}

			// create a prepared statement and a statement
			PreparedStatement preparedPull = null;
			String pullStatement = "SELECT * FROM answer WHERE questionID = ?;";

			// don't autocommit the statement
			c.setAutoCommit(false);
			// set the prepeared statement
			preparedPull = c.prepareStatement(pullStatement);
			// set the parameters of the prepared statement
			preparedPull.setLong(1, questionID);

			// execute the query, this returns a resultset
			ResultSet rs = preparedPull.executeQuery();

			while (rs.next()) {
				answer = new Answer();
				answer.setAnswerID(rs.getLong("answerID"));
				answer.setParticipationID(rs.getLong("participationID"));
				answer.setQuestionID(rs.getLong("questionID"));
				answer.setAnswer(rs.getString("answerQuizer"));
				answer.setCorrectAnswer(rs.getBoolean("correctanswer"));
				answer.setStartTime(new Date(rs.getTimestamp("answerDateStart")));
				answer.setEndTime(new Date(rs.getTimestamp("answerDateEnd")));

				answers.add(answer);
			}

			// close the connection
			preparedPull.close();
			// Database.closeDatabase();
		} catch (SQLException e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(1);
		}

		return answers;
	}
	
	/**
	 * This method will read all the answeres in the answer table and return
	 * every single record back as an item object array.
	 *
	 * @return an item object array constructed from all the answeres in the
	 *         database.
	 */
	public static ArrayList<Answer> pullAll() {
		ArrayList<Answer> answers = new ArrayList<>();
		Answer answer = null;
		try {
			Connection c = Database.getConnection();
			if (c == null) {
				Database.openDatabase();
				c = Database.getConnection();
			}

			Statement s = null;

			s = c.createStatement();

			ResultSet rs = s.executeQuery("SELECT * FROM answer;");

			while (rs.next()) {
				answer = new Answer();
				answer.setAnswerID(rs.getLong("answerID"));
				answer.setParticipationID(rs.getLong("participationID"));
				answer.setQuestionID(rs.getLong("questionID"));
				answer.setAnswer(rs.getString("answerQuizer"));
				answer.setCorrectAnswer(rs.getBoolean("correctanswer"));
				answer.setStartTime(new Date(rs.getTimestamp("answerDateStart")));
				answer.setEndTime(new Date(rs.getTimestamp("answerDateEnd")));

				answers.add(answer);
			}

			// close the connection
			s.close();
			// Database.closeDatabase();
		} catch (SQLException e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(1);
		}

		return answers;
	}

	/**
	 * This method will delete the answer row from the tabel
	 * 
	 * @param Answer
	 *            the answer to delete
	 * @return true on success, false on fail
	 */
	public static boolean deleteByID(long answerID) {
		if (answerID < 0)
			return false;
		if (existsByID(answerID) == false)
			return false;
		try {
			Connection c = Database.getConnection();
			if (c == null) {
				Database.openDatabase();
				c = Database.getConnection();
			}

			// create a prepared statement and a statement
			PreparedStatement deleteAnswer = null;
			String deleteStatement = "DELETE FROM answer WHERE answerID = ?;";
			// don't autocommit the statement
			c.setAutoCommit(false);
			// set the prepeared statement
			deleteAnswer = c.prepareStatement(deleteStatement);
			// set the parameters of the prepared statement
			deleteAnswer.setLong(1, answerID);
			// execute the statement
			deleteAnswer.executeUpdate();

			c.commit();
			// Database.closeDatabase();
			return true;

		} catch (SQLException e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(1);
		}
		return false;
	}

	/**
	 * This method will delete the answer row from the tabel
	 * 
	 * @param Answer
	 *            the answer to delete
	 * @return true on success, false on fail
	 */
	public static boolean deleteByObject(Answer a) {
		if (a == null)
			return false;
		if (existsByID(a.getAnswerID()) == false)
			return false;
		try {
			Connection c = Database.getConnection();
			if (c == null) {
				Database.openDatabase();
				c = Database.getConnection();
			}

			c.setAutoCommit(false);
			Statement s = null;

			s = c.createStatement();
			String sql = "DELETE FROM answer WHERE answerID = " + a.getAnswerID() + ";";
			s.executeUpdate(sql);

			s.close();
			c.commit();
			// Database.closeDatabase();
			return true;
		} catch (SQLException e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(1);
		}
		return false;
	}

	/**
	 * check if an answer with a certain ID exists in the database
	 * 
	 * @param long
	 *            the id of the answer to look for
	 * @return false if the answer does not exists, true if the answer exists
	 * @TODO refactor this, not actually pulling the whole answer, just
	 *       returning the ID oof the answer and compare to be sure it's the
	 *       same.
	 */
	public static boolean existsByID(long answerID) {
		if (answerID < 0)
			return false;

		try {
			Connection c = Database.getConnection();
			if (c == null) {
				Database.openDatabase();
				c = Database.getConnection();
			}

			// create a prepared statement and a statement
			PreparedStatement preparedPull = null;
			String pullStatement = "SELECT answerID FROM answer WHERE answerID = ?;";

			// don't autocommit the statement
			c.setAutoCommit(false);
			// set the prepeared statement
			preparedPull = c.prepareStatement(pullStatement);
			// set the parameters of the prepared statement
			preparedPull.setLong(1, answerID);

			// execute the query, this returns a resultset
			ResultSet rs = preparedPull.executeQuery();

			long pulledID = -1;
			if (rs.next()) {
				pulledID = rs.getLong(1);
			}
			// close statement and connection
			preparedPull.close();
			// Database.closeDatabase();

			if (pulledID != answerID)
				return false;
			return true;

		} catch (SQLException e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(1);
		}

		return false;
	}

	/**
	 * This method retrieves the count of rows in this table
	 * 
	 * @return an int, the number or rows in this table
	 */
	public static int countOf() {
		try {
			Connection c = Database.getConnection();
			if (c == null) {
				Database.openDatabase();
				c = Database.getConnection();
			}

			int count = 0;
			Statement s = null;

			s = c.createStatement();

			ResultSet rs = s.executeQuery("SELECT count(*) FROM answer;");

			if (rs.next()) {
				count = rs.getInt(1);
			}

			// close the connection
			s.close();
			// Database.closeDatabase();

			return count;
		} catch (SQLException e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(1);
		}

		return 0;
	}

}
