package com.softwave.core.database.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import com.softwave.core.container.quiz.QType;
import com.softwave.core.container.quiz.Question;
import com.softwave.core.database.Database;
import com.softwave.core.util.Language;
import com.softwave.core.util.StringUtils;

/**
 * QuestionDAO class is used to access the individual tables in the database,
 * every method in this class is static will do a specific thing: adding an item
 * to the question table, removing, updating and reading data from the database.
 *
 * @author eddi
 */
public class QuestionDAO {
	public static boolean pushOrUpdate(Question question) {
		if (question == null)
			return false;

		// check if the address is not number 0, does not exist allready
		if (question.getQuestionID() == -1 || existsByID(question.getQuestionID()) == false)
			return push(question); // create a new address
		// update the address
		return update(question);
	}

	/**
	 * This method will add an question to the question table.
	 *
	 * @param addres
	 *            the question to be stored
	 * @return the newly created ID, or -1 on fail
	 * @throws Exception
	 */
	public static boolean push(Question question) {
		// check if the question has a valid ID
		if (question.getQuestionID() != -1)
			return false;
		// check if the question does not exist allready
		if (existsByID(question.getQuestionID()) == true)
			return false;
		// check if it has a valid quiz
		if (QuizDAO.existsByID(question.getQuizID()) == false)
			return false;

		// check if the question has a designconfig
		// if so, first save the designconfig
		if (question.getDesignConfig() != null) {
			DesignConfigDAO.pushOrUpdate(question.getDesignConfig());
		}

		try {
			Connection c = Database.getConnection();
			if (c == null) {
				Database.openDatabase();
				c = Database.getConnection();
			}

			// create a prepared statement and a statement
			PreparedStatement preparedPush = null;
			String pushStatement = "INSERT INTO question (questionnumber, questionlanguage, name, type, correctanswer, possibleAnswers, "
					+ "time, needanswer, quizID, designconfigID) VALUES (?,?,?,?,?,?,?,?,?,?);";

			// don't autocommit the statement
			c.setAutoCommit(false);
			// set the prepeared statement
			preparedPush = c.prepareStatement(pushStatement, PreparedStatement.RETURN_GENERATED_KEYS);

			// set the parameters of the prepared statement
			preparedPush.setInt(1, question.getQuestionNumber());
			preparedPush.setString(2, question.getQuestionLang().toCodeString());
			preparedPush.setString(3, question.getName());
			preparedPush.setString(4, String.valueOf(question.getType().name()));
			preparedPush.setString(5, StringUtils.join(question.getPossibleAnswers()));
			preparedPush.setString(6, question.getCorrectAnswer());
			preparedPush.setInt(7, question.getTime());
			preparedPush.setBoolean(8, question.isNeedsAnswer());
			preparedPush.setLong(9, question.getQuizID());
			if (question.getDesignConfig() != null)
				preparedPush.setLong(10, question.getDesignConfig().getDesignConfigID());
			else
				preparedPush.setNull(10, java.sql.Types.INTEGER);


			// execute the query, this returns a resultset
			int rowsUpdated = preparedPush.executeUpdate();

			// update the Question ID
			try (ResultSet insertID = preparedPush.getGeneratedKeys()) {
				if (insertID.next()) {
					question.setQuestionID(insertID.getLong(1));
					// now that question has an id, check if the question has
					// files attached to it, if so, save the files

					if (question.getPictures() != null && question.getPictures().size() > 0) {
						question.getPictures().stream().forEach(e -> {
							// set the questionID of the qfile
							e.setQuestionID(question.getQuestionID());
							QFileDAO.pushOrUpdate(e);
						});
					}

					preparedPush.close();
					c.commit();
					// Database.closeDatabase();
					// if the push did not work and more or less then 1 row
					// changed
					if (rowsUpdated == 1) {
						return true;
					}
				} else {
					throw new SQLException("Creating user failed, no ID obtained.");
				}
			}

		} catch (SQLException e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(1);
		}
		// something else went wrong
		return false;
	}

	/**
	 * this is a method to update an existing row in the database. It checks if
	 * the row exists, and then updates it
	 * 
	 * @param a
	 *            the question to update, should have a valid id
	 * @return true on success false on fail
	 * @TODO ADD PREPARED STATEMENT !
	 */
	public static boolean update(Question question) {
		// check if the row exists in the database
		if (existsByID(question.getQuestionID()) == false)
			return false;

		// check if the question has a designconfig
		// if so, first save the designconfig
		if (question.getDesignConfig() != null) {
			DesignConfigDAO.pushOrUpdate(question.getDesignConfig());
			// TODO is the designConfigID set?
		}

		try {
			Connection c = Database.getConnection();
			if (c == null) {
				Database.openDatabase();
				c = Database.getConnection();
			}

			// create a prepared statement and a statement
			PreparedStatement preparedUpdate = null;
			String pushStatement = "UPDATE question SET questionnumber=?, questionlanguage=?, name=?, type=?, correctanswer=?, possibleAnswers=?, "
					+ "time=?, needanswer=?, quizID=?, designconfigID=? WHERE questionID=?;";

			// don't autocommit the statement
			c.setAutoCommit(false);
			// set the prepeared statement
			preparedUpdate = c.prepareStatement(pushStatement);
			// set the parameters of the prepared statement
			// set the parameters of the prepared statement
			preparedUpdate.setInt(1, question.getQuestionNumber());
			preparedUpdate.setString(2, question.getQuestionLang().toCodeString());
			preparedUpdate.setString(3, question.getName());
			preparedUpdate.setString(4, String.valueOf(question.getType().name()));
			preparedUpdate.setString(5, question.getCorrectAnswer());
			preparedUpdate.setString(6, StringUtils.join(question.getPossibleAnswers()));
			preparedUpdate.setInt(7, question.getTime());
			preparedUpdate.setBoolean(8, question.isNeedsAnswer());
			preparedUpdate.setLong(9, question.getQuizID());
			if (question.getDesignConfig() != null)
				preparedUpdate.setLong(10, question.getDesignConfig().getDesignConfigID());
			else
				preparedUpdate.setNull(10, java.sql.Types.INTEGER);
			preparedUpdate.setLong(11, question.getQuestionID());

			// execute the query, this returns a resultset
			int rowsUpdated = preparedUpdate.executeUpdate();

			preparedUpdate.close();
			c.commit();
			// Database.closeDatabase();

			// check if the row was updated
			if (rowsUpdated == 1) {
				// now that question has an id, check if the question has
				// files attached to it, if so, save the files
				if (question.getPictures() != null && question.getPictures().size() > 0) {
					question.getPictures().stream().forEach(e -> {
						e.setQuestionID(question.getQuestionID());
						QFileDAO.pushOrUpdate(e);
					});
				}
				return true;
			}
			return false;

		} catch (SQLException e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(1);
		}
		return false;
	}

	/**
	 * This method will look up every question in the database that has the same
	 * city as the given one. It will then return an Arraylist of those
	 * questiones
	 * 
	 * @param String
	 *            this is the city you want to look for
	 * @return null if no question was found, Question if the question was found
	 *         an pulled.
	 */
	public static Question pullByID(long questionID) {
		if (questionID < 0)
			return null;
		if (existsByID(questionID) == false)
			return null;

		Question question = null;

		try {
			Connection c = Database.getConnection();
			if (c == null) {
				Database.openDatabase();
				c = Database.getConnection();
			}

			// create a prepared statement and a statement
			PreparedStatement preparedPull = null;
			String pullStatement = "SELECT * FROM question WHERE questionID = ?;";

			// don't autocommit the statement
			c.setAutoCommit(false);
			// set the prepeared statement
			preparedPull = c.prepareStatement(pullStatement);
			// set the parameters of the prepared statement
			preparedPull.setLong(1, questionID);

			// execute the query, this returns a resultset
			ResultSet rs = preparedPull.executeQuery();
			if (rs.next()) {
				question = new Question();
				question.setQuestionID(rs.getLong("questionID"));
				question.setQuestionNumber(rs.getInt("questionnumber"));
				question.setQuestionLang(Language.createFromString(rs.getString("questionlanguage")));
				question.setName(rs.getString("name"));
				question.setType(QType.valueOf(rs.getString("type")));
				question.setTime(rs.getInt("time"));
				question.getPossibleAnswers().clear();
				question.addPossibleAnswer(StringUtils.split(rs.getString("possibleAnswers")));
				question.setCorrectAnswer(rs.getString("correctanswer"));
				question.setNeedsAnswer(rs.getBoolean("needanswer"));
				question.setQuizID(rs.getLong("quizID"));
				// this pulles the designconfig with the retrieved ID
				question.setDesignConfig(DesignConfigDAO.pullByID(rs.getLong("designconfigID")));
			}
			// look for files with this questionID and pull the files
			question.setPictures(QFileDAO.pullByQuestionID(questionID));

			// close statement and connection
			preparedPull.close();
			// Database.closeDatabase();

		} catch (SQLException e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(1);
		}

		return question;
	}

	/**
	 * This method will look up every question in the database that has the same
	 * city as the given one. It will then return an Arraylist of those
	 * questiones
	 * 
	 * @param String
	 *            this is the city you want to look for
	 * @return an arraylist with all questiones that have the specified city
	 */
	public static ArrayList<Question> pullByQuizID(long quizID) {
		if (quizID < 0)
			return null;
		// TODO add a check to see if Quiz exists
		// if(QuizDAO.existsByID(quizID) == false)
		// return null;

		ArrayList<Question> questions = new ArrayList<>();

		Question question = null;

		try {
			Connection c = Database.getConnection();
			if (c == null) {
				Database.openDatabase();
				c = Database.getConnection();
			}

			// create a prepared statement and a statement
			PreparedStatement preparedPull = null;
			String pullStatement = "SELECT * FROM question WHERE quizID=?;";

			// don't autocommit the statement
			c.setAutoCommit(false);
			// set the prepeared statement
			preparedPull = c.prepareStatement(pullStatement);
			// set the parameters of the prepared statement
			preparedPull.setLong(1, quizID);

			// execute the query, this returns a resultset
			ResultSet rs = preparedPull.executeQuery();

			while (rs.next()) {
				question = new Question();
				question.setQuestionID(rs.getLong("questionID"));
				question.setQuestionNumber(rs.getInt("questionnumber"));
				question.setQuestionLang(Language.createFromString(rs.getString("questionlanguage")));
				question.setName(rs.getString("name"));
				question.setType(QType.valueOf(rs.getString("type")));
				question.setTime(rs.getInt("time"));
				question.setCorrectAnswer(rs.getString("correctanswer"));
				question.getPossibleAnswers().clear();
				question.addPossibleAnswer(StringUtils.split(rs.getString("possibleAnswers")));
				question.setNeedsAnswer(rs.getBoolean("needanswer"));
				question.setQuizID(rs.getLong("quizID"));
				// this pulles the designconfig with the retrieved ID
				question.setDesignConfig(DesignConfigDAO.pullByID(rs.getLong("designconfigID")));
				// look for files with this questionID and pull the files
				question.setPictures(QFileDAO.pullByQuestionID(rs.getLong("questionID")));

				// add the question to the arraylist
				questions.add(question);
			}

			// close the statement and connection
			preparedPull.close();
			// Database.closeDatabase();

		} catch (SQLException e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(1);
		}

		return questions;
	}

	/**
	 * This method will read all the questiones in the question table and return
	 * every single record back as an item object array.
	 *
	 * @return an item object array constructed from all the questiones in the
	 *         database.
	 */
	public static ArrayList<Question> pullAll() {
		ArrayList<Question> questions = new ArrayList<>();
		Question question = null;
		try {
			Connection c = Database.getConnection();
			if (c == null) {
				Database.openDatabase();
				c = Database.getConnection();
			}

			Statement s = null;

			s = c.createStatement();

			ResultSet rs = s.executeQuery("SELECT * FROM question;");

			while (rs.next()) {
				question = new Question();
				question.setQuestionID(rs.getLong("questionID"));
				question.setQuestionNumber(rs.getInt("questionnumber"));
				question.setQuestionLang(Language.createFromString(rs.getString("questionlanguage")));
				question.setName(rs.getString("name"));
				question.setType(QType.valueOf(rs.getString("type")));
				question.setTime(rs.getInt("time"));
				question.setCorrectAnswer(rs.getString("correctanswer"));
				question.getPossibleAnswers().clear();
				question.addPossibleAnswer(StringUtils.split(rs.getString("possibleAnswers")));
				question.setNeedsAnswer(rs.getBoolean("needanswer"));
				question.setQuizID(rs.getLong("quizID"));
				// this pulles the designconfig with the retrieved ID
				question.setDesignConfig(DesignConfigDAO.pullByID(rs.getLong("designconfigID")));
				// look for files with this questionID and pull the files
				question.setPictures(QFileDAO.pullByQuestionID(rs.getLong("questionID")));

				// add the question to the list
				questions.add(question);
			}

			// close the connection
			s.close();
			// Database.closeDatabase();
		} catch (SQLException e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(1);
		}

		return questions;
	}

	/**
	 * This method will delete the question row from the tabel
	 * 
	 * @param Question
	 *            the question to delete
	 * @return true on success, false on fail
	 */
	public static boolean deleteByID(long questionID) {
		if (questionID < 0)
			return false;
		if (existsByID(questionID) == false)
			return false;

		try {
			Connection c = Database.getConnection();
			if (c == null) {
				Database.openDatabase();
				c = Database.getConnection();
			}

			// delete the designconfig first
			DesignConfigDAO.deleteByID(getDesignConfigIDByQuestionID(questionID));

			// create a prepared statement and a statement
			PreparedStatement deleteQuestion = null;
			String deleteStatement = "DELETE FROM question WHERE questionID = ?" + ";";
			// don't autocommit the statement
			c.setAutoCommit(false);
			// set the prepeared statement
			deleteQuestion = c.prepareStatement(deleteStatement);
			// set the parameters of the prepared statement
			deleteQuestion.setLong(1, questionID);
			// execute the statement
			deleteQuestion.executeUpdate();

			c.commit();
			// Database.closeDatabase();
			return true;

		} catch (SQLException e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(1);
		}
		return false;
	}

	/**
	 * This method will delete the question row from the tabel that matches a
	 * given quiz id
	 * 
	 * @param quiz
	 *            the quizID linked to the question to delete
	 * @return true on success, false on fail
	 */
	public static boolean deleteByQuizID(long quizID) {
		if (quizID < 0)
			return false;
		if (QuizDAO.existsByID(quizID) == false)
			return false;

		try {
			Connection c = Database.getConnection();
			if (c == null) {
				Database.openDatabase();
				c = Database.getConnection();
			}

			// get all the questionID's to later delete the designconfigs with
			ArrayList<Long> dcIDs = getDcIDsByQuizID(quizID);

			// create a prepared statement and a statement
			PreparedStatement deleteQuestion = null;
			String deleteStatement = "DELETE FROM question WHERE quizID = ?" + ";";
			// don't autocommit the statement
			c.setAutoCommit(false);
			// set the prepeared statement
			deleteQuestion = c.prepareStatement(deleteStatement);
			// set the parameters of the prepared statement
			deleteQuestion.setLong(1, quizID);
			// execute the statement
			deleteQuestion.executeUpdate();

			c.commit();
			// Database.closeDatabase();

			// finally delete the linked designconfigs
			if (dcIDs != null && dcIDs.size() > 0)
				dcIDs.forEach(e -> DesignConfigDAO.deleteByID(e));
			return true;

		} catch (SQLException e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(1);
		}
		return false;
	}

	/**
	 * This method will delete the question row from the tabel
	 * 
	 * @param Question
	 *            the question to delete
	 * @return true on success, false on fail
	 */
	public static boolean deleteByObject(Question a) {
		if (a == null)
			return false;
		if (existsByID(a.getQuestionID()) == false)
			return false;
		try {
			Connection c = Database.getConnection();
			if (c == null) {
				Database.openDatabase();
				c = Database.getConnection();
			}

			// get the designconfigId to delete it later
			long dcID = -1;
			if (a.getDesignConfig() != null)
				dcID = a.getDesignConfig().getDesignConfigID();

			c.setAutoCommit(false);
			Statement s = null;

			s = c.createStatement();
			String sql = "DELETE FROM question WHERE questionID = " + a.getQuestionID() + ";";
			s.executeUpdate(sql);

			s.close();
			c.commit();
			// Database.closeDatabase();

			// delete the designconfig
			DesignConfigDAO.deleteByID(dcID);
			return true;
		} catch (SQLException e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(1);
		}
		return false;
	}

	/**
	 * check if an question with a certain ID exists in the database
	 * 
	 * @param long
	 *            the id of the question to look for
	 * @return false if the question does not exists, true if the question
	 *         exists
	 * @TODO refactor this, not actually pulling the whole question, just
	 *       returning the ID oof the question and compare to be sure it's the
	 *       same.
	 */
	public static boolean existsByID(long questionID) {
		if (questionID < 0)
			return false;

		try {
			Connection c = Database.getConnection();
			if (c == null) {
				Database.openDatabase();
				c = Database.getConnection();
			}

			// create a prepared statement and a statement
			PreparedStatement preparedPull = null;
			String pullStatement = "SELECT questionID FROM question WHERE questionID = ?;";

			// don't autocommit the statement
			c.setAutoCommit(false);
			// set the prepeared statement
			preparedPull = c.prepareStatement(pullStatement);
			// set the parameters of the prepared statement
			preparedPull.setLong(1, questionID);

			// execute the query, this returns a resultset
			ResultSet rs = preparedPull.executeQuery();

			long pulledID = -1;
			if (rs.next()) {
				pulledID = rs.getLong(1);
			}
			// close statement and connection
			preparedPull.close();
			// Database.closeDatabase();

			if (pulledID != questionID)
				return false;
			return true;

		} catch (SQLException e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(1);
		}

		return false;
	}

	/**
	 * this is a method that finds a designconfigID for a given questionID
	 * 
	 * @param questionID
	 *            the id for which we want to find a dseignconfigID in the table
	 * @return -1 on fail, or a long with the designconfigID
	 */
	public static long getDesignConfigIDByQuestionID(long questionID) {
		if (questionID < 0)
			return -1;

		try {
			Connection c = Database.getConnection();
			if (c == null) {
				Database.openDatabase();
				c = Database.getConnection();
			}

			// create a prepared statement and a statement
			PreparedStatement preparedPull = null;
			String pullStatement = "SELECT designconfigID FROM question WHERE questionID = ?;";

			// don't autocommit the statement
			c.setAutoCommit(false);
			// set the prepeared statement
			preparedPull = c.prepareStatement(pullStatement);
			// set the parameters of the prepared statement
			preparedPull.setLong(1, questionID);

			// execute the query, this returns a resultset
			ResultSet rs = preparedPull.executeQuery();

			long pulledID = -1;
			if (rs.next()) {
				pulledID = rs.getLong(1);
			}
			// close statement and connection
			preparedPull.close();
			// Database.closeDatabase();

			return pulledID;

		} catch (SQLException e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(1);
		}

		return -1;
	}

	/**
	 * This method will retrieve all questionID's linked to a quizID
	 * 
	 * @return an ArrayList constructed from all the questionID's in the
	 *         database for a given quizID
	 */
	public static ArrayList<Long> getQuestionIDsByQuizID(long quizID) {
		ArrayList<Long> questionIDs = new ArrayList<>();

		try {
			Connection c = Database.getConnection();
			if (c == null) {
				Database.openDatabase();
				c = Database.getConnection();
			}

			// create a prepared statement and a statement
			PreparedStatement preparedGet = null;
			String getStatement = "SELECT questionID FROM question WHERE quizID = ?;";

			// don't autocommit the statement
			c.setAutoCommit(false);
			// set the prepeared statement
			preparedGet = c.prepareStatement(getStatement);
			// set the parameters of the prepared statement
			preparedGet.setLong(1, quizID);

			// execute the query, this returns a resultset
			ResultSet rs = preparedGet.executeQuery();

			c.commit();
			// Database.closeDatabase();

			while (rs.next()) {
				// add the question to the list
				questionIDs.add(rs.getLong("questionID"));
			}

			// close the connection
			preparedGet.close();
			// Database.closeDatabase();
		} catch (SQLException e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(1);
		}

		return questionIDs;
	}

	/**
	 * This method will retrieve all designconfigID's linked to a quizID
	 * 
	 * @return an ArrayList constructed from all the designconfigID's in the
	 *         database for a given quizID
	 */
	public static ArrayList<Long> getDcIDsByQuizID(long quizID) {
		ArrayList<Long> dcIDs = new ArrayList<>();

		try {
			Connection c = Database.getConnection();
			if (c == null) {
				Database.openDatabase();
				c = Database.getConnection();
			}

			// create a prepared statement and a statement
			PreparedStatement preparedGet = null;
			String getStatement = "SELECT designconfigID FROM question WHERE quizID = ?;";

			// don't autocommit the statement
			c.setAutoCommit(false);
			// set the prepeared statement
			preparedGet = c.prepareStatement(getStatement);
			// set the parameters of the prepared statement
			preparedGet.setLong(1, quizID);

			// execute the query, this returns a resultset
			ResultSet rs = preparedGet.executeQuery();

			c.commit();
			// Database.closeDatabase();

			while (rs.next()) {
				// add the question to the list
				dcIDs.add(rs.getLong("designconfigID"));
			}

			// close the connection
			preparedGet.close();
			// Database.closeDatabase();
		} catch (SQLException e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(1);
		}

		return dcIDs;
	}

	/**
	 * This method retrieves the count of rows in this table
	 * 
	 * @return an int, the number or rows in this table
	 */
	public static int countOf() {
		try {
			Connection c = Database.getConnection();
			if (c == null) {
				Database.openDatabase();
				c = Database.getConnection();
			}

			int count = 0;
			Statement s = null;

			s = c.createStatement();

			ResultSet rs = s.executeQuery("SELECT count(*) FROM question;");

			if (rs.next()) {
				count = rs.getInt(1);
			}

			// close the connection
			s.close();
			// Database.closeDatabase();

			return count;
		} catch (SQLException e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(1);
		}

		return 0;
	}

}
