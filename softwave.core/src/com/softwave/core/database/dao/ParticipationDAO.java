package com.softwave.core.database.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import com.softwave.core.container.quiz.Participation;
import com.softwave.core.database.Database;
import com.softwave.core.util.Date;

/**
 * The ParticipationDAO class is used to access the individual tables in the
 * database, every method in this class is static will do a specific thing:
 * adding an item to the participation table, removing, updating and reading
 * data from the database.
 *
 * @author eddi
 */
public class ParticipationDAO {

	public static boolean pushOrUpdate(Participation participation) {
		if (participation == null)
			return false;
		// check if the participationID is not number 0, does not exist allready
		if (participation.getParticipationID() == -1 || existsByID(participation.getParticipationID()) == false)
			return push(participation); // create a new participation
		// update the participation
		return update(participation);
	}

	/**
	 * This method will add an participation to the participation table.
	 *
	 * @param addres
	 *            the participation to be stored
	 * @return the newly created ID, or -1 on fail
	 */
	public static boolean push(Participation participation) {
		if (participation == null)
			return false;
		// check if the participation has a valid ID
		if (participation.getParticipationID() != -1)
			return false;
		// check if the participation does not exist allready
		if (existsByID(participation.getParticipationID()) == true)
			return false;

		try {
			Connection c = Database.getConnection();
			if (c == null) {
				Database.openDatabase();
				c = Database.getConnection();
			}

			// do not autocommit
			c.setAutoCommit(false);

			// create a prepared statement and a statement
			PreparedStatement preparedPush = null;

			String pushStatement = "INSERT INTO participation (userID, quizID, tryDate, timeElapsed, score) VALUES (?,?,?,?,?);";

			// set the prepeared statement
			preparedPush = c.prepareStatement(pushStatement, PreparedStatement.RETURN_GENERATED_KEYS);
			// set the Participation-members
			preparedPush.setLong(1, participation.getQuizzer().getUserID());
			preparedPush.setLong(2, participation.getQuiz().getQuizID());
			if(participation.getStartTime() != null)
			preparedPush.setTimestamp(3, participation.getStartTime().convertLocalToTimestamp());
			else
				preparedPush.setNull(3, java.sql.Types.TIMESTAMP);
			preparedPush.setInt(4, participation.getTimeElapsed());
			preparedPush.setDouble(5, participation.getScore());

			// execute the query, this returns a resultset
			int rowsUpdated = preparedPush.executeUpdate();

			// update the Participation ID
			try (ResultSet rs = preparedPush.getGeneratedKeys()) {
				if (rs.next()) {
					// update the iD and timestamp
					participation.setParticipationID(rs.getLong(1));
					// update the answers particiationID
					participation.getAnswers().stream().forEach(e -> {
						e.setParticipationID(participation.getParticipationID());
					});
				} else {
					throw new SQLException("Creating participation failed, no ID obtained.");
				}
			} finally {
				preparedPush.close();
				c.commit();
				// Database.closeDatabase();
				// push the answers
				if(participation.getAnswers() != null ){
					participation.getAnswers().stream().forEach(e -> {
						AnswerDAO.pushOrUpdate(e);
					});
				}
				if (rowsUpdated != 1) {
					return false;
				}
			}

		} catch (SQLException e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(1);
		}
		return true;
	}

	/**
	 * this is a method to update an existing row in the database. It checks if
	 * the row exists, and then updates it
	 * 
	 * @param a
	 *            the participation to update, should have a valid id
	 * @return true on success false on fail
	 * @TODO ADD PREPARED STATEMENT !
	 */
	public static boolean update(Participation participation) {
		if (participation == null)
			return false;
		// check if the row exists in the database
		if (existsByID(participation.getParticipationID()) == false)
			return false;

		try {
			Connection c = Database.getConnection();
			if (c == null) {
				Database.openDatabase();
				c = Database.getConnection();
			}

			// do not autocommit
			c.setAutoCommit(false);

			// create a prepared statement and a statement
			PreparedStatement preparedUpdate = null;

			String updateStatement = "UPDATE participation SET userID=?, quizID=?, tryDate=?, "
					+ "timeElapsed=?, score=? WHERE participationID=?;";

			// set the prepeared statement
			preparedUpdate = c.prepareStatement(updateStatement, PreparedStatement.RETURN_GENERATED_KEYS);
			// set the Participation-members
			preparedUpdate.setLong(1, participation.getQuizzer().getUserID());
			preparedUpdate.setLong(2, participation.getQuiz().getQuizID());
			preparedUpdate.setTimestamp(3, participation.getStartTime().convertLocalToTimestamp());
			preparedUpdate.setInt(4, participation.getTimeElapsed());
			preparedUpdate.setDouble(5, participation.getScore());
			preparedUpdate.setLong(6, participation.getParticipationID());

			// execute the query, this returns a resultset
			int rowsUpdated = preparedUpdate.executeUpdate();

			preparedUpdate.close();
			c.commit();
			// Database.closeDatabase();
			// push the Answers
			if(participation.getAnswers() != null ){
				participation.getAnswers().stream().forEach(e -> {
					AnswerDAO.pushOrUpdate(e);
				});
			}
			// check if the row was updated
			if (rowsUpdated == 1)
				return true;
			return false;

		} catch (SQLException e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(1);
		}
		return false;
	}

	/**
	 * This method will look up every participation in the database that has the
	 * same city as the given one. It will then return an Arraylist of those
	 * participationes
	 * 
	 * @param String
	 *            this is the city you want to look for
	 * @return null if no participation was found, Address if the participation
	 *         was found an pulled.
	 */
	public static Participation pullByID(long participationID) {
		if (participationID < 0)
			return null;
		if (existsByID(participationID) == false)
			return null;

		Participation participation = null;

		try {
			Connection c = Database.getConnection();
			if (c == null) {
				Database.openDatabase();
				c = Database.getConnection();
			}

			// create a prepared statement and a statement
			PreparedStatement preparedPull = null;
			String pullStatement = "SELECT * FROM participation WHERE participationID = ?;";

			// don't autocommit the statement
			c.setAutoCommit(false);
			// set the prepeared statement
			preparedPull = c.prepareStatement(pullStatement);
			// set the parameters of the prepared statement
			preparedPull.setLong(1, participationID);

			// execute the query, this returns a resultset
			ResultSet rs = preparedPull.executeQuery();

			// only get the first participation out, normally there is only one
			// participation.
			if (rs.next()) {
				participation = new Participation();
				participation.setParticipationID(rs.getLong("participationID"));
				participation.setQuizzer(UserDAO.pullByID(rs.getLong("userID")));
				participation.setQuiz(QuizDAO.pullByID(rs.getLong("quizID")));
				participation.setStartTime(new Date(rs.getTimestamp("tryDate")));
				participation.setTimeElapsed(rs.getInt("timeElapsed"));
				participation.setScore(rs.getDouble("score"));
				// get the answers
				participation.setAnswers(AnswerDAO.pullByParticipationID(participationID));
			}

			// close statement and connection
			preparedPull.close();
			// Database.closeDatabase();

		} catch (SQLException e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(1);
		}

		return participation;
	}

	/**
	 * This method will read all the participations in the participation table
	 * and return every participation linked to a ceratin user back as an item
	 * object array.
	 *
	 * @return an ArrayList constructed from all the participations in the
	 *         database.
	 */
	public static ArrayList<Participation> pullByQuizID(long quizID) {
		if(quizID < 0)
			return null;
		if(QuizDAO.existsByID(quizID) == false)
			return null;
		
		ArrayList<Participation> participations = new ArrayList<>();
		Participation participation = null;

		try {
			Connection c = Database.getConnection();
			if (c == null) {
				Database.openDatabase();
				c = Database.getConnection();
			}
			// create a prepared statement and a statement
			PreparedStatement preparedPull = null;
			String pullStatement = "SELECT * FROM answer WHERE quizID = ?;";

			// don't autocommit the statement
			c.setAutoCommit(false);
			// set the prepeared statement
			preparedPull = c.prepareStatement(pullStatement);
			// set the parameters of the prepared statement
			preparedPull.setLong(1, quizID);

			// execute the query, this returns a resultset
			ResultSet rs = preparedPull.executeQuery();

			while (rs.next()) {
				participation = new Participation();
				participation.setParticipationID(rs.getLong("participationID"));
				participation.setQuizzer(UserDAO.pullByID(rs.getLong("userID")));
				participation.setQuiz(QuizDAO.pullByID(rs.getLong("quizID")));
				participation.setStartTime(new Date(rs.getTimestamp("tryDate")));
				participation.setTimeElapsed(rs.getInt("timeElapsed"));
				participation.setScore(rs.getDouble("score"));
				// get the answers
				participation.setAnswers(AnswerDAO.pullByParticipationID(participation.getParticipationID()));

				participations.add(participation);
			}

			// close the connection
			preparedPull.close();
			// Database.closeDatabase();
		} catch (SQLException e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(1);
		}

		return participations;
	}
	
	/**
	 * This method will read all the participations in the participation table
	 * and return every participation linked to a ceratin user back as an item
	 * object array.
	 *
	 * @return an ArrayList constructed from all the participations in the
	 *         database.
	 */
	public static ArrayList<Participation> pullByUserID(long userID) {
		if(userID < 0)
			return null;
		if(UserDAO.existsByID(userID) == false)
			return null;
		
		ArrayList<Participation> participations = new ArrayList<>();
		Participation participation = null;

		try {
			Connection c = Database.getConnection();
			if (c == null) {
				Database.openDatabase();
				c = Database.getConnection();
			}
			// create a prepared statement and a statement
			PreparedStatement preparedPull = null;
			String pullStatement = "SELECT * FROM answer WHERE userID = ?;";

			// don't autocommit the statement
			c.setAutoCommit(false);
			// set the prepeared statement
			preparedPull = c.prepareStatement(pullStatement);
			// set the parameters of the prepared statement
			preparedPull.setLong(1, userID);

			// execute the query, this returns a resultset
			ResultSet rs = preparedPull.executeQuery();

			while (rs.next()) {
				participation = new Participation();
				participation.setParticipationID(rs.getLong("participationID"));
				participation.setQuizzer(UserDAO.pullByID(rs.getLong("userID")));
				participation.setQuiz(QuizDAO.pullByID(rs.getLong("quizID")));
				participation.setStartTime(new Date(rs.getTimestamp("tryDate")));
				participation.setTimeElapsed(rs.getInt("timeElapsed"));
				participation.setScore(rs.getDouble("score"));
				// get the answers
				participation.setAnswers(AnswerDAO.pullByParticipationID(participation.getParticipationID()));
				participations.add(participation);
			}

			// close the connection
			preparedPull.close();
			// Database.closeDatabase();
		} catch (SQLException e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(1);
		}

		return participations;
	}

	/**
	 * This method will read all the participations in the participation table
	 * and return every single record back as an item object array.
	 *
	 * @return an ArrayList constructed from all the participations in the
	 *         database.
	 */
	public static ArrayList<Participation> pullAll() {
		ArrayList<Participation> participations = new ArrayList<>();
		Participation participation = null;

		try {
			Connection c = Database.getConnection();
			if (c == null) {
				Database.openDatabase();
				c = Database.getConnection();
			}

			Statement s = null;

			s = c.createStatement();

			ResultSet rs = s.executeQuery("SELECT * FROM participation;");

			while (rs.next()) {
				participation = new Participation();
				participation.setParticipationID(rs.getLong("participationID"));
				participation.setQuizzer(UserDAO.pullByID(rs.getLong("userID")));
				participation.setQuiz(QuizDAO.pullByID(rs.getLong("quizID")));
				participation.setStartTime(new Date(rs.getTimestamp("tryDate")));
				participation.setTimeElapsed(rs.getInt("timeElapsed"));
				participation.setScore(rs.getDouble("score"));
				// get the answers
				participation.setAnswers(AnswerDAO.pullByParticipationID(participation.getParticipationID()));
				
				participations.add(participation);
			}

			// close the connection
			s.close();
			// Database.closeDatabase();
		} catch (SQLException e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(1);
		}

		return participations;
	}

	/**
	 * This method will delete the participation row from the tabel
	 * 
	 * @param Address
	 *            the participation to delete
	 * @return true on success, false on fail
	 */
	public static boolean deleteByID(long participationID) {
		if (participationID < 0)
			return false;
		if (existsByID(participationID) == false)
			return false;
		try {
			Connection c = Database.getConnection();
			if (c == null) {
				Database.openDatabase();
				c = Database.getConnection();
			}

			// TODO the "ON DELETE CASCADE" doens not work in the database
			// create a prepared statement and a statement
			PreparedStatement preparedDelete = null;
			String deleteStatement = "DELETE FROM participation WHERE participationID = ?;";
			// don't autocommit the statement
			c.setAutoCommit(false);
			// set the prepeared statement
			preparedDelete = c.prepareStatement(deleteStatement);
			// set the parameters of the prepared statement
			preparedDelete.setLong(1, participationID);
			// execute the statement
			preparedDelete.executeUpdate();

			c.commit();
			// Database.closeDatabase();
			return true;

		} catch (SQLException e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(1);
		}
		return false;
	}

	/**
	 * This method will delete the participation row from the tabel
	 * 
	 * 
	 * @param Address
	 *            the participation to delete
	 * @return true on success, false on fail
	 */
	public static boolean deleteByObject(Participation a) {
		if (a == null)
			return false;
		if (existsByID(a.getParticipationID()) == false)	
			return false;
		try {
			Connection c = Database.getConnection();
			if (c == null) {
				Database.openDatabase();
				c = Database.getConnection();
			}

			c.setAutoCommit(false);
			Statement s = null;

			// TODO the "ON DELETE CASCADE" doens not work in the database
			s = c.createStatement();
			String sql = "DELETE FROM participation WHERE participationID = " + a.getParticipationID() + ";";
			s.executeUpdate(sql);

			s.close();
			c.commit();
			// Database.closeDatabase();
			return true;
		} catch (SQLException e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(1);
		}
		return false;
	}

	/**
	 * check if an participation with a certain ID exists in the database
	 * 
	 * @param long
	 *            the id of the participation to look for
	 * @return false if the participation does not exists, true if the
	 *         participation exists
	 * @TODO refactor this, not actually pulling the whole participation, just
	 *       returning the ID oof the participation and compare to be sure it's
	 *       the same.
	 */
	public static boolean existsByID(long participationID) {
		if (participationID < 0)
			return false;

		try {
			Connection c = Database.getConnection();
			if (c == null) {
				Database.openDatabase();
				c = Database.getConnection();
			}

			// create a prepared statement and a statement
			PreparedStatement preparedPull = null;
			String pullStatement = "SELECT participationID FROM participation WHERE participationID = ?;";

			// don't autocommit the statement
			c.setAutoCommit(false);
			// set the prepeared statement
			preparedPull = c.prepareStatement(pullStatement);
			// set the parameters of the prepared statement
			preparedPull.setLong(1, participationID);

			// execute the query, this returns a resultset
			ResultSet rs = preparedPull.executeQuery();

			long pulledID = -1;
			if (rs.next()) {
				pulledID = rs.getLong(1);
			}
			// close statement and connection
			preparedPull.close();
			// Database.closeDatabase();

			if (pulledID != participationID)
				return false;
			return true;

		} catch (SQLException e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(1);
		}

		return false;
	}

	/**
	 * This method retrieves the count of rows in this table
	 * 
	 * @return an int, the number or rows in this table
	 */
	public static int countOf() {
		try {
			Connection c = Database.getConnection();
			if (c == null) {
				Database.openDatabase();
				c = Database.getConnection();
			}

			int count = 0;
			Statement s = null;

			s = c.createStatement();

			ResultSet rs = s.executeQuery("SELECT count(*) FROM participation;");

			if (rs.next()) {
				count = rs.getInt(1);
			}

			// close the connection
			s.close();
			// Database.closeDatabase();

			return count;
		} catch (SQLException e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(1);
		}

		return 0;
	}

}