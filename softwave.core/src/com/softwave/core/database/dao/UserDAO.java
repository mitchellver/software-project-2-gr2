package com.softwave.core.database.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;

import com.softwave.core.container.design.DesignConfig;
import com.softwave.core.container.user.Address;
import com.softwave.core.container.user.Mail;
import com.softwave.core.container.user.Number;
import com.softwave.core.container.user.account.AnonUser;
import com.softwave.core.container.user.account.Client;
import com.softwave.core.container.user.account.User;
import com.softwave.core.container.user.account.UserInfo;
import com.softwave.core.database.Database;
import com.softwave.core.security.HashString;
import com.softwave.core.util.Date;

/**
 * The UserDAO class is used to access the individual tables in the database,
 * every method in this class is static will do a specific thing: adding an item
 * to the User table, removing, updating and reading data from the database.
 *
 * @author eddi
 */
public class UserDAO {

	public static boolean pushOrUpdate(User user) {
		if (user == null)
			return false;
		// check if the userID is not number 0, does not exist allready
		if (user.getUserID() == -1 || existsByID(user.getUserID()) == false)
			return push(user); // create a new user
		// update the user
		return update(user);
	}

	/**
	 * This method will add an user to the user table.
	 *
	 * @param addres
	 *            the user to be stored
	 * @return the newly created ID, or -1 on fail
	 */
	public static boolean push(User user) {
		if (user == null)
			return false;
		// check if the user has a valid ID
		if (user.getUserID() != -1)
			return false;
		// check if the user does not exist allready
		if (existsByID(user.getUserID()) == true)
			return false;

		// TODO first push the linked objects
		// we need the id's, we receive them after the push is done
		// designConfig
		// address
		// permissions (an Object with a map of bools and permissions?
		if (user instanceof Client && ((Client) user).getDesignConfig() != null)
			DesignConfigDAO.push(((Client) user).getDesignConfig());
		if (user.getUserInfo() != null && user.getUserInfo().getAddress() != null)
			AddressDAO.push(user.getUserInfo().getAddress());

		try {
			Connection c = Database.getConnection();
			if (c == null) {
				Database.openDatabase();
				c = Database.getConnection();
			}

			// do not autocommit
			c.setAutoCommit(false);

			// create a prepared statement
			PreparedStatement preparedPush = null;

			// this creates a prepared statement
			preparedPush = createPreparedStatement(true, user, preparedPush, c);

			// execute the query, this returns a resultset
			int rowsUpdated = preparedPush.executeUpdate();

			// update the User ID
			try (ResultSet rs = preparedPush.getGeneratedKeys()) {
				if (rs.next()) {
					// update the iD and timestamp
					user.setUserID(rs.getLong(1));
				} else {
					throw new SQLException("Creating user failed, no ID obtained.");
				}
			} finally {
				preparedPush.close();
				c.commit();
				// Database.closeDatabase();
				// if the push did not work and more or less then 1 row changed
				if (rowsUpdated != 1) {
					return false;
				}
			}

		} catch (SQLException e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(1);
		}
		// update the TimeStamp
		// this must happen AFTER the tryCatch of the main connection, or the
		// connection will be erroneously closed
		user.setCreateTime(new Date(getTimestampByUserID(user.getUserID())));
		return true;
	}

	/**
	 * this is a method to update an existing row in the database. It checks if
	 * the row exists, and then updates it
	 * 
	 * @param a
	 *            the user to update, should have a valid id
	 * @return true on success false on fail
	 * @TODO ADD PREPARED STATEMENT !
	 */
	public static boolean update(User user) {
		if (user == null)
			return false;
		// check if the row exists in the database
		if (existsByID(user.getUserID()) == false)
			return false;

		// TODO first push the linked objects
		// permissions (an Object with a map of bools and permissions?
		if (user instanceof Client && ((Client) user).getDesignConfig() != null)
			DesignConfigDAO.push(((Client) user).getDesignConfig());
		if (user.getUserInfo() != null && user.getUserInfo().getAddress() != null)
			AddressDAO.push(user.getUserInfo().getAddress());

		try {
			Connection c = Database.getConnection();
			if (c == null) {
				Database.openDatabase();
				c = Database.getConnection();
			}

			// do not autocommit
			c.setAutoCommit(false);

			// create a prepared statement and a statement
			PreparedStatement preparedUpdate = null;

			preparedUpdate = createPreparedStatement(false, user, preparedUpdate, c);

			// execute the query, this returns a resultset
			int rowsUpdated = preparedUpdate.executeUpdate();

			preparedUpdate.close();
			c.commit();
			// Database.closeDatabase();

			// check if the row was updated
			if (rowsUpdated == 1)
				return true;
			return false;

		} catch (SQLException e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(1);
		}
		return false;
	}

	/**
	 * This method will look up every user in the database that has the same
	 * city as the given one. It will then return an Arraylist of those useres
	 * 
	 * @param String
	 *            this is the city you want to look for
	 * @return null if no user was found, Address if the user was found an
	 *         pulled.
	 */
	public static User pullByID(long userID) {
		if (userID < 0)
			return null;
		if (existsByID(userID) == false)
			return null;

		User user = null;

		try {
			Connection c = Database.getConnection();
			if (c == null) {
				Database.openDatabase();
				c = Database.getConnection();
			}

			// create a prepared statement and a statement
			PreparedStatement preparedPull = null;
			String pullStatement = "SELECT * FROM user WHERE userID = ?;";

			// don't autocommit the statement
			c.setAutoCommit(false);
			// set the prepeared statement
			preparedPull = c.prepareStatement(pullStatement);
			// set the parameters of the prepared statement
			preparedPull.setLong(1, userID);

			// execute the query, this returns a resultset
			ResultSet rs = preparedPull.executeQuery();

			// only get the first user out, normally there is only one user.
			if (rs.next()) {
				// this will return a user with Addres UserInfo and designConfig
				// all set
				user = createUser(rs);
			}

			// close statement and connection
			preparedPull.close();
			// Database.closeDatabase();

		} catch (SQLException e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(1);
		}
		if (user instanceof Client)
			((Client) user).setDesignConfig(DesignConfigDAO.pullByID(getDesignConfigIDByUserID(user.getUserID())));
		if (user.getUserInfo() != null)
			user.getUserInfo().setAddress(AddressDAO.pullByID(getAddressIDByUserID(user.getUserID())));
		return user;
	}

	/**
	 * This method will look up the user in the database with matching username
	 * .
	 * 
	 * @param String
	 *            this is the username you want to look for
	 * @return the user with this username
	 */
	public static User pullByUsername(String username) {
		User user = null;

		try {
			Connection c = Database.getConnection();
			if (c == null) {
				Database.openDatabase();
				c = Database.getConnection();
			}

			// create a prepared statement and a statement
			PreparedStatement preparedPull = null;
			String pullStatement = "SELECT * FROM user WHERE username = ?;";

			// don't autocommit the statement
			c.setAutoCommit(false);
			// set the prepeared statement
			preparedPull = c.prepareStatement(pullStatement);
			// set the parameters of the prepared statement
			preparedPull.setString(1, username);

			// execute the query, this returns a resultset
			ResultSet rs = preparedPull.executeQuery();

			if (rs.next()) {
				// this will return a user with Addres UserInfo and designConfig
				// all set
				user = createUser(rs);
			}

			// close the statement and connection
			preparedPull.close();
			// Database.closeDatabase();

		} catch (SQLException e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(1);
		}

		if (user instanceof Client)
			((Client) user).setDesignConfig(DesignConfigDAO.pullByID(getDesignConfigIDByUserID(user.getUserID())));
		if (user.getUserInfo() != null)
			user.getUserInfo().setAddress(AddressDAO.pullByID(getAddressIDByUserID(user.getUserID())));
		return user;
	}

	/**
	 * This method will look up the user in the database with matching
	 * firstname.
	 * 
	 * @param String
	 *            this is the firstname you want to look for
	 * @return an ArrayList of all users with given firstname
	 */
	public static ArrayList<User> pullByFirstname(String firstname) {
		ArrayList<User> users = new ArrayList<>();

		try {
			Connection c = Database.getConnection();
			if (c == null) {
				Database.openDatabase();
				c = Database.getConnection();
			}

			// create a prepared statement and a statement
			PreparedStatement preparedPull = null;
			String pullStatement = "SELECT * FROM user WHERE lower(firstname) LIKE lower('%?%');";

			// don't autocommit the statement
			c.setAutoCommit(false);
			// set the prepeared statement
			preparedPull = c.prepareStatement(pullStatement);
			// set the parameters of the prepared statement
			preparedPull.setString(1, firstname);

			// execute the query, this returns a resultset
			ResultSet rs = preparedPull.executeQuery();

			while (rs.next()) {
				// this will return a user with Addres UserInfo and designConfig
				// all set
				users.add(createUser(rs));
			}

			// close the statement and connection
			preparedPull.close();
			// Database.closeDatabase();

		} catch (SQLException e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(1);
		}
		users.stream().forEach(e -> {
			if (e instanceof Client)
				((Client) e).setDesignConfig(DesignConfigDAO.pullByID(getDesignConfigIDByUserID(e.getUserID())));
			if (e.getUserInfo() != null)
				e.getUserInfo().setAddress(AddressDAO.pullByID(getAddressIDByUserID(e.getUserID())));
		});
		return users;
	}

	/**
	 * This method will look up the user in the database with matching lastname.
	 * 
	 * @param String
	 *            this is the lastname you want to look for
	 * @return an ArrayList of all users with given lastname
	 */
	public static ArrayList<User> pullByLasttname(String lastname) {
		// we do not check on an empty string, since we might want to pull all
		// users without a lastname

		ArrayList<User> users = new ArrayList<>();

		try {
			Connection c = Database.getConnection();
			if (c == null) {
				Database.openDatabase();
				c = Database.getConnection();
			}

			// create a prepared statement and a statement
			PreparedStatement preparedPull = null;
			String pullStatement = "SELECT * FROM user WHERE lower(lastname) LIKE lower('%?%');";

			// don't autocommit the statement
			c.setAutoCommit(false);
			// set the prepeared statement
			preparedPull = c.prepareStatement(pullStatement);
			// set the parameters of the prepared statement
			preparedPull.setString(1, lastname);

			// execute the query, this returns a resultset
			ResultSet rs = preparedPull.executeQuery();

			while (rs.next()) {
				// this will return a user with Addres UserInfo and designConfig
				// all set
				users.add(createUser(rs));
			}

			// close the statement and connection
			preparedPull.close();
			// Database.closeDatabase();

		} catch (SQLException e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(1);
		}
		users.stream().forEach(e -> {
			if (e instanceof Client)
				((Client) e).setDesignConfig(DesignConfigDAO.pullByID(getDesignConfigIDByUserID(e.getUserID())));
			if (e.getUserInfo() != null)
				e.getUserInfo().setAddress(AddressDAO.pullByID(getAddressIDByUserID(e.getUserID())));
		});
		return users;
	}

	/**
	 * This method will read all the users in the user table and return every
	 * single record back as an item object array.
	 *
	 * @return an ArrayList constructed from all the users in the database.
	 */
	public static ArrayList<User> pullAll() {
		ArrayList<User> users = new ArrayList<>();

		try {
			Connection c = Database.getConnection();
			if (c == null) {
				Database.openDatabase();
				c = Database.getConnection();
			}

			Statement s = null;

			s = c.createStatement();

			ResultSet rs = s.executeQuery("SELECT * FROM user;");

			while (rs.next()) {
				users.add(createUser(rs));
			}

			// close the connection
			s.close();
			// Database.closeDatabase();
		} catch (SQLException e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(1);
		}

		users.stream().forEach(e -> {
			if (e instanceof Client)
				((Client) e).setDesignConfig(DesignConfigDAO.pullByID(getDesignConfigIDByUserID(e.getUserID())));
			if (e.getUserInfo() != null)
				e.getUserInfo().setAddress(AddressDAO.pullByID(getAddressIDByUserID(e.getUserID())));
		});
		return users;
	}

	/**
	 * This method will delete the user row from the tabel
	 * 
	 * @param Address
	 *            the user to delete
	 * @return true on success, false on fail
	 */
	public static boolean deleteByID(long userID) {
		if (userID < 0)
			return false;
		if (existsByID(userID) == false)
			return false;
		try {
			Connection c = Database.getConnection();
			if (c == null) {
				Database.openDatabase();
				c = Database.getConnection();
			}

			// get the designconfigID and addressID to delete them after we
			// delete the user
			long adID = getAddressIDByUserID(userID);
			long dcID = getDesignConfigIDByUserID(userID);

			// TODO the "ON DELETE CASCADE" doens not work in the database
			// create a prepared statement and a statement
			PreparedStatement preparedDelete = null;
			String deleteStatement = "DELETE FROM user WHERE userID = ?" + ";";
			// don't autocommit the statement
			c.setAutoCommit(false);
			// set the prepeared statement
			preparedDelete = c.prepareStatement(deleteStatement);
			// set the parameters of the prepared statement
			preparedDelete.setLong(1, userID);
			// execute the statement
			preparedDelete.executeUpdate();

			c.commit();
			// Database.closeDatabase();

			// delete the address and designconfig
			AddressDAO.deleteByID(adID);
			DesignConfigDAO.deleteByID(dcID);

			return true;

		} catch (SQLException e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(1);
		}
		return false;
	}

	/**
	 * This method will delete the user row from the tabel
	 * 
	 * 
	 * @param Address
	 *            the user to delete
	 * @return true on success, false on fail
	 */
	public static boolean deleteByObject(User a) {
		if (a == null)
			return false;
		if (existsByID(a.getUserID()) == false)
			return false;
		try {
			Connection c = Database.getConnection();
			if (c == null) {
				Database.openDatabase();
				c = Database.getConnection();
			}

			// get the ID's of address and designconfig if they exist, so we can
			// delete them later
			long adID = -1;
			long dcID = -1;
			if (a.getUserInfo() != null && a.getUserInfo().getAddress() != null)
				adID = a.getUserInfo().getAddress().getAddressID();
			if (a instanceof Client && ((Client) a).getDesignConfig() != null)
				dcID = ((Client) a).getDesignConfig().getDesignConfigID();

			c.setAutoCommit(false);
			Statement s = null;

			// TODO the "ON DELETE CASCADE" doens not work in the database
			s = c.createStatement();
			String sql = "DELETE FROM user WHERE userID = " + a.getUserID() + ";";
			s.executeUpdate(sql);

			s.close();
			c.commit();
			// Database.closeDatabase();

			// delete the designconfig an address
			AddressDAO.deleteByID(adID);
			DesignConfigDAO.deleteByID(dcID);

			return true;
		} catch (SQLException e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(1);
		}
		return false;
	}

	public static boolean canLogin(String username, String password) {
		if (username == "" || password == "")
			return false;

		try {
			Connection c = Database.getConnection();
			if (c == null) {
				Database.openDatabase();
				c = Database.getConnection();
			}

			// create a prepared statement and a statement
			PreparedStatement preparedPull = null;
			String pullStatement = "SELECT userID FROM user WHERE username = ? AND password = ?;";

			// don't autocommit the statement
			c.setAutoCommit(false);
			// set the prepeared statement
			preparedPull = c.prepareStatement(pullStatement);
			// set the parameters of the prepared statement
			preparedPull.setString(1, username);
			preparedPull.setString(2, password);

			// execute the query, this returns a resultset
			ResultSet rs = preparedPull.executeQuery();

			long pulledID = -1;
			if (rs.next()) {
				pulledID = rs.getLong(1);
			}
			// close statement and connection
			preparedPull.close();
			// Database.closeDatabase();

			if (pulledID == -1)
				return false;
			return true;

		} catch (SQLException e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(1);
		}

		return false;
	}

	/**
	 * this method retieves the designconfigID for a give userID, it is used by
	 * the delete methods.
	 * 
	 * @param userID
	 *            the userID for which we want to retrieve the designconfigID
	 * @return the long designconfigIDID or -1 on not found
	 */
	public static long getDesignConfigIDByUserID(long userID) {
		if (userID < 0)
			return -1;

		try {
			Connection c = Database.getConnection();
			if (c == null) {
				Database.openDatabase();
				c = Database.getConnection();
			}

			// create a prepared statement and a statement
			PreparedStatement preparedPull = null;
			String pullStatement = "SELECT designconfigID FROM user WHERE userID = ?;";

			// don't autocommit the statement
			c.setAutoCommit(false);
			// set the prepeared statement
			preparedPull = c.prepareStatement(pullStatement);
			// set the parameters of the prepared statement
			preparedPull.setLong(1, userID);

			// execute the query, this returns a resultset
			ResultSet rs = preparedPull.executeQuery();

			long pulledID = -1;
			if (rs.next()) {
				pulledID = rs.getLong(1);
			}
			// close statement and connection
			preparedPull.close();
			// Database.closeDatabase();

			return pulledID;

		} catch (SQLException e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(1);
		}

		return -1;
	}

	/**
	 * this method retieves the designconfigID for a give userID, it is used by
	 * the delete methods.
	 * 
	 * @param userID
	 *            the userID for which we want to retrieve the designconfigID
	 * @return the long addressID or -1 on not found
	 */
	public static long getAddressIDByUserID(long userID) {
		if (userID < 0)
			return -1;

		try {
			Connection c = Database.getConnection();
			if (c == null) {
				Database.openDatabase();
				c = Database.getConnection();
			}

			// create a prepared statement and a statement
			PreparedStatement preparedPull = null;
			String pullStatement = "SELECT addressID FROM user WHERE userID = ?;";

			// don't autocommit the statement
			c.setAutoCommit(false);
			// set the prepeared statement
			preparedPull = c.prepareStatement(pullStatement);
			// set the parameters of the prepared statement
			preparedPull.setLong(1, userID);

			// execute the query, this returns a resultset
			ResultSet rs = preparedPull.executeQuery();

			long pulledID = -1;
			if (rs.next()) {
				pulledID = rs.getLong(1);
			}
			// close statement and connection
			preparedPull.close();
			// Database.closeDatabase();

			return pulledID;

		} catch (SQLException e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(1);
		}

		return -1;
	}

	/**
	 * this method retieves the create_time for a give userID, it is used by the
	 * push and update method
	 * 
	 * @param userID
	 *            the userID for which we want to retrieve the designconfigID
	 * @return timestamp from the database or null on fail
	 */
	public static Timestamp getTimestampByUserID(long userID) {
		if (userID < 0)
			return null;

		try {
			Connection c = Database.getConnection();
			if (c == null) {
				Database.openDatabase();
				c = Database.getConnection();
			}

			// create a prepared statement and a statement
			PreparedStatement preparedPull = null;
			String pullStatement = "SELECT create_time FROM user WHERE userID = ?;";

			// don't autocommit the statement
			c.setAutoCommit(false);
			// set the prepeared statement
			preparedPull = c.prepareStatement(pullStatement);
			// set the parameters of the prepared statement
			preparedPull.setLong(1, userID);

			// execute the query, this returns a resultset
			ResultSet rs = preparedPull.executeQuery();

			Timestamp t = null;
			if (rs.next()) {
				t = rs.getTimestamp(1);
			}
			// close statement and connection
			preparedPull.close();
			// Database.closeDatabase();

			return t;

		} catch (SQLException e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(1);
		}

		return null;
	}

	/**
	 * a method to initialize a preparedStatement for the push as well as the
	 * update function.
	 * 
	 * @param isPush
	 *            this is a boolean which tells us we need to setup a statement
	 *            for push or update method
	 * @param user
	 *            this is the user object from which to collect the data
	 * @param ps
	 *            this is the empty prepared statement
	 * @param c
	 *            the connection to set the prepared statement
	 * @throws SQLException
	 */
	private static PreparedStatement createPreparedStatement(boolean isPush, User user, PreparedStatement ps,
			Connection c) throws SQLException {
		if (isPush) {
			String pushStatement = "INSERT INTO user (firstname, lastname, email, " + "username, password, "
					+ "phonenumber, addressID, designconfigID, createdbyID) VALUES (?,?,?,?,?,?,?,?,?);";

			// set the prepeared statement
			ps = c.prepareStatement(pushStatement, PreparedStatement.RETURN_GENERATED_KEYS);
		} else {
			String updateStatement = "UPDATE user SET firstname=?, lastname=?, email=?, " + "username=?, password=?, "
					+ "phonenumber=?, addressID=?, designconfigID=? ,createdbyID =? " + "WHERE userID=?;";

			// set the prepeared statement
			ps = c.prepareStatement(updateStatement, PreparedStatement.RETURN_GENERATED_KEYS);
			
			// set the userID
			ps.setLong(10, user.getUserID());
		}
		// set the User-members
		if (user.getUserInfo() != null) {
			ps.setString(1, user.getUserInfo().getFirstName());
			ps.setString(2, user.getUserInfo().getLastName());

			if (user.getUserInfo().getEmail() != null)
				ps.setString(3, user.getUserInfo().getEmail().getEmail());
		} else {
			// fill in empty strings
			ps.setString(1, "");
			ps.setString(2, "");
			ps.setString(3, "");

		}
		// TODO these next two fields are not implemented yet in the java
		// structure.
		// role
		// permissionsID

		// if you push a null value to the createtime, mysql will timestamp
		// again. We don't want this aftes a user had it's creationtime set
		// TODO maybe it's best NEVER to overwrite the timestamp
		// if (user.getCreateTime() != null) {
		// // convert our Date object to a timestamp
		// ps.setTimestamp(7, user.getCreateTime().convertLocalToTimestamp());
		// }

		// check if objects are not null, or we get nullpointerexceptions
		if (user.getUserInfo() != null) {
			if (user.getUserInfo().getPhonenumber() != null)
				ps.setString(6, user.getUserInfo().getPhonenumber().getNumber());
			if (user.getUserInfo().getAddress() != null)
				ps.setLong(7, user.getUserInfo().getAddress().getAddressID());
		} else {
			ps.setString(6, "");
			ps.setNull(7, java.sql.Types.INTEGER);
		}
		// set the parameters unique to a Client/KnownUser
		if (user instanceof Client) {
			Client client = (Client) user;
			ps.setString(4, client.getUsername());
			ps.setString(5, client.getPassword().getData());
			if (client.getDesignConfig() != null)
				ps.setLong(8, client.getDesignConfig().getDesignConfigID());
		} else {
			ps.setString(4, "");
			ps.setString(5, "");
			ps.setNull(8, java.sql.Types.INTEGER);
		}

		// set the id of the user that createdthis user
		ps.setLong(9, user.getcreateID());
		return ps;
	}

	/**
	 * This function creates a user, it returns a Client or an AnonUser
	 * according to the username and password.
	 * 
	 * @param rs
	 *            the resultset that was pulled from the db
	 * @return a User, which is either a Client or an AnonUser
	 * @throws SQLException
	 */
	private static User createUser(ResultSet rs) throws SQLException {
		User user = null;
		Mail email = null;
		Number phone = null;
		DesignConfig dc = null;
		Address a = null;

		boolean isClient = true;
		// TODO there is a bug in this
		// check to see if it is a Client
		String username = rs.getString("username");
		String password = rs.getString("password");
		if (username.trim().isEmpty() || password.trim().isEmpty())
			isClient = false;

		if (rs.getString("email") != null || rs.getString("email") != "")
			email = new Mail(rs.getString("email"));
		if (rs.getString("phonenumber") != null || rs.getString("phonenumber") != "")
			phone = new Number(rs.getString("phonenumber"));

		UserInfo uInfo = null;
		// TODO refactor this in a function, or another way of doing this
		if (rs.getString("email").trim().isEmpty() && rs.getString("phonenumber").trim().isEmpty()
				&& rs.getString("firstname").trim().isEmpty() && rs.getString("lastname").trim().isEmpty()
				&& rs.getLong("addressID") == 0) {
			uInfo = null;
		} else {
			// set the UserInfo
			uInfo = new UserInfo(email, phone, a, rs.getString("firstname"), rs.getString("lastname"));
		}
		// TODO Role is not implemented in java structure, delete it from
		// the database?
		// user.setRole();
		// TODO also the permissions

		if (isClient) {
			// create the User, which is of type Client
			Client client = new Client(rs.getString("username"), new HashString(rs.getString("password")));
			client.setDesignConfig(dc);
			// put this client inside the user to return it
			user = client;
		} else {
			// create an anonymous user
			AnonUser anon = new AnonUser();
			user = anon;
		}

		// set the members for the User
		user.setUserID(rs.getLong("userID"));
		if (uInfo != null) {
			user.setUserInfo(uInfo);
			user.getUserInfo().setAddress(a);
		}
		user.setCreateTime(new Date(rs.getTimestamp("create_time")));

		return user;
	}

	/**
	 * check if an user with a certain ID exists in the database
	 * 
	 * @param long
	 *            the id of the user to look for
	 * @return false if the user does not exists, true if the user exists
	 * @TODO refactor this, not actually pulling the whole user, just returning
	 *       the ID oof the user and compare to be sure it's the same.
	 */
	public static boolean existsByID(long userID) {
		if (userID < 0)
			return false;

		try {
			Connection c = Database.getConnection();
			if (c == null) {
				Database.openDatabase();
				c = Database.getConnection();
			}

			// create a prepared statement and a statement
			PreparedStatement preparedPull = null;
			String pullStatement = "SELECT userID FROM user WHERE userID = ?;";

			// don't autocommit the statement
			c.setAutoCommit(false);
			// set the prepeared statement
			preparedPull = c.prepareStatement(pullStatement);
			// set the parameters of the prepared statement
			preparedPull.setLong(1, userID);

			// execute the query, this returns a resultset
			ResultSet rs = preparedPull.executeQuery();

			long pulledID = -1;
			if (rs.next()) {
				pulledID = rs.getLong(1);
			}
			// close statement and connection
			preparedPull.close();
			// Database.closeDatabase();

			if (pulledID != userID)
				return false;
			return true;

		} catch (SQLException e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(1);
		}

		return false;
	}

	/**
	 * This method retrieves the count of rows in this table
	 * 
	 * @return an int, the number or rows in this table
	 */
	public static int countOf() {
		try {
			Connection c = Database.getConnection();
			if (c == null) {
				Database.openDatabase();
				c = Database.getConnection();
			}

			int count = 0;
			Statement s = null;

			s = c.createStatement();

			ResultSet rs = s.executeQuery("SELECT count(*) FROM user;");

			if (rs.next()) {
				count = rs.getInt(1);
			}

			// close the connection
			s.close();
			// Database.closeDatabase();

			return count;
		} catch (SQLException e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(1);
		}

		return 0;
	}
	/**
	 * This method will update the active row from the  user
	 * 
	 * @param userID
	 *            the ID to update the  user
	 * @return true on success, false on fail
	 */
	public boolean setinactive(long userID) {
		if (userID < 0)
			return false;
		if (existsByID(userID) == false)
			return false;
		try {
			Connection c = Database.getConnection();
			if (c == null) {
				Database.openDatabase();
				c = Database.getConnection();
			}

			// create a prepared statement and a statement
			PreparedStatement setinactiveuser = null;
			String updateStatement = "UPDATE user set active=1 WHERE userID =?;";
			// don't autocommit the statement
			c.setAutoCommit(false);
			// set the prepeared statement
			setinactiveuser = c.prepareStatement(updateStatement);
			// set the parameters of the prepared statement
			setinactiveuser.setLong(1, userID);
			// execute the statement
			setinactiveuser.executeUpdate();

			c.commit();
			// Database.closeDatabase();
			return true;

		} catch (SQLException e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(1);
		}
		return false;
	}

	/**
	 * This method will update the  user row from the tabel
	 * 
	 * @param a
	 *            the  user to update
	 * @return true on success, false on fail
	 */
	public boolean setinactive(User a) {
		if (a == null)
			return false;
		if (existsByID(a.getUserID()) == false)
			return false;
		try {
			Connection c = Database.getConnection();
			if (c == null) {
				Database.openDatabase();
				c = Database.getConnection();
			}

			c.setAutoCommit(false);
			Statement s = null;

			s = c.createStatement();
			String sql = "UPDATE user set active= 1 WHERE userID = " + a.getUserID() + ";";
			s.executeUpdate(sql);

			s.close();
			c.commit();
			// Database.closeDatabase();
			return true;
		} catch (SQLException e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(1);
		}
		return false;
	}

	/**
	 * this is a method to update an existing row of active in the database. It
	 * checks if the row exists, and then updates it
	 * 
	 * @param userID
	 *            the  user to update, should have a valid id
	 * @return true on success false on fail
	 * @TODO ADD PREPARED STATEMENT !
	 */
	public boolean setactive(long userID) {
		if (userID < 0)
			return false;
		if (existsByID(userID) == false)
			return false;
		try {
			Connection c = Database.getConnection();
			if (c == null) {
				Database.openDatabase();
				c = Database.getConnection();
			}

			// create a prepared statement and a statement
			PreparedStatement setinactiveuser = null;
			String updateStatement = "UPDATE user set active=0 WHERE userID =?;";
			// don't autocommit the statement
			c.setAutoCommit(false);
			// set the prepeared statement
			setinactiveuser = c.prepareStatement(updateStatement);
			// set the parameters of the prepared statement
			setinactiveuser.setLong(1, userID);
			// execute the statement
			setinactiveuser.executeUpdate();

			c.commit();
			// Database.closeDatabase();
			return true;

		} catch (SQLException e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(1);
		}
		return false;
	}

	/**
	 * this is a method to update an existing row active in the database. It
	 * checks if the row exists, and then updates it
	 * 
	 * @param a
	 *            the user to update, should have a valid id
	 * @return true on success false on fail
	 * @TODO ADD PREPARED STATEMENT !
	 */
	public boolean setactive(User a) {
		if (a == null)
			return false;
		if (existsByID(a.getUserID()) == false)
			return false;
		try {
			Connection c = Database.getConnection();
			if (c == null) {
				Database.openDatabase();
				c = Database.getConnection();
			}

			c.setAutoCommit(false);
			Statement s = null;

			s = c.createStatement();
			String sql = "UPDATE address set active= 0 WHERE addressID = " + a.getUserID() + ";";
			s.executeUpdate(sql);

			s.close();
			c.commit();
			// Database.closeDatabase();
			return true;
		} catch (SQLException e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(1);
		}
		return false;
	}
}