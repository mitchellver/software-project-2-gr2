package com.softwave.core.database;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.UnknownHostException;

import com.softwave.core.container.quiz.QType;
import com.softwave.core.container.quiz.Question;
import com.softwave.core.container.quiz.Quiz;
import com.softwave.core.logic.QuizManagementHandler;

import jcifs.UniAddress;
import jcifs.smb.NtlmPasswordAuthentication;
import jcifs.smb.SmbException;
import jcifs.smb.SmbFile;
import jcifs.smb.SmbFileInputStream;
import jcifs.smb.SmbFileOutputStream;
import jcifs.smb.SmbSession;

/**
 * This is a class that handles saving and downloading of files to an SMB
 * server. It is used to handle all file down-and uploads like logo's, videos,
 * pictures ...
 * 
 * Here are some
 * 
 *
 * @author eddi
 *
 */
public class SmbHandler {
	/** the IP address of the smb server */
	private static final String SMB_ADDRESS = "10.3.50.31";
	private static final String SMB_USER_NAME = "softwave";
	private static final String SMB_PASSWORD = "sjUqY83ehVwG3Eqt";
	/** The path to the shared folder, to append a file or foldername to */
	private static final String SMB_PATH = "smb://" + SMB_ADDRESS + "/share/";
	/**
	 * This is a path that needs to be put in front of the oath we get from the
	 * SMB server, so we can save it offline in the right place
	 */
	private static final String LOCAL_PATH = ".." + File.separator;
	/**
	 * the domain, it holds the address in an understandable way for the JCifs
	 * Library
	 */
	private static UniAddress domain;
	/**
	 * the authentication object, it contains the address, username and password
	 */
	private static NtlmPasswordAuthentication authentication;

	/**
	 * The constructor that sets the authentication. It uses the addres username
	 * and password
	 */
	private SmbHandler() {
		authentication = new NtlmPasswordAuthentication(SMB_ADDRESS, SMB_USER_NAME, SMB_PASSWORD);

	}

	/**
	 * An inner class that creates a unique instance of the SMBHandler class,
	 * this gives us a singleton of the SmbHandler.
	 */
	private static class SmbHandlerHolder {
		private static final SmbHandler instance = new SmbHandler();
	}

	/**
	 * The methtod that returns a unique instance of the SMBHandler class. that
	 * is created in the SmbHandlerHolder
	 * 
	 * @return
	 */
	public static SmbHandler getInstancte() {
		return SmbHandlerHolder.instance;
	}

	/**
	 * This method logs on to the smbServer, after this method files can be
	 * down- and uploaded.
	 * 
	 * @throws SmbException
	 * @throws UnknownHostException
	 */
	public void getConnection() throws SmbException, UnknownHostException {
		domain = UniAddress.getByName(SMB_ADDRESS);
		SmbSession.logon(domain, authentication);
	}

	/**
	 * This method gets a file from the smb server;
	 * 
	 * @param path
	 */
	public File pullFile(String path) {
		File file = null;

		try {
			SmbFile smbFile = new SmbFile(SMB_PATH + path, authentication);
			SmbFileInputStream smbfis = new SmbFileInputStream(smbFile);
			FileOutputStream fos = new FileOutputStream(new File("../" + path));

			byte[] b = new byte[8192];
			int n;
			while ((n = smbfis.read(b)) > 0) {
				fos.write(b, 0, n);
			}
			smbfis.close();
			fos.close();

		} catch (IOException e) {
			e.printStackTrace();
		}

		return file;
	}

	/**
	 * This method pushes a new file to the smb server, it receives a relative
	 * path. The method trims the relative part of the path, checks if the file
	 * exists, deletes the exiting file, creates new directories if nescesarry
	 * and then finally pushes the file.
	 * 
	 * @param path
	 *            the relative path of the file you want to push e.g.
	 *            "../data/myPic.jpg"
	 */
	private void pushFile(String path) {
		String localPath = path;

		// first trim the relative .. from the path
		path = trimRelativePath(path);

		String savePath = SMB_PATH + path;
		SmbFile smbFile;
		try {
			// first check if the file exists and delete it.
			delete(path);
			// create the needed directories
			createNewDirectories(path);
			smbFile = new SmbFile(SMB_PATH + path, authentication);
			// create a new file, so that we can stream the data into it
			smbFile.createNewFile();

			// create the needed streams
			// input stream reading the file
			FileInputStream fis = new FileInputStream(localPath);
			// outputsream streaming to sbmServer
			SmbFileOutputStream smbFos = (SmbFileOutputStream) smbFile.getOutputStream();

			byte[] b = new byte[8192];
			int n;
			while ((n = fis.read(b)) > 0) {
				smbFos.write(b, 0, n);
			}

			fis.close();
			smbFos.close();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
	}

	/**
	 * This method deletes a file on the smb server if that file exists.
	 * 
	 * @param path
	 *            thepath to the file to delete
	 * @return true if deleted, false on not deleted
	 */
	private boolean delete(String path) {
		path = trimRelativePath(path);
		String smbPath = SMB_PATH + path;
		try {
			SmbFile smbFile = new SmbFile(smbPath, authentication);
			if (smbFile.exists()) {
				smbFile.delete();
				return true;
			}
		} catch (SmbException | MalformedURLException e) {
			e.printStackTrace();
		}
		return false;
	}

	/**
	 * a method that creates all folders that do not exist in a path
	 * 
	 * @param path
	 *            the path for which to create the subdirectories
	 */
	public void createNewDirectories(String path) {
		int lastSep = path.lastIndexOf(File.separator);
		String directories = path.substring(0, lastSep);
		try {
			if (exists(directories) == false)
				new SmbFile(SMB_PATH + directories, authentication).mkdir();
		} catch (SmbException e) {
			e.printStackTrace();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
	}

	/**
	 * this method trims the relative part of the path e.g. "./pic.png" or
	 * "./../../pic.png" becomes "pic.png"
	 * 
	 * @param relPath
	 *            the path for which the method will trim
	 * @return
	 */
	public String trimRelativePath(String relPath) {
		String trimPath = relPath;
		while (!trimPath.isEmpty()
				&& (trimPath.startsWith("." + File.separator) || trimPath.startsWith(".." + File.separator))) {
			trimPath = trimPath.substring(trimPath.indexOf('/') + 1);
		}
		return trimPath;
	}

	/**
	 * This is a method to check if a file exists on the smb server.
	 * 
	 * @param path
	 *            the path of the file, it starts from the root of the smb
	 *            server
	 * @return true if the file exists false if the file does not exists
	 */
	private boolean exists(String path) {
		String smbPath = SMB_PATH + path;
		SmbFile smbFile;
		try {
			smbFile = new SmbFile(smbPath, authentication);
			return smbFile.exists();
		} catch (MalformedURLException | SmbException e) {
			e.printStackTrace();
		}

		return false;
	}

}
