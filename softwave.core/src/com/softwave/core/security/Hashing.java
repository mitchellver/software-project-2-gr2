package com.softwave.core.security;

public class Hashing {

	private String password;
	private String generatedSecuredPasswordHash = "";
	private static String salt = "$2a$12$jC6Nfw9XEQSFEnt/V5s3Uu";

	public Hashing(String password) {
		super();
		
		/*
		 * mogelijkheid tot elke keer een andere salt te gebruiken
		 * generatedSecuredPasswordHash = BCrypt.hashpw(password,
		 * BCrypt.gensalt(12));
		 */
		generatedSecuredPasswordHash = BCrypt.hashpw(password, salt);
		this.password = generatedSecuredPasswordHash;
	}

	public String getPassword() {
		return password;
	}

	public static String hashPassword(String password) {
		return BCrypt.hashpw(password, salt);
	}

	public boolean matchPassword(String match) {
		if (BCrypt.checkpw(match, generatedSecuredPasswordHash) == true) {
			return true;
		} else {
			return false;
		}
	}
}