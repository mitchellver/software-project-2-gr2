package com.softwave.core.security;

/**
 * This class will contain the hash string as object HashString.
 *
 * @author adeel
 * 
 */

public class HashString {

	/**
	 * The class HashString has one private String data member known as "data".
	 */

	private String data;

	/**
	 * @param data
	 *            The main constructor of class HashString contains one String
	 *            data member data. The constructor will create an object
	 *            HashString by using a String "data" as parameter.
	 */

	public HashString(String data) {
		super();
		
		if(data == null){
			return;
		}
		
		this.data = data;

	}

	/**
	 * getData() a getter method for returning a String data of the class
	 * HashString. Mainly used to have access to the private data member of the
	 * class.
	 * 
	 * @return String data.
	 */

	public String getData() {
		return data;
	}

	/**
	 * setData(String data) a setter method used to set a String as data for the
	 * HashString Object.
	 * 
	 * @param data
	 */

	public void setData(String data) {
		
		if(data == null){
			return;
		}
		this.data = data;
	}

	/**
	 * The toString method is used to print all data of a class. In this case we
	 * print out data as hashstring.
	 * 
	 * @return String/text with the data.
	 */

	@Override
	public String toString() {
		return "Hashstring [data=" + data + "]";
	}

	/**
	 * The hashcode method allows algorithms and data structures to put objects
	 * into compartements. Objects that are equal must have the same hash code
	 * within a running process.
	 * 
	 * @return result: The hashcode returns values that can be used to compare
	 *         objects.
	 */

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((data == null) ? 0 : data.hashCode());
		return result;
	}

	/**
	 * The equals method is used to make equal comparison between two objects.
	 * 
	 * @return boolean: The function returns a true of false based on the
	 *         result. If there is a match of objects it will most likely return
	 *         a true where as if it is not a match a false will be returned.
	 */

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		HashString other = (HashString) obj;
		if (data == null) {
			if (other.data != null)
				return false;
		} else if (!data.equals(other.getData()))
			return false;
		return true;
	}

}
